package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedImage_Model {
	@SerializedName("total_images")
	public String total_images;
	@SerializedName("images_information")
	public NewsfeedImageInformation_model[] images_information;

	public NewsfeedImage_Model(String total_images, NewsfeedImageInformation_model[] images_information) {
		this.total_images = total_images;
		this.images_information = images_information;
	}

	public String getTotal_images() {
		return total_images;
	}

	public NewsfeedImageInformation_model[] getImages_information() {
		return images_information;
	}

}
