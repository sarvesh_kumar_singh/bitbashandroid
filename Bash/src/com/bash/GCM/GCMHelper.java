package com.bash.GCM;

import java.io.IOException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMHelper {

	static GoogleCloudMessaging gcm;
	static Context context;
	static String regId;
	
	public static void checkDeviceRegistration(Context mContext)
	{
		context = mContext;
		if(PreferenceManager.getInstance().getGCMRegistrationId() == null ||
				PreferenceManager.getInstance().getGCMRegistrationId().length() == 0){
			registerInBackground();
		}
	}
	
	private static void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regId = gcm.register(AppConstants.GOOGLE_PROJECT_ID);
					Log.e("RegisterActivity", "registerInBackground - regId: "+ regId);
					
				} catch (IOException ex) {
					Log.e("RegisterActivity", "Error");
				}
				Log.e("RegisterActivity", "AsyncTask completed");
				return regId;
			}

			@Override
			protected void onPostExecute(String msg) {
				PreferenceManager.getInstance().setGCMRegistrationId(regId);
			}
		}.execute(null, null, null);
	}
	
	
	
}
