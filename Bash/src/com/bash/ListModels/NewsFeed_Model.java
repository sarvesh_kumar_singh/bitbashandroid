package com.bash.ListModels;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class NewsFeed_Model {

	@SerializedName("idpost")
	public String idpost;
	@SerializedName("post_type")
	public String post_type;
	@SerializedName("post_content")
	public String post_content;
	@SerializedName("recorded_on")
	public String recorded_on;
	@SerializedName("posted_by_iduser")
	public String posted_by_iduser;
	@SerializedName("poster_full_name")
	public String poster_full_name;
	@SerializedName("poster_image_location")
	public String poster_image_location;
	@SerializedName("NewsfeedImage_Model")
	public NewsfeedImage_Model images;
	@SerializedName("likes")
	public NewsfeedLikes_Model likes;
	@SerializedName("islikes")
	public boolean islikes;
	@SerializedName("commentCount")
	public String commentCount;
	@SerializedName("likeCount")
	public String likeCount;
	@SerializedName("payers")
	public NewsfeedPayers_model payers;
	@SerializedName("paid_for")
	public NewsfeedPaidfor_model paid_for;

	public NewsFeed_Model(String idpost, String post_type, String post_content,
			String recorded_on, String posted_by_iduser,
			String poster_full_name, String poster_image_location,
			NewsfeedImage_Model images, NewsfeedLikes_Model likes,boolean islikes, String commentCount,String likeCount,
			NewsfeedPayers_model payers, NewsfeedPaidfor_model paid_for) {
		this.idpost = idpost;
		this.post_type = post_type;
		this.post_content = post_content;
		this.recorded_on = recorded_on;
		this.posted_by_iduser = posted_by_iduser;
		this.poster_full_name = poster_full_name;
		this.poster_image_location = poster_image_location;
		this.images = images;
		this.likes = likes;
		this.islikes = islikes;
		this.commentCount = commentCount;
		this.likeCount = likeCount;
		this.payers = payers;
		this.paid_for = paid_for;

	}

	public String getIdpost() {
		return idpost;
	}

	public String getPost_type() {
		return post_type;
	}

	public String getPost_content() {
		return post_content;
	}

	public String getRecorded_on() {
		return recorded_on;
	}

	public String getPosted_by_iduser() {
		return posted_by_iduser;
	}

	public String getPoster_full_name() {
		return poster_full_name;
	}

	public String getPoster_image_location() {
		return poster_image_location;
	}

	public NewsfeedImage_Model getImages() {
		return images;
	}

	public NewsfeedLikes_Model getLikes() {
		return likes;
	}
	public boolean getIslike() {
		return islikes;
	}
	public void setIslike(boolean islikes) {
		this.islikes = islikes;
	}

	public String getCommentCount() {
		return commentCount;
	}
	
	public void setLikeCount(String likeCount) {
		this.likeCount=likeCount;
	}
	
	public String getLikeCount() {
		return likeCount;
	}

	public void setCommentsCount(String commentCount) {
		this.commentCount = commentCount;
	}

	public NewsfeedPayers_model getPayers() {
		return payers;
	}

	public NewsfeedPaidfor_model getPaid_for() {
		return paid_for;
	}

}
