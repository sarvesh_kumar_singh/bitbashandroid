package com.bash.ListModels;

import java.util.ArrayList;

import com.bash.GsonClasses.MyTabs_Model.payers;
import com.bash.GsonClasses.MyTabs_Model.receivers;
import com.google.gson.annotations.SerializedName;

public class Transactions_information {
	@SerializedName("idtrans")
	public String idtrans;
	@SerializedName("trans_type")
	public String trans_type;
	@SerializedName("entered_by_full_name")
	public String entered_by_full_name;
	@SerializedName("entered_by_iduser")
	public String entered_by_iduser;
	@SerializedName("currency")
	public String currency;
	@SerializedName("total_amount")
	public String total_amount;
	@SerializedName("ratio")
	public String ratio;
	@SerializedName("category")
	public String category;
	@SerializedName("date")
	public String date;
	@SerializedName("transaction_title")
	public String transaction_title;
	@SerializedName("datetime")
	public String datetime;
	@SerializedName("timestamp")
	public String timestamp;
	@SerializedName("status")
	public String status;
	
	@SerializedName("payers")
	public ArrayList<Payers> payerslist = new ArrayList<Payers>();
	
	@SerializedName("receivers")
	public ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
	

	public String getIdtrans() {
		return this.idtrans;
	}

	public String getTrans_type() {
		return this.trans_type;
	}
	
	public String getEntered_by_full_name(){
		return this.entered_by_full_name;
	}

	public String getEntered_by_iduser() {
		return this.entered_by_iduser;
	}

	public String getCurrency() {
		return this.currency;
	}

	public String getTotal_amount() {
		return this.total_amount;
	}
	
	public String getRatio() {
		return this.ratio;
	}

	public String getCategory() {
		return this.category;
	}

	public String getDate() {
		return this.date;
	}

	public String getTransaction_title() {
		return this.transaction_title;
	}

	public String getDatetime() {
		return this.datetime;
	}

	public String getTimestamp() {
		return this.timestamp;
	}

	public String getStatus() {
		return this.status;
	}
	
	public ArrayList<Payers> getPayers() {
		return this.payerslist;
	}

	public ArrayList<Receivers> getReceivers() {
		return this.receiverslist;
	}
	
	

	public Transactions_information(String idtrans, String trans_type, String entered_by_full_name,
			String entered_by_iduser, String currency, String total_amount, String ratio,
			String category, String date, String transaction_title,
			String datetime, String timestamp, String status, ArrayList<Payers> payerslist, ArrayList<Receivers> receiverslist) {
		this.idtrans = idtrans;
		this.trans_type = trans_type;
		this.entered_by_full_name = entered_by_full_name;
		this.entered_by_iduser = entered_by_iduser;
		this.currency = currency;
		this.total_amount = total_amount;
		this.ratio = ratio;
		this.category = category;
		this.date = date;
		this.transaction_title = transaction_title;
		this.datetime = datetime;
		this.timestamp = timestamp;
		this.status = status;
		this.payerslist=payerslist;
		this.receiverslist=receiverslist;
	}
}
