package com.bash.Activities;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.GsonClasses.MyTabs_Model;
import com.bash.ListModels.GroupTransactionDetail_Model;
import com.bash.ListModels.Group_Model;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Receivers;
import com.bash.ListModels.Summary_Model;
import com.bash.ListModels.Transactions_information;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class Summary_Activity extends Activity implements
		AsyncResponse, OnClickListener {
	Context mContext;
	Activity mActivity;
	MyAsynTaskManager myAsyncTask;
	public MyTabs_Model pendingNewClass;
	public ArrayList<Transactions_information> feedList1, feedlistNew;
	public ArrayList<Summary_Model> feedList = new ArrayList<Summary_Model>();
	ArrayList<Payers> payerslist = new ArrayList<Payers>();
	ArrayList<Summary_Model> summarylist = new ArrayList<Summary_Model>();
	MyGroupsAdapter adapter;
	public static SwipeListView lviMyTabs;
	public Transactions_information tabModel;
	int pos = 0,totalAmount=0, amtpaid, hadtopay, myAmtPay, meHadtopay;
	boolean show=false;

	public static View totalbalanceView, youOweView, youAreOweView;
	ImageView iviBack;
	TextView tviGroupName, tviThisMonth, tviLastMonth, tviAll, tviThisMonthLevel, tviLastMonthLevel, tviAllLevel, tviGroupAmount, tviShareAmount;
	ListView lviSummary;
	
	String idGroup, picturePath = "",group_member_id, groupName, image_location, currency;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_summary);
		Bundle bundle = getIntent().getExtras();
		idGroup  = bundle.getString("idGroup");
		init();
		
		tviGroupName.setText(bundle.getString("groupName") + " expense summary");
	}
	
	


	private void init() {
		// TODO Auto-generated method stub
		mActivity = Summary_Activity.this;
		mContext = Summary_Activity.this;
		
		iviBack = (ImageView)findViewById(R.id.iviBack);
		tviGroupName = (TextView)findViewById(R.id.tviGroupName);
		tviThisMonth = (TextView)findViewById(R.id.tviThisMonth);
		tviLastMonth = (TextView)findViewById(R.id.tviLastMonth);
		tviAll = (TextView)findViewById(R.id.tviAll);
		tviThisMonthLevel = (TextView)findViewById(R.id.tviThisMonthLevel);
		tviLastMonthLevel = (TextView)findViewById(R.id.tviLastMonthLevel);
		tviAllLevel = (TextView)findViewById(R.id.tviAllLevel);
		
		tviGroupAmount = (TextView)findViewById(R.id.tviGroupAmount);
		tviShareAmount = (TextView)findViewById(R.id.tviShareAmount);
		
		lviSummary = (ListView)findViewById(R.id.lviSummary);
		
		iviBack.setOnClickListener(this);
		tviThisMonth.setOnClickListener(this);
		tviLastMonth.setOnClickListener(this);
		tviAll.setOnClickListener(this);
		
		feedList1 = new ArrayList<Transactions_information>();
		adapter = new MyGroupsAdapter(mActivity, summarylist);
		lviSummary.setAdapter(adapter);
		getGroupDetailList();
	}

	public void getGroupDetailList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupDetailList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group", "getAllGroupDetails",
						idGroup, PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
	}
	
	
	

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
		String idtrans, date, transaction_title, datetime, timestamp, amount = null, amountType = null;
		if (from.equalsIgnoreCase("getGroupDetailList")) {
			if (output != null) {
				try {
					
					JSONObject jObj=new JSONObject(output);
					if(jObj.getBoolean("result")){
						//ArrayList<Transactions_information> transaction=new ArrayList<Transactions_information>();
						group_member_id = jObj.getString("group_member_id");
						
						JSONObject jObjGroupInfo = jObj.getJSONObject("group_information");
						
						groupName = jObjGroupInfo.getString("groupname");
						image_location = jObjGroupInfo.getString("imagepath");
						
						JSONArray transArray=jObj.getJSONArray("transactions_information");
						//for(int i=0;i<transArray.length();i++){
						for(int i=0;i<transArray.length();i++){
							JSONObject res=transArray.getJSONObject(i);
							
							idtrans = res.getString("idtrans"); 
							date = res.getString("date"); 
							transaction_title = res.getString("transaction_title");
							datetime = res.getString("datetime");
							timestamp = res.getString("timestamp");
							
							totalAmount += Integer.parseInt(res.getString("total_amount"));
							
							//ArrayList<Payers> payerslist = new ArrayList<Payers>();
							ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
							
							ArrayList<Group_Model> group_information = new ArrayList<Group_Model>();
							ArrayList<Group_Model> group_information_recever = new ArrayList<Group_Model>();
							
							JSONObject payerObj=res.getJSONObject("payers");
							JSONObject rcvrObj=res.getJSONObject("receivers");
							
							int totalTranAmount = Integer.parseInt(res.getString("total_amount"));
							
							
							JSONArray rcvrArray=rcvrObj.getJSONArray("receivers_information");
							if(payerObj.getBoolean("result")){
								JSONArray payerArray=payerObj.getJSONArray("payers_information");
								for(int p=0;p<payerArray.length();p++){
									JSONObject payerRes=payerArray.getJSONObject(p);
									if(payerRes.getString("iduser").equals(group_member_id)){
										if(payerRes.getString("payment_type").equals("0")){
											amtpaid = Integer.parseInt(payerRes.getString("amount"));
											hadtopay = Integer.parseInt(payerRes.getString("had_to_pay"));
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
										} else if(payerRes.getString("payment_type").equals("1")){
											int ratio=0,ratioAmount;
											for(int pay=0;pay<payerslist.size();pay++){
							    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
							    			}
							    			for(int rec=0;rec<receiverslist.size();rec++){
							    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
							    			}
							    			ratioAmount=totalTranAmount/ratio;
							    			
							    			amtpaid = Integer.parseInt(payerRes.getString("amount"));
											hadtopay = ratioAmount * Integer.parseInt(payerRes.getString("had_to_pay"));
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
							    			
										} else if(payerRes.getString("payment_type").equals("2")){
											
											amtpaid = Integer.parseInt(payerRes.getString("amount"));
											hadtopay = (totalTranAmount * Integer.parseInt(payerRes.getString("had_to_pay")))/100;
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
											
										}
										//amount = "";
										
									}
									
									JSONObject payerGroup = payerRes.getJSONObject("group");
									group_information.clear();
									if(payerGroup.getBoolean("result")){
										JSONObject payerGroupinformation = payerGroup.getJSONObject("group_information");
										group_information.add(new Group_Model(payerGroupinformation.getString("idgroup"), payerGroupinformation.getString("groupname"), payerGroupinformation.getString("image_location")));
									}
									payerslist.add(new Payers(payerRes.getString("full_name"), payerRes.getString("image_location"), payerRes.getString("idpayer"), payerRes.getString("idtrans"), payerRes.getString("user_type"), payerRes.getString("iduser"), String.valueOf(amtpaid), payerRes.getString("payment_type"), String.valueOf(hadtopay), group_information));
								}
							}
							if(rcvrObj.getBoolean("result")){
								for(int r=0; r<rcvrArray.length(); r++){
									JSONObject rcvrRes = rcvrArray.getJSONObject(r);
									/*JSONObject rcvrGroup = rcvrRes.getJSONObject("group");
									group_information_recever.clear();
									if(rcvrGroup.getBoolean("result")){
										JSONObject rcvrGroupinformation = rcvrGroup.getJSONObject("group_information");
										group_information_recever.add(new Group_Model(rcvrGroupinformation.getString("idgroup"), rcvrGroupinformation.getString("groupname"), rcvrGroupinformation.getString("image_location")));
									}*/
									if(rcvrRes.getString("iduser").equals(group_member_id)){
										if(rcvrRes.getString("payment_type").equals("0")){
											amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
										} else if(rcvrRes.getString("payment_type").equals("1")){
											int ratio=0,ratioAmount;
											for(int pay=0;pay<payerslist.size();pay++){
							    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
							    			}
							    			for(int rec=0;rec<receiverslist.size();rec++){
							    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
							    			}
							    			ratioAmount=totalTranAmount/ratio;
							    			
							    			amtpaid = ratioAmount * Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
							    			
										} else if(rcvrRes.getString("payment_type").equals("2")){
											
											amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
										}
										//amount = "";
										
									}
									
									receiverslist.add(new Receivers(rcvrRes.getString("full_name"), rcvrRes.getString("image_location"), rcvrRes.getString("idreceiver"), rcvrRes.getString("idtrans"), rcvrRes.getString("user_type"), rcvrRes.getString("iduser"), rcvrRes.getString("amount"), rcvrRes.getString("payment_type"), group_information_recever));
									payerslist.add(new Payers(rcvrRes.getString("full_name"), rcvrRes.getString("image_location"), rcvrRes.getString("idreceiver"), rcvrRes.getString("idtrans"), rcvrRes.getString("user_type"), rcvrRes.getString("iduser"), String.valueOf(amtpaid), rcvrRes.getString("payment_type"), "receve", group_information_recever));
								}
							}
							//Collections.sort(payerslist);
							Collections.sort(receiverslist);
							feedList1.add(new Transactions_information(res.getString("idtrans"),res.getString("trans_type"),res.getString("entered_by_iduser"),
									res.getString("currency"),res.getString("total_amount"),"", res.getString("category"),res.getString("date"),res.getString("transaction_title"),
									res.getString("datetime"),res.getString("timestamp"),res.getString("status"),res.getString("entered_by_full_name"), payerslist,receiverslist));
							//feedList.add(new GroupTransactionDetail_Model(idtrans, groupName, res.getString("entered_by_full_name"), image_location, res.getString("currency"), date, transaction_title, datetime, timestamp, amount, res.getString("total_amount"), amountType, payerslist, receiverslist));
						}
						
						for (int payer = 0; payer < payerslist.size(); payer++){
							boolean replace=false;
							if(payerslist.get(payer).getUser_type().equals("5")){
								payerslist.get(payer).setUser_type("1");
							}
							for(int s = 0; s < summarylist.size(); s++){
								if(summarylist.get(s).getUser_type().equals("5")){
									summarylist.get(s).setUser_type("1");
								}
								if(summarylist.get(s).getIduser().equals(payerslist.get(payer).getIduser()) && 
										summarylist.get(s).getUser_type().equals(payerslist.get(payer).getUser_type())){
									String ttlAmnt, hadtoPay;
									if(summarylist.get(s).getHad_to_pay().equals("receve")){
										ttlAmnt = String.valueOf(Integer.parseInt(payerslist.get(payer).getAmount())+Integer.parseInt(summarylist.get(s).getAmount()));
										hadtoPay = "receve";
									} else {
										ttlAmnt = String.valueOf(Integer.parseInt(payerslist.get(payer).getAmount())+Integer.parseInt(summarylist.get(s).getAmount()));
										if(payerslist.get(payer).getHad_to_pay().equals("receve")){
											hadtoPay = summarylist.get(s).getHad_to_pay();
										} else {
											hadtoPay = String.valueOf(Integer.parseInt(payerslist.get(payer).getHad_to_pay())+Integer.parseInt(summarylist.get(s).getHad_to_pay()));
										}
									}
									summarylist.set(s, new Summary_Model(payerslist.get(payer).getFull_name(),payerslist.get(payer).getImage_location(), 
											payerslist.get(payer).getIdpayer(), payerslist.get(payer).getUser_type(), payerslist.get(payer).getIduser(),
											ttlAmnt, payerslist.get(payer).getPayment_type(), hadtoPay));
									replace = true;
								} else if(summarylist.get(s).getIduser().equals(payerslist.get(payer).getIduser()) && 
										summarylist.get(s).getUser_type().equals("1") && payerslist.get(payer).getUser_type().equals("5")
										|| summarylist.get(s).getUser_type().equals("5") && payerslist.get(payer).getUser_type().equals("1")){
									
									String ttlAmnt, hadtoPay;
									if(summarylist.get(s).getHad_to_pay().equals("receve")){
										ttlAmnt = String.valueOf(Integer.parseInt(payerslist.get(payer).getAmount())+Integer.parseInt(summarylist.get(s).getAmount()));
										hadtoPay = "receve";
									} else {
										ttlAmnt = String.valueOf(Integer.parseInt(payerslist.get(payer).getAmount())+Integer.parseInt(summarylist.get(s).getAmount()));
										if(payerslist.get(payer).getHad_to_pay().equals("receve")){
											hadtoPay = summarylist.get(s).getHad_to_pay();
										} else {
											hadtoPay = String.valueOf(Integer.parseInt(payerslist.get(payer).getHad_to_pay())+Integer.parseInt(summarylist.get(s).getHad_to_pay()));
										}
									}
									summarylist.set(s, new Summary_Model(payerslist.get(payer).getFull_name(),payerslist.get(payer).getImage_location(), 
											payerslist.get(payer).getIdpayer(), payerslist.get(payer).getUser_type(), payerslist.get(payer).getIduser(),
											ttlAmnt, payerslist.get(payer).getPayment_type(), hadtoPay));
									replace = true;
								}
							}
							if(!replace){
							summarylist.add(new Summary_Model(payerslist.get(payer).getFull_name(),payerslist.get(payer).getImage_location(), 
									payerslist.get(payer).getIdpayer(), payerslist.get(payer).getUser_type(), payerslist.get(payer).getIduser(),
									payerslist.get(payer).getAmount(), payerslist.get(payer).getPayment_type(), payerslist.get(payer).getHad_to_pay()));
							}
							replace = false;
						}
							
						tviGroupAmount.setText("₹ "+ String.valueOf(totalAmount));
						for(int i=0; i<summarylist.size(); i++){
							if(summarylist.get(i).getIduser().equals(group_member_id)){
					        	if(summarylist.get(i).getHad_to_pay().equals("receve")){
					        		meHadtopay = Integer.parseInt(summarylist.get(i).getAmount());
					        	} else {
					        		myAmtPay += Integer.parseInt(summarylist.get(i).getAmount());
					        		meHadtopay += Integer.parseInt(summarylist.get(i).getHad_to_pay());
					        	}
							}	
				        }
						
						tviShareAmount.setText("₹ "+ (myAmtPay - meHadtopay));
						
						adapter.notifyDataSetChanged();
					
				}else{
					//((RelativeLayout) mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
					DialogManager.showDialog(mActivity, "No Friends Found!");
					//updateAdapter(newList);
				}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity, "Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		}

	}

	public class MyGroupsAdapter extends BaseAdapter
	{
		 	public ArrayList<Summary_Model> feedList = new ArrayList<Summary_Model>();
		 	//public ArrayList<Transactions_information> originalList = new ArrayList<Transactions_information>();
		 	public Activity context;
		 	public LinearLayout.LayoutParams backViewParams;
		    
		    public MyGroupsAdapter(Activity context, ArrayList<Summary_Model> feedList) {
		        this.context = context;
		    	this.feedList = feedList;
		    	//this.originalList = feedList;
		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
		    			LayoutParams.MATCH_PARENT);
		    }
		    
		    public void notifyWithDataSet(ArrayList<Summary_Model> newlist){
		    	this.feedList.clear();
		    	this.feedList = newlist;
		    	//this.originalList = newlist;
		    	Log.e("Size of Adapter", String.valueOf(feedList.size()));
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        Summary_Model listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	if(convertView == null)
		        	 {
		        	  	convertView = inflater.inflate(R.layout.custom_trans_detail_listview, null);
			            holder = new ViewHolder();
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.iviFriendImageSource = (ImageView) convertView.findViewById(R.id.iviFriendImageSource);
		        	}
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        	
		        if(listItem.getHad_to_pay().equals("receve")){
		        	
		        }
		        holder.friendName.setText(listItem.getFull_name());
		        
		        if(listItem.getImage_location() != null && listItem.getImage_location().length()!= 0 && holder.iviFriendImageSource != null){
		        	ImageLoader.getInstance().displayImage(listItem.getImage_location(), 
		        			holder.iviFriendImageSource, BashApplication.options, BashApplication.animateFirstListener);
		        }else{
		        	holder.iviFriendImageSource.setImageResource(R.drawable.addphoto_img_block);
		        }
		        
		        return convertView;
		    }

		    
		    @Override
		    public int getCount() {
		    	//	checkEmptyList();
		        return feedList.size();
		    }

		    @Override
		    public Summary_Model getItem(int position) {
		        return feedList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }

		    private class ViewHolder {
		        TextView friendName;
		        ImageView iviFriendImageSource;
		    }
		  
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviBack:
			finish();
			break;
		case R.id.tviThisMonth:
			setBackGround();
			Toast.makeText(mContext, "This month data comming Soon", Toast.LENGTH_LONG).show();
			tviThisMonthLevel.setBackgroundColor(Color.parseColor("#FF6D00"));
			break;
		case R.id.tviLastMonth:
			setBackGround();
			Toast.makeText(mContext, "Last month data comming Soon", Toast.LENGTH_LONG).show();
			tviLastMonthLevel.setBackgroundColor(Color.parseColor("#FF6D00"));
			break;
		case R.id.tviAll:
			setBackGround();
			tviAllLevel.setBackgroundColor(Color.parseColor("#FF6D00"));
			break;
		default:
			break;
		}
	}
	
	void setBackGround() {
		tviThisMonthLevel.setBackgroundColor(Color.parseColor("#00BDCB"));
		tviThisMonthLevel.setBackgroundColor(Color.parseColor("#00BDCB"));
		tviLastMonthLevel.setBackgroundColor(Color.parseColor("#00BDCB"));
		tviAllLevel.setBackgroundColor(Color.parseColor("#00BDCB"));
	}
	
	
}
