package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.MyProfileActivity;
import com.bash.Activities.SlidingActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_List_Class;
import com.bash.ListModels.Friends_List_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyFriendsFragment extends BaseFragment implements AsyncResponse {
	MyAsynTaskManager myAsyncTask;
	public static View mRootView;
	public static SwipeListView friendsListView;
	public MyFriendsAdapter adapter;
	public ArrayList<Friends_List_Model> feedList;
	Friends_List_Class responseForFriends;
	int pos = 0;
	
	private FloatingActionMenu fab;
	private FloatingActionButton fab1, fab2, fab3;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_myfriends_page, null);
		initializeView();
		
		fab = (FloatingActionMenu) mRootView.findViewById(R.id.menu1);
	 	fab1 = (FloatingActionButton) mRootView.findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) mRootView.findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) mRootView.findViewById(R.id.fab3);
        
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);
        fab3.setOnClickListener(clickListener);  
        
        fab.hideMenuButton(false);
        fab.setClosedOnTouchOutside(true);
        fab.showMenuButton(true);
        fab.setIconAnimated(true);
		
		return mRootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Sarvesh((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Sarvesh
		// ((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		// Initialize Activity Views
		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
						((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
					}
				});
		
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.menu_btn, "", "Friends", "", 0);

		// friendsListView = (SwipeSwipeListView)
		// mRootView.findViewById(R.id.friendsListView);
		friendsListView = (SwipeListView) mRootView.findViewById(R.id.friendsListView);

		friendsListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there
																		// are
																		// five
																		// swiping
																		// modes
		friendsListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); // there
																				// are
																				// four
																				// swipe
																				// actions
		friendsListView.setOffsetLeft(PreferenceManager.getInstance().getPercentageFromWidth(38));
		friendsListView.setAnimationTime(50); // animarion time
		friendsListView.setSwipeOpenOnLongPress(true); // enable or disable
														// SwipeOpenOnLongPress

		feedList = new ArrayList<Friends_List_Model>();

		adapter = new MyFriendsAdapter(getActivity(), feedList);

		friendsListView.setAdapter(adapter);

		

		// Capture Text in EditText
		((EditText) mRootView.findViewById(R.id.searchBox)).addTextChangedListener(new TextWatcher() {

					@Override
					public void afterTextChanged(Editable arg0) {
						// TODO Auto-generated method stub
						String text = ((EditText) mRootView
								.findViewById(R.id.searchBox)).getText()
								.toString().toLowerCase(Locale.getDefault());
						adapter.getFilter().filter(text);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}
				});

		getFriendsList();
		friendsListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			 @Override
	         public void onClickFrontView(int position) {
				 Intent profileIntent = new Intent(((SlidingActivity) getActivity()), MyProfileActivity.class);
					profileIntent.putExtra("idUser", feedList.get(position).getContactid());
					profileIntent.putExtra("Profilename", feedList.get(position).getFullname());
					profileIntent.putExtra("image", feedList.get(position).getImagepath());
					profileIntent.putExtra("amount", feedList.get(position).getAmount());
					profileIntent.putExtra("type", feedList.get(position).getAmounttype());
					startActivity(profileIntent);
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	             friendsListView.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	         }
	     });

	}

	public void updateAdapter(ArrayList<Friends_List_Model> newList) {
		/*
		 * feedList.clear(); feedList =
		 * DataBaseManager.getInstance().getFriendsList();
		 */
		// this.feedList = newList;
		// feedList = DataBaseManager.getInstance().getFriendsList();
		if (newList != null && newList.size() != 0) {
			adapter.notifyWithDataSet(newList);
		}
	}

	public void getFriendsList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getFriendsList", getActivity(),
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "friend", "friendslist",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
		
	}

	public class MyFriendsAdapter extends BaseAdapter implements Filterable {
		public ArrayList<Friends_List_Model> feedList = new ArrayList<Friends_List_Model>();
		public ArrayList<Friends_List_Model> originalList = new ArrayList<Friends_List_Model>();
		public Activity context;
		ViewHolder holder = null;
		public LinearLayout.LayoutParams backViewParams;
		public FriendFilter filter;

		public MyFriendsAdapter(Activity context,
				ArrayList<Friends_List_Model> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20),
					LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<Friends_List_Model> newlist) {
			this.feedList = newlist;
			this.originalList = newlist;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			Friends_List_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				if (convertView == null) {
					convertView = inflater.inflate(R.layout.custom_myfriendongroup_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
					holder.tviPayRec = (TextView) convertView.findViewById(R.id.tviPayRec);
					holder.tviAmount = (TextView) convertView.findViewById(R.id.tviAmount);
					holder.iviCommentantImage = (ImageView) convertView.findViewById(R.id.iviCommentantImage);
					holder.llAmount = (LinearLayout) convertView.findViewById(R.id.llAmount);
					holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
					holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
					holder.tviCharge = (TextView) convertView.findViewById(R.id.tviCharge);
					holder.tviPay = (TextView) convertView.findViewById(R.id.tviPay);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
				holder.backView.setLayoutParams(backViewParams);
				// holder.friendImageSource.setImageResource(listItem.getfriendImageSource());
				holder.friendName.setText(listItem.getFullname());

				holder.deleteFriend.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						pos = position;
						// TODO Auto-generated method stub
						removeFriendFromWebservice(pos);
						/*
						 * removeItemFromList(position);
						 * MyFriendsFragment.friendsListView
						 * .closeAnimate(position);
						 */
					}
				});
				
				
					
				holder.friendName.setTag(position);
				holder.friendName.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent profileIntent = new Intent(((SlidingActivity) getActivity()), MyProfileActivity.class);
						profileIntent.putExtra("idUser", feedList.get((Integer) v.getTag()).getContactid());
						profileIntent.putExtra("Profilename", feedList.get((Integer) v.getTag()).getFullname());
						profileIntent.putExtra("image", feedList.get((Integer) v.getTag()).getImagepath());
						profileIntent.putExtra("amount", feedList.get((Integer) v.getTag()).getAmount());
						profileIntent.putExtra("type", feedList.get((Integer) v.getTag()).getAmounttype());
						profileIntent.putExtra("usertype", feedList.get((Integer) v.getTag()).getUsertype());
						startActivity(profileIntent);
						
					}
				});
				holder.tviCharge.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pos = position;
						startActivity(new Intent(getActivity(),AddTabActivity.class));
						/*Toast.makeText(context, "Charge will Come soon...",
								Toast.LENGTH_LONG).show();*/
					}
				});
				holder.tviPay.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						pos = position;
						startActivity(new Intent(getActivity(),AddTabActivity.class));
						/*Toast.makeText(context, "Pay will Come soon...",
								Toast.LENGTH_LONG).show();*/
					}
				});
			
			if (!listItem.getAmounttype().equals("None")) {
				holder.llAmount.setVisibility(View.VISIBLE);
				String amount = null, amtType = null;
				if(listItem.getAmounttype().equals("Pay")){
					holder.llAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
					amtType = "+";
				}else{
					holder.llAmount.setBackgroundResource(R.drawable.amount_displayred_s);
					amtType = "-";
				}
				if(listItem.getCurrency().equals("INR")){
					amount = listItem.getAmount() + " "+AppConstants.RASYMBOL;
				}else{
					amount = listItem.getAmount() + "";
				}
				holder.tviPayRec.setText(amtType);
				holder.tviAmount.setText(amount);
			} else {
				holder.llAmount.setVisibility(View.GONE);
			}

			if (listItem.getImagepath() != null && listItem.getImagepath().length() != 0 && holder.iviCommentantImage != null) {
				ImageLoader.getInstance().displayImage(listItem.getImagepath(),
						holder.iviCommentantImage, BashApplication.options,
						BashApplication.animateFirstListener);
			} else {
				holder.iviCommentantImage.setImageResource(R.drawable.addphoto_img_block);
			}

			return convertView;
		}

		/*
		 * public void checkEmptyList(){ if(feedList.size() == 0){
		 * this.feedList.add(emptyList); } }
		 */
		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public Friends_List_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName, tviPayRec, tviAmount, tviCharge, tviPay;
			ImageView iviCommentantImage, deleteFriend;
			RelativeLayout backView;
			LinearLayout llAmount;
		}
		
		// Filter Class
 	    public void filter(String charText) {
 	        charText = charText.toLowerCase(Locale.getDefault());
 	        Log.e("charText", charText+"");
 	        Log.e("Originallengh", String.valueOf(originalList.size()));
 	        feedList.clear();
 	        if (charText.length() == 0) {
 	        	feedList.addAll(originalList);
 	        }else
 	        {
 	            for (Friends_List_Model wp : originalList)
 	            {
 	                if (wp.getFullname().startsWith(charText))
 	                {
 	                	Log.e("Groupname", wp.getFullname());
 	                	feedList.add(wp);
 	                }
 	            }
 	        }
 	        this.notifyDataSetChanged();
 	        this.notifyDataSetInvalidated();
 	    }
		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			if (filter == null)
				filter = new FriendFilter();
			return filter;
		}

		// filter Class...
		private class FriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalList;
					results.count = originalList.size();
				} else {
					// We perform filtering operation
					List<Friends_List_Model> tempList = new ArrayList<Friends_List_Model>();
					for (Friends_List_Model p : feedList) {
						if (p.getFullname()
								.toUpperCase()
								.startsWith(constraint.toString().toUpperCase()))
							tempList.add(p);
					}
					results.values = tempList;
					results.count = tempList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				// Now we have to inform the adapter about the new list filtered
				if (results.count == 0)
					notifyDataSetInvalidated();
				else {
					feedList = (ArrayList<Friends_List_Model>) results.values;
					notifyDataSetChanged();
				}
			}
		}

	}

	public void removeItemFromList(int position) {
		DataBaseManager.getInstance().unFriendwithUserId(this.feedList.get(position).contactid);
		feedList.remove(position);
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}

	public void removeFriendFromWebservice(final int position) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("removeFriendFromWebservice",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "iduser", "idfriend" }, new String[] {
						"user", "removefriend",
						PreferenceManager.getInstance().getUserId(),
						feedList.get(position).contactid });
		// getisFriend()
		Log.e("***************", "**************");

		Log.e("My User Id", PreferenceManager.getInstance().getUserId());
		Log.e("Removing Friend Id", feedList.get(position).contactid);
		myAsyncTask.execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		ArrayList<Friends_List_Model> newList1 = new ArrayList<Friends_List_Model>();

		if (from.equalsIgnoreCase("getFriendsList")) {
			if (output != null) {

				try {
					Gson gson = new Gson();

					responseForFriends = gson.fromJson(output,
							Friends_List_Class.class);

					if (BuildConfig.DEBUG)
						Log.e("Json Response", output);
					//JSONObject jobj= new JSONObject(output);
					if (responseForFriends.getResult() && Integer.parseInt(responseForFriends.friends_count) > 0) {

						ArrayList<Friends_List_Model> newList = new ArrayList<Friends_List_Model>();
						for (int i = 0; i < responseForFriends.friendlist.size(); i++) {

							Log.e("Friend Id", responseForFriends.friendlist.get(i).contactid);
							Log.e("Friend number", responseForFriends.friendlist.get(i).email);
							Log.e("Friend number", responseForFriends.friendlist.get(i).phone_no);
							Log.e("Friend name", responseForFriends.friendlist.get(i).fullname);
							Log.e("Friend imagepath", responseForFriends.friendlist.get(i).imagepath);

							feedList.add(new Friends_List_Model(
									responseForFriends.friendlist.get(i).contactid,
									responseForFriends.friendlist.get(i).fullname,
									responseForFriends.friendlist.get(i).email,
									responseForFriends.friendlist.get(i).phone_no,
									responseForFriends.friendlist.get(i).imagepath,
									responseForFriends.friendlist.get(i).usertype,
									responseForFriends.friendlist.get(i).currency,
									responseForFriends.friendlist.get(i).amount,
									responseForFriends.friendlist.get(i).amounttype));
						}

						adapter.notifyWithDataSet(feedList);
						
					} else {
						// ((RelativeLayout)
						// mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
						DialogManager.showDialog(getActivity(),
								"No Friends Found!");
						// updateAdapter(newList);
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(),
							"Server Error Occured! Try Again!");
				}

			} else {

				// TODO: handle exception
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");

			}
		} else if (from.equalsIgnoreCase("removeFriendFromWebservice")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						MyFriendsFragment.friendsListView.closeAnimate(pos);
						removeItemFromList(pos);
						DialogManager.showDialog(getActivity(),
								"Friend Removed Successfully!");
					} else {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Remove Friend! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(),
							"Server Error Occured! Try Again!");
				}
			} else {
				// TODO: handle exception
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");
			}
		}

	}
	
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.menu1:         	
                break;
                case R.id.fab1:
                	((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Fragment(), true);
                    break;
                case R.id.fab2:
                	((BaseFragment)getParentFragment()).replaceFragment(new InviteFriends_Fragment(), true);
    				((SlidingActivity)getActivity()).currentFragment=MyFriendsFragment.this;
                    break;
                case R.id.fab3:
                    startActivity(new Intent(getActivity(),AddTabActivity.class));
                    break;
            }  	
        }
    };

}
