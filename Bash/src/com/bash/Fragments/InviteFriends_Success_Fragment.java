package com.bash.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;

public class InviteFriends_Success_Fragment extends BaseFragment {
	View mRootView;
	TextView tviName;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.fragment_invitefriends_success, null);
		init();
		// outputText = (TextView) mRootView.findViewById(R.id.textView1);
		// fetchContacts();

		return mRootView;
	}

	private void init() {
		// TODO Auto-generated method stub
		String value = getArguments().getString("name");
		tviName=(TextView)mRootView.findViewById(R.id.tviName);
		tviName.setText("Add Tab with\n"+value);
		((SlidingActivity) getActivity()).setUpTopBarFields(0,
				"", "", "Skip", 0);

		
		((TextView) getActivity().findViewById(R.id.toprightText))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						 ((BaseFragment)getParentFragment()).replaceFragment(((SlidingActivity)getActivity()).currentFragment, true);

					}
				});

	}

}
