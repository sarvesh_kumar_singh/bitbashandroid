package com.bash.Providers;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

public class CurvedImageBitmapProvider {
	
	/*    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
	        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
	                .getHeight(), Config.ARGB_8888);
	        Canvas canvas = new Canvas(output);

	        final int color = 0xff424242;
	        final Paint paint = new Paint();
	        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	        final RectF rectF = new RectF(rect);
	        final float roundPx = pixels;

	        paint.setAntiAlias(true);
	        canvas.drawARGB(0, 0, 0, 0);
	        paint.setColor(color);
	        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

	        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	        canvas.drawBitmap(bitmap, rect, rect, paint);

	        return output;
	    }
	    */
	    /*public static Bitmap getRoundedImageBitmap(Bitmap bitmap) {
	    	final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
	    	final Canvas canvas = new Canvas(output);
	    	 
	    	final int color = Color.RED;
	    	final Paint paint = new Paint();
	    	final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    	final RectF rectF = new RectF(rect);
	    	 
	    	paint.setAntiAlias(true);
	    	canvas.drawARGB(0, 0, 0, 0);
	    	paint.setColor(color);
	    	canvas.drawOval(rectF, paint);
	    	 
	    	paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	    	canvas.drawBitmap(bitmap, rect, rect, paint);
	    	 
	    	bitmap.recycle();
	    	 
	    	return output;
	    } */
	    
	    /*public static Bitmap getRoundedImageBitmap(Bitmap bitmap) {
	    	try {
	    		System.gc();
		    	Bitmap outputBitmap = null;
		    	if(bitmap != null){
		    		  final int width = bitmap.getWidth();
		  	        final int height = bitmap.getHeight();
		  	        
		  	        outputBitmap.recycle();
		  	        bitmap.recycle();
		  	        
		  	      final BitmapFactory.Options options = new BitmapFactory.Options();
		  	      options.inSampleSize = 8;
		  	      
		 
		  	      outputBitmap = BitmapFactory.decodeStream(in, null, options);
		  	  
		  	    outputBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
		  	    
		  	      outputBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);

		  	        final Path path = new Path();
		  	        path.addCircle(
		  	                  (float)(width / 2)
		  	                , (float)(height / 2)
		  	                , (float) Math.min(width, (height / 2))
		  	                , Path.Direction.CCW);

		  	        final Canvas canvas = new Canvas(outputBitmap);
		  	        canvas.clipPath(path);
		  	        canvas.drawBitmap(bitmap, 0, 0, null);
		    	}
		      
		        return outputBitmap;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
	    
	    }*/
}
	
