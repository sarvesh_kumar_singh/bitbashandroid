package com.bash.Fragments;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

import org.lucasr.twowayview.TwoWayView;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.TwoWayViewPhotosAdapter;
import com.bash.CustomViews.CircularImageView;
import com.bash.CustomViews.ContactsCompletionView;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.AsyncResponse;
import com.tokenautocomplete.FilteredArrayAdapter;

public class AddTabReceiveFragment   extends Fragment implements
AsyncResponse{
	
	TwoWayView horizontalViewPhotos;
	ArrayList<PhotosListModel> photosList;
	TwoWayViewPhotosAdapter horizontalPhotoAdapter;
	ContactsCompletionView searchView;
	FilteredArrayAdapter<Person> adapter;
    ArrayList<Person> personList;
    LinearLayout moretwouserlinear,twouserlinear;
    ImageView arrowpayid,arrowreceiveid,datepickerimageview,categoryimageview;
    CircularImageView receiveuserimageView,paiduserimageView;
    TextView paidusernametext,receiveusernametext,paidby,currencyText;
    AutofitTextView amountedittext;
    EditText titleedittext;
	
	View mRootView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.addtab_pay_fragment_, null);
		
		searchView = (ContactsCompletionView) mRootView.findViewById(R.id.searchView);
		moretwouserlinear = (LinearLayout) mRootView.findViewById(R.id.moretwouserlinear);
		twouserlinear = (LinearLayout) mRootView.findViewById(R.id.twouserlinear);
		arrowpayid = (ImageView) mRootView.findViewById(R.id.arrowpayid);
		arrowreceiveid = (ImageView) mRootView.findViewById(R.id.arrowreceiveid);
		datepickerimageview = (ImageView) mRootView.findViewById(R.id.datepickerimageview);
		categoryimageview = (ImageView) mRootView.findViewById(R.id.categoryimageview);
		receiveuserimageView = (CircularImageView) mRootView.findViewById(R.id.receiveuserimageView);
		paiduserimageView = (CircularImageView) mRootView.findViewById(R.id.paiduserimageView);
		paidusernametext = (TextView) mRootView.findViewById(R.id.paidusernametext);
		receiveusernametext = (TextView) mRootView.findViewById(R.id.receiveusernametext);
		paidby = (TextView) mRootView.findViewById(R.id.paidby);
		currencyText = (TextView) mRootView.findViewById(R.id.currencyText);
		amountedittext = (AutofitTextView) mRootView.findViewById(R.id.amountedittext);
		titleedittext = (EditText) mRootView.findViewById(R.id.titleedittext);
		
		
		
		intializeUI();
		return mRootView;
	}
	private void intializeUI(){
		personList = new ArrayList<Person>();
		
		arrowpayid.setVisibility(View.GONE);
		arrowreceiveid.setVisibility(View.VISIBLE);
		
		twouserlinear.setVisibility(View.VISIBLE);
		moretwouserlinear.setVisibility(View.GONE);
		
		datepickerimageview.setOnClickListener(clickListener);
		categoryimageview.setOnClickListener(clickListener);
		currencyText.setOnClickListener(clickListener);
	}
	
	
	
	

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
	}
	 private View.OnClickListener clickListener = new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            String text = "";
	        
	            switch (v.getId()) {
	            
	            case R.id.datepickerimageview:         	
	                text = "datepickerimageview";
	                break;
	            case R.id.categoryimageview:         	
		                text = "categoryimageview";
		                break;
	            case R.id.currencyText:         	
		                text = "currencyText";
		                break;
	               
	            }  	
	           Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();            	                  
	        }
	    };

}

