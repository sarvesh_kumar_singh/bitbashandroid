package com.bash.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class AddTransactionActivity extends FragmentActivity{
	
	private FragmentTabHost fragmenthost;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addtransaction);
        initializeFragmentTabHost();
    }

    private void initializeFragmentTabHost() {
    	
    	fragmenthost = (FragmentTabHost) findViewById(R.id.tabhost);
 		fragmenthost.setup(this, getSupportFragmentManager(), R.id.frames_holder);
 		
 		View GroupView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_graybackground_selector_bg, null);
		View PayView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_graybackground_selector_bg, null);
		View ChargeView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_graybackground_selector_bg, null);
		
		((TextView) GroupView.findViewById(R.id.tabText)).setText("Group");
		((TextView) PayView.findViewById(R.id.tabText)).setText("Pay");
		((TextView) ChargeView.findViewById(R.id.tabText)).setText("Charge");
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.ADD_DESCRIPTION_GROUP_TAG).setIndicator(GroupView),					
				ContainerProvider.AddDescription_Group_Container.class, null);
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.ADD_DESCRIPTION_PAY_TAG).setIndicator(PayView),
				ContainerProvider.AddDescription_Pay_Container.class, null);
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.ADD_DESCRIPTION_CHARGE_TAG).setIndicator(ChargeView),					
				ContainerProvider.AddDescription_Charge_Container.class, null);
		
		((ImageView)findViewById(R.id.topleftsideImage)).setImageResource(R.drawable.back_btn);
		
		((ImageView)findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();	
			}
		});	
		
		((ImageView)findViewById(R.id.toprightsideImage)).setBackgroundResource(0);
		
		//((TextView)findViewById(R.id.topmiddleText)).setText("bash");
		
 	}
    
    public void setUpTopBarFields(int leftSideImage, String middleText, int rightSideImage) {
    	((ImageView) findViewById(R.id.topleftsideImage)).setImageResource(leftSideImage);
    	((ImageView) findViewById(R.id.toprightsideImage)).setImageResource(rightSideImage);
    	//((TextView) findViewById(R.id.topmiddleText)).setText(middleText);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

}
