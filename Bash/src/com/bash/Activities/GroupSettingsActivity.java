package com.bash.Activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.Fragments.MyGroupsFragment;
import com.bash.GsonClasses.GroupSetting_Class;
import com.bash.GsonClasses.groupmembers;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.Singleton;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GroupSettingsActivity extends Activity implements AsyncResponse,
		OnClickListener {
	private Activity mActivity;
	private Context mContext;
	int pos = 0;
	MyAsynTaskManager myAsyncTask;
	View mRootView, addgroupAlertView;
	// public static SwipeListView myGroupListView;
	public ListView lviGroupFriendList;
	Dialog addmemberDialog;
	MyFriendsAdapter adapter;
	GroupSetting_Class responseForFriends;
	public ArrayList<groupmembers> feedList = new ArrayList<groupmembers>();
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView iviGroupImage, fileImage, iviBack, iviDelete;
	private TextView tviGroupName, tviHouse, tviTrip, tviParty, tviOther;
	String picturePath = "", image_path,idGroup, imgPath;;
	LinearLayout llAddbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_settings);
		Bundle extras = getIntent().getExtras();
		image_path = extras.getString("image");
		idGroup = extras.getString("idGroup");
		
		initializeView();
		//initializePictureAlertView();
		iviGroupImage.setOnClickListener(this);
		iviBack.setOnClickListener(this);
		//iviOption.setOnClickListener(this);
		tviHouse.setOnClickListener(this);
		tviTrip.setOnClickListener(this);
		tviParty.setOnClickListener(this);
		tviOther.setOnClickListener(this);
		iviDelete.setOnClickListener(this);
		llAddbutton.setOnClickListener(this);
		tviGroupName.setText(extras.getString("profileName")+" - Group members");
		
		
		if(image_path != null && image_path.length()!= 0 && iviGroupImage != null){
        	ImageLoader.getInstance().displayImage(image_path, 
        			iviGroupImage, BashApplication.options, BashApplication.animateFirstListener);
        }
		
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(Singleton.getInstance().refresh){
			feedList.clear();
			getGroupList();
			Singleton.getInstance().refresh = false;
		}
	}
	

	private void initializeView() {
		// TODO Auto-generated method stub
		mContext = GroupSettingsActivity.this;
		mActivity = GroupSettingsActivity.this;
		iviGroupImage = (ImageView)findViewById(R.id.iviGroupImage);
		iviBack = (ImageView) findViewById(R.id.iviBack);
		//iviOption = (ImageView) findViewById(R.id.iviOption);
		tviGroupName=(TextView)findViewById(R.id.tviGroupName);
		tviHouse = (TextView) findViewById(R.id.tviHouse);
		tviTrip = (TextView) findViewById(R.id.tviTrip);
		tviParty = (TextView) findViewById(R.id.tviParty);
		tviOther = (TextView) findViewById(R.id.tviOther);
		lviGroupFriendList=(ListView)findViewById(R.id.lviGroupFriendList);
		iviDelete=(ImageView)findViewById(R.id.iviDelete);
		llAddbutton=(LinearLayout)findViewById(R.id.llAddbutton);
		
		feedList.clear();
		
		adapter = new MyFriendsAdapter(mActivity, feedList);
		lviGroupFriendList.setAdapter(adapter);

		getGroupList();

	}

	private void initializeAlertView() {

		addmemberDialog = new Dialog(mContext);
		addmemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		addgroupAlertView = View.inflate(mContext,
				R.layout.alert_addgroup_dialog, null);
		addmemberDialog.setContentView(addgroupAlertView);

		fileImage = ((ImageView) addgroupAlertView
				.findViewById(R.id.groupImage));

		((ImageView) addgroupAlertView.findViewById(R.id.cancelDialog))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						addmemberDialog.dismiss();
					}
				});
		// block
		// ((ImageView)
		// addgroupAlertView.findViewById(R.id.addtoGroupButton)).setOnClickListener(new
		// OnClickListener() {
		// @Override
		// public void onClick(View v)
		// {
		// createGroupInServer(((EditText)addgroupAlertView.findViewById(R.id.groupText)).getText().toString());
		// }
		// });

		((ImageView) addgroupAlertView.findViewById(R.id.groupImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.show();
					}
				});

	}

	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(mContext,
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(mContext);
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);

		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						startActivityForResult(i, AppConstants.CODE_GALLERY);
					}
				});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getContentResolver().insert(
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
								values);
						Intent intent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								mCapturedImageURI);
						startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});

	}

	public void getGroupList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupSettings", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action", "idgroup" },
				new String[] { "group", "groupsettings", idGroup });
		myAsyncTask.execute();
	}

	public void createGroupInServer(final String groupName) {

		MyAsynImageTaskManager
				.setupParamsAndUrl(AppUrlList.ACTION_URL,
						new String[] { "module", "action", "iduser",
								"groupname", "groupimage" }, new String[] {
								"group", "add",
								PreferenceManager.getInstance().getUserId(),
								groupName, picturePath }, new String[] { "0",
								"0", "0", "0", "1" });

		new MyAsynImageTaskManager(mActivity,
				new MyAsynImageTaskManager.LoadListener() {

					@Override
					public void onLoadComplete(final String jsonResponse) {

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								try {
									if (jsonResponse == null) {
										Log.e("Null", "retured");
									}

									JSONObject rootObj = new JSONObject(
											jsonResponse);
									if (rootObj.getBoolean("result")) {
										

										adapter.notifyWithDataSet(feedList);
										// adapter.notifyDataSetChanged();
										addmemberDialog.dismiss();
										DialogManager.showDialog(mActivity,
												"Group Created Successfully!");

										
									} else {
										if (rootObj.getString("msg").equals(
												"Group Name exist"))
											DialogManager
													.showDialog(mActivity,
															"Group Name is Already Exist!");
										else
											DialogManager
													.showDialog(mActivity,
															"Error In Creating New Group! Try Again!");
									}
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
									DialogManager.showDialog(mActivity,
											"Server Error Occured! Try Again!");
								}
							}
						});

					}

					@Override
					public void onError(final String errorMessage) {
						runOnUiThread(new Runnable() {
							public void run() {
								DialogManager.showDialog(mActivity,
										errorMessage);
							}
						});

					}
				}).execute();

	}

	/*public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == mActivity.RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						fileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == mActivity.RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						fileImage);
			}
			break;
		default:
			break;
		}

	}*/
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
            	//setCapturedImage(getImagePath());
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
 
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions); 
                   
                    iviGroupImage.setImageBitmap(bitmap);
 
                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
 
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("path of image from gallery......******************.........", picturePath+"");
                iviGroupImage.setImageBitmap(thumbnail);
            }
        }
    }   
	
	public class MyFriendsAdapter extends BaseAdapter {
		public ArrayList<groupmembers> feedList = new ArrayList<groupmembers>();
		public ArrayList<groupmembers> originalList = new ArrayList<groupmembers>();
		public Activity context;
		public LinearLayout.LayoutParams backViewParams;

		public MyFriendsAdapter(Activity context, ArrayList<groupmembers> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20),
					LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<groupmembers> newlist) {
			this.feedList.clear();
			this.feedList = newlist;
			this.originalList = newlist;
			Log.e("Size of Adapter", String.valueOf(feedList.size()));
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			groupmembers listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				if (convertView == null) {
					convertView = inflater.inflate(R.layout.custom_editprofile_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
					holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
					holder.iviDeleteFriend = (ImageView) convertView.findViewById(R.id.iviDeleteFriend);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			//if (!listItem.getphone_no().equals("0")) {

				holder.friendName.setText(listItem.getfullname());
				if(listItem.getimagepath() != null && listItem.getimagepath().length()!= 0 && holder.friendImageSource != null){
		        	ImageLoader.getInstance().displayImage(listItem.getimagepath(), 
		        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
		        }else{
		        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
		        }
				holder.iviDeleteFriend.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						pos = position;
						// TODO Auto-generated method stub
						 removeFriendFromWebservice(pos);
						 //removeItemFromList(position);
					}
				});
				
			//}

			if (listItem.getimagepath() != null
					&& listItem.getimagepath().length() != 0
					&& holder.friendImageSource != null) {
				ImageLoader.getInstance().displayImage(listItem.getimagepath(),
						holder.friendImageSource, BashApplication.options,
						BashApplication.animateFirstListener);
			} else {
				holder.friendImageSource
						.setImageResource(R.drawable.addphoto_img_block);
			}

			return convertView;
		}

		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public groupmembers getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName;
			ImageView friendImageSource, iviDeleteFriend;
		}

	}
	
	public void removeFriendFromWebservice(final int position) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("removeFriendFromGroup",
				mActivity, AppUrlList.ACTION_URL, new String[] { "module",
						"action", "iduser", "idgroup", "friend", "usertype" }, 
						new String[] {"group", "update", PreferenceManager.getInstance().getUserId(),
				idGroup,feedList.get(position).getiduser(), feedList.get(position).getusertype()});
		myAsyncTask.execute();

	}

	public void removeGroupFromServer(final String groupId, final int poistion) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		Log.e("Group Name", groupId);
		myAsyncTask.setupParamsAndUrl("removeGroupFromServer", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group",
						"remove", groupId,
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		
		if (from.equalsIgnoreCase("getGroupSettings")) {
			if (output != null) {

				try {
					if (BuildConfig.DEBUG)
						Log.e("Json Response", output);
					Gson gson = new Gson();

					responseForFriends = gson.fromJson(output,
							GroupSetting_Class.class);
					if(responseForFriends.grouptype.equals("House")){
						setBackGround();
						tviHouse.setTextColor(Color.parseColor("#FFFFFF"));
						tviHouse.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
					}else if(responseForFriends.grouptype.equals("Trip")){
						setBackGround();
						tviTrip.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
						tviTrip.setTextColor(Color.parseColor("#FFFFFF"));
					}else if(responseForFriends.grouptype.equals("Party")){
						setBackGround();
						tviParty.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
						tviParty.setTextColor(Color.parseColor("#FFFFFF"));
					}else if(responseForFriends.grouptype.equals("Other")){
						setBackGround();
						tviOther.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
						tviOther.setTextColor(Color.parseColor("#FFFFFF"));
					}
					
					if (responseForFriends.result) {

						
						for (int i = 0; i < responseForFriends.groupmembers.size(); i++) {

							
							feedList.add(new groupmembers(
									responseForFriends.groupmembers.get(i).getiduser(),
									responseForFriends.groupmembers.get(i).getfullname(),
									responseForFriends.groupmembers.get(i).getemail(),
									responseForFriends.groupmembers.get(i).getphone_no(),
									responseForFriends.groupmembers.get(i).getphone_no(), 
									responseForFriends.groupmembers.get(i).getphone_no()));
						}

						
						adapter.notifyDataSetChanged();

					} else {
						DialogManager
								.showDialog(mActivity, "No Friends Found!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("removeFriendFromGroup")) {
			if (output != null) {

				try {
					if (output == null) {
						Log.e("Null", "retured");
					}
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						removeItemFromList(pos);
						DialogManager.showDialog(mActivity,
								"Friend Deleted Successfully!");

					} else {
						DialogManager.showDialog(mActivity,
								"Error in Delete Group! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		} else if (from.equalsIgnoreCase("exitfromGroupFromServer")) {
			if (output != null) {

				try {
					if (output == null) {
						Log.e("Null", "retured");
					}
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						DialogManager.showDialog(mActivity,
								"Group Deleted Successfully!");
						finish();

					} else {
						DialogManager.showDialog(mActivity,
								"Error in Delete Group! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		}
	}

	public void updateAdapter(ArrayList<groupmembers> newList) {
		/*
		 * feedList.clear(); feedList =
		 * DataBaseManager.getInstance().getFriendsList();
		 */
		// this.feedList = newList;
		// feedList = DataBaseManager.getInstance().getFriendsList();
		if (newList != null && newList.size() != 0) {
			adapter.notifyWithDataSet(newList);
		}
	}

	public void removeItemFromList(int position) {
		MyGroupsFragment.myGroupListView.closeAnimate(position);
		feedList.remove(position);
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}

	 private void selectImage() {
		 
	        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
	 
	        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
	        builder.setTitle("Add Photo!");
	        builder.setItems(options, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int item) {
	                if (options[item].equals("Take Photo"))
	                {
	                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
	                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
	                    startActivityForResult(intent, 1);
	                }
	                else if (options[item].equals("Choose from Gallery"))
	                {
	                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                    startActivityForResult(intent, 2);
	 
	                }
	                else if (options[item].equals("Cancel")) {
	                    dialog.dismiss();
	                }
	            }
	        });
	        builder.show();
	    }
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviGroupImage:
			selectImage();
			break;
		case R.id.iviBack:
			mActivity.finish();
			break;
		/*case R.id.iviOption:
			openOptionsMenu();
			
			break;*/
		case R.id.tviHouse:
			setBackGround();
			tviHouse.setTextColor(Color.parseColor("#FFFFFF"));
			tviHouse.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			break;
		case R.id.tviTrip:
			setBackGround();
			tviTrip.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviTrip.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		case R.id.tviParty:
			setBackGround();
			tviParty.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviParty.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		case R.id.tviOther:
			setBackGround();
			tviOther.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviOther.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		case R.id.iviDelete:
			exitGroup();
			break;
		case R.id.llAddbutton:
			Intent friendsIntent = new Intent(mContext, AddFriends_Activity.class);
			friendsIntent.putExtra("idGroup", idGroup);
			startActivity(friendsIntent);
			//Toast.makeText(mContext, "Add Member operation come soon", Toast.LENGTH_LONG).show();
			break;
		default:
			break;
		}
	}

	private void exitGroup() {
		// TODO Auto-generated method stub
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("exitfromGroupFromServer", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action", "idgroup", "iduser" }, 
						new String[] { "group", "exitgroup", idGroup, PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
	}

	void setBackGround() {
		tviHouse.setTextColor(Color.parseColor("#00BDCB"));
		tviHouse.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviTrip.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviTrip.setTextColor(Color.parseColor("#00BDCB"));
		tviParty.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviParty.setTextColor(Color.parseColor("#00BDCB"));
		tviOther.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviOther.setTextColor(Color.parseColor("#00BDCB"));
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.group_menu, menu);
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.itemSettings:
			// do part 1
			Toast.makeText(mContext, "First Item Click", Toast.LENGTH_LONG)
					.show();
			return true;
		case R.id.itemViewFeed:
			// do part 2
			Toast.makeText(mContext, "Second Item Click", Toast.LENGTH_LONG)
					.show();
			return true;
		case R.id.itemDelete:
			// do part 3
			Toast.makeText(mContext, "Third Item Click", Toast.LENGTH_LONG)
					.show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
