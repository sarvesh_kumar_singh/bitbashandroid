package com.bash.GsonClasses;

import java.util.ArrayList;

import com.bash.GsonClasses.Friends_Class.friendlist;
import com.bash.ListModels.BashUsers_Class;
import com.google.gson.annotations.SerializedName;

public class Friends_List_Class {
	@SerializedName("result")
	public boolean result;

	@SerializedName("msg")
	public String msg;
	
	@SerializedName("friends_count")
	public String friends_count;
	
	@SerializedName("friends")
	public ArrayList<friendlist> friendlist = new ArrayList<friendlist>();

	public class friendlist {

		@SerializedName("contactid")
		public String contactid;
		@SerializedName("fullname")
		public String fullname;
		@SerializedName("email")
		public String email;
		@SerializedName("phone_no")
		public String phone_no;
		@SerializedName("imagepath")
		public String imagepath;
		@SerializedName("usertype")
		public String usertype;
		@SerializedName("currency")
		public String currency;
		@SerializedName("amount")
		public String amount;
		@SerializedName("amounttype")
		public String amounttype;

		public String getContactid() {
			return this.contactid;
		}

		public String getFullname() {
			return this.fullname;
		}

		public String getEmail() {
			return this.email;
		}

		public String getPhone_no() {
			return this.phone_no;
		}

		public String getImagepath() {
			return this.imagepath;
		}

		public String getUsertype() {
			return this.usertype;
		}
		
		public String getCurrency() {
			return this.currency;
		}

		public String getAmount() {
			return this.amount;
		}

		public String getAmounttype() {
			return this.amounttype;
		}

		public void setFields(String contactid, String fullname, String email,
				String phone_no, String imagepath, String usertype, String currency, String amount, String amounttype) {
			this.contactid = contactid;
			this.fullname = fullname;
			this.email = email;
			this.phone_no = phone_no;
			this.imagepath = imagepath;
			this.usertype = usertype;
			this.currency = currency;
			this.amount = amount;
			this.amounttype = amounttype;
		}
	}
	
	public boolean getResult() {
		return this.result;
	}

}
