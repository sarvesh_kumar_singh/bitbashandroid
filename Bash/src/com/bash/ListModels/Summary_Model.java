package com.bash.ListModels;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Summary_Model {
	@SerializedName("full_name")
	public String full_name;
	@SerializedName("image_location")
	public String image_location;
	@SerializedName("idpayer")
	public String idpayer;
	@SerializedName("user_type")
	public String user_type;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("amount")
	public String amount;
	@SerializedName("payment_type")
	public String payment_type;
	@SerializedName("had_to_pay")
	public String had_to_pay;
	
	public String getFull_name() {
		return this.full_name;
	}

	public String getImage_location(){
		return this.image_location;
	}
	
	public String getIdpayer() {
		return this.idpayer;
	}

	public String getUser_type() {
		return this.user_type;
	}

	public String getIduser() {
		return this.iduser;
	}

	public String getAmount() {
		return this.amount;
	}

	public String getPayment_type() {
		return this.payment_type;
	}

	public String getHad_to_pay() {
		return this.had_to_pay;
	}
	
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
	public Summary_Model(String full_name, String image_location, String idpayer, String user_type, String iduser,
			String amount, String payment_type, String had_to_pay) {
		this.full_name = full_name;
		this.image_location = image_location;
		this.idpayer = idpayer;
		this.user_type = user_type;
		this.iduser = iduser;
		this.amount = amount;
		this.payment_type = payment_type;
		this.had_to_pay = had_to_pay;
	}
	
}
