package com.bash.Activities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.ListModels.FeedCommentInformation_Model;
import com.bash.ListModels.NewsFeed_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class Comment_Activity extends Activity implements AsyncResponse {
	Context mContext;
	Activity mActivity;
	String idPost = null,likeName=null;
	EditText txtComment;
	TextView tviLikesCountText;
	ImageView iviCommentLike, iviMakeComment;
	boolean visible = false,islike=false;
	String name="",image="";

	String commentactionview = "";
	View view;
	int pos = 0;
	MyAsynTaskManager myAsyncTask;
	View mRootView;
	ListView lviCommentsList;
	MyFeedCommentAdapter adapter;
	ArrayList<FeedCommentInformation_Model> feedList = new ArrayList<FeedCommentInformation_Model>();
	ArrayList<FeedCommentInformation_Model> commentList = new ArrayList<FeedCommentInformation_Model>();
	public NewsFeed_Model transactionDetails;
	ImageView commentLikeButton;
	String commentaction = "";

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comment_page);
		Bundle extras = getIntent().getExtras();
		idPost = extras.getString("idPost");
		islike = extras.getBoolean("like");
		likeName = extras.getString("name");
		init();
		if(islike){
			iviCommentLike.setImageResource(R.drawable.like_button_fill_s_comm);
		}
		if(likeName.length()>0){
			tviLikesCountText.setText(likeName);
		}
		iviMakeComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				makeAnComment(txtComment.getText().toString());
			}
		});
		txtComment.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() > 0 && !visible) {
					visible = true;
					iviMakeComment.setVisibility(View.VISIBLE);
				} else if (s.length() == 0 && visible) {
					visible = false;
					iviMakeComment.setVisibility(View.GONE);
				}
			}
		});
	};

	/*
	 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container, Bundle savedInstanceState) {
	 * 
	 * mRootView = inflater.inflate(R.layout.fragment_comment_page, null);
	 * return mRootView; }
	 */

	private void init() {
		// TODO Auto-generated method stub
		mContext = Comment_Activity.this;
		mActivity = Comment_Activity.this;
		txtComment = (EditText) findViewById(R.id.txtComment);
		tviLikesCountText = (TextView) findViewById(R.id.tviLikesCountText);
		iviCommentLike = (ImageView) findViewById(R.id.iviCommentLike);
		iviMakeComment = (ImageView) findViewById(R.id.iviMakeComment);
		lviCommentsList=(ListView)findViewById(R.id.lviCommentsList);
		adapter = new MyFeedCommentAdapter(this, feedList);
		lviCommentsList.setAdapter(adapter);
		getCommentList();
	}

	/*
	 * public Comment_Activity(NewsFeed_Model transactionDetails) {
	 * this.transactionDetails = transactionDetails; // Log.e("Transaction Id",
	 * transactionDetails.getIdTrans()); }
	 */

	/*
	 * @Override public void onActivityCreated(Bundle savedInstanceState) { //
	 * TODO Auto-generated method stub
	 * super.onActivityCreated(savedInstanceState); initializeView(); }
	 */

	// private void initializeView() {

	/*
	 * ((HomeActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn,
	 * "Add Comment", 0);
	 * 
	 * ((ImageView) getActivity().findViewById(R.id.topleftsideImage))
	 * .setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) {
	 * HomeFragment.parentFragment.popFragment(); } });
	 */

	// ImageLoader.getInstance().displayImage(
	// transactionDetails.getImagepath(),
	// ((ImageView)mRootView.findViewById(R.id.userImage)));

	/*
	 * ImageLoader.getInstance().displayImage(transactionDetails.
	 * getPoster_image_location(),
	 * ((ImageView)mRootView.findViewById(R.id.userImage)),
	 * BashApplication.options, BashApplication.animateFirstListener);
	 */

	/*
	 * ImageLoader.getInstance().displayImage(transactionDetails.getImagepath
	 * (), ((ImageView)mRootView.findViewById(R.id.userImage)),
	 * BashApplication.options, BashApplication.animateFirstListener);
	 */

	/*
	 * ImageLoader.getInstance().loadImage(transactionDetails.getImagepath(),
	 * new ImageLoadingListener() {
	 * 
	 * @Override public void onLoadingStarted(String arg0, View arg1) { // TODO
	 * Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onLoadingFailed(String arg0, View arg1, FailReason
	 * arg2) { // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onLoadingComplete(String arg0, View arg1, Bitmap
	 * arg2) {
	 * ((ImageView)mRootView.findViewById(R.id.userImage)).setImageBitmap
	 * (CurvedImageBitmapProvider.getRoundedImageBitmap(arg2)); }
	 * 
	 * @Override public void onLoadingCancelled(String arg0, View arg1) { //
	 * TODO Auto-generated method stub
	 * 
	 * } });
	 */

	/*
	 * ImageLoader.getInstance().loadImage(transactionDetails.getImagepath(),
	 * new SimpleImageLoadingListener() {
	 * 
	 * @Override public void onLoadingComplete(String imageUri, View view,
	 * Bitmap loadedImage) { ((ImageView)mRootView.findViewById(R.id.userImage
	 * )).setImageBitmap(CurvedImageBitmapProvider
	 * .getRoundedImageBitmap(loadedImage)); } });
	 */
	/*
	 * ((ImageView)mRootView.findViewById(R.id.userImage)).setImageBitmap(
	 * CurvedImageBitmapProvider.getRoundedCornerBitmap(
	 * (BitmapFactory.decodeResource(getActivity().getResources(),
	 * R.drawable.addphoto_img_block)), 10));
	 */

	/*
	 * ((TextView)
	 * mRootView.findViewById(R.id.resonforPayment)).setText(Html.fromHtml(
	 * "<font color='#00BDCC' size='16'>"+ "JohnDoe" + "</font>" + " paid "
	 * +"<font color='#00BDCC' size='16'>"+ "Jane White" + "</font>" +
	 * " for the delicious cheese sandwitch. Cheers to the good times." ));
	 */

	/*
	 * commentLikeButton = (ImageView) mRootView
	 * .findViewById(R.id.commentLikeButton);
	 * 
	 * if (transactionDetails.getIslike())
	 * commentLikeButton.setImageResource(R.drawable.like_btn_select); else
	 * commentLikeButton.setImageResource(R.drawable.like_btn_deselect);
	 * 
	 * commentLikeButton.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { if
	 * (transactionDetails.getIslike()) { commentaction = "unlikefeed"; //
	 * makeAnLike(transactionDetails.getIdTrans(), // "unlikefeed"); } else {
	 * commentaction = "feedlike"; //
	 * makeAnLike(transactionDetails.getIdTrans(), "feedlike"); }
	 * 
	 * } });
	 * 
	 * myfeedListView = (ListView)
	 * mRootView.findViewById(R.id.commentsListView);
	 * 
	 * adapter = new MyFeedCommentAdapter(this, feedList);
	 * 
	 * myfeedListView.setAdapter(adapter);
	 */

	// ((TextView)
	// mRootView.findViewById(R.id.placeofPayment)).setText(transactionDetails.getLocation());
	// Sarvesh
	/*
	 * ((TextView) mRootView.findViewById(R.id.resonforPayment)).
	 * setText(Html.fromHtml( "<font color='#00BDCC' size='16'>"+
	 * transactionDetails.getPaidBy() + "</font>" + " paid "
	 * +"<font color='#00BDCC' size='16'>"+ transactionDetails.getPaidFor() +
	 * "</font> for " + transactionDetails.getFeedComment()));
	 */

	/*
	 * ((TextView) mRootView.findViewById(R.id.likesCountText))
	 * .setText(transactionDetails.getLikeCount());
	 */
	// ((TextView)
	// mRootView.findViewById(R.id.commentsCount)).setText(transactionDetails.getCommentsCount());

	/*
	 * ((ImageView) findViewById(R.id.makeCommentButton))
	 * .setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { if (((EditText) mRootView
	 * .findViewById(R.id.commentEditText)).getText() != null && ((EditText)
	 * mRootView .findViewById(R.id.commentEditText))
	 * .getText().toString().length() != 0) makeAnComment(((EditText) mRootView
	 * .findViewById(R.id.commentEditText)) .getText().toString()); else
	 * Toast.makeText(mContext, "Please Enter Any Comment In Box!",
	 * Toast.LENGTH_SHORT).show(); } });
	 */

	/*
	 * if(PreferenceManager.getInstance().getUserImagePath() != null &&
	 * PreferenceManager.getInstance().getUserImagePath().length() != 0)
	 * ImageLoader .getInstance().displayImage(PreferenceManager.getInstance()
	 * .getUserImagePath(), ((ImageView)
	 * mRootView.findViewById(R.id.userImage)));
	 */
	// getCommentList();
	// }

	public void makeAnLike(String commetnId, final String commentAction) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("makeAnLike", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idtrans", "iduser" }, new String[] { "feed",
						commentAction, commetnId,
						PreferenceManager.getInstance().getUserId() });

		myAsyncTask.execute();
	}

	public void makeAnComment(String commentText) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("makeAnComment", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser", "idpost", "comment" }, new String[] { "post",
						"commentOnAPost", PreferenceManager.getInstance().getUserId(), idPost, commentText });
		// PreferenceManager.getInstance().getUserId()

		myAsyncTask.execute();

	}

	public void getCommentList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		// getlatest
		myAsyncTask.setupParamsAndUrl("getCommentList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idpost" }, new String[] { "post", "getComments",
						idPost });
		myAsyncTask.execute();

	}

	public class MyFeedCommentAdapter extends BaseAdapter {
		private ArrayList<FeedCommentInformation_Model> feedList = new ArrayList<FeedCommentInformation_Model>();
		private Activity context;

		public MyFeedCommentAdapter(Activity context,
				ArrayList<FeedCommentInformation_Model> feedList) {
			this.context = context;
			this.feedList = feedList;
		}

		public void notifyDataSet() {
			this.notifyDataSetChanged();
		}

		public void notifyWithDataSet(ArrayList<FeedCommentInformation_Model> newList) {
			this.feedList = newList;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			final FeedCommentInformation_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						R.layout.comment_feed_listview, null);
				holder = new ViewHolder();

				holder.tviCommentantName = (TextView) convertView
						.findViewById(R.id.tviCommentantName);
				holder.tviCommentText = (TextView) convertView
						.findViewById(R.id.tviCommentText);
				holder.iviCommentantImage = (ImageView) convertView
						.findViewById(R.id.iviCommentantImage);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.tviCommentantName.setText(listItem.getCommenter_name());
			holder.tviCommentText.setText(listItem.getComment());
			
			// holder.commentantImage.setImageResource(listItem.getcommentantImage());

			//final ImageView commentantImageView = holder.iviCommentantImage;

			ImageLoader.getInstance().displayImage(
					listItem.getCommenter_image(), holder.iviCommentantImage,
					BashApplication.options,
					BashApplication.animateFirstListener);

			if (listItem.getCommenter_image() != null
					&& listItem.getCommenter_image().length() != 0) {
				ImageLoader.getInstance().displayImage(
						listItem.getCommenter_image(), holder.iviCommentantImage,
						BashApplication.options,
						BashApplication.animateFirstListener);
			} else
				holder.iviCommentantImage
						.setImageResource(R.drawable.addphoto_img_block);

			/*holder.timeofComment.setText(listItem.gettimeofComment());

			if (listItem.getisLiked().equals("1"))
				holder.commentLikeButton
						.setImageResource(R.drawable.like_icon_selected);
			else
				holder.commentLikeButton
						.setImageResource(R.drawable.like_icon_unselected);*/

			// final View view = convertView;
			view = convertView;

			/*holder.commentLikeBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (listItem.getisLiked().equals("1")) {
						pos = position;
						commentactionview = "unlikecomment";
						makeAnLikeView(listItem.getcommentId(), view,
								commentactionview, pos);
					} else {
						pos = position;
						commentactionview = "commentlike";
						makeAnLikeView(listItem.getcommentId(), view,
								commentactionview, pos);
					}

				}
			});*/

			/*
			 * holder.commentantImage.setImageBitmap(CurvedImageBitmapProvider.
			 * getRoundedCornerBitmap(
			 * (BitmapFactory.decodeResource(context.getResources(),
			 * R.drawable.addphoto_img_block)), 10));
			 */

			/*
			 * if(listItem.isLike) holder.isLikeButton.setChecked(true); else
			 * holder.isLikeButton.setChecked(false);
			 * 
			 * if(listItem.isComment) holder.isCommentButton.setChecked(true);
			 * else holder.isCommentButton.setChecked(false);
			 */
			return convertView;
		}

		@Override
		public int getCount() {
			return feedList.size();
		}

		@Override
		public FeedCommentInformation_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public class ViewHolder {
			TextView tviCommentantName;
			TextView tviCommentText;
			ImageView iviCommentantImage;
		}

	}

	public void makeAnLikeView(String commetnId, final View view,
			final String commentAction, final int position) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("makeAnLikeView", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idcomment", "iduser" }, new String[] { "feed",
						commentAction, commetnId,
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
		if (from.equalsIgnoreCase("makeAnLike")) {
			if (output != null) {

				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {

						// likeTextView.setText(rootObj.getString("likes_count"));
						((TextView) mRootView.findViewById(R.id.likesCountText))
								.setText(rootObj.getString("likes_count"));

						transactionDetails.setLikeCount(((TextView) mRootView
								.findViewById(R.id.likesCountText)).getText()
								.toString());

						if (commentaction.equals("unlikefeed")) {
							transactionDetails.setIslike(false);// ("0");
							commentLikeButton
									.setImageResource(R.drawable.like_btn_deselect);
						} else {
							transactionDetails.setIslike(true);// ("1");
							commentLikeButton
									.setImageResource(R.drawable.like_btn_select);
						}

					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("makeAnComment")) {
			if (output != null) {
				try {

					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						
						commentList.add(new FeedCommentInformation_Model(
								"", idPost, PreferenceManager.getInstance().getUserId(), name, image, txtComment.getText().toString()));
					}
					adapter.notifyWithDataSet(commentList);
					txtComment.setText("");
					// ((TextView)
					// mRootView.findViewById(R.id.commentsCount)).setText(String.valueOf(
					// Integer.parseInt(transactionDetails.getCommentsCount()) +
					// 1));
					// transactionDetails.setCommentsCount(((TextView)
					// mRootView.findViewById(R.id.commentsCount)).getText().toString());
					// adapter.notifyWithDataSet(feedList);
					/*((EditText) mRootView.findViewById(R.id.commentEditText))
							.setText("");
					lviCommentsList.setSelection(adapter.getCount());
					Toast.makeText(mContext, "Your comment has been posted!",
							Toast.LENGTH_SHORT).show();
					Utils.closeSoftInputBoard(((EditText) mRootView
							.findViewById(R.id.commentEditText)));*/

					// if(((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).getVisibility()
					// == View.VISIBLE)
					// {
					// ((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.GONE);
					// }
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}
			} else {

			}
		} else if (from.equalsIgnoreCase("getCommentList")) {
			if (output != null) {
				
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {

						JSONArray jsonArray = rootObj.getJSONArray("comments_information");
						JSONObject jObj;
						
						for (int i = 0; i < jsonArray.length(); i++) {
							jObj = jsonArray.getJSONObject(i);
							if(jObj.getString("iduser").equals(PreferenceManager.getInstance().getUserId())){
								name=jObj.getString("commenter_name"); 
								image=jObj.getString("commenter_image");
							}
							commentList.add(new FeedCommentInformation_Model(
									jObj.getString("idcomment"), jObj
											.getString("idpost"), jObj
											.getString("iduser"), jObj
											.getString("commenter_name"), jObj
											.getString("commenter_image"), jObj
											.getString("comment")));
						}
						
						//feedList = newList;
						adapter.notifyWithDataSet(commentList);

						// if(feedList.size() == 0)
						// ((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.VISIBLE);
						// else
						// ((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.GONE);
						//
					}
				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("makeAnLikeView")) {
			if (output != null) {

				try {

					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {

						((TextView) view.findViewById(R.id.commentLikeCount))
								.setText(rootObj.getString("likes_count"));

						/*feedList.get(pos).setcommentLikeCount(
								rootObj.getString("likes_count"));

						if (commentactionview.equals("unlikecomment")) {
							feedList.get(pos).setisLiked("0");
							((ImageView) view
									.findViewById(R.id.commentLikeButton))
									.setImageResource(R.drawable.like_btn_blue_deselect);
						} else {
							feedList.get(pos).setisLiked("1");
							((ImageView) view
									.findViewById(R.id.commentLikeButton))
									.setImageResource(R.drawable.like_btn_select);
						}*/
						adapter.notifyDataSetChanged();
					} else {
						Toast.makeText(mContext,
								"Error In Makeing Like/Unlike! Try Again!",
								Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {
				// TODO: handle exception
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		}
	}
}
