package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.bash.R;
import com.bash.Application.BashApplication;

public class MyWallet_Banking_SavedCards_Fragment extends Fragment{

	Spinner daysSpinner;
	Spinner yearsSpinner;
	View mRootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_wallbank_debitpage, null);
		initializeView();
		return mRootView;
	}

	private void initializeView() {
		// TODO Auto-generated method stub

		daysSpinner = (Spinner) mRootView.findViewById(R.id.daysSpinner);
		yearsSpinner = (Spinner) mRootView.findViewById(R.id.yearsSpinner);

		daysSpinner.setAdapter(BashApplication.daysAdapter);
		yearsSpinner.setAdapter(BashApplication.yearsAdapter);

	}

}
