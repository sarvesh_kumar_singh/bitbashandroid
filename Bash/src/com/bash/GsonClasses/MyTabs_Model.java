package com.bash.GsonClasses;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class MyTabs_Model {

	@SerializedName("result")
	public boolean result;

	@SerializedName("msg")
	public String msg;

	@SerializedName("transactions_information")
	public ArrayList<transactions_information> friendlist = new ArrayList<transactions_information>();

	public boolean getResult() {
		return this.result;
	}

	public class transactions_information {

		@SerializedName("idtrans")
		public String idtrans;
		@SerializedName("trans_type")
		public String trans_type;
		@SerializedName("entered_by_iduser")
		public String entered_by_iduser;
		@SerializedName("currency")
		public String currency;
		@SerializedName("total_amount")
		public String total_amount;
		@SerializedName("category")
		public String category;
		@SerializedName("date")
		public String date;
		@SerializedName("transaction_title")
		public String transaction_title;
		@SerializedName("datetime")
		public String datetime;
		@SerializedName("timestamp")
		public String timestamp;
		@SerializedName("status")
		public String status;
		@SerializedName("payers")
		public ArrayList<payers> payerslist = new ArrayList<payers>();
		@SerializedName("receivers")
		public ArrayList<receivers> receiverslist = new ArrayList<receivers>();

		public String getIdtrans() {
			return this.idtrans;
		}

		public String getTrans_type() {
			return this.trans_type;
		}

		public String getEntered_by_iduser() {
			return this.entered_by_iduser;
		}

		public String getCurrency() {
			return this.currency;
		}

		public String getTotal_amount() {
			return this.total_amount;
		}

		public String getCategory() {
			return this.category;
		}

		public String getDate() {
			return this.date;
		}

		public String getTransaction_title() {
			return this.transaction_title;
		}

		public String getDatetime() {
			return this.datetime;
		}

		public String getTimestamp() {
			return this.timestamp;
		}

		public String getStatus() {
			return this.status;
		}

		public void setFields(String idtrans, String trans_type,
				String entered_by_iduser, String currency, String total_amount,
				String category, String date, String transaction_title,
				String datetime, String timestamp, String status) {
			this.idtrans = idtrans;
			this.trans_type = trans_type;
			this.entered_by_iduser = entered_by_iduser;
			this.currency = currency;
			this.total_amount = total_amount;
			this.category = category;
			this.date = date;
			this.transaction_title = transaction_title;
			this.datetime = datetime;
			this.timestamp = timestamp;
			this.status = status;
		}
	}

	public class payers {

		@SerializedName("result")
		public boolean result;

		@SerializedName("msg")
		public String msg;

		@SerializedName("payers_information")
		public payers_information payers_information;

		public class payers_information {
			@SerializedName("full_name")
			public String full_name;
			@SerializedName("idpayer")
			public String idpayer;
			@SerializedName("idtrans")
			public String idtrans;
			@SerializedName("user_type")
			public String user_type;
			@SerializedName("iduser")
			public String iduser;
			@SerializedName("amount")
			public String amount;
			@SerializedName("payment_type")
			public String payment_type;
			@SerializedName("had_to_pay")
			public String had_to_pay;

			public String getFull_name() {
				return this.full_name;
			}

			public String getIdpayer() {
				return this.idpayer;
			}

			public String getIdtrans() {
				return this.idtrans;
			}

			public String getUser_type() {
				return this.user_type;
			}

			public String getIduser() {
				return this.iduser;
			}

			public String getAmount() {
				return this.amount;
			}

			public String getPayment_type() {
				return this.payment_type;
			}

			public String getHad_to_pay() {
				return this.had_to_pay;
			}

			public void setFields(String full_name, String idpayer,
					String idtrans, String user_type, String iduser,
					String amount, String payment_type, String had_to_pay) {
				this.full_name = full_name;
				this.idpayer = idpayer;
				this.idtrans = idtrans;
				this.user_type = user_type;
				this.iduser = iduser;
				this.amount = amount;
				this.payment_type = payment_type;
				this.had_to_pay = had_to_pay;

			}
		}

	}

	public class receivers {
		@SerializedName("result")
		public boolean result;

		@SerializedName("msg")
		public String msg;

		@SerializedName("receivers_information")
		public receivers_information receivers_information;

		public class receivers_information {
			@SerializedName("full_name")
			public String full_name;
			@SerializedName("idreceiver")
			public String idreceiver;
			@SerializedName("idtrans")
			public String idtrans;
			@SerializedName("user_type")
			public String user_type;
			@SerializedName("iduser")
			public String iduser;
			@SerializedName("payment_type")
			public String payment_type;
			@SerializedName("amount")
			public String amount;

			public String getFull_name() {
				return this.full_name;
			}

			public String getIdreceiver() {
				return this.idreceiver;
			}

			public String getIdtrans() {
				return this.idtrans;
			}

			public String getUser_type() {
				return this.user_type;
			}

			public String getIduser() {
				return this.iduser;
			}

			public String getAmount() {
				return this.amount;
			}

			public String getPayment_type() {
				return this.payment_type;
			}

			public void setFields(String full_name, String idreceiver,
					String idtrans, String user_type, String iduser,
					String amount, String payment_type) {
				this.full_name = full_name;
				this.idreceiver = idreceiver;
				this.idtrans = idtrans;
				this.user_type = user_type;
				this.iduser = iduser;
				this.amount = amount;
				this.payment_type = payment_type;

			}
		}

	}
}
