package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;

public class WalletFragment extends Fragment{

	View mRootView;
	public static BaseFragment parentFragmentClass;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mywallet_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 
		 parentFragmentClass = ((BaseFragment)getParentFragment());
		 
		 ((SlidingActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn,"", "Wallet","", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Sarvesh
					((SlidingActivity)getActivity()).dLayout.openDrawer(Gravity.LEFT);
				}
		 });
	
		 
		 ((RelativeLayout)mRootView.findViewById(R.id.depositIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new PaymentFragment(), true);
					//parentFragmentClass.replaceFragment(new DepositFragment(), true);
				
				}
		 });
		 
		 ((RelativeLayout)mRootView.findViewById(R.id.withdrawIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new WithDrawFragment(), true);
				}
		 });
		 
		 
		 ((RelativeLayout)mRootView.findViewById(R.id.statementIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new StatementFragment(), true);
				}
		 });
		 
		 ((RelativeLayout)mRootView.findViewById(R.id.bankIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new BankingFragment(), true);
				}
		 });
		 
		 
		 
		 
		 
	}
	
	
}
