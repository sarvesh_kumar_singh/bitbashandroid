package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class Group_Model{
	@SerializedName("idgroup")
	public String idgroup;
	@SerializedName("groupname")
	public String groupname;
	@SerializedName("image_location")
	public String image_location;
	

	public String getIdgroup() {
		return this.idgroup;
	}
	
	public String getGroupname() {
		return this.groupname;
	}

	public String getImage_location() {
		return this.image_location;
	}

	
	public Group_Model(String idgroup, String groupname,
			String image_location) {
		this.idgroup = idgroup;
		this.groupname = groupname;
		this.image_location = image_location;
	}

	
}
