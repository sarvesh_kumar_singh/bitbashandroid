package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class MyTrans_Friends_Class {

	@SerializedName("firstname")
	public String firstname;
	
	@SerializedName("imagepath")
	public String imagepath;
	
	@SerializedName("charge")
	public String charge; 
	
	@SerializedName("idpaid_by")
	public String idpaid_by;
	
	@SerializedName("idpaid_for")
	public String idpaid_for;
	
	 
	public void setFieldValues(String firstname, String imagepath, String charge, String status, 
			String recorded_on, String feed_comment, String location, String idpaid_by, String idpaid_for){
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public MyTrans_Friends_Class(String firstname, String imagepath, String charge, String idpaid_by, String idpaid_for){
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public String getidPaidFor(){
		return idpaid_for;
	}
	
	public String getIdPaidBy(){
		return idpaid_by;
	}
	
	public String getFristName(){
		return firstname;
	}
	public String getImagepath(){
		return imagepath;
	}
	
	public String getCharge(){
		return charge;
	}
	
	 
	
}

