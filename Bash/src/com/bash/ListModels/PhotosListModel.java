package com.bash.ListModels;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotosListModel implements Parcelable {
	String ID = "";
	String imageLocation = "";
	String name = "";
	boolean imageSelect ;
	boolean imageItemWiseSelect ;
	boolean imageLongpress ;
	private String userType = "";
	float paidamount = 0.0f;
    float receiveamount = 0.0f;
	
	public PhotosListModel(String userType,String ID,String imageLocation,String name){
		this.userType = userType;
		this.ID = ID;
		this.imageLocation = imageLocation;
		this.name = name;
		this.imageSelect = true;
		this.imageLongpress = false;
	}
	public PhotosListModel(){
		
	}
	
	
	public final float getPaidamount() {
		return paidamount;
	}
	public final void setPaidamount(float paidamount) {
		this.paidamount = paidamount;
	}
	public final float getReceiveamount() {
		return receiveamount;
	}
	public final void setReceiveamount(float receiveamount) {
		this.receiveamount = receiveamount;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getImageLocation() {
		return imageLocation;
	}
	public void setImageLocation(String imageLocation) {
		this.imageLocation = imageLocation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isImageSelect() {
		return imageSelect;
	}
	public void setImageSelect(boolean imageSelect) {
		this.imageSelect = imageSelect;
	}
	public boolean isImageLongpress() {
		return imageLongpress;
	}
	public void setImageLongpress(boolean imageLongpress) {
		this.imageLongpress = imageLongpress;
	}

	public boolean isImageItemWiseSelect() {
		return imageItemWiseSelect;
	}
	public void setImageItemWiseSelect(boolean imageItemWiseSelect) {
		this.imageItemWiseSelect = imageItemWiseSelect;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
		dest.writeString(name);
		dest.writeString(userType);
		dest.writeString(ID);
		dest.writeString(imageLocation);
		dest.writeByte((byte) (imageSelect ? 1 : 0));
		dest.writeByte((byte) (imageLongpress ? 1 : 0));
		dest.writeByte((byte) (imageItemWiseSelect ? 1 : 0));
		dest.writeFloat(paidamount);
		dest.writeFloat(receiveamount);
		
	}
	public static final Parcelable.Creator<PhotosListModel> CREATOR =
			new Parcelable.Creator<PhotosListModel>() {

		@Override
		public PhotosListModel createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			PhotosListModel photosList = new PhotosListModel();
			
			photosList.name = source.readString();
			photosList.userType = source.readString();
			photosList.ID = source.readString();		
			photosList.imageLocation = source.readString();
			photosList.imageSelect = source.readByte() != 0;
			photosList.imageLongpress = source.readByte() != 0;
			photosList.imageItemWiseSelect = source.readByte() != 0;
			photosList.paidamount = source.readFloat();
			photosList.receiveamount = source.readFloat();
			
	
			
			return photosList;
		}

		@Override
		public PhotosListModel[] newArray(int size) {
			// TODO Auto-generated method stub
			return new PhotosListModel[size];  
		}
	};

	
	}
