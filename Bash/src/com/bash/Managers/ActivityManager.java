package com.bash.Managers;

import com.bash.Application.BashApplication;

import android.app.Activity;
import android.content.Intent;

public class ActivityManager {

	public static void startActivity(Activity acivity, Class<?> destinationClass){
		acivity.startActivity(new Intent(acivity, destinationClass));
	}
}
