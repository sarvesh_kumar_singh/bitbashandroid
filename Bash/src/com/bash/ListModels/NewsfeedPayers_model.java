package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedPayers_model {
	@SerializedName("total_payers")
	public String total_payers;
	@SerializedName("payers_information")
	public NewsfeedPayersInformation_Model[] payers_information;

	public NewsfeedPayers_model(String total_payers, NewsfeedPayersInformation_Model[] payers_information) {
		this.total_payers = total_payers;
		this.payers_information = payers_information;

	}

	public String getTotal_payers() {
		return total_payers;
	}

	public NewsfeedPayersInformation_Model[] getPayers_information() {
		return payers_information;
	}

}
