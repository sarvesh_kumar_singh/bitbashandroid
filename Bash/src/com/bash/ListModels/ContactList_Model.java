package com.bash.ListModels;

public class ContactList_Model {
	
	public String contactId, contactName, contactNumber, contactEmailId;
	
	public ContactList_Model() {
		// TODO Auto-generated constructor stub
	}
	public ContactList_Model(String contactId, String contactName, String contactNumber, String contactEmailId){
		this.contactId = contactId;
		this.contactEmailId = contactEmailId;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
	}
	
	public void setcontactId(String contactId){
		this.contactId = contactId;
	}
	
	public void setcontactEmailId(String contactEmailId){
		this.contactEmailId = contactEmailId;
	}
	
	public void setcontactName(String contactName){
		this.contactName = contactName;
	}
	
	public void setcontactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}
	
	public String getcontactId(){
		return contactId;
	}
	
	public String getcontactEmailId(){
		return contactEmailId;
	}
	
	public String getcontactName(){
		return contactName;
	}
	
	public String getcontactNumber(){
		return contactNumber;
	}
	
}

