package com.bash.GsonClasses;

import java.util.ArrayList;

import com.bash.ListModels.BashUsers_Class;
import com.google.gson.annotations.SerializedName;

public class GroupSetting_Class {

	@SerializedName("result")
	public boolean result;
	@SerializedName("msg")
	public String msg;
	@SerializedName("idgroup")
	public boolean idgroup;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("groupname")
	public boolean groupname;
	@SerializedName("grouptype")
	public String grouptype;
	@SerializedName("groupadmin")
	public boolean groupadmin;
	@SerializedName("imagepath")
	public String imagepath;
	@SerializedName("recorded_on")
	public boolean recorded_on;
	@SerializedName("edited_by")
	public String edited_by;
	@SerializedName("edited_on")
	public boolean edited_on;
	@SerializedName("status")
	public String status;

	@SerializedName("groupmembers")
	public ArrayList<groupmembers> groupmembers = new ArrayList<groupmembers>();
	
}
