package com.bash.Managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
 
public class InternetManager {
     
    public static boolean TYPE_CONNECTED = true, TYPE_NOT_CONNECTED = false;
     
     
    public static boolean getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
 
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_CONNECTED;
             
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_CONNECTED;
        } 
        return TYPE_NOT_CONNECTED;
    }
   
}