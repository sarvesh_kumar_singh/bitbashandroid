package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedPaidforInformation_Model {
	@SerializedName("idpost_paid_for")
	public String idpost_paid_for;
	@SerializedName("idpost")
	public String idpost;
	@SerializedName("paid_for_type")
	public String paid_for_type;
	@SerializedName("user_or_group_id")
	public String user_or_group_id;
	@SerializedName("fullname")
	public String fullname;
	

	public NewsfeedPaidforInformation_Model(String idpost_paid_for, String idpost, String paid_for_type,
			 String user_or_group_id, String fullname) {
		this.idpost_paid_for = idpost_paid_for;
		this.idpost = idpost;
		this.paid_for_type = paid_for_type;
		this.user_or_group_id = user_or_group_id;
		this.fullname = fullname;
	}

	public String getIdpost_paid_for() {
		return idpost_paid_for;
	}

	public String getIdpost() {
		return idpost;
	}

	public String getPaid_for_type() {
		return paid_for_type;
	}

	public String getUser_or_group_id() {
		return user_or_group_id;
	}

	public String getFullname() {
		return fullname;
	}
}
