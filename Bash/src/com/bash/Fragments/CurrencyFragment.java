package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_List_Class;
import com.bash.ListModels.Currency_List_Model;
import com.bash.ListModels.Friends_List_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;

public class CurrencyFragment extends BaseFragment implements AsyncResponse {
	MyAsynTaskManager myAsyncTask;
	public static View mRootView;
	public static ListView lviCurrency;
	public MyFriendsAdapter adapter;
	public ArrayList<Currency_List_Model> feedList;
	Friends_List_Class responseForFriends;
	int pos = 0;

	//ImageButton imageButton;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_currency_page, null);
		initializeView();
		return mRootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Sarvesh((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Sarvesh
		// ((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		// Initialize Activity Views
		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
						((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
					}
				});
		
		/*imageButton = (ImageButton) mRootView.findViewById(R.id.imageButton);

		imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment) getParentFragment()).replaceFragment(
						new InviteFriends_Fragment(), true);
				((HomeActivity) getActivity()).currentFragment = CurrencyFragment.this;
			}
		});*/
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.back_icon_ads,
				"", "Currency", "", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment) getParentFragment()).popFragment();
			}
		});
		// friendsListView = (SwipeSwipeListView)
		// mRootView.findViewById(R.id.friendsListView);
		lviCurrency = (ListView) mRootView.findViewById(R.id.lviCurrency);

		feedList = new ArrayList<Currency_List_Model>();

		adapter = new MyFriendsAdapter(getActivity(), feedList);

		lviCurrency.setAdapter(adapter);

		// Capture Text in EditText
		((EditText) mRootView.findViewById(R.id.searchBox))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void afterTextChanged(Editable arg0) {
						// TODO Auto-generated method stub
						String text = ((EditText) mRootView
								.findViewById(R.id.searchBox)).getText()
								.toString().toLowerCase(Locale.getDefault());
						adapter.getFilter().filter(text);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}
				});
		getCurrencyList();
		//getFriendsList();

	}

	private void getCurrencyList() {
		// TODO Auto-generated method stub
		for(int i = 0; i < currencySymbol.length; i++){
			feedList.add(new Currency_List_Model(currencySymbol[i][0], currencySymbol[i][1]));
		}
		adapter.notifyWithDataSet(feedList);
	}

	public void updateAdapter(ArrayList<Currency_List_Model> newList) {
		/*
		 * feedList.clear(); feedList =
		 * DataBaseManager.getInstance().getFriendsList();
		 */
		// this.feedList = newList;
		// feedList = DataBaseManager.getInstance().getFriendsList();
		if (newList != null && newList.size() != 0) {
			adapter.notifyWithDataSet(newList);
		}
	}

	public void getFriendsList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getFriendsList", getActivity(),
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "friend", "friendslist",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();

	}

	public class MyFriendsAdapter extends BaseAdapter implements Filterable {
		public ArrayList<Currency_List_Model> feedList = new ArrayList<Currency_List_Model>();
		public ArrayList<Currency_List_Model> originalList = new ArrayList<Currency_List_Model>();
		public Activity context;
		ViewHolder holder = null;
		public LinearLayout.LayoutParams backViewParams;
		public FriendFilter filter;

		public MyFriendsAdapter(Activity context,
				ArrayList<Currency_List_Model> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20),
					LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<Currency_List_Model> newlist) {
			this.feedList = newlist;
			this.originalList = newlist;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			Currency_List_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				if (convertView == null) {
					convertView = inflater.inflate(R.layout.custom_currency_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.currencycode);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.friendName.setText(listItem.getCurrencyName() + " - " + listItem.getCountryName());
			
			holder.friendName.setTag(position);
			holder.friendName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});

			return convertView;
		}

		/*
		 * public void checkEmptyList(){ if(feedList.size() == 0){
		 * this.feedList.add(emptyList); } }
		 */
		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public Currency_List_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName;
		}

		// Filter Class
		public void filter(String charText) {
			charText = charText.toLowerCase(Locale.getDefault());
			Log.e("charText", charText + "");
			Log.e("Originallengh", String.valueOf(originalList.size()));
			feedList.clear();
			if (charText.length() == 0) {
				feedList.addAll(originalList);
			} else {
				for (Currency_List_Model wp : originalList) {
					if (wp.getCountryName().startsWith(charText)) {
						Log.e("Groupname", wp.getCountryName());
						feedList.add(wp);
					}
				}
			}
			this.notifyDataSetChanged();
			this.notifyDataSetInvalidated();
		}

		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			if (filter == null)
				filter = new FriendFilter();
			return filter;
		}

		// filter Class...
		private class FriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalList;
					results.count = originalList.size();
				} else {
					// We perform filtering operation
					List<Currency_List_Model> tempList = new ArrayList<Currency_List_Model>();
					for (Currency_List_Model p : feedList) {
						if (p.getCountryName()
								.toUpperCase()
								.startsWith(constraint.toString().toUpperCase()))
							tempList.add(p);
					}
					results.values = tempList;
					results.count = tempList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// Now we have to inform the adapter about the new list filtered
				if (results.count == 0)
					notifyDataSetInvalidated();
				else {
					feedList = (ArrayList<Currency_List_Model>) results.values;
					notifyDataSetChanged();
				}
			}
		}

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		ArrayList<Friends_List_Model> newList1 = new ArrayList<Friends_List_Model>();

		if (from.equalsIgnoreCase("getFriendsList")) {
			if (output != null) {

				try {
					Gson gson = new Gson();

					responseForFriends = gson.fromJson(output,
							Friends_List_Class.class);

					if (BuildConfig.DEBUG)
						Log.e("Json Response", output);
					// JSONObject jobj= new JSONObject(output);
					if (responseForFriends.getResult()) {

						/*
						 * DataBaseManager.getInstance().storeFriendsList(
						 * responseForFriends.friendlist);
						 */
						// updateAdapter();
						// adapter.notifyDataSetChanged();
						// newList.clear();

						ArrayList<Friends_List_Model> newList = new ArrayList<Friends_List_Model>();
						// responseForFriends.getClass().getName()
						for (int i = 0; i < responseForFriends.friendlist
								.size(); i++) {

							Log.e("Friend Id",
									responseForFriends.friendlist.get(i).contactid);
							Log.e("Friend number",
									responseForFriends.friendlist.get(i).email);
							Log.e("Friend number",
									responseForFriends.friendlist.get(i).phone_no);
							Log.e("Friend name",
									responseForFriends.friendlist.get(i).fullname);
							Log.e("Friend imagepath",
									responseForFriends.friendlist.get(i).imagepath);

							/*
							 * feedList.add(new Friends_List_Model(
							 * responseForFriends.friendlist.get(i).contactid,
							 * responseForFriends.friendlist.get(i).fullname,
							 * responseForFriends.friendlist.get(i).email,
							 * responseForFriends.friendlist.get(i).phone_no,
							 * responseForFriends.friendlist.get(i).imagepath,
							 * responseForFriends.friendlist.get(i).usertype,
							 * responseForFriends.friendlist.get(i).currency,
							 * responseForFriends.friendlist.get(i).amount,
							 * responseForFriends
							 * .friendlist.get(i).amounttype));
							 */
						}

						// Log.e("Size of file",
						// String.valueOf(feedList.size()));
						// updateAdapter(feedList);
						adapter.notifyWithDataSet(feedList);

					} else {
						// ((RelativeLayout)
						// mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
						DialogManager.showDialog(getActivity(),
								"No Friends Found!");
						// updateAdapter(newList);
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(),
							"Server Error Occured! Try Again!");
				}

			} else {

				// TODO: handle exception
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");

			}
		}

	}
	
	String[][] currencySymbol = {
			{ "AED", "United Arab Emirates dirham" },
			{ "AFN", "Afghani" },
			{ "ALL", "Lek" },
			{ "AMD", "Armenian Dram" },
			{ "ANG", "Netherlands Antillian Guilder" },
			{ "AOA", "Kwanza" },
			{ "ARS", "Argentine Peso" },
			{ "AUD", "Australian Dollar" },
			{ "AWG", "Aruban Guilder" },
			{ "AZN", "Azerbaijanian Manat" },
			{ "BAM", "Convertible Marks" },
			{ "BBD", "Barbados Dollar" },
			{ "BDT", "Bangladeshi Taka" },
			{ "BGN", "Bulgarian Lev" },
			{ "BHD", "Bahraini Dinar" },
			{ "BIF", "Burundian Franc" },
			{ "BMD", "Bermudian Dollar (customarily known as Bermuda Dollar)" },
			{ "BND", "Brunei Dollar" },
			{ "BOB", "Boliviano" },
			{ "BOV", "Bolivian Mvdol (Funds code)" },
			{ "BRL", "Brazilian Real" },
			{ "BSD", "Bahamian Dollar" },
			{ "BTN", "Ngultrum" },
			{ "BWP", "Pula" },
			{ "BYR", "Belarussian Ruble" },
			{ "BZD", "Belize Dollar" },
			{ "CAD", "Canadian Dollar" },
			{ "CDF", "Franc Congolais" },
			{ "CHE", "WIR Euro (complementary currency)" },
			{ "CHF", "Swiss Franc" },
			{ "CHW", "WIR Franc (complementary currency)" },
			{ "CLF", "Unidades de formento (Funds code)" },
			{ "CLP", "Chilean Peso" },
			{ "CNY", "Yuan Renminbi" },
			{ "COP", "Colombian Peso" },
			{ "COU", "Unidad de Valor Real" },
			{ "CRC", "Costa Rican Colon" },
			{ "CUP", "Cuban Peso" },
			{ "CVE", "Cape Verde Escudo" },
			{ "CYP", "Cyprus Pound" },
			{ "CZK", "Czech Koruna" },
			{ "DJF", "Djibouti Franc" },
			{ "DKK", "Danish Krone" },
			{ "DOP", "Dominican Peso" },
			{ "DZD", "Algerian Dinar" },
			{ "EEK", "Kroon" },
			{ "EGP", "Egyptian Pound" },
			{ "ERN", "Nakfa" },
			{ "ETB", "Ethiopian Birr" },
			{ "EUR", "Euro" },
			{ "FJD", "Fiji Dollar" },
			{ "FKP", "Falkland Islands Pound" },
			{ "GBP", "Pound Sterling" },
			{ "GEL", "Lari" },
			{ "GHS", "Cedi" },
			{ "GIP", "Gibraltar pound" },
			{ "GMD", "Dalasi" },
			{ "GNF", "Guinea Franc" },
			{ "GTQ", "Quetzal" },
			{ "GYD", "Guyana Dollar" },
			{ "HKD", "Hong Kong Dollar" },
			{ "HNL", "Lempira" },
			{ "HRK", "Croatian Kuna" },
			{ "HTG", "Haiti Gourde" },
			{ "HUF", "Forint" },
			{ "IDR", "Rupiah" },
			{ "ILS", "New Israeli Shekel" },
			{ "INR", "Indian Rupee" },
			{ "IQD", "Iraqi Dinar" },
			{ "IRR", "Iranian Rial" },
			{ "ISK", "Iceland Krona" },
			{ "JMD", "Jamaican Dollar" },
			{ "JOD", "Jordanian Dinar" },
			{ "JPY", "Japanese yen" },
			{ "KES", "Kenyan Shilling" },
			{ "KGS", "Som" },
			{ "KHR", "Riel" },
			{ "KMF", "Comoro Franc" },
			{ "KPW", "North Korean Won" },
			{ "KRW", "South Korean Won" },
			{ "KWD", "Kuwaiti Dinar" },
			{ "KYD", "Cayman Islands Dollar" },
			{ "KZT", "Tenge" },
			{ "LAK", "Kip" },
			{ "LBP", "Lebanese Pound" },
			{ "LKR", "Sri Lanka Rupee" },
			{ "LRD", "Liberian Dollar" },
			{ "LSL", "Loti" },
			{ "LTL", "Lithuanian Litas" },
			{ "LVL", "Latvian Lats" },
			{ "LYD", "Libyan Dinar" },
			{ "MAD", "Moroccan Dirham" },
			{ "MDL", "Moldovan Leu" },
			{ "MGA", "Malagasy Ariary" },
			{ "MKD", "Denar" },
			{ "MMK", "Kyat" },
			{ "MNT", "Tugrik" },
			{ "MOP", "Pataca" },
			{ "MRO", "Ouguiya" },
			{ "MTL", "Maltese Lira" },
			{ "MUR", "Mauritius Rupee" },
			{ "MVR", "Rufiyaa" },
			{ "MWK", "Kwacha" },
			{ "MXN", "Mexican Peso" },
			{ "MXV", "Mexican Unidad de Inversion (UDI) (Funds code)" },
			{ "MYR", "Malaysian Ringgit" },
			{ "MZN", "Metical" },
			{ "NAD", "Namibian Dollar" },
			{ "NGN", "Naira" },
			{ "NIO", "Cordoba Oro" },
			{ "NOK", "Norwegian Krone" },
			{ "NPR", "Nepalese Rupee" },
			{ "NZD", "New Zealand Dollar" },
			{ "OMR", "Rial Omani" },
			{ "PAB", "Balboa" },
			{ "PEN", "Nuevo Sol" },
			{ "PGK", "Kina" },
			{ "PHP", "Philippine Peso" },
			{ "PKR", "Pakistan Rupee" },
			{ "PLN", "Zloty" },
			{ "PYG", "Guarani" },
			{ "QAR", "Qatari Rial" },
			{ "RON", "Romanian New Leu" },
			{ "RSD", "Serbian Dinar" },
			{ "RUB", "Russian Ruble" },
			{ "RWF", "Rwanda Franc" },
			{ "SAR", "Saudi Riyal" },
			{ "SBD", "Solomon Islands Dollar" },
			{ "SCR", "Seychelles Rupee" },
			{ "SDG", "Sudanese Pound" },
			{ "SEK", "Swedish Krona" },
			{ "SGD", "Singapore Dollar" },
			{ "SHP", "Saint Helena Pound" },
			{ "SKK", "Slovak Koruna" },
			{ "SLL", "Leone" },
			{ "SOS", "Somali Shilling" },
			{ "SRD", "Surinam Dollar" },
			{ "STD", "Dobra" },
			{ "SYP", "Syrian Pound" },
			{ "SZL", "Lilangeni" },
			{ "THB", "Baht" },
			{ "TJS", "Somoni" },
			{ "TMM", "Manat" },
			{ "TND", "Tunisian Dinar" },
			{ "TOP", "Pa'anga" },
			{ "TRY", "New Turkish Lira" },
			{ "TTD", "Trinidad and Tobago Dollar" },
			{ "TWD", "New Taiwan Dollar" },
			{ "TZS", "Tanzanian Shilling" },
			{ "UAH", "Hryvnia" },
			{ "UGX", "Uganda Shilling" },
			{ "USD", "US Dollar" },
			{ "UYU", "Peso Uruguayo" },
			{ "UZS", "Uzbekistan Som" },
			{ "VEB", "Venezuelan bolívar" },
			{ "VND", "Vietnamese đồng" },
			{ "VUV", "Vatu" },
			{ "WST", "Samoan Tala" },
			{ "XAF", "CFA Franc BEAC" },
			{ "XAG", "Silver (one Troy ounce)" },
			{ "XAU", "Gold (one Troy ounce)" },
			{ "XBA", "European Composite Unit (EURCO) (Bonds market unit)" },
			{ "XBB", "European Monetary Unit (E.M.U.-6) (Bonds market unit)" },
			{ "XBC", "European Unit of Account 9 (E.U.A.-9) (Bonds market unit)" },
			{ "XBD", "European Unit of Account 17 (E.U.A.-17) (Bonds market unit)" },
			{ "XCD", "East Caribbean Dollar" },
			{ "XDR", "Special Drawing Rights" },
			{ "XFO", "Gold franc (special settlement currency)" },
			{ "XFU", "UIC franc (special settlement currency)" },
			{ "XOF", "CFA Franc BCEAO" },
			{ "XPD", "Palladium (one Troy ounce)" }, { "XPF", "CFP franc" },
			{ "XPT", "Platinum (one Troy ounce)" },
			{ "XTS", "Code reserved for testing purposes" },
			{ "XXX", "No currency" }, { "YER", "Yemeni Rial" },
			{ "ZAR", "South African Rand" }, { "ZMK", "Kwacha" },
			{ "ZWD", "Zimbabwe Dollar" }};

}
