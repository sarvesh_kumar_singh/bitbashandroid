package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedImageInformation_model {
	@SerializedName("idimage")
	public String idimage;
	@SerializedName("idpost")
	public String idpost;
	@SerializedName("image_location")
	public String image_location;

	public NewsfeedImageInformation_model(String idimage, String idpost,
			String image_location) {
		this.idimage = idimage;
		this.idpost = idpost;
		this.image_location = image_location;
	}

	public String getIdimage() {
		return idimage;
	}

	public String getIdpost() {
		return idpost;
	}

	public String getImage_location() {
		return image_location;
	}

}
