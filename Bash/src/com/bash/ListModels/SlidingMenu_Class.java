package com.bash.ListModels;

public class SlidingMenu_Class {

	public String menuName;
	Integer menuId;
	
	public SlidingMenu_Class(String menuName, Integer menuId) {
		this.menuName = menuName;
		this.menuId = menuId;
	}
	
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	
	
}
