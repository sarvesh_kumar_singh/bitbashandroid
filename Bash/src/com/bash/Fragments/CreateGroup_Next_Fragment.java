package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.GroupDetailActivity;
import com.bash.Activities.SlidingActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CreateGroup_Next_Fragment extends BaseFragment implements AsyncResponse {
	View mRootView;
	private ListView lviFriendsList,lviSelectedFriendsList;
	private EditText txtEnterFriendsName;
	int pos = 0;
	MyAsynTaskManager myAsyncTask;
	MyFriendsAdapter adapter;
	SeletedFriendsAdapter friendsadapter;
	Friends_Class responseForFriends;
	public ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
	public ArrayList<BashUsers_Class> selectedfeedList = new ArrayList<BashUsers_Class>();
	ViewGroup viewContainer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		viewContainer = container;
		mRootView = inflater.inflate(R.layout.fragment_creategroup_next, null);
		init();
		return mRootView;
	}

	private void init() {
		// TODO Auto-generated method stub
		txtEnterFriendsName = (EditText) mRootView.findViewById(R.id.txtEnterFriendsName);
		lviFriendsList = (ListView) mRootView.findViewById(R.id.lviFriendsList);
		lviSelectedFriendsList = (ListView) mRootView.findViewById(R.id.lviSelectedFriendsList);

		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn,"",
				"Create Group","", R.drawable.enterpayee);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
				});
		((ImageView) getActivity().findViewById(R.id.toprightsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						addFriends();
					}
				});
		feedList.clear();
		adapter = new MyFriendsAdapter(getActivity(), feedList);
		lviFriendsList.setAdapter(adapter);
		friendsadapter =new SeletedFriendsAdapter(getActivity(), selectedfeedList);
		lviSelectedFriendsList.setAdapter(friendsadapter);

		getGroupList();
		
		txtEnterFriendsName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String text = txtEnterFriendsName.getText().toString();
                adapter.getFilter().filter(text);
                if(text.length()>0){
                	lviFriendsList.setVisibility(View.VISIBLE);
                	lviSelectedFriendsList.setVisibility(View.INVISIBLE);
                }else {
                	lviFriendsList.setVisibility(View.INVISIBLE);
                	lviSelectedFriendsList.setVisibility(View.VISIBLE);
                }
            }
 
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3) {
            }
 
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
            }
        });
		
		lviFriendsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				BashUsers_Class listItem = (BashUsers_Class) lviFriendsList.getItemAtPosition(position);//getListItem(position);
				selectedfeedList.add(new BashUsers_Class(listItem.idfriend, listItem.phone_no, listItem.name, listItem.imagepath, listItem.is_send_request, listItem.is_request_received, listItem.is_friend));
				friendsadapter.notifyDataSetChanged();
				lviFriendsList.setVisibility(View.INVISIBLE);
            	lviSelectedFriendsList.setVisibility(View.VISIBLE);
            	txtEnterFriendsName.setText("");
            	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
            		      getActivity().INPUT_METHOD_SERVICE);
            	imm.hideSoftInputFromWindow(viewContainer.getWindowToken(), 0);
			}
		});

	}
	public void addFriends() 
	{
		String friendsName="",userType="";
		for(int i=0;i<selectedfeedList.size();i++){
			if(i==0){
				friendsName=selectedfeedList.get(i).getname();
				userType="Bash";
			}else if(i>0){
				friendsName=friendsName+","+selectedfeedList.get(i).getname();
				userType=userType+","+"Bash";
			}
		}
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("addFriendsinGroup",getActivity(),AppUrlList.ACTION_URL, 
			new String[] { "module", "action", "idgroup", "friends", "usertype"}, 
			new String[] { "group", "addfriends", ((SlidingActivity)getActivity()).groupId, friendsName, userType});
		myAsyncTask.execute();
	}
	public void getGroupList() {
		feedList.add(new BashUsers_Class("1", "123456", "Aphrodit", "",	"FALSE", "FALSE", "TRUE"));
		feedList.add(new BashUsers_Class("2", "123456", "Athena", "", "FALSE", "FALSE", "TRUE"));
		feedList.add(new BashUsers_Class("3", "123456", "Hera", "", "FALSE", "FALSE", "TRUE"));
		feedList.add(new BashUsers_Class("4", "123456", "Jeus", "", "FALSE", "FALSE", "TRUE"));
		adapter.notifyDataSetChanged();
		/*myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupList", getActivity(),
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "group", "grouplist",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();*/
	}
	public class SeletedFriendsAdapter extends BaseAdapter {
		public ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
		public ArrayList<BashUsers_Class> originalList = new ArrayList<BashUsers_Class>();
		public Activity context;
		public LinearLayout.LayoutParams backViewParams;

		public SeletedFriendsAdapter(Activity context, ArrayList<BashUsers_Class> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20), LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<BashUsers_Class> newlist) {
			this.feedList.clear();
			this.feedList = newlist;
			this.originalList = newlist;
			Log.e("Size of Adapter", String.valueOf(feedList.size()));
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			BashUsers_Class listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(getActivity());
				if (convertView == null) {
					convertView = inflater.inflate(R.layout.custom_selectedfriends_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
					holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
					holder.iviDeleteFriend = (ImageView) convertView.findViewById(R.id.iviDeleteFriend);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Log.e("String from Adpater", listItem.getphone_no());

			if (!listItem.getphone_no().equals("0")) {
				
				holder.friendName.setText(listItem.getname());
				
				holder.iviDeleteFriend.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						pos = position;
						// TODO Auto-generated method stub
						removeItemFromList(position);
						// removeFriendFromWebservice(pos);
						/*
						 * removeItemFromList(position);
						 * MyFriendsFragment.friendsListView
						 * .closeAnimate(position);
						 */
					}
				});
			}

			if (listItem.getimagepath() != null	&& listItem.getimagepath().length() != 0 && holder.friendImageSource != null) {
				ImageLoader.getInstance().displayImage(listItem.getimagepath(),	holder.friendImageSource, 
						BashApplication.options, BashApplication.animateFirstListener);
			} else {
				holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
			}

			return convertView;
		}

		public void removeItemFromList(int position)
		{
			this.feedList.remove(position);
			//checkEmptyList();
			this.notifyDataSetInvalidated();
			this.notifyDataSetChanged();
	    }
		
		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public BashUsers_Class getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName;
			ImageView friendImageSource, iviDeleteFriend;
		}
	}
	
	public BashUsers_Class getListItem(int position) {
		return selectedfeedList.get(position);
	}

	public class MyFriendsAdapter extends BaseAdapter implements Filterable {
		public ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
		public ArrayList<BashUsers_Class> originalList = new ArrayList<BashUsers_Class>();
		public Activity context;
		public LinearLayout.LayoutParams backViewParams;
		public FriendFilter filter;

		public MyFriendsAdapter(Activity context, ArrayList<BashUsers_Class> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20), LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<BashUsers_Class> newlist) {
			this.feedList.clear();
			this.feedList = newlist;
			this.originalList = newlist;
			Log.e("Size of Adapter", String.valueOf(feedList.size()));
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			BashUsers_Class listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(getActivity());
				if (convertView == null) {
					convertView = inflater.inflate(R.layout.custom_friends_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
					holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
					
					
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			Log.e("String from Adpater", listItem.getphone_no());

			if (!listItem.getphone_no().equals("0")) {
				
				holder.friendName.setText(listItem.getname());
				
			}

			if (listItem.getimagepath() != null	&& listItem.getimagepath().length() != 0 && holder.friendImageSource != null) {
				ImageLoader.getInstance().displayImage(listItem.getimagepath(),	holder.friendImageSource, 
						BashApplication.options, BashApplication.animateFirstListener);
			} else {
				holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
			}

			return convertView;
		}

		
		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public BashUsers_Class getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName;
			ImageView friendImageSource;
		}

		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			if (filter == null)
				filter = new FriendFilter();
			return filter;
		}

		// filter Class...
		private class FriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalList;
					results.count = originalList.size();
				} else {
					// We perform filtering operation
					List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
					for (BashUsers_Class p : feedList) {
						if (p.getname().toUpperCase().startsWith(constraint.toString().toUpperCase()))
							tempList.add(p);
					}
					results.values = tempList;
					results.count = tempList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				// Now we have to inform the adapter about the new list filtered
				if (results.count == 0)
					notifyDataSetInvalidated();
				else {
					feedList = (ArrayList<BashUsers_Class>) results.values;
					notifyDataSetChanged();
				}
			}
		}

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if (from.equalsIgnoreCase("addFriendsinGroup")) {
			if (output != null) {

				try {
					JSONObject jObj=new JSONObject(output);
					if(jObj.getBoolean("result")){
						//Toast.makeText(getActivity(), jObj.getString("msg"), Toast.LENGTH_LONG).show();
						((BaseFragment) getParentFragment()).popAllFragment();
						Intent profileIntent = new Intent(((SlidingActivity)getActivity()), GroupDetailActivity.class);
						profileIntent.putExtra("Profilename", ((SlidingActivity)getActivity()).groupName);
						startActivity(profileIntent);
					}else{
						Toast.makeText(getActivity(), jObj.getString("msg"), Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}
}
