package com.bash.Activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.GsonClasses.Friends_List_Class;
import com.bash.ListModels.Friends_List_Model;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Summary_Model;
import com.bash.ListModels.Transactions_information;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.Singleton;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class AddFriends_Activity extends Activity implements AsyncResponse,
		OnClickListener {
	Context mContext;
	Activity mActivity;
	MyAsynTaskManager myAsyncTask;
	public ArrayList<Transactions_information> feedList1, feedlistNew;
	public ArrayList<Friends_List_Model> feedList = new ArrayList<Friends_List_Model>();
	ArrayList<Payers> payerslist = new ArrayList<Payers>();
	ArrayList<Summary_Model> summarylist = new ArrayList<Summary_Model>();
	MyFriendsAdapter adapter;
	Friends_List_Class responseForFriends;

	ImageView iviBack;
	ListView lviSummary;

	String idGroup, picturePath = "", group_member_id, groupName,
			image_location, currency;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addfriends);
		Bundle bundle = getIntent().getExtras();
		idGroup = bundle.getString("idGroup");
		init();

	}

	private void init() {
		// TODO Auto-generated method stub
		mActivity = AddFriends_Activity.this;
		mContext = AddFriends_Activity.this;

		iviBack = (ImageView) findViewById(R.id.iviBack);
		lviSummary = (ListView) findViewById(R.id.lviSummary);

		iviBack.setOnClickListener(this);
		feedList1 = new ArrayList<Transactions_information>();
		adapter = new MyFriendsAdapter(mActivity, feedList);
		lviSummary.setAdapter(adapter);

		// Capture Text in EditText
		((EditText) findViewById(R.id.searchBox))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void afterTextChanged(Editable arg0) {
						// TODO Auto-generated method stub
						String text = ((EditText) findViewById(R.id.searchBox))
								.getText().toString().toLowerCase(Locale.getDefault());
						adapter.getFilter().filter(text);
					}

					@Override
					public void beforeTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onTextChanged(CharSequence arg0, int arg1,
							int arg2, int arg3) {
						// TODO Auto-generated method stub
					}
				});

		getFriendsList();
	}

	/*public void getGroupDetailList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getFriendsDetailList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group",
						"getAllGroupDetails", idGroup,
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
	}*/

	public void updateAdapter(ArrayList<Friends_List_Model> newList) {
		if (newList != null && newList.size() != 0) {
			adapter.notifyWithDataSet(newList);
		}
	}

	public void getFriendsList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getFriendsList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "friend", "friendslist",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();

	}

	public class MyFriendsAdapter extends BaseAdapter implements Filterable {
		public ArrayList<Friends_List_Model> feedList = new ArrayList<Friends_List_Model>();
		public ArrayList<Friends_List_Model> originalList = new ArrayList<Friends_List_Model>();
		public Activity context;
		ViewHolder holder = null;
		public LinearLayout.LayoutParams backViewParams;
		public FriendFilter filter;

		public MyFriendsAdapter(Activity context,
				ArrayList<Friends_List_Model> feedList) {
			this.context = context;
			this.feedList = feedList;
			this.originalList = feedList;
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager
					.getInstance().getPercentageFromWidth(20),
					LayoutParams.MATCH_PARENT);
		}

		public void notifyWithDataSet(ArrayList<Friends_List_Model> newlist) {
			this.feedList = newlist;
			this.originalList = newlist;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			Friends_List_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				if (convertView == null) {
					convertView = inflater.inflate(
							R.layout.custom_trans_detail_listview, null);
					holder = new ViewHolder();
					holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
					holder.iviCommentantImage = (ImageView) convertView.findViewById(R.id.iviFriendImageSource);
				}
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.friendName.setText(listItem.getFullname());

			holder.friendName.setTag(position);
			holder.friendName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					/*Intent profileIntent = new Intent(mActivity, MyProfileActivity.class);
					profileIntent.putExtra("idUser", feedList.get((Integer) v.getTag()).getContactid());
					profileIntent.putExtra("Profilename", feedList.get((Integer) v.getTag()).getFullname());
					profileIntent.putExtra("image", feedList.get((Integer) v.getTag()).getImagepath());
					profileIntent.putExtra("amount", feedList.get((Integer) v.getTag()).getAmount());
					profileIntent.putExtra("type", feedList.get((Integer) v.getTag()).getAmounttype());
					profileIntent.putExtra("usertype", feedList.get((Integer) v.getTag()).getUsertype());
					startActivity(profileIntent);*/
					
					addFriendsinGroup(feedList.get((Integer) v.getTag()).getContactid(), feedList.get((Integer) v.getTag()).getUsertype());
				}
			});

			if (listItem.getImagepath() != null && listItem.getImagepath().length() != 0 && holder.iviCommentantImage != null) {
				ImageLoader.getInstance().displayImage(listItem.getImagepath(),
						holder.iviCommentantImage, BashApplication.options,
						BashApplication.animateFirstListener);
			} else {
				holder.iviCommentantImage.setImageResource(R.drawable.addphoto_img_block);
			}

			return convertView;
		}

		

		@Override
		public int getCount() {
			// checkEmptyList();
			return feedList.size();
		}

		@Override
		public Friends_List_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		private class ViewHolder {
			TextView friendName;
			ImageView iviCommentantImage;
			
		}

		// Filter Class
		public void filter(String charText) {
			charText = charText.toLowerCase(Locale.getDefault());
			Log.e("charText", charText + "");
			Log.e("Originallengh", String.valueOf(originalList.size()));
			feedList.clear();
			if (charText.length() == 0) {
				feedList.addAll(originalList);
			} else {
				for (Friends_List_Model wp : originalList) {
					if (wp.getFullname().startsWith(charText)) {
						Log.e("Groupname", wp.getFullname());
						feedList.add(wp);
					}
				}
			}
			this.notifyDataSetChanged();
			this.notifyDataSetInvalidated();
		}

		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			if (filter == null)
				filter = new FriendFilter();
			return filter;
		}

		// filter Class...
		private class FriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalList;
					results.count = originalList.size();
				} else {
					// We perform filtering operation
					List<Friends_List_Model> tempList = new ArrayList<Friends_List_Model>();
					for (Friends_List_Model p : feedList) {
						if (p.getFullname()
								.toUpperCase()
								.startsWith(constraint.toString().toUpperCase()))
							tempList.add(p);
					}
					results.values = tempList;
					results.count = tempList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// Now we have to inform the adapter about the new list filtered
				if (results.count == 0)
					notifyDataSetInvalidated();
				else {
					feedList = (ArrayList<Friends_List_Model>) results.values;
					notifyDataSetChanged();
				}
			}
		}

	}

	public void removeItemFromList(int position) {
		DataBaseManager.getInstance().unFriendwithUserId(
				this.feedList.get(position).contactid);
		feedList.remove(position);
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}

	public void removeFriendFromWebservice(final int position) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl(
				"removeFriendFromWebservice",
				mActivity,
				AppUrlList.ACTION_URL,
				new String[] { "module", "action", "iduser", "idfriend" },
				new String[] { "user", "removefriend",
						PreferenceManager.getInstance().getUserId(),
						feedList.get(position).contactid });
		// getisFriend()
		Log.e("***************", "**************");

		Log.e("My User Id", PreferenceManager.getInstance().getUserId());
		Log.e("Removing Friend Id", feedList.get(position).contactid);
		myAsyncTask.execute();

	}
	
	public void addFriendsinGroup(String idFriend, String userType) {
		// TODO Auto-generated method stub
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("addFriendsinGroup", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action", "idgroup", "friends", "usertype"},
						new String[] { "group", "addfriends", idGroup, idFriend, userType});
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub

		if (from.equalsIgnoreCase("getFriendsList")) {
			if (output != null) {

				try {
					Gson gson = new Gson();

					responseForFriends = gson.fromJson(output,
							Friends_List_Class.class);

					if (BuildConfig.DEBUG)
						Log.e("Json Response", output);
					// JSONObject jobj= new JSONObject(output);
					if (responseForFriends.getResult()) {


						ArrayList<Friends_List_Model> newList = new ArrayList<Friends_List_Model>();
						// responseForFriends.getClass().getName()
						for (int i = 0; i < responseForFriends.friendlist
								.size(); i++) {

							Log.e("Friend Id",
									responseForFriends.friendlist.get(i).contactid);
							Log.e("Friend number",
									responseForFriends.friendlist.get(i).email);
							Log.e("Friend number",
									responseForFriends.friendlist.get(i).phone_no);
							Log.e("Friend name",
									responseForFriends.friendlist.get(i).fullname);
							Log.e("Friend imagepath",
									responseForFriends.friendlist.get(i).imagepath);

							feedList.add(new Friends_List_Model(
									responseForFriends.friendlist.get(i).contactid,
									responseForFriends.friendlist.get(i).fullname,
									responseForFriends.friendlist.get(i).email,
									responseForFriends.friendlist.get(i).phone_no,
									responseForFriends.friendlist.get(i).imagepath,
									responseForFriends.friendlist.get(i).usertype,
									responseForFriends.friendlist.get(i).currency,
									responseForFriends.friendlist.get(i).amount,
									responseForFriends.friendlist.get(i).amounttype));
						}

						adapter.notifyWithDataSet(feedList);

					} else {
						// ((RelativeLayout)
						// mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
						DialogManager
								.showDialog(mActivity, "No Friends Found!");
						// updateAdapter(newList);
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				// TODO: handle exception
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		} else if (from.equalsIgnoreCase("addFriendsinGroup")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						// MyFriendsFragment.friendsListView.closeAnimate(pos);
						// removeItemFromList(pos);
						Singleton.getInstance().refresh = true;
						
						Typeface customFont = Typeface.createFromAsset(mActivity.getAssets(), "myriad.ttf");
						final Dialog dialog = new Dialog(mActivity,R.style.Dialog_No_Border);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.custom_alert_dialog);
						LinearLayout topLayout=(LinearLayout)dialog.findViewById(R.id.topLayout);
						TextView textView1=(TextView)dialog.findViewById(R.id.textView1);
						textView1.setTypeface(customFont);
						TextView msgText=(TextView)dialog.findViewById(R.id.mobileNumberText);
						msgText.setTypeface(customFont);
						msgText.setText("Friend Added Successfully!");
						Button btn_ok=(Button)dialog.findViewById(R.id.btn_ok);
						btn_ok.setTypeface(customFont);
						topLayout.setBackgroundColor(Color.TRANSPARENT);
						
						dialog.show();
						btn_ok.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								dialog.dismiss();
								finish();
								}
						});
						
						
					} else {
						DialogManager.showDialog(mActivity,
								"Error Occured to Adding Friend! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}
			} else {
				// TODO: handle exception
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("removeFriendFromWebservice")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						// MyFriendsFragment.friendsListView.closeAnimate(pos);
						// removeItemFromList(pos);
						DialogManager.showDialog(mActivity,
								"Friend Removed Successfully!");
					} else {
						DialogManager.showDialog(mActivity,
								"Error Occured to Remove Friend! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}
			} else {
				// TODO: handle exception
				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviBack:
			finish();
			break;

		default:
			break;
		}
	}

}
