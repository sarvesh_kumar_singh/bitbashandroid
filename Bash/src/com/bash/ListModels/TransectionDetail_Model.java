package com.bash.ListModels;

public class TransectionDetail_Model {
	public String trans_text;
	public String image_path;

	public String getTrans_text() {
		return trans_text;
	}

	public String getImage_path() {
		return image_path;
	}

	public TransectionDetail_Model(String trans_text, String image_path) {
		this.trans_text = trans_text;
		this.image_path = image_path;
	}

}
