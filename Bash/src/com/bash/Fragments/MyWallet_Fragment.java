package com.bash.Fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Activities.SlidingActivity;
import com.bash.Adapters.MyTabsAdapter_Onl;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Pending_New_Class;
import com.bash.ListModels.MyTabs_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;

@SuppressLint("ValidFragment")
public class MyWallet_Fragment extends BaseFragment implements
		AsyncResponse, OnClickListener {
	MyAsynTaskManager myAsyncTask;
	public Pending_New_Class pendingNewClass;
	public ArrayList<MyTabs_Model> feedList;
	View mRootView;
	public static BaseFragment parentFragmentClass;
	public static FragmentTabHost mTabhost;
	public ListView lviMyTabs;
	public MyTabs_Model tabModel;
	public MyTabsAdapter_Onl myTabsAdapter;
	private TextView tviLoadWallet,tviWithdraw,tviLoadWalletUnderline,tviWithdrawUnderline,tviConfirm;
	private EditText txtAmount;
	private ListView lviPayment_list;
	private LinearLayout llStatement,llAddNewCard;

	

	public static View totalbalanceView, youOweView, youAreOweView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.mywaller_fragment, null);
		init();
		/*mTabhost = (FragmentTabHost) mRootView
				.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(),
				R.id.fragmentframe_holder);
		lviMyTabs = (ListView) mRootView.findViewById(R.id.lviMyTabs);
		feedList = new ArrayList<MyTabs_Model>();*/

		// initializeListValues();
		((SlidingActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn,"", "My Money","", 0);
		//initializeTabHost();
		// initializeRangeSeekBar();
		tviLoadWallet.setOnClickListener(this);
		tviWithdraw.setOnClickListener(this);
		tviConfirm.setOnClickListener(this);
		llStatement.setOnClickListener(this);
		llAddNewCard.setOnClickListener(this);
		
		return mRootView;
	}

	

	private void init() {
		// TODO Auto-generated method stub
		parentFragmentClass = ((BaseFragment)getParentFragment());
		tviLoadWallet=(TextView)mRootView.findViewById(R.id.tviLoadWallet);
		tviWithdraw=(TextView)mRootView.findViewById(R.id.tviWithdraw);
		tviLoadWalletUnderline=(TextView)mRootView.findViewById(R.id.tviLoadWalletUnderline);
		tviWithdrawUnderline=(TextView)mRootView.findViewById(R.id.tviWithdrawUnderline);
		tviConfirm=(TextView)mRootView.findViewById(R.id.tviConfirm);
		txtAmount=(EditText)mRootView.findViewById(R.id.txtAmount);
		lviPayment_list=(ListView)mRootView.findViewById(R.id.lviPayment_list);
		llStatement=(LinearLayout)mRootView.findViewById(R.id.llStatement);
		llAddNewCard=(LinearLayout)mRootView.findViewById(R.id.llAddNewCard);
		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
			.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
				}
		});
	}

	private void initializeTabHost() {

		totalbalanceView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);
		youOweView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);
		youAreOweView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);

		mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable totalbaltoSpan = new SpannableString("Total Balance\n"
				+ AppConstants.RASYMBOL + " 0");
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.GREEN), 13,
				totalbaltoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) totalbalanceView.findViewById(R.id.tabText))
				.setText(totalbaltoSpan);

		Spannable toreceivetoSpan = new SpannableString("To Receive\n"
				+ AppConstants.RASYMBOL + " 0");
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.GREEN), 10,
				toreceivetoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youOweView.findViewById(R.id.tabText))
				.setText(toreceivetoSpan);

		Spannable topaytoSpan = new SpannableString("To Pay\n"
				+ AppConstants.RASYMBOL + " 0");
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 5,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.RED), 6,
				topaytoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youAreOweView.findViewById(R.id.tabText))
				.setText(topaytoSpan);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_TAG)
						.setIndicator(totalbalanceView),
				ContainerProvider.Tab_MyTransactions_TotalBalacne_Container.class,
				null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_OWE_TAG)
				.setIndicator(youOweView),
				ContainerProvider.Tab_MyTransactions_YouOwe_Container.class,
				null);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_ARE_OWED_TAG)
						.setIndicator(youAreOweView),
				ContainerProvider.Tab_MyTransactions_YouAreOwed_Container.class,
				null);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {

			}
		});

		getPendingTransactionDetails();
	}

	public void getPendingTransactionDetails() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getPendingTransactionDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "iduser" }, new String[] { "feed",
						"pendingtransaction",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
		

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		tabModel = new MyTabs_Model("Aphrodit", "", "300.00", "yes", "no");
		feedList.add(tabModel);
		tabModel.setFieldValues("Athena", "", "30.00", "no", "yes");
		feedList.add(tabModel);
		tabModel.setFieldValues("Hera", "", "50.00", "yes", "no");
		feedList.add(tabModel);
		tabModel.setFieldValues("Jeus", "", "150.00", "no", "yes");
		feedList.add(tabModel);
		myTabsAdapter = new MyTabsAdapter_Onl(getActivity(), feedList);

		lviMyTabs.setAdapter(myTabsAdapter);
		if (from.equalsIgnoreCase("getPendingTransactionDetails")) {
			if (output != null) {

				try {
					Gson gson = new Gson();
					pendingNewClass = gson.fromJson(output,
							Pending_New_Class.class);
					if (pendingNewClass.result) {

						/*
						 * totalfglist =
						 * pendingNewClass.total_bal.getTotalBalValues();
						 * youowefglist =
						 * pendingNewClass.you_owe.getYouAreOweValues();
						 * youareowfglist =
						 * pendingNewClass.you_are_owe.getYouAreOweValues();
						 */

						if (pendingNewClass.total_bal.getTotalBalValues() == null)
							Log.e("NULL88888888888888",
									"**********************");

						HomeActivity.total_friendadapter
								.notifyWithDataSet(pendingNewClass.total_bal
										.getTotalBalValues());
						HomeActivity.youowe_friendadapter
								.notifyWithDataSet(pendingNewClass.you_owe
										.getYouAreOweValues());
						HomeActivity.youareowed_friendadapter
								.notifyWithDataSet(pendingNewClass.you_are_owe
										.getYouAreOweValues());

						((TextView) totalbalanceView.findViewById(R.id.tabText))
								.setText("Total Balance\n"
										+ AppConstants.RASYMBOL + " "
										+ pendingNewClass.total_bal.total);
						((TextView) youOweView.findViewById(R.id.tabText))
								.setText("You Owe\n" + AppConstants.RASYMBOL
										+ " " + pendingNewClass.you_owe.total);
						((TextView) youAreOweView.findViewById(R.id.tabText))
								.setText("You Are Owed\n"
										+ AppConstants.RASYMBOL + " "
										+ pendingNewClass.you_are_owe.total);

						/*
						 * MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter
						 * (youowe_friendadapter);
						 * MyTransaction_Home_YouAreOwed_Fragment
						 * .generalView.setAdapter(youareowed_friendadapter);
						 * MyTransaction_Home_Total_Fragment
						 * .generalView.setAdapter(total_friendadapter);
						 */

						MyTransaction_Home_YouOwe_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_YouOwe_Fragment.generalView
								.expandGroup(1);
						MyTransaction_Home_YouAreOwed_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_YouAreOwed_Fragment.generalView
								.expandGroup(1);
						MyTransaction_Home_Total_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_Total_Fragment.generalView
								.expandGroup(1);

						

					} else {
						Toast.makeText(getActivity(), "No Transactions Found!",
								Toast.LENGTH_SHORT).show();
					}
					// new storeTransactionDetailsInDb().execute();

				} catch (Exception e) {
					e.printStackTrace();
					// DialogManager.showDialog(getActivity(),
					// "Server Error Occured! Try Again!");
				}

			} else {
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");
			}

		}

	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tviLoadWallet:
			tviLoadWalletUnderline.setVisibility(View.VISIBLE);
			tviWithdrawUnderline.setVisibility(View.INVISIBLE);
			break;
		case R.id.tviWithdraw:
			tviWithdrawUnderline.setVisibility(View.VISIBLE);
			tviLoadWalletUnderline.setVisibility(View.INVISIBLE);
			break;
		case R.id.tviConfirm:
	
			break;
		case R.id.llStatement:
			parentFragmentClass.replaceFragment(new StatementFragment(), true);
			break;
		case R.id.llAddNewCard:
			parentFragmentClass.replaceFragment(new AddCard_Fragment(), true);
			break;

		default:
			break;
		}
	}

}
