package com.bash.Fragments;

import java.util.ArrayList;

import org.lucasr.twowayview.TwoWayView;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Adapters.TwoWayViewPhotosAdapter;
import com.bash.CustomViews.CircularImageView;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Utils.AppConstants;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;


public class AddTabPayFragment extends Fragment {

	TwoWayView horizontalViewPhotos;
	public ArrayList<PhotosListModel> photosList;
	public TwoWayViewPhotosAdapter horizontalPhotoAdapter;
	
	LinearLayout moretwouserlinear, twouserlinear;
	ImageView arrowpayid, arrowreceiveid;
	CircularImageView receiveuserimageView, paiduserimageView;
	Button btnSave, btnSavePay;
	TextView paidusernametext, receiveusernametext;
	View mRootView;
	private int fragTag;
	
	public static AddTabPayFragment AddTabPayFragmentObj=null;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.addtab_pay_fragment_, null);
		
		AddTabPayFragmentObj = AddTabPayFragment.this;
	
		String fragTagstr;

		if (AddTabActivity.mTabhost.getCurrentTabTag() != null) {

			fragTagstr = AddTabActivity.mTabhost.getCurrentTabTag().toString();

			if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_PAY_TAG))) {
				fragTag = 1;
			} else if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_SPLIT_TAG))) {
				fragTag = 2;
			} else if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_RECEIVE_TAG))) {
				fragTag = 3;
			}
		}

		
		moretwouserlinear = (LinearLayout) mRootView
				.findViewById(R.id.moretwouserlinear);
		twouserlinear = (LinearLayout) mRootView
				.findViewById(R.id.twouserlinear);
		arrowpayid = (ImageView) mRootView.findViewById(R.id.arrowpayid);
		arrowreceiveid = (ImageView) mRootView
				.findViewById(R.id.arrowreceiveid);
		
		receiveuserimageView = (CircularImageView) mRootView
				.findViewById(R.id.receiveuserimageView);
		paiduserimageView = (CircularImageView) mRootView
				.findViewById(R.id.paiduserimageView);
		paidusernametext = (TextView) mRootView
				.findViewById(R.id.paidusernametext);
		receiveusernametext = (TextView) mRootView
				.findViewById(R.id.receiveusernametext);
		
		
		intializeUI();

		return mRootView;
	}

	private void intializeUI() {
		
		if (photosList == null) {
			photosList = new ArrayList<PhotosListModel>();
		}	
		horizontalViewPhotos = (TwoWayView) mRootView
				.findViewById(R.id.horizontalvcrollviewphoto);

		horizontalPhotoAdapter = new TwoWayViewPhotosAdapter(getActivity(),
				R.layout.item_twowayhorizontal_layout, photosList);
		horizontalViewPhotos.setAdapter(horizontalPhotoAdapter);

		if (fragTag == 1) {
		
			twouserlinear.setVisibility(View.VISIBLE);
			moretwouserlinear.setVisibility(View.GONE);
			arrowpayid.setVisibility(View.VISIBLE);
			arrowreceiveid.setVisibility(View.GONE);
			
			ArrayList<Person> addedPersonList = AddTabActivity.AddTabActivityObj.addedPersonList;
			if(addedPersonList.size() > 0){
				showSinglePayLayout(addedPersonList.size());
			}
		} else if (fragTag == 2) {
			
			twouserlinear.setVisibility(View.GONE);
			moretwouserlinear.setVisibility(View.VISIBLE);	
			
			ArrayList<Person> addedPersonList = AddTabActivity.AddTabActivityObj.addedPersonList;
			if(addedPersonList.size() > 0){
				showSplitPayLayout();
			}
			
			
			horizontalPhotoAdapter.notifyDataSetChanged();
		} else if (fragTag == 3) {

			twouserlinear.setVisibility(View.VISIBLE);
			moretwouserlinear.setVisibility(View.GONE);
			arrowpayid.setVisibility(View.GONE);
			arrowreceiveid.setVisibility(View.VISIBLE);
			
			ArrayList<Person> addedPersonList = AddTabActivity.AddTabActivityObj.addedPersonList;
			if(addedPersonList.size() > 0){
				showSinglePayLayout(addedPersonList.size());
			}
		}
	}

	public void showSinglePayLayout(int size) {
		 ArrayList<Person> addedPersonList = AddTabActivity.AddTabActivityObj.addedPersonList;
		
		if ((addedPersonList.get(0).getImage_location() != null)
				&& (addedPersonList.get(0).getImage_location().length() > 0)) {
			UrlImageViewHelper.setUrlDrawable(paiduserimageView,
					addedPersonList.get(0).getImage_location());
		}
		if ((addedPersonList.get(0).getName() != null)
				&& (addedPersonList.get(0).getName().length() > 0)) {
			paidusernametext.setText(addedPersonList.get(0).getName());
		}

		if (size == 2) {
			if (fragTag == 3)
				AddTabActivity.AddTabActivityObj.paidby.setText(addedPersonList.get(1).getName());
			if (fragTag == 1)
			AddTabActivity.AddTabActivityObj.paidby.setText(addedPersonList.get(0).getName());

			if ((addedPersonList.get(1).getImage_location() != null)
					&& (addedPersonList.get(1).getImage_location().length() > 0)) {
				UrlImageViewHelper.setUrlDrawable(receiveuserimageView,
						addedPersonList.get(1).getImage_location());
			}
			if ((addedPersonList.get(1).getName() != null)
					&& (addedPersonList.get(1).getName().length() > 0)) {
				receiveusernametext.setText(addedPersonList.get(1).getName());
				
			} else {
			}
		}

	}
public void showSplitPayLayout(){
	
	photosList.clear();
	
	ArrayList<Person> addedPersonList = AddTabActivity.AddTabActivityObj.addedPersonList;

	for (Person person : addedPersonList) {
		boolean contains = false;
		for (PhotosListModel photosListModel : photosList) {
			if (photosListModel.getID().equals(person.getId())) {

				contains = true;
			}
		}
		if (!contains) {

			photosList
					.add(new PhotosListModel(person.getUserType(),
							person.getId(), person
									.getImage_location(), person
									.getName()));
		}
	}
	horizontalPhotoAdapter.notifyDataSetChanged();	
}

}
