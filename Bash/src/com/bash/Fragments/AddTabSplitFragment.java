package com.bash.Fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabSplitActivity;
import com.bash.Activities.ItemWiseSplitActivity;
import com.bash.Activities.SocialPageActivity;
import com.bash.Adapters.SplitTabAdapters;
import com.bash.Application.BashApplication;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;

public class AddTabSplitFragment extends Fragment implements AsyncResponse {

	LinearLayout ratioamountlinear, equalamountlinear, unequalamountlinear,
			percentamountlinear;
	ListView spittablistview;
	TextView totalpercent, totalunequalamount, equaltotalamount,
			ratiototalamount;
	public static TextView pendingpercent, totalsplitpercent,
			pendingunequalamount, totalsplitunequalamount;
	Button btnSave, btnSavePay;
	private AlertDialog continuedialog = null;

	private String categorySelect = "";
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "";
	private ArrayList<Person> addedPersonArrayList;
	private ArrayList<PhotosListModel> photoListArrayList;

	SplitTabAdapters splitTabAdapters;

	ArrayList<HashMap<String, String>> paidUserInfo;
	ArrayList<HashMap<String, String>> receiveUserInfo;
	
	JSONArray jsonArrayPayer = new JSONArray();
	JSONArray jsonArrayReceive = new JSONArray();

	MyAsynTaskManager myAsyncTask = null;

	View mRootView;
	private int fragTag;

	String userId = "";
	
	Bundle fragbundle = null;
	private int transId;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.addsplit_tab_fragment, null);

		fragbundle = getArguments();
		String tabTag = fragbundle.getString(AppConstants.FRAGMENT_TAG, AppConstants.TAB_PAY_TAG);

		currencyCodeString = fragbundle.getString("CurrencyCodeString");
		categorySelect = fragbundle.getString("CategorySelect");
		dateString = fragbundle.getString("DateString");
		transationTitle = fragbundle.getString("TransationTitle");
		transationAnount = fragbundle.getString("TransationAnount");
		addedPersonArrayList = fragbundle.getParcelableArrayList("PaidUserDetails");
		photoListArrayList = fragbundle.getParcelableArrayList("ReceiveUserDetails");

		if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_RATIO_TAG))) {
			fragTag = 1;
		} else if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_UNEQUAL_TAG))) {
			fragTag = 2;
		} else if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_EQUAL_TAG))) {
			fragTag = 3;
		} else if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_PERCENT_TAG))) {
			fragTag = 4;
		} else if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_ITEMWISE_TAG))) {
			fragTag = 5;
		}

		ratioamountlinear = (LinearLayout) mRootView.findViewById(R.id.ratioamountlinear);
		equalamountlinear = (LinearLayout) mRootView.findViewById(R.id.equalamountlinear);
		unequalamountlinear = (LinearLayout) mRootView.findViewById(R.id.unequalamountlinear);
		percentamountlinear = (LinearLayout) mRootView.findViewById(R.id.percentamountlinear);

		pendingpercent = (TextView) mRootView.findViewById(R.id.pendingpercent);
		totalpercent = (TextView) mRootView.findViewById(R.id.totalpercent);
		totalsplitpercent = (TextView) mRootView.findViewById(R.id.totalsplitpercent);
		pendingunequalamount = (TextView) mRootView.findViewById(R.id.pendingunequalamount);
		totalunequalamount = (TextView) mRootView.findViewById(R.id.totalunequalamount);
		totalsplitunequalamount = (TextView) mRootView.findViewById(R.id.totalsplitunequalamount);
		equaltotalamount = (TextView) mRootView.findViewById(R.id.equaltotalamount);
		ratiototalamount = (TextView) mRootView.findViewById(R.id.ratiototalamount);

		spittablistview = (ListView) mRootView.findViewById(R.id.spittablistview);

		userId = PreferenceManager.getInstance().getUserId();

		intializeUI();

		return mRootView;
	}

	private void intializeUI() {
// Sarvesh Remove for two person
		/*if ((photoListArrayList.size() == 2) && (fragTag == 3)){

			AddTabSplitActivity.tvContinue.setText("Continue");

		} else if (photoListArrayList.size() > 2) {

			AddTabSplitActivity.tvContinue.setText("Save");
		}*/
		AddTabSplitActivity.tvContinue.setText("Save");
		
		if (fragTag == 1) {
			ratioamountlinear.setVisibility(View.VISIBLE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.GONE);

			ratiototalamount.setText(transationAnount);

			findRatioEqualAmount();
			SetListAdapter();

		} else if (fragTag == 2) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.VISIBLE);
			percentamountlinear.setVisibility(View.GONE);

			totalunequalamount.setText(transationAnount);
			totalsplitunequalamount.setText(transationAnount);
			pendingunequalamount.setText("0");

			findEqualAmount();
			SetListAdapter();

		} else if (fragTag == 3) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.VISIBLE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.GONE);

			equaltotalamount.setText(transationAnount);

			findEqualAmount();
			SetListAdapter();

		} else if (fragTag == 4) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.VISIBLE);

			totalsplitpercent.setText("100");
			pendingpercent.setText("0");

			findPercentAmount();
			SetListAdapter();

		}

		AddTabSplitActivity.tvContinue.setOnClickListener(clickListener);
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			
			
			case R.id.continuesavebtn:

				sendPayTransationDetails();

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();

				break;
			case R.id.continuesavepaybtn:
				
				sendPayTransationDetails();

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();
				
				
				
				break;

			case R.id.toprightsidecontinuetext:

				/*if ((AddTabSplitActivity.tvContinue.getText().toString().trim()).equals("Continue")) {

				if (fragTag == 3) {

					showContinueAlertDialog();
					
					}

				} else */
					if ((AddTabSplitActivity.tvContinue.getText().toString()
						.trim()).equals("Save")) {

						if (fragTag == 1) {
							if (Math.round(splitTabAdapters.findTotalPercent()) == Float.parseFloat(transationAnount)) {
								setRatioPayerReceiverDetails();
								sendRatioSplitDetails();
							}else  {
								Toast.makeText(getActivity(), "Please check entered amounts", Toast.LENGTH_SHORT).show();
							}
						} else if (fragTag == 2) {
							if (Math.round(splitTabAdapters.findTotalAmount()) == Float.parseFloat(transationAnount)) {
								setEqualPayerReceiverDetails();
								sendUnEqualSplitDetails();
							} else {
								Toast.makeText(getActivity(), "Please check entered amounts", Toast.LENGTH_SHORT).show();
							}
						} else if (fragTag == 3) {
							if (Math.round(splitTabAdapters.findTotalAmount()) == Float.parseFloat(transationAnount)) {
							setEqualPayerReceiverDetails();
							sendEqualSplitDetails();
							}else {
								Toast.makeText(getActivity(), "Please check entered amounts", Toast.LENGTH_SHORT).show();
							}
						} else if (fragTag == 4) {
							if (Math.round(splitTabAdapters.findTotalPercent()) == Float.parseFloat(transationAnount)) {
								setPerentEqualPayerReceiverDetails();
								sendPercentSplitDetails();
							}else  {
								Toast.makeText(getActivity(), "Please check entered amounts", Toast.LENGTH_SHORT).show();
							}
						} 
					}

				break;

			}
		}
	};

	private void findEqualAmount() {
		int splitcount = 0;
		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				splitcount += 1;
			}
		}
		Float equalAmount = ((Float.parseFloat(transationAnount)) / (splitcount));

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(equalAmount);
			}
		}
	}

	private void findRatioEqualAmount() {

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(1.0f);
			}
		}
	}

	private void findPercentAmount() {
		int splitcount = 0;
		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				splitcount += 1;
			}
		}
		Float equalAmount = (((float) 100) / (splitcount));

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(Float.parseFloat(String.format( "%.2f", equalAmount)));
			}
		}
	}

	private void SetListAdapter() {

		splitTabAdapters = new SplitTabAdapters(getActivity(), R.layout.item_splittab_layout, photoListArrayList, 
				fragTag, Float.parseFloat(transationAnount));

		spittablistview.setAdapter(splitTabAdapters);
	}

	/*private void setUnEqualPayerReceiverDetails() {
		jsonArrayPayer = new JSONArray();
		jsonArrayReceive = new JSONArray();
		
		JSONObject jsonObject1 = null;
		JSONObject jsonObject2 = null;
		
		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			
			jsonObject1 = new JSONObject();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}
				try {
					jsonObject1.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
					jsonObject1.put("idpayer", person.getId());
					jsonObject1.put("paid_amount", "" + paidAmt);
					jsonObject1.put("had_to_pay", "" + (int) receiveAmt);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				jsonArrayPayer.put(jsonObject1);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			
			jsonObject2 = new JSONObject();
			try {
				jsonObject2.put("user_type",BashApplication.userTypeHash.get(photoListObj.getUserType()));
				jsonObject2.put("idreceiver", photoListObj.getID());
				jsonObject2.put("had_to_pay",(int) photoListObj.getReceiveamount());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonArrayReceive.put(jsonObject2);


		}

	}*/
	
	private void setEqualPayerReceiverDetails() {
		jsonArrayPayer = new JSONArray();
		jsonArrayReceive = new JSONArray();
		
		JSONObject jsonObject1 = null;
		JSONObject jsonObject2 = null;
		
		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			
			jsonObject1 = new JSONObject();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}
				try {
					jsonObject1.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
					jsonObject1.put("idpayer", person.getId());
					jsonObject1.put("paid_amount", "" + paidAmt);
					jsonObject1.put("had_to_pay", "" + (int) receiveAmt);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				jsonArrayPayer.put(jsonObject1);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			
			jsonObject2 = new JSONObject();
			try {
				jsonObject2.put("user_type",BashApplication.userTypeHash.get(photoListObj.getUserType()));
				jsonObject2.put("idreceiver", photoListObj.getID());
				jsonObject2.put("had_to_pay",(int) photoListObj.getReceiveamount());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonArrayReceive.put(jsonObject2);


		}

	}

	private void setRatioPayerReceiverDetails() {

		jsonArrayPayer = new JSONArray();
		jsonArrayReceive = new JSONArray();
		
		JSONObject jsonObject1 = null;
		JSONObject jsonObject2 = null;

		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			jsonObject1 = new JSONObject();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}
				try {
					jsonObject1.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
					jsonObject1.put("idpayer", person.getId());
					jsonObject1.put("paid_amount", "" + paidAmt);
					jsonObject1.put("had_to_pay_ratio", "" + (int) receiveAmt);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				jsonArrayPayer.put(jsonObject1);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			jsonObject2 = new JSONObject();
			try {
				jsonObject2.put("user_type",BashApplication.userTypeHash.get(photoListObj.getUserType()));
				jsonObject2.put("idreceiver", photoListObj.getID());
				jsonObject2.put("had_to_pay_ratio",(int) photoListObj.getReceiveamount());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonArrayReceive.put(jsonObject2);
		}

	}
	private void setPerentEqualPayerReceiverDetails() {
		jsonArrayPayer = new JSONArray();
		jsonArrayReceive = new JSONArray();
		
		JSONObject jsonObject1 = null;
		JSONObject jsonObject2 = null;
		

		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			jsonObject1 = new JSONObject();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}
				
				try {
					jsonObject1.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
					jsonObject1.put("idpayer", person.getId());
					jsonObject1.put("paid_amount", "" + paidAmt);
					jsonObject1.put("had_to_pay_percent", "" + (int) receiveAmt);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				jsonArrayPayer.put(jsonObject1);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			jsonObject2 = new JSONObject();
			
			try {
				jsonObject2.put("user_type",BashApplication.userTypeHash.get(photoListObj.getUserType()));
				jsonObject2.put("idreceiver", photoListObj.getID());
				jsonObject2.put("had_to_pay_percent",(int) photoListObj.getReceiveamount());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonArrayReceive.put(jsonObject2);

		}

	} 

	private void sendEqualSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("EqualSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertEqualSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayer.toString(), jsonArrayReceive.toString() });
		myAsyncTask.execute();

	}

	private void sendUnEqualSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("UnEqualSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertUnequalSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayer.toString(), jsonArrayReceive.toString() });
		myAsyncTask.execute();
		
	}

	private void sendRatioSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("RatioSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertRatioSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayer.toString(), jsonArrayReceive.toString() });
		myAsyncTask.execute();

	}
	private void sendPercentSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("PercentSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertPercentSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayer.toString(), jsonArrayReceive.toString() });
		myAsyncTask.execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub

		if (from.equalsIgnoreCase("EqualSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				
				try {
					JSONObject jsonObject = new JSONObject(output);	
					boolean result = jsonObject.getBoolean("result");
					if(result){
						transId = Integer.parseInt(jsonObject.getString("idtrans"));
						startSocialPage();
					}	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("UnEqualSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				
				try {
					JSONObject jsonObject = new JSONObject(output);	
					boolean result = jsonObject.getBoolean("result");
					
					if(result){
						transId = Integer.parseInt(jsonObject.getString("idtrans"));
						startSocialPage();
					}	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("RatioSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				
				try {
					JSONObject jsonObject = new JSONObject(output);	
					boolean result = jsonObject.getBoolean("result");
					
					if(result){
						transId = Integer.parseInt(jsonObject.getString("idtrans"));
						startSocialPage();
					}	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("PercentSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				
				try {
					JSONObject jsonObject = new JSONObject(output);	
					boolean result = jsonObject.getBoolean("result");
					if(result){
						transId = Integer.parseInt(jsonObject.getString("idtrans"));
						startSocialPage();
					}	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("PayTransationDetails")) {
			if (output != null && output.length() > 0) {
				try {
					JSONObject jsonObject = new JSONObject(output);	
					boolean result = jsonObject.getBoolean("result");
					
					if(result){
						transId = Integer.parseInt(jsonObject.getString("idtrans"));
						startSocialPage();
					}	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}

	}
	private void startSocialPage(){
		
		Intent socialIntent = new Intent(getActivity(), SocialPageActivity.class);
		socialIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		socialIntent.putExtra("transationdetails", fragbundle);
		socialIntent.putExtra("transID", transId);
		socialIntent.putExtra("postType", "pay");
		startActivity(socialIntent);
	}
	protected void showContinueAlertDialog() {
		// TODO Auto-generated method stub

		Context context = getActivity();

		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.continue_save_dialog, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		continuedialog = alertDialogBuilder.create();
		continuedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = continuedialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.TOP | Gravity.RIGHT;
		wmlp.x = 10; // x position
		wmlp.y = 10; // y position
		continuedialog.show();
		btnSave = (Button) view.findViewById(R.id.continuesavebtn);
		btnSavePay = (Button) view.findViewById(R.id.continuesavepaybtn);
		btnSavePay.setVisibility(View.VISIBLE);

		btnSave.setOnClickListener(clickListener);
		btnSavePay.setOnClickListener(clickListener);

	}
	private void sendPayTransationDetails() {

			myAsyncTask = new MyAsynTaskManager();
			myAsyncTask.delegate = this;
			myAsyncTask.setupParamsAndUrl("PayTransationDetails",
					getActivity(), AppUrlList.ACTION_URL, new String[] {
							"module", "action", "payer_iduser",
							"receiver_user_type", "receiver_user_id",
							"transaction_currency", "transaction_amount",
							"transaction_category", "transaction_date",
							"transaction_title" }, new String[] {
							"transaction", "insertPayTransaction", userId,
							""+BashApplication.userTypeHash.get(addedPersonArrayList.get(1).getUserType()), addedPersonArrayList.get(1).getId(),
							currencyCodeString, transationAnount,
							"" + categorySelect, dateString, transationTitle });
			myAsyncTask.execute();
		} 
}
