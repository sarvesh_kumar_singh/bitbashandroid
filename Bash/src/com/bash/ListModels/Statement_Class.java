package com.bash.ListModels;

public class Statement_Class {

	public String dateText, transactionTypeText, amountText;

	public Statement_Class(String dateText, String transactionTypeText,
			String amountText) {
		super();
		this.dateText = dateText;
		this.transactionTypeText = transactionTypeText;
		this.amountText = amountText;
	}

	public String getDateText() {
		return dateText;
	}

	public void setDateText(String dateText) {
		this.dateText = dateText;
	}

	public String getTransactionTypeText() {
		return transactionTypeText;
	}

	public void setTransactionTypeText(String transactionTypeText) {
		this.transactionTypeText = transactionTypeText;
	}

	public String getAmountText() {
		return amountText;
	}

	public void setAmountText(String amountText) {
		this.amountText = amountText;
	}
	
	 
}

