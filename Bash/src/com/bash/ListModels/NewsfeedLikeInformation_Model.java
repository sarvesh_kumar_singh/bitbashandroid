package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedLikeInformation_Model {
	@SerializedName("idlike")
	public String idlike;
	@SerializedName("idpost")
	public String idpost;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("fullname")
	public String fullname;

	public NewsfeedLikeInformation_Model(String idlike, String idpost,
			String iduser, String fullname) {
		this.idlike = idlike;
		this.idpost = idpost;
		this.iduser = iduser;
		this.fullname = fullname;
	}

	public String getIdlike() {
		return idlike;
	}

	public String getIdpost() {
		return idpost;
	}

	public String getIduser() {
		return iduser;
	}

	public String getFullname() {
		return fullname;
	}
}
