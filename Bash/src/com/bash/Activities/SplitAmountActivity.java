package com.bash.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bash.R;

public class SplitAmountActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_split_amount_page);
		initializeViews();
	}
	
	private void initializeViews() {
		// TODO Auto-generated method stub
		((ImageView) findViewById(R.id.topleftsideImage)).setImageResource(R.drawable.back_btn);
		((ImageView) findViewById(R.id.toprightsideImage)).setBackgroundResource(0);
		
		((ImageView) findViewById(R.id.topleftsideImage)).setOnClickListener(this);
		((RelativeLayout) findViewById(R.id.doneButton)).setOnClickListener(this);
		((RelativeLayout) findViewById(R.id.cancelButton)).setOnClickListener(this);
		/*
		((ImageView) findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});*/
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		finish();
	}
}
