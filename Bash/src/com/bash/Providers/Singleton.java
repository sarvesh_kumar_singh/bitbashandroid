package com.bash.Providers;

import java.util.ArrayList;

import com.bash.ListModels.ContactList_Model;
import com.bash.ListModels.GroupTransactionDetail_Model;

public class Singleton {
	private static Singleton instance;

	public static boolean refresh;
	public static GroupTransactionDetail_Model listItem;
	public static ArrayList<ContactList_Model> contactList;

	public static void initInstance() {
		if (instance == null) {
			// Create the instance
			instance = new Singleton();
		}
	}

	public static Singleton getInstance() {
		// Return the instance
		return instance;
	}
}
