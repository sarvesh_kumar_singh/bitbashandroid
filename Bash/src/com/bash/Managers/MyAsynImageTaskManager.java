package com.bash.Managers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.util.Log;

public class MyAsynImageTaskManager extends AsyncTask<String, String, String>{

	static LoadListener loadListener;
	public ProgressDialog progressDialog;
	public static MultipartEntity params = new MultipartEntity();
 	public static String posturl;
	Activity myActivity;
	
	public MyAsynImageTaskManager(Activity myActivity, LoadListener loadListener)
	{
		this.myActivity = myActivity;
		this.loadListener = loadListener;
	}
	
	public static void setupParamsAndUrl(String url, String[] keywords, String[] values, String[] isImage)
	{
		try {
			posturl = url;
			MultipartEntity entity = new MultipartEntity();
			for(int i = 0; i < keywords.length; i++)
			{
				if(isImage[i].equals("0")){
					entity.addPart(keywords[i], new StringBody(values[i]));
				}
				else{
					if(values[i] != null && values[i].length() != 0)
						entity.addPart(keywords[i], new FileBody(new File(values[i])));
					else
						entity.addPart(keywords[i], new StringBody(""));
				}
			}
			params = entity;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void setupParamsAndUrl(String url, MultipartEntity passingParam){
		posturl = url;
		params = passingParam;
	}
	
	private void cancelAsynTask(){
		MyAsynImageTaskManager.this.cancel(true);
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		progressDialog = new ProgressDialog(myActivity);
		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(false);
		progressDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				// TODO Auto-generated method stub
				Log.e("Progress Dialog Closed!", "Manually!");
				cancelAsynTask();
			}
		});
		progressDialog.show();
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progressDialog.dismiss();
		if(result != null)
			loadListener.onLoadComplete(result);
	}
	
	@Override
	protected String doInBackground(String... result) {
		System.gc();
		return getResponseFromServer();
	}

	
	public String getResponseFromServer()
	{
			try 
			{
		    HttpClient client = new DefaultHttpClient();
		    HttpPost post = new HttpPost(posturl);
		    Log.e("Post Url ", posturl);
		    HttpParams paramss = client.getParams();
			HttpConnectionParams.setConnectionTimeout(paramss, 20000);
			HttpConnectionParams.setSoTimeout(paramss, 20000);
			post.setEntity(params);
	        HttpResponse httpResponse = client.execute(post);
	        HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();
            if(is == null)
            	{
            	progressDialog.dismiss();
            	 return null;
            	}
            	else
            	{
		            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		            StringBuilder sb = new StringBuilder();
		            String line = null; 
		            
		        	while ((line = reader.readLine()) != null) 
		            {
		                sb.append(line + "\n");
		            }
		            
		            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is));
		            StringBuilder stringBuffer = new StringBuilder();
		            String singleLine = null;

		            while ((singleLine = reader.readLine()) != null) {
		            	stringBuffer.append(line);
		            }
		            is.close();
		            Log.e("Reponse : ", sb.toString());
		            return sb.toString();
            	}
			}
           catch(SocketTimeoutException e)
			{
            	e.printStackTrace();
            	progressDialog.dismiss();
				loadListener.onError("Connection Time Out! Try Again!");
			}
            catch(IOException e)
            { 
            	e.printStackTrace();
            	progressDialog.dismiss();
            	loadListener.onError("Server Error Occured! Try Again!");
            }
		return null;
	}
	
	abstract public static class LoadListener{
		abstract public void onLoadComplete(String jsonResponse);
		abstract public void onError(String errorMessage);
	}
	
}


