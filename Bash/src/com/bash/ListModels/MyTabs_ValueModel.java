package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class MyTabs_ValueModel implements Comparable{
	@SerializedName("full_name")
	public String full_name;
	@SerializedName("date")
	public String date;
	@SerializedName("datetime")
	public String datetime;
	@SerializedName("timestamp")
	public String timestamp;
	@SerializedName("type")
	public String type;
	@SerializedName("user_type")
	public String user_type;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("image")
	public String image;
	@SerializedName("amount")
	public String amount;

	public String getFull_name() {
		return this.full_name;
	}

	public String getDate() {
		return this.date;
	}
	
	public String getDatetime() {
		return this.datetime;
	}
	
	public String getTimestamp() {
		return this.timestamp;
	}

	public String getType() {
		return this.type;
	}

	public String getUser_type() {
		return this.user_type;
	}

	public String getIduser() {
		return this.iduser;
	}

	public String getAmount() {
		return this.amount;
	}

	public String getImage() {
		return this.image;
	}

	public MyTabs_ValueModel(String full_name, String date, String datetime, String timestamp, String user_type, String iduser,
			String amount, String image, String type) {
		this.full_name = full_name;
		this.date=date;
		this.datetime=datetime;
		this.timestamp=timestamp;
		this.user_type = user_type;
		this.iduser = iduser;
		this.amount = amount;
		this.image = image;
		this.type = type;

	}

	@Override
	public int compareTo(Object another) {
		// TODO Auto-generated method stub
		int compareage=Integer.parseInt(((MyTabs_ValueModel)another).getAmount());
        /* For Ascending order*/
        //return Integer.parseInt(this.amount)-compareage;
        /* For Descending order do like this */
        return compareage-Integer.parseInt(this.amount);
	}
}
