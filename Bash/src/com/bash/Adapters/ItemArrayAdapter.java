package com.bash.Adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.CurrencyModel;

public class ItemArrayAdapter extends ArrayAdapter<CurrencyModel> {
	private ArrayList<CurrencyModel> scoreList ;
	private List<CurrencyModel> countrylist = null;

    static class ItemViewHolder {
        TextView currencyCode;
        TextView currencyText;
    }

    public ItemArrayAdapter(Context context, int textViewResourceId, List<CurrencyModel> scoreList) {
        super(context, textViewResourceId);
        this.countrylist = scoreList;
        this.scoreList = new ArrayList<CurrencyModel>();
        this.scoreList.addAll(countrylist);
    }


    @Override
	public int getCount() {
		return this.countrylist.size();
	}

    @Override
	public CurrencyModel getItem(int index) {
		return countrylist.get(index);
	}

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        ItemViewHolder viewHolder;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.custom_currency_listview, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.currencyCode = (TextView) row.findViewById(R.id.currencycode);
            viewHolder.currencyText = (TextView) row.findViewById(R.id.currencytext);
            row.setTag(viewHolder);
		} else {
            viewHolder = (ItemViewHolder)row.getTag();
        }
        CurrencyModel currencyObj = getItem(position);
        viewHolder.currencyCode.setText(currencyObj.getCurrencyCode() + " - ");
        viewHolder.currencyText.setText(currencyObj.getCurrencyText());
		return row;
	}
    
 // Filter Class
 	public void filter(String charText) {
 		charText = charText.toLowerCase(Locale.getDefault());
 		countrylist.clear();
 		if (charText.length() == 0) {
 			countrylist.addAll(scoreList);
 		} 
 		else 
 		{
 			for (CurrencyModel wp : scoreList) 
 			{
 				if (wp.getCurrencyText().toLowerCase(Locale.getDefault()).contains(charText)) 
 				{
 					countrylist.add(wp);
 				}
 			}
 		}
 		notifyDataSetChanged();
 	}
}