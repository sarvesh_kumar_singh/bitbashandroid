package com.bash.Activities;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.SlidingMenuAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.ContactList_Model;
import com.bash.ListModels.SlidingMenu_Class;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Providers.Singleton;
import com.bash.Utils.AppConstants;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SlidingActivity extends FragmentActivity {
	
	public FragmentTabHost fragmenthost;
	public DrawerLayout dLayout;
	private ActionBarDrawerToggle mDrawerToggle;

	public SlidingMenuAdapter menuAdapter;
	public ArrayList<SlidingMenu_Class> menuList = new ArrayList<SlidingMenu_Class>();
	ListView dList;
	Button button;
	ImageView topleftsideImage;
	public static Fragment currentFragment;
	private ImageView iviProfileImage;
	private TextView tviUserName, tviUserAmount;
	
	public String groupId, groupName, groupAdmin, groupImage;
	
	public ContentResolver contentResolver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home_drawer);
		
		//PreferenceManager.getInstance().setUserId("50");
		
		topleftsideImage = (ImageView) findViewById(R.id.topleftsideImage);
		dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		dList = (ListView) findViewById(R.id.slidelistView);
		iviProfileImage = (ImageView) findViewById(R.id.iviProfileImage);
		tviUserName = (TextView) findViewById(R.id.tviUserName);
		tviUserAmount = (TextView) findViewById(R.id.tviUserAmount);
		contentResolver = getContentResolver();
		
		initializeFragmentTabHost();
		initializedSlidingMenuDatas();
		
		mDrawerToggle = new ActionBarDrawerToggle(this, dLayout,
				R.drawable.menu_btn, R.string.drawer_open, R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				//getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				//getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};
		// Set the drawer toggle as the DrawerListener
		dLayout.setDrawerListener(mDrawerToggle);

		fragmenthost = (FragmentTabHost) findViewById(R.id.tabhost);

		fragmenthost.setup(this, getSupportFragmentManager(),
				R.id.content_frame);

		fragmenthost.addTab(
				fragmenthost.newTabSpec(AppConstants.HOME_FRAGMENT_TAG)
						.setIndicator(""),
				ContainerProvider.Home_Container.class, null);
		
		
		//dList.setSelector(android.R.color.holo_blue_dark);
		
		

		topleftsideImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(SlidingActivity.this, "Menu Clicked", Toast.LENGTH_LONG).show();
				dLayout.openDrawer(Gravity.LEFT);
			}
		});

		iviProfileImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * fragmenthost
				 * .setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG
				 * );
				 */
				// getSlidingMenu().toggle();
				//slidingmenu_layout.toggleMenu();
				dLayout.closeDrawers();
				Intent profileIntent = new Intent(SlidingActivity.this, MyProfileActivity.class);
				profileIntent.putExtra("idUser", PreferenceManager.getInstance().getUserId());
				profileIntent.putExtra("Profilename", PreferenceManager.getInstance().getUser_FullName());
				profileIntent.putExtra("image", PreferenceManager.getInstance().getUserImagePath());
				profileIntent.putExtra("amount", "");
				profileIntent.putExtra("type", "");
				profileIntent.putExtra("usertype", "Bash");
				startActivity(profileIntent);
			}
		});

		((TextView) findViewById(R.id.tviUserName)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * fragmenthost
				 * .setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG
				 * );
				 */
				// getSlidingMenu().toggle();
				//slidingmenu_layout.toggleMenu();
				dLayout.closeDrawers();
				Intent profileIntent = new Intent(SlidingActivity.this, MyProfileActivity.class);
				profileIntent.putExtra("idUser", PreferenceManager.getInstance().getUserId());
				profileIntent.putExtra("Profilename",  PreferenceManager.getInstance().getUser_FullName());
				profileIntent.putExtra("image", PreferenceManager.getInstance().getUserImagePath());
				profileIntent.putExtra("amount", "");
				profileIntent.putExtra("type", "");
				profileIntent.putExtra("usertype", "0");
				startActivity(profileIntent);
			}
		});

		
	}

	
	private void initializeFragmentTabHost() {

		// TODO Auto-generated method stub
		fragmenthost = (FragmentTabHost) findViewById(R.id.tabhost);

		fragmenthost.setup(this, getSupportFragmentManager(),
				R.id.frames_holder);

		fragmenthost.addTab(
				fragmenthost.newTabSpec(AppConstants.HOME_FRAGMENT_TAG)
						.setIndicator(""),
				ContainerProvider.Home_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Newsfeed")
				.setIndicator(""), ContainerProvider.Newsfeed_Container.class,
				null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Notifications")
				.setIndicator(""),
				ContainerProvider.Notification_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Wallet").setIndicator(""),
				ContainerProvider.MyWallet_Container.class, null);

		fragmenthost.addTab(
				fragmenthost.newTabSpec("Friends").setIndicator(""),
				ContainerProvider.MyFriends_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Group").setIndicator(""),
				ContainerProvider.MyGroups_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Settings")
				.setIndicator(""),
				ContainerProvider.Tab_Mybash_Container.class, null);

		fragmenthost.addTab(
				fragmenthost.newTabSpec("Support").setIndicator(""),
				ContainerProvider.Support_Container.class, null);

		
	}
	private void initializedSlidingMenuDatas() {
		menuList.clear();

		for (int i = 0; i < MenuTitles.length; i++) {
			menuList.add(new SlidingMenu_Class(MenuTitles[i], MenuIcons[i]));
		}
		menuAdapter = new SlidingMenuAdapter(getApplicationContext(), menuList);
		dList.setAdapter(menuAdapter);

		dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						
						//mDrawerToggle.onDrawerClosed(view);
						dLayout.closeDrawers();
						if (menuAdapter.getItem(position).getMenuName().equals("My Tabs")) {
							fragmenthost.setCurrentTabByTag(AppConstants.HOME_FRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName().equals("Newsfeed")) {
							fragmenthost.setCurrentTabByTag("Newsfeed");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Notifications")) {
							fragmenthost.setCurrentTabByTag("Notifications");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Wallet")) {
							fragmenthost.setCurrentTabByTag("Wallet");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Friends")) {
							fragmenthost.setCurrentTabByTag("Friends");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Group")) {
							fragmenthost.setCurrentTabByTag("Group");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Settings")) {
							fragmenthost.setCurrentTabByTag("Settings");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Invite People")) {
							fragmenthost.setCurrentTabByTag(AppConstants.INVITEPEOPLE_FRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName().equals("Support")) {
							fragmenthost.setCurrentTabByTag("Support");
						} else if (menuAdapter.getItem(position).getMenuName().equals("Logout")) {
							PreferenceManager.getInstance().resetUserDetails();
							ActivityManager.startActivity(SlidingActivity.this, BashLandingPage.class);
							BashApplication.isUserLoggedIn = false;
							finish();
						}
					}
				});
		applySlidingMenuValues();

	}
	public void initializeContact(ArrayList<ContactList_Model> contactList) {
		Singleton.getInstance().contactList = contactList;
	}
	
	public ArrayList<ContactList_Model> getContactList() {
		return Singleton.getInstance().contactList;
	}
	
	public void setUpTopBarFields(int leftSideImage, String leftText,
			String middleText, String rightText, int rightSideImage) {
		((ImageView) findViewById(R.id.topleftsideImage)).setImageResource(leftSideImage);
		((ImageView) findViewById(R.id.toprightsideImage)).setImageResource(rightSideImage);
		((TextView) findViewById(R.id.topleftText)).setText(leftText);
		((TextView) findViewById(R.id.topmiddleText)).setText(middleText);
		if(rightText.length()>0){
			((TextView) findViewById(R.id.toprightText)).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.toprightText)).setText(rightText);
		}else{
			((TextView) findViewById(R.id.toprightText)).setVisibility(View.GONE);
		}
	}
	public void setUserAmount(String symbol, String amount){
		if(symbol.equals("+")){
			tviUserAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
			tviUserAmount.setTextColor(Color.parseColor("#ffffff"));
			tviUserAmount.setText(AppConstants.RASYMBOL + " + " + amount);
        } else if(symbol.equals("-")){
        	tviUserAmount.setBackgroundResource(R.drawable.amount_displayred_s);
			tviUserAmount.setTextColor(Color.parseColor("#ffffff"));
        	tviUserAmount.setText(AppConstants.RASYMBOL + " - " + amount);
        } else {
        	tviUserAmount.setText(" ");
        }
	}
	
	public void applySlidingMenuValues() {
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				PreferenceManager.getInstance().getPercentageFromWidth(25),
				PreferenceManager.getInstance().getPercentageFromHeight(25));
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		params.setMargins(0, 10, 0, 0);
		iviProfileImage.setLayoutParams(params);

		if (PreferenceManager.getInstance().getUserCredits() != null
				&& PreferenceManager.getInstance().getUserCredits().length() != 0)
			tviUserAmount.setText(AppConstants.RASYMBOL + " " + PreferenceManager.getInstance().getUserCredits());
		else
			tviUserAmount.setText(AppConstants.RASYMBOL + " 0");

		if (PreferenceManager.getInstance().getUserImagePath() != null && PreferenceManager.getInstance().getUserImagePath().length() != 0) {
			ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), 
					iviProfileImage, BashApplication.options, BashApplication.animateFirstListener);
		} else
			iviProfileImage.setImageResource(R.drawable.picture_icon_s);
		
		 if (PreferenceManager.getInstance().getUserFullName() != null && PreferenceManager.getInstance().getUserFullName().length() != 0)
			 tviUserName.setText(PreferenceManager.getInstance().getUserFullName());
		 
	}
	
	private final String[] MenuTitles = { "My Tabs", "Newsfeed",
			"Notifications", "Wallet", "Friends", "Group", "Settings",
			"Support", "Logout" };
	private final Integer[] MenuIcons = { R.drawable.home_icon,
			R.drawable.bashmoments_icon, R.drawable.privacy_icon,
			R.drawable.cashout_icon, R.drawable.privacy_icon,
			R.drawable.group_icon,
			R.drawable.privacy_icon, R.drawable.support_icon,
			R.drawable.privacy_icon };
	boolean doubleBackToExitPressedOnce = false;
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		boolean isPopFragment = false;
		/*if (dLayout.isMenuShown()) {
			slidingmenu_layout.toggleMenu();
			return;
		}*/
		if(mDrawerToggle.isDrawerIndicatorEnabled()){
			 dLayout.closeDrawers();   
		}
		String currentTabTag = fragmenthost.getCurrentTabTag();

		if (currentTabTag.equals(AppConstants.HOME_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.HOME_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_WALLET_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_WALLET_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag
				.equals(AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(
							AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_GROUPS_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_GROUPS_FRAGMENT_TAG))
					.popFragment();
		}
		else if (currentTabTag.equals(AppConstants.INVITEPEOPLE_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.INVITEPEOPLE_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.SUPPORT_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.SUPPORT_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_PROFILE_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_CARDS_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_CARDS_FRAGMENT_TAG))
					.popFragment();
		}

		if (!isPopFragment) {
			if (doubleBackToExitPressedOnce) {
				finish();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Please click BACK again to exit",
					Toast.LENGTH_SHORT).show();

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
		}
	}
	
}
