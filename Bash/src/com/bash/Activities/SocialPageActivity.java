package com.bash.Activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bash.R;
import com.bash.Adapters.GalleryAdapter;
import com.bash.Application.BashApplication;
import com.bash.ListModels.CustomGallery;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.Action;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.Facebook;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public class SocialPageActivity extends Activity implements AsyncResponse{
	
	/* Shared preference keys */
	private static final String PREF_NAME = "sample_twitter_pref";
	private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
	private static final String PREF_USER_NAME = "twitter_user_name";
	
	/* Any number for uniquely distinguish your request */
	public static final int WEBVIEW_REQUEST_CODE = 100;
	
	TextView skipText, tviAddPhotos;
	EditText CommentEdtText;
	Button shareBtn;
	ImageView iviInfoSymb, ivBash, ivFacebook,ivTwitter;
	
	private ProgressDialog pDialog;

	private String consumerKey = "";
	private String consumerSecret = "";
	private String callbackUrl = "";
	private String oAuthVerifier = "";
	
	private static Twitter twitter;
	private static RequestToken requestToken;
	
	private static SharedPreferences mSharedPreferences;
	
	private boolean bashClickbool = true;
	private boolean fbClickbool = true;
	private boolean twitterClickbool = false;
	
	TwoWayView twViewPhotos;
	ArrayList<PhotosListModel> photosList;
	//TwoWayViewSocialAdapter socialPhotoAdapter;
	
	Handler handler;
	GalleryAdapter adapter;

	ImageView imgSinglePick;
	Button btnGalleryPick;
	Button btnGalleryPickMul;

	String action;
	ViewSwitcher viewSwitcher;
	ImageLoader imageLoader;
	ArrayList<CustomGallery> dataT=null;
	ArrayList<String> imagesList=null;
	MyAsynTaskManager myAsyncTask;
	String fbPhotoAddress = null;
	
	private UiLifecycleHelper uiHelper;
	
	private int transID;
	private String postType = "";
	private String categorySelect = "";
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "";
	private ArrayList<Person> addedPersonArrayList;
	private ArrayList<PhotosListModel> photoListArrayList;
	int fb,twittertype;
	private int bash = 1;
	boolean fbsuccess,twitterbool;
	
	JSONArray jsonArrayPayer = null;
	JSONArray jsonArrayReceive = null;
	JSONObject sharedList = null;
	JSONArray jsonArrayImage = null;

	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	
	Facebook mFacebook = null;

	
	private String status = "";
	private SimpleFacebook mSimpleFacebook;
	Profile userProfile;
	Bundle bundle;
	String userId = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);
        
		setContentView(R.layout.add_social);
		
		/* Enabling strict mode */
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		
		Intent itemwiseIntent = getIntent();			
		
		transID = itemwiseIntent.getIntExtra("transID", 0);
		postType = itemwiseIntent.getStringExtra("postType");
		bundle = itemwiseIntent.getBundleExtra("transationdetails");
		
		currencyCodeString = bundle.getString("CurrencyCodeString");
		categorySelect = bundle.getString("CategorySelect");
		dateString = bundle.getString("DateString");
		transationTitle = bundle.getString("TransationTitle");
		transationAnount = bundle.getString("TransationAnount");
		addedPersonArrayList = bundle.getParcelableArrayList("PaidUserDetails");
		photoListArrayList = bundle.getParcelableArrayList("ReceiveUserDetails");
		
		setPayerReceiver();
		
		skipText = (TextView) findViewById(R.id.toprightsocialtext);
		CommentEdtText = (EditText) findViewById(R.id.commentsedit);
		iviInfoSymb = (ImageView) findViewById(R.id.iviInfoSymb);
		ivBash = (ImageView) findViewById(R.id.bashsocial);
		ivFacebook = (ImageView) findViewById(R.id.facebooksocial);
		ivTwitter = (ImageView) findViewById(R.id.twittersocial);
		tviAddPhotos = (TextView) findViewById(R.id.tviAddPhotos);
		shareBtn = (Button) findViewById(R.id.socialshare);
		twViewPhotos = (TwoWayView) findViewById(R.id.twViewPhotos);
		
		iviInfoSymb.setVisibility(View.GONE);
		userId = PreferenceManager.getInstance().getUserId();
		initImageLoader();
		initializeViews();
	}
	private void initializeViews(){
		
		imagesList = new ArrayList<String>();
		
		initTwitterConfigs();
		
		/* Check if required twitter keys are set */
		if (TextUtils.isEmpty(consumerKey) || TextUtils.isEmpty(consumerSecret)) {
			Toast.makeText(this, "Twitter key and secret not configured",
					Toast.LENGTH_SHORT).show();
			return;
		}
		
		mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
		boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
		firstLoginSatus(isLoggedIn);
		
		 dataT = new ArrayList<CustomGallery>();
		
		skipText.setOnClickListener(clickListener);
		ivBash.setOnClickListener(clickListener);
		ivFacebook.setOnClickListener(clickListener);
		ivTwitter.setOnClickListener(clickListener);
		tviAddPhotos.setOnClickListener(clickListener);
		shareBtn.setOnClickListener(clickListener);
	
		
		
		handler = new Handler();
		adapter = new GalleryAdapter(getApplicationContext(), imageLoader);
		adapter.setMultiplePick(false);
		twViewPhotos.setAdapter(adapter);
	}
	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state,
	              Exception exception) {
	         if (state.isOpened()) {
	              Log.d("FacebookSampleActivity", "Facebook session opened");
	         } else if (state.isClosed()) {
	              Log.d("FacebookSampleActivity", "Facebook session closed");
	         }
	    }
	};
	private void initImageLoader() {
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
				this).defaultDisplayImageOptions(defaultOptions).memoryCache(
				new WeakMemoryCache());
	
		ImageLoaderConfiguration config = builder.build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
	}
	private void initTwitterConfigs() {
		consumerKey = getString(R.string.twitter_consumer_key);
		consumerSecret = getString(R.string.twitter_consumer_secret);
		callbackUrl = getString(R.string.twitter_callback);
		oAuthVerifier = getString(R.string.twitter_oauth_verifier);
	}
	
	
	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
	
			switch (v.getId()) {
	
			case R.id.toprightsocialtext:
				
				sharedOnList();
				//sendItemWiseSplitDetails();
				finishActivity();
	
				//Toast.makeText(SocialPageActivity.this, "Skip", Toast.LENGTH_SHORT).show();
				
				break;
			case R.id.bashsocial:
				//Toast.makeText(SocialPageActivity.this, "Bash", Toast.LENGTH_SHORT).show();
				
				if(!bashClickbool){
					ivBash.setImageResource(R.drawable.bash_tick_log);	
					bashClickbool = true;
					bash = 1;
					
				}else{
					ivBash.setImageResource(R.drawable.bash_icon_s);
					bashClickbool = false;
					bash = 0;
				}
				
				break;
			case R.id.facebooksocial:			
	
			
				if(!fbClickbool){
					ivFacebook.setImageResource(R.drawable.fb_new_pressed);	
					fbClickbool = true;
					
					
				}else{
					ivFacebook.setImageResource(R.drawable.fb_new_rest);
					fbClickbool = false;
					
				}
				
					//Toast.makeText(SocialPageActivity.this, "Facebook", Toast.LENGTH_SHORT).show();
				
				break;
			case R.id.twittersocial:
				if(!twitterClickbool){
					ivTwitter.setImageResource(R.drawable.twitter_new_pressed);	
					twitterClickbool = true;
					//loginToTwitter();
				}else{
					ivTwitter.setImageResource(R.drawable.twitter_new_rest);
					twitterClickbool = false;
				}
								
				//Toast.makeText(SocialPageActivity.this, "twitter", Toast.LENGTH_SHORT).show();
			
			break;
			case R.id.tviAddPhotos:
				Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
				startActivityForResult(i, 200);
				
				//ivAddPhotos.setVisibility(View.GONE);
				twViewPhotos.setVisibility(View.VISIBLE);
				
				//Toast.makeText(SocialPageActivity.this, "add photos", Toast.LENGTH_SHORT).show();
			
			break;
			case R.id.socialshare:
				sendItemWiseSplitDetails();
				status = CommentEdtText.getText().toString();
				
				if (status.trim().length() > 0) {
					
					if(fbClickbool){
						
						if(PreferenceManager.getInstance().getUserFacebookId() == null){
								
						faceBookLogin();
						}else{
							
							if(dataT.size() > 0){
								
								postWall();
							}else{
								
								postStatusMessage();
							}					
						}
							
							
						}else{
						//	Toast.makeText(SocialPageActivity.this, "facebook login failure!!", Toast.LENGTH_SHORT).show();
						}
					
					
					
					
					if(twitterClickbool){
					new updateTwitterStatus().execute(status);
					}else{
						//Toast.makeText(SocialPageActivity.this, "Twitter login failure!!", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(SocialPageActivity.this, "Message is empty!!", Toast.LENGTH_SHORT).show();
				}
				//Toast.makeText(SocialPageActivity.this, "socialshare", Toast.LENGTH_SHORT).show();
			
			break;
			}
			
		}
	};
	private void saveTwitterInfo(AccessToken accessToken) {
		
		long userID = accessToken.getUserId();
		
		User user;
		try {
			user = twitter.showUser(userID);
		
			String username = user.getName();
	
			/* Storing oAuth tokens to shared preferences */
			Editor e = mSharedPreferences.edit();
			e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
			e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
			e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
			e.putString(PREF_USER_NAME, username);
			e.commit();
	
		} catch (TwitterException e1) {
			e1.printStackTrace();
			twitterClickbool = false;
		}
	}
	private void loginToTwitter() {
		boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
		
		if (!isLoggedIn) {
			final ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(consumerKey);
			builder.setOAuthConsumerSecret(consumerSecret);
	
			final Configuration configuration = builder.build();
			final TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();
	
			try {
				requestToken = twitter.getOAuthRequestToken(callbackUrl);
	
				/**
				 *  Loading twitter login page on webview for authorization 
				 *  Once authorized, results are received at onActivityResult
				 *  */
				final Intent intent = new Intent(this, WebViewActivity.class);
				intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
				startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
				
			} catch (TwitterException e) {
				e.printStackTrace();
				twitterClickbool = false;
			}
		} else {
			
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
			String[] all_path = data.getStringArrayExtra("all_path");
	
			dataT.clear();
	
			for (String string : all_path) {
				CustomGallery item = new CustomGallery();
				item.sdcardPath = string;
	
				dataT.add(item);
			}
	
			//viewSwitcher.setDisplayedChild(0);
			adapter.addAll(dataT);
		}
		else if (requestCode == WEBVIEW_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			String verifier = data.getExtras().getString(oAuthVerifier);
			try {
				AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
	
				long userID = accessToken.getUserId();
				final User user = twitter.showUser(userID);
				String username = user.getName();
				
				saveTwitterInfo(accessToken);
	
			} catch (Exception e) {
				Log.e("Twitter Login Failed", e.getMessage());
				twitterClickbool = false;
			}
		}
	
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void firstLoginSatus(boolean isLoggedIn){
		
		if (isLoggedIn) {
	
		} else {
	
			Uri uri = getIntent().getData();
			
			if (uri != null && uri.toString().startsWith(callbackUrl)) {
			
				String verifier = uri.getQueryParameter(oAuthVerifier);
	
				try {
					
					/* Getting oAuth authentication token */
					AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
	
					/* Getting user id form access token */
					long userID = accessToken.getUserId();
					final User user = twitter.showUser(userID);
					final String username = user.getName();
	
					/* save updated token */
					saveTwitterInfo(accessToken);
					
				} catch (Exception e) {
					Log.e("Failed to login Twitter!!", e.getMessage());
					twitterClickbool = false;
				}
			}
	
		}
		
	}
	class updateTwitterStatus extends AsyncTask<String, String, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			pDialog = new ProgressDialog(SocialPageActivity.this);
			pDialog.setMessage("Posting to twitter...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
	
		protected Void doInBackground(String... args) {
	
			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(consumerKey);
				builder.setOAuthConsumerSecret(consumerSecret);
				
				// Access Token
				String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");
	
				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
	
				// Update status
				for(CustomGallery customGallery:dataT){
					StatusUpdate statusUpdate = new StatusUpdate(status);
					File theFile = new File(customGallery.sdcardPath);
					InputStream is = null;
					try {
						 is = new FileInputStream(theFile);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					statusUpdate.setMedia("", is);
					
					twitter4j.Status response = twitter.updateStatus(statusUpdate);
					Log.d("Status", response.getText());
				}
				
			
	
				
				
			} catch (TwitterException e) {
				Log.d("Failed to post!", e.getMessage());
			}
			return null;
		}
	
		@Override
		protected void onPostExecute(Void result) {
			
			/* Dismiss the progress dialog after sharing */
			pDialog.dismiss();
			
			Toast.makeText(SocialPageActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
	
			// Clearing EditText field
			twitterbool = true;
			CommentEdtText.setText("");
			dataT.clear();
			adapter.notifyDataSetChanged();
		}
	
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent backHome = new Intent(SocialPageActivity.this,SlidingActivity.class);
		backHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(backHome);
		finish();
		
	}
	
	
	private void faceBookLogin(){
		
		mSimpleFacebook.getInstance().login(new OnLoginListener() {
			@Override
			public void onFail(String reason) {
				Toast.makeText(getApplicationContext(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();					
			}
			
			@Override
			public void onException(Throwable throwable) {
				Toast.makeText(getApplicationContext(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();
				
			}
			
			@Override
			public void onThinking() {
				
			}
			
			@Override
			public void onNotAcceptingPermissions(Type type) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLogin() {
				Toast.makeText(getApplicationContext(),"Redirecting...", Toast.LENGTH_SHORT).show();
				//SimpleFacebook.getInstance().get
				mSimpleFacebook.getProfile(onProfileListener);
			
			}
		});
	}
	OnProfileListener onProfileListener = new OnProfileListener() {         
	    @Override
	    public void onComplete(Profile profile) {
	    	userProfile = profile;
	    	Log.e("Id", userProfile.getId());
	    	PreferenceManager.getInstance().setUserFacebookId(userProfile.getId());
	
			if(dataT.size() > 0){
				
				postWall();
			}else{
				
				postStatusMessage();
			}	
	    }
	};
	public void postStatusMessage() {
	         Request request = Request.newStatusUpdateRequest(
	                    Session.getActiveSession(), status,
	                    new Request.Callback() {
	                         @Override
	                         public void onCompleted(Response response) {
	                              if (response.getError() != null){
	                                    Toast.makeText(SocialPageActivity.this,
	                                    		response.getError().toString(),
	                                              Toast.LENGTH_LONG).show();
	                                    fbsuccess = false;
	                              }else{
	
	                            	  CommentEdtText.setText("");
	                                  fbsuccess = true;
	                                  }
	                              
	                              
	                         }
	                    });
	         request.executeAsync();
	}
	
	public boolean checkPermissions() {
	    Session s = Session.getActiveSession();
	    if (s != null) {
	         return s.getPermissions().contains("publish_actions");
	    } else
	         return false;
	}
	
	public void requestPermissions() {
	    Session s = Session.getActiveSession();
	    if (s != null)
	         s.requestNewPublishPermissions(new Session.NewPermissionsRequest(this, PERMISSIONS));
	                    
	}
	
	@Override
	public void onResume() {
	      super.onResume();
	      uiHelper.onResume();
	}
	
	@Override
	public void onPause() {
	      super.onPause();
	      uiHelper.onPause();
	}
	
	@Override
	public void onDestroy() {
	      super.onDestroy();
	      uiHelper.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedState) {
	      super.onSaveInstanceState(savedState);
	      uiHelper.onSaveInstanceState(savedState);
	}
	
	private void postWall(){
		
		Bitmap myBitmap = null;
		for(CustomGallery customGallery:dataT){
		
		File imgFile = new  File(customGallery.sdcardPath);
		 
		if(imgFile.exists()){
			
		    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		// create Photo instance and add some properties
		Photo photo = new Photo.Builder()
		    .setImage(myBitmap)
		    .setName(status)
		    .build();
		
		mSimpleFacebook.publish(photo, onPublishListener);
		}
		}
	}
	OnPublishListener onPublishListener = new OnPublishListener() {
	    @Override
	    public void onComplete(String id) {
	        Log.i("finish", "Published successfully. id = " + id);
	        
	        convertBase64();
	        fbsuccess = true;
	        dataT.clear();
	        CommentEdtText.setText("");
	        adapter.notifyDataSetChanged();
	        //ivAddPhotos.setVisibility(View.VISIBLE);
			twViewPhotos.setVisibility(View.GONE);
	       
	    }
	};
	
	private void convertBase64(){
		jsonArrayImage = new JSONArray();
		String base64str = "";
		
		//jsonArrayImage = null;
		imagesList.clear();
		
		for(CustomGallery customGallery:dataT){	
			
				try {
					base64str = encodeFileToBase64Binary(customGallery.sdcardPath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					JSONObject imgObject = new JSONObject();
					imgObject.put("image_string:", base64str);
					jsonArrayImage.put(imgObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//imagesList.add(base64str);
		}
		
		
	}
	private String encodeFileToBase64Binary(String fileName) throws IOException {
	
		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
	
		return encodedString;
	}
	
	private static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
	
		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];
	
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
	
		if (offset < bytes.length) {
			is.close();
			throw new IOException("Could not completely read file " + file.getName());
	
		}
	
		is.close();
		return bytes;
	}
		private void setPayerReceiver(){
		
			jsonArrayPayer = new JSONArray();
			jsonArrayReceive = new JSONArray();
			
			JSONObject jsonObject1 = null;
			JSONObject jsonObject2 = null;
			
			if(photoListArrayList.size() > 1){
				for (Person person : addedPersonArrayList) {
				
					jsonObject1 = new JSONObject();
			
					if (person.isPaidpersonbool()) {
			
			
						for (PhotosListModel photoObj : photoListArrayList) {
			
							if (photoObj.getID().equals(person.getId())) {
								
								photoListArrayList.remove(photoObj);
								break;
							}
						}
						
						try {
							jsonObject1.put("payer_type", BashApplication.userTypeHash.get(person.getUserType()));
							jsonObject1.put("user_or_group_id", Integer.parseInt(person.getId()));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						jsonArrayPayer.put(jsonObject1);
					}
				}
			}else{
				for (Person person : addedPersonArrayList) {
					
					if (photoListArrayList.size() > 0 && photoListArrayList.get(0).getID().equals(person.getId())) {
						
						//addedPersonArrayList.remove(person);
						
					}else{
						
						jsonObject1 = new JSONObject();
						try {
							
							jsonObject1.put("payer_type", BashApplication.userTypeHash.get(addedPersonArrayList.get(0).getUserType()));
							jsonObject1.put("user_or_group_id", Integer.parseInt(addedPersonArrayList.get(0).getId()));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						jsonArrayPayer.put(jsonObject1);
					}
				}
					
				
				
			}
		
			for (PhotosListModel photoListObj : photoListArrayList) {
				jsonObject2 = new JSONObject();
				
				try {
					
					jsonObject2.put("paid_for_type",BashApplication.userTypeHash.get(photoListObj.getUserType()));
					jsonObject2.put("user_or_group_id", Integer.parseInt(photoListObj.getID()));		
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				jsonArrayReceive.put(jsonObject2);
		
			}
		}
	private void sharedOnList(){
		
		sharedList = new JSONObject();
		
		if(fbsuccess){
			
			fb = 1;
		}else{
			
			fb = 0;
		}
		if(twitterbool){
				
				twittertype = 1;
			}else{
				
				twittertype = 0;
			}
			
		try {
			sharedList.put("bash", bash);
			sharedList.put("fb", fb);
			sharedList.put("twitter", twittertype);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	private void sendItemWiseSplitDetails() {
		convertBase64();
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		
		
			myAsyncTask.setupParamsAndUrl("SocialPage",
					this, AppUrlList.ACTION_URL, new String[] { 
					"module", "action", "idtrans", "post_type", "post_content", "posted_by_iduser", "shared_on",
					"payers", "paid_for", "images"},
					new String[] { "transaction", "insertAPost", "" + transID, postType, transationTitle, userId, sharedList.toString(),
					jsonArrayPayer.toString(), jsonArrayReceive.toString(), jsonArrayImage.toString()});
			
		myAsyncTask.execute();
	}
	
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if (from.equalsIgnoreCase("SocialPage")) {
	
			if (output != null && output.length() > 0) {
				JSONObject baseJsonObject;
				try {
					baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					String msgStr = baseJsonObject.getString("msg");
					if (result) {
						Toast.makeText(SocialPageActivity.this, msgStr, Toast.LENGTH_SHORT).show();
						finishActivity();
						
					} else {
						Toast.makeText(SocialPageActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
public void finishActivity(){
	Intent backHome = new Intent(SocialPageActivity.this,SlidingActivity.class);
	backHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	startActivity(backHome);
	finish();
}

}