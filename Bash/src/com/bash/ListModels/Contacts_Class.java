package com.bash.ListModels;

public class Contacts_Class {
	
	public String contactName, contactNumber, contactEmailId;
	public boolean isBashUser = false,  isRequestSent = false , isRequestReceived = false, isFriend = false;
	
	public Contacts_Class() {
		// TODO Auto-generated constructor stub
	}
	public Contacts_Class(String contactName, String contactNumber, String contactEmailId){
		this.contactEmailId = contactEmailId;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
	}
	
	public void setcontactEmailId(String contactEmailId){
		this.contactEmailId = contactEmailId;
	}
	
	public void setcontactName(String contactName){
		this.contactName = contactName;
	}
	
	public void setcontactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}
	
	public String getcontactEmailId(){
		return contactEmailId;
	}
	
	public String getcontactName(){
		return contactName;
	}
	
	public String getcontactNumber(){
		return contactNumber;
	}
	
}

