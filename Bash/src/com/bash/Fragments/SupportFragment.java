package com.bash.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Managers.AsyncResponse;

public class SupportFragment extends BaseFragment implements AsyncResponse{
private EditText txtQuery;
private boolean visible=false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mRootView = inflater.inflate(R.layout.fragment_support, null);
		initializeViews();
		((SlidingActivity) getActivity()).currentFragment = this;
		return mRootView;
	}
	private void initializeViews() {
		// TODO Auto-generated method stub
		txtQuery=(EditText)mRootView.findViewById(R.id.txtQuery);
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.menu_btn,"",
				"Support","", R.drawable.forward_icon_s);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
			}
		});
		((ImageView) getActivity().findViewById(R.id.toprightsideImage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), txtQuery.getText().toString(), Toast.LENGTH_LONG).show();
			}
		});
		((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setVisibility(View.INVISIBLE);
		txtQuery.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.length()>0 && !visible){
					visible=true;
					((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setVisibility(View.VISIBLE);
				}else if(s.length()==0 && visible){
					visible=false;
					((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setVisibility(View.INVISIBLE);
				}
			}
		});
	}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
	}

}
