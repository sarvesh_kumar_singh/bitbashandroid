package com.bash.Activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.ListModels.GroupTransactionDetail_Model;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Receivers;
import com.bash.ListModels.TransectionDetail_Model;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.Singleton;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TransectionDetailActivity extends Activity {
	Activity mActivity;
	TextView tviTransectionName, tviTotalAmount, tviEditedBy, tviReceiveText, tviDelete, tviEdit;
	ImageView iviBack;
	ListView lviTransectionFriends;
	GroupTransactionDetail_Model listItem ;
	ArrayList<TransectionDetail_Model> trans_detail;
	ArrayList<Payers> payers;
	ArrayList<Receivers> receiver;
	MyGroupsAdapter adapter;
	String pref,group_member_id;
	int totalTranAmount,ratioAmount,amt=0,paidAmt=0,hadtopay=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_transection_detail);
		group_member_id = getIntent().getExtras().getString("group_member_id");
		listItem = Singleton.getInstance().listItem;
		
		init();
		
		trans_detail = new ArrayList<TransectionDetail_Model>();
		adapter = new MyGroupsAdapter(mActivity, trans_detail);
		lviTransectionFriends.setAdapter(adapter);
		
		payers = new ArrayList<Payers>();
		receiver = new ArrayList<Receivers>();
		
		totalTranAmount = Integer.parseInt(listItem.getTotalAmount());
		tviTransectionName.setText(listItem.getTransaction_title());
		if(listItem.getCurrency().equals("INR")){
			pref = "₹ ";
			tviTotalAmount.setText("₹  "+listItem.getTotalAmount());
		} else {
			pref = "$ ";
			tviTotalAmount.setText("$  "+listItem.getTotalAmount());
		}
		
		payers = listItem.getPayers();
		receiver = listItem.getReceivers();
		getList();
		
		iviBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	private void getList() {
		// TODO Auto-generated method stub
		for(int p = 0; p < payers.size(); p++){
			if(payers.get(p).getIduser().equals(group_member_id)){
				if(payers.get(p).getPayment_type().equals("0")){
					paidAmt += Integer.parseInt(payers.get(p).getAmount());
					hadtopay += Integer.parseInt(payers.get(p).getHad_to_pay());
					/*if(paidAmt > hadtopay){
						amt = paidAmt - hadtopay;
						tviReceiveText.setText("You have to receive " + pref + amt);
					} else if(hadtopay > paidAmt){
						amt = hadtopay - paidAmt;
						tviReceiveText.setText("You have to pay " + pref + amt);
					} else {
						tviReceiveText.setText("You have not a part of this transection");
					}*/
				} else if(payers.get(p).getPayment_type().equals("1")){
					int ratio=0;
					for(int pay=0;pay<payers.size();pay++){
	    				ratio=ratio+Integer.parseInt(payers.get(pay).getHad_to_pay());
	    			}
	    			for(int rec=0;rec<receiver.size();rec++){
	    				ratio=ratio+Integer.parseInt(receiver.get(rec).getAmount());
	    			}
	    			ratioAmount=totalTranAmount/ratio;
	    			int amtpaid = ratioAmount * Integer.parseInt(payers.get(p).getAmount());
	    			
	    			paidAmt += Integer.parseInt(payers.get(p).getAmount());
					hadtopay += amtpaid * Integer.parseInt(payers.get(p).getHad_to_pay());
					
					/*if(paidAmt > hadtopay){
						amt = paidAmt - hadtopay;
						tviReceiveText.setText("You have to receive " + pref + amt);
					} else if(hadtopay > paidAmt){
						amt = hadtopay - paidAmt;
						tviReceiveText.setText("You have to pay " + pref + amt);
					} else {
						tviReceiveText.setText("You have not a part of this transection");
					}*/
	    			
				} else if(payers.get(p).getPayment_type().equals("2")){
					paidAmt += Integer.parseInt(payers.get(p).getAmount());
					int per = totalTranAmount * Integer.parseInt(payers.get(p).getHad_to_pay());
					hadtopay += per / 100;
					
					/*if(paidAmt > hadtopay){
						amt = paidAmt - hadtopay;
						tviReceiveText.setText("You have to receive " + pref + amt);
					} else if(hadtopay > paidAmt){
						amt = hadtopay - paidAmt;
						tviReceiveText.setText("You have to pay " + pref + amt);
					} else {
						tviReceiveText.setText("You have not a part of this transection");
					}*/
					
				}
			} /*else {
				tviReceiveText.setText("You have not a part of this transection");
			}*/
			if(payers.get(p).getPayment_type().equals("0")){
				trans_detail.add(new TransectionDetail_Model(payers.get(p).getFull_name() + " paid " + pref + payers.get(p).getAmount()+ " & owes " + payers.get(p).getHad_to_pay(), payers.get(p).getImage_location()));
			} else if(payers.get(p).getPayment_type().equals("1")){
				int ratio=0;
				for(int pay=0;pay<payers.size();pay++){
    				ratio=ratio+Integer.parseInt(payers.get(pay).getHad_to_pay());
    			}
    			for(int rec=0;rec<receiver.size();rec++){
    				ratio=ratio+Integer.parseInt(receiver.get(rec).getAmount());
    			}
    			ratioAmount=totalTranAmount/ratio;
    			
    			int amtpaid = ratioAmount * Integer.parseInt(payers.get(p).getAmount());
    			
    			trans_detail.add(new TransectionDetail_Model(payers.get(p).getFull_name() + " paid " + pref + payers.get(p).getAmount()+ " & owes " + amtpaid, payers.get(p).getImage_location()));
			} else if(payers.get(p).getPayment_type().equals("2")){
				int per = totalTranAmount * Integer.parseInt(payers.get(p).getHad_to_pay());
				trans_detail.add(new TransectionDetail_Model(payers.get(p).getFull_name() + " paid " + pref + payers.get(p).getAmount()+ " & owes " + per/100, payers.get(p).getImage_location()));
				
			}
		}
		
		for(int r = 0; r < receiver.size(); r++){
			if(receiver.get(r).getIduser().equals(group_member_id)){
				if(receiver.get(r).getPayment_type().equals("0")){
					hadtopay += Integer.parseInt(receiver.get(r).getAmount());
					//tviReceiveText.setText("You have to pay " + pref + amt);
				} else if(receiver.get(r).getPayment_type().equals("1")){
					int amtpaid = ratioAmount * Integer.parseInt(receiver.get(r).getAmount());
					hadtopay += amtpaid;
					//tviReceiveText.setText("You have to pay " + pref + amt);
				} else if(receiver.get(r).getPayment_type().equals("2")){
					int per = totalTranAmount * Integer.parseInt(receiver.get(r).getAmount());
					hadtopay += per/100;
					//tviReceiveText.setText("You have to pay " + pref + amt);
				}
			} /*else {
				tviReceiveText.setText("You have not a part of this transection");
			}*/
			if(receiver.get(r).getPayment_type().equals("0")){
				trans_detail.add(new TransectionDetail_Model(receiver.get(r).getFull_name() + " owes " + receiver.get(r).getAmount(), receiver.get(r).getImage_location()));
			} else if(receiver.get(r).getPayment_type().equals("1")){
				int amtpaid = ratioAmount * Integer.parseInt(receiver.get(r).getAmount());
				trans_detail.add(new TransectionDetail_Model(receiver.get(r).getFull_name() + " owes " + amtpaid, receiver.get(r).getImage_location()));
			} else if(receiver.get(r).getPayment_type().equals("2")){
				int per = totalTranAmount * Integer.parseInt(receiver.get(r).getAmount());
				trans_detail.add(new TransectionDetail_Model(receiver.get(r).getFull_name() + " owes " + per/100, receiver.get(r).getImage_location()));
			}
		}
		if(hadtopay > paidAmt){
			amt = hadtopay - paidAmt;
			tviReceiveText.setText("You have to receive " + pref + amt);
		}else if(paidAmt > hadtopay){
			amt = paidAmt - hadtopay;
			tviReceiveText.setText("You have to receive " + pref + amt);
		}else{
			tviReceiveText.setText("You have not a part of this transection");
		}
		
		
		adapter.notifyDataSetChanged();
	}

	private void init() {
		// TODO Auto-generated method stub
		mActivity = TransectionDetailActivity.this;
		tviTransectionName = (TextView) findViewById(R.id.tviTransectionName);
		tviTotalAmount = (TextView) findViewById(R.id.tviTotalAmount);
		tviEditedBy = (TextView) findViewById(R.id.tviEditedBy);
		tviReceiveText = (TextView) findViewById(R.id.tviReceiveText);
		tviDelete = (TextView) findViewById(R.id.tviDelete);
		tviEdit = (TextView) findViewById(R.id.tviEdit);
		
		iviBack = (ImageView) findViewById(R.id.iviBack);
		lviTransectionFriends = (ListView) findViewById(R.id.lviTransectionFriends);
		
		String[] suffixes =
				  //    0     1     2     3     4     5     6     7     8     9
				     { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
				  //    10    11    12    13    14    15    16    17    18    19
				       "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
				  //    20    21    22    23    24    25    26    27    28    29
				       "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
				  //    30    31
				       "th", "st" };
		
				  SimpleDateFormat format3=new SimpleDateFormat("yy-MM-dd");
				  Date date = null; // your date
				  try {
					  date = format3.parse(listItem.getDate());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    Calendar cal = Calendar.getInstance();
				    cal.setTime(date);
				    
				    int day = cal.get(Calendar.DAY_OF_MONTH);
				    String monthString = new SimpleDateFormat("MMMM").format(date);
				    String yearString = new SimpleDateFormat("yy").format(date);
				    String dayString = day + suffixes[day];
				    tviEditedBy.setText("Added by " + listItem.getEntered_by_full_name() + " on " + dayString + " " + monthString + " " + yearString);
		//SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		//String date = formatter.format(Date.parse(listItem.getDate()));
		
	}
	
	public class MyGroupsAdapter extends BaseAdapter
	{
		 	public ArrayList<TransectionDetail_Model> trans_detail = new ArrayList<TransectionDetail_Model>();
		 	public Activity context;
		 	public LinearLayout.LayoutParams backViewParams;
		    public MyGroupsAdapter(Activity context, ArrayList<TransectionDetail_Model> feedList) {
		        this.context = context;
		    	this.trans_detail = feedList;
		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
		    			LayoutParams.MATCH_PARENT);
		    }
		    
		    public void notifyWithDataSet(ArrayList<TransectionDetail_Model> newlist){
		    	this.trans_detail.clear();
		    	this.trans_detail = newlist;
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        TransectionDetail_Model listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	if(convertView == null)
		        	 {
		        	  	convertView = inflater.inflate(R.layout.custom_trans_detail_listview, null);
			            holder = new ViewHolder();
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.iviFriendImageSource = (ImageView) convertView.findViewById(R.id.iviFriendImageSource);
		        	}
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		       
		        	holder.friendName.setText(listItem.getTrans_text());
		        	
			        if(listItem.getImage_path() != null && listItem.getImage_path().length()!= 0 && holder.iviFriendImageSource != null){
			        	ImageLoader.getInstance().displayImage(listItem.getImage_path(), 
			        			holder.iviFriendImageSource, BashApplication.options, BashApplication.animateFirstListener);
			        }else{
			        	holder.iviFriendImageSource.setImageResource(R.drawable.addphoto_img_block);
			        }
		        
		        return convertView;
		    }

		    @Override
		    public int getCount() {
		    	//	checkEmptyList();
		        return trans_detail.size();
		    }

		    @Override
		    public TransectionDetail_Model getItem(int position) {
		        return trans_detail.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }

		    private class ViewHolder {
		        TextView friendName;
		        ImageView iviFriendImageSource;
		    }

	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.transection_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
