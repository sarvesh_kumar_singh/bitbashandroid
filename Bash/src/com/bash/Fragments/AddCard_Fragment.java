package com.bash.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;

public class AddCard_Fragment extends BaseFragment implements OnClickListener {
	View mRootView;
	private TextView tviDebit, tviCredit;
	private ImageView iviDebit, iviCardType;
	private EditText txtEnterNickName,txtCardNo1, txtCardNo2, txtCardNo3, txtCardNo4, txtMonth,
			txtYear;
	private boolean toogleclick = false;
	private String firstChar, secondChar;
	private int cardlength=0,card1length=0,card2length=0,card3length=0,card4length=0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.activity_addcard, null);
		init();
		return mRootView;
	}

	/*
	 * @Override protected void onCreate(Bundle savedInstanceState) {
	 * super.onCreate(savedInstanceState);
	 * setContentView(R.layout.activity_addcard);
	 * ((ImageView)findViewById(R.id.imageView2)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView3)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView1)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView4)).setOnClickListener(this); }
	 * 
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */
	private void init() {
		// TODO Auto-generated method stub
		txtEnterNickName = (EditText) mRootView.findViewById(R.id.txtEnterNickName);
		tviDebit = (TextView) mRootView.findViewById(R.id.tviDebit);
		tviCredit = (TextView) mRootView.findViewById(R.id.tviCredit);
		iviDebit = (ImageView) mRootView.findViewById(R.id.iviDebit);
		iviCardType = (ImageView) mRootView.findViewById(R.id.iviCardType);
		txtCardNo1 = (EditText) mRootView.findViewById(R.id.txtCardNo1);
		txtCardNo2 = (EditText) mRootView.findViewById(R.id.txtCardNo2);
		txtCardNo3 = (EditText) mRootView.findViewById(R.id.txtCardNo3);
		txtCardNo4 = (EditText) mRootView.findViewById(R.id.txtCardNo4);
		txtMonth = (EditText) mRootView.findViewById(R.id.txtMonth);
		txtYear = (EditText) mRootView.findViewById(R.id.txtYear);
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn,
				"Add Card","","Add", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
				});
		((TextView) getActivity().findViewById(R.id.toprightText))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// ((BaseFragment)getParentFragment()).popFragment();
					}
				});

		iviDebit.setOnClickListener(this);
		txtCardNo1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 4) {
					txtCardNo2.requestFocus();
					firstChar = s.toString().substring(0, 1);
					secondChar = s.toString().substring(1, 2);
					Log.i("First & Second Char", firstChar + " " + secondChar);
				}
					card1length=s.length();
				
			}
		});
		txtCardNo2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 4) {
					txtCardNo3.requestFocus();
				}
				card2length=s.length();
			}
		});
		txtCardNo3.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 4) {
					txtCardNo4.requestFocus();
				}
				card3length=s.length();
			}
		});
		txtCardNo4.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cardlength=0;
				card4length=s.length();
				cardlength=card1length+card2length+card3length+card4length;
					if (cardlength==13 && firstChar.equals("4")) {
						iviCardType.setImageResource(R.drawable.visa_s);
						txtMonth.requestFocus();
					}else if (cardlength==15 && firstChar.equals("3") && (secondChar.equals("4") || secondChar.equals("7"))) {
						iviCardType.setImageResource(R.drawable.amex_s);
						txtMonth.requestFocus();
					}else if (cardlength==16 && firstChar.equals("5") && (Integer.parseInt(secondChar) >= 1 && Integer.parseInt(secondChar) <= 5)) {
						iviCardType.setImageResource(R.drawable.mastercard_s);
					} else if (cardlength==16 && firstChar.equals("4")) {
						iviCardType.setImageResource(R.drawable.visa_s);
					} 
				
			}
		});
		txtMonth.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() > 0 && Integer.parseInt(s.toString()) > 12) {
					Toast.makeText(getActivity(), "Please Enter a valid month",
							Toast.LENGTH_LONG).show();
				} else if (s.length() == 2) {
					txtYear.requestFocus();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviDebit:
			toogleClick();
			break;
		default:
			break;
		}
	}

	private void toogleClick() {
		// TODO Auto-generated method stub
		if (!toogleclick) {
			tviCredit.setTextColor(Color.BLACK);
			tviDebit.setTextColor(Color.GRAY);
			iviDebit.setImageResource(R.drawable.toggle_s);
			toogleclick = true;
		} else if (toogleclick) {
			tviCredit.setTextColor(Color.GRAY);
			tviDebit.setTextColor(Color.BLACK);
			iviDebit.setImageResource(R.drawable.toggle_inv_s);
			toogleclick = false;
		}
	}
}
