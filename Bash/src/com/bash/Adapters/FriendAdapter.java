package com.bash.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.bash.R;
import com.bash.ListModels.Change_Friend_Class;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FriendAdapter extends ArrayAdapter<Change_Friend_Class> {
	
    private ArrayList<Change_Friend_Class> items;
    private ArrayList<Change_Friend_Class> itemsAll;
    private ArrayList<Change_Friend_Class> myDialogList;
    private ArrayList<Change_Friend_Class> suggestions;
    private int viewResourceId;
    private ImageView myviewListbtn;
    private TextView myinfoText;
    private Activity context;
    
    
    public FriendAdapter(Activity context, int viewResourceId, ArrayList<Change_Friend_Class> items, ImageView viewListbtn,
    		TextView infoText) {
    	super(context, viewResourceId, items);
    	this.context = context;
        this.myviewListbtn = viewListbtn;
        this.myinfoText = infoText;
        this.items = items;
        this.itemsAll = (ArrayList<Change_Friend_Class>) items.clone();
        this.suggestions = new ArrayList<Change_Friend_Class>();
        this.viewResourceId = viewResourceId;
        
        initializeChargeEqualAlertView();
        
        this.myviewListbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialogList = getSelectionList();
				split_equal_dialog.show();
				selectionAdapter.notifyDataSetChanged();
				myinfoText.setText(getSelectionList().size()+"");
			}
		});
        
        myinfoText.setText("0");
    }
    
    
    View split_equal_view;
    Dialog split_equal_dialog;
    public SelectionFriendsAdapter selectionAdapter;
    public ListView splitEqualView;
    
    private void initializeChargeEqualAlertView() {

		split_equal_view = View.inflate(context, R.layout.alert_viewlist_window, null);
		split_equal_dialog = new Dialog(context);
		split_equal_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		split_equal_dialog.setContentView(split_equal_view);
		
		((Button) split_equal_view.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				split_equal_dialog.dismiss();
			}
		});
		
		((Button) split_equal_view.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				split_equal_dialog.dismiss();
			}
		});
		
		((TextView) split_equal_view.findViewById(R.id.titleText)).setText("Split By Equal");
		
		myDialogList = new ArrayList<Change_Friend_Class>();
		
		splitEqualView = (ListView) split_equal_view.findViewById(R.id.splitedListView);
		
		selectionAdapter= new SelectionFriendsAdapter();
		
		splitEqualView.setAdapter(selectionAdapter);
   }
    
    
    class SelectionFriendsAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return myDialogList.size();
		}

		@Override
		public Change_Friend_Class getItem(int position) {
			// TODO Auto-generated method stub
			return myDialogList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

				ViewHolder holder = null;
			
				final Change_Friend_Class listItem = getItem(position);
				
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	convertView = inflater.inflate(R.layout.custom_viewlist_listview, null);
		            holder = new ViewHolder();
		            holder.selectionBox = (CheckBox) convertView.findViewById(R.id.selectionBox);
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        
		        holder.friendName.setText(listItem.getFriendName());
		        
		        holder.selectionBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked){
							myDialogList.get(position).setSelected(true);
						}else{
							myDialogList.get(position).setSelected(false);
						}
						myinfoText.setText(getSelectionList().size()+"");
						//myinfoText.setText(selectionList.size());
					}
				});
		        
	            if(listItem.getFriendImagePath() != null && listItem.getFriendImagePath().length() != 0)
	            		ImageLoader.getInstance().displayImage(listItem.getFriendImagePath(), holder.userImage);
	        
	            return convertView;
	         }
	    		
	  		  private class ViewHolder {
	  		    	CheckBox selectionBox;
	  		    	TextView friendName;
	  		    	ImageView userImage;
	  		     }
	  	}
	

    public ArrayList<Change_Friend_Class> getSelectionList() {
    	ArrayList<Change_Friend_Class> tempList= new ArrayList<Change_Friend_Class>();
    	for(int i = 0; i < itemsAll.size(); i++){
    		if(itemsAll.get(i).isSelected())
    			tempList.add(itemsAll.get(i));
    	}
    	Log.e("Selected List", tempList.size()+"");
    	return tempList;
    }
    
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        Change_Friend_Class Change_Friend_Class = items.get(position);
        if (Change_Friend_Class != null) {
            TextView Change_Friend_ClassNameLabel = (TextView) v.findViewById(R.id.text1);
            CheckBox  checkbox = (CheckBox) v.findViewById(R.id.isSelected);
            if (Change_Friend_ClassNameLabel != null) {
//              Log.i(MY_DEBUG_TAG, "getView Change_Friend_Class Name:"+Change_Friend_Class.getFriendName());
                Change_Friend_ClassNameLabel.setText(Change_Friend_Class.getFriendName());
            }
            
            checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						getItem(position).setSelected(true);
					}else{
						getItem(position).setSelected(false);
					}
					myinfoText.setText(getSelectionList().size()+"");
					//myinfoText.setText(selectionList.size());
				}
			});
            
            if(Change_Friend_Class.isSelected()){
            	checkbox.setChecked(true);
				//myinfoText.setText((Integer.parseInt(myinfoText.getText().toString()) + 1)+"");
            }else{
            	checkbox.setChecked(false);
				//myinfoText.setText((Integer.parseInt(myinfoText.getText().toString()) - 1)+"");
            }
            
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Change_Friend_Class)(resultValue)).getFriendName(); 
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
        	if(constraint == null || constraint.length() == 0){
        		/*Log.e("Time", "123");
           	 	suggestions.clear();
                for (Change_Friend_Class Change_Friend_Class : itemsAll) {
                        suggestions.add(Change_Friend_Class);
                }
                */
        		FilterResults filterResults = new FilterResults();
                filterResults.values = itemsAll;
                filterResults.count = itemsAll.size();
                return filterResults;
        	}
        	else if(constraint != null) {
                suggestions.clear();
                for (Change_Friend_Class Change_Friend_Class : itemsAll) {
                    if(Change_Friend_Class.getFriendName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(Change_Friend_Class);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            }
			return new FilterResults(); 
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Change_Friend_Class> filteredList = (ArrayList<Change_Friend_Class>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (Change_Friend_Class c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };
    

}
