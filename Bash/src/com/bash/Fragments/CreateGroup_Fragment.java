package com.bash.Fragments;

import java.io.ByteArrayOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CreateGroup_Fragment extends BaseFragment implements AsyncResponse,OnClickListener {
	View mRootView;
	private TextView tviHouse, tviTrip,tviParty,tviOther;
	private ImageView iviProfileImage;
	private EditText txtEnterGroupName;
	MyAsynTaskManager  myAsyncTask;
	Uri mCapturedImageURI;
	String groupType=""; 
	String picturePath="";
	
	public String imageBase64String;
	String selectedImage_path;
	String filePath="";
	String FILE_NAME="";
	Uri selectedImage;
	
	private static final int PICK_FROM_FILE = 3;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.fragment_creategroup, null);
		((SlidingActivity)getActivity()).currentFragment = this;
		init();
		return mRootView;
	}

	/*
	 * @Override protected void onCreate(Bundle savedInstanceState) {
	 * super.onCreate(savedInstanceState);
	 * setContentView(R.layout.activity_addcard);
	 * ((ImageView)findViewById(R.id.imageView2)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView3)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView1)).setOnClickListener(this);
	 * ((ImageView)findViewById(R.id.imageView4)).setOnClickListener(this); }
	 * 
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */
	private void init() {
		// TODO Auto-generated method stub
		txtEnterGroupName = (EditText) mRootView.findViewById(R.id.txtEnterGroupName);
		tviHouse = (TextView) mRootView.findViewById(R.id.tviHouse);
		tviTrip = (TextView) mRootView.findViewById(R.id.tviTrip);
		tviParty = (TextView) mRootView.findViewById(R.id.tviParty);
		tviOther = (TextView) mRootView.findViewById(R.id.tviOther);
		iviProfileImage = (ImageView) mRootView.findViewById(R.id.iviProfileImage);
		
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn,
				"Back","","Continue", R.drawable.enterpayee);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
				});
		/*((ImageView) getActivity().findViewById(R.id.toprightsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// ((BaseFragment)getParentFragment()).popFragment();
						//((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Next_Fragment(), true);
						ProcessUserImage();
					}
				});*/
		((TextView) getActivity().findViewById(R.id.toprightText))
			.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// ((BaseFragment)getParentFragment()).popFragment();
					//((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Next_Fragment(), true);
					ProcessUserImage();
				}
			});

		tviHouse.setOnClickListener(this);
		tviTrip.setOnClickListener(this);
		tviParty.setOnClickListener(this);
		tviOther.setOnClickListener(this);
		iviProfileImage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviProfileImage:
			/*Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);*/
			/*Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 3);*/
			 Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
             intent.setType("image/*");

             Intent chooser = Intent.createChooser(intent, "Choose a Picture");
             getActivity().startActivityForResult(chooser, AppConstants.CODE_GALLERY);
			break;
		case R.id.tviHouse:
			setBackGround();
			groupType="House";
			tviHouse.setTextColor(Color.parseColor("#FFFFFF"));
			tviHouse.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			break;
		case R.id.tviTrip:
			setBackGround();
			groupType="Trip";
			tviTrip.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviTrip.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		case R.id.tviParty:
			setBackGround();
			groupType="Party";
			tviParty.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviParty.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		case R.id.tviOther:
			setBackGround();
			groupType="Other";
			tviOther.setBackgroundResource(R.drawable.rounded_appcolor_fillbox);
			tviOther.setTextColor(Color.parseColor("#FFFFFF"));
			break;
		default:
			break;
		}
	}

	void setBackGround() {
		tviHouse.setTextColor(Color.parseColor("#00BDCB"));
		tviHouse.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviTrip.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviTrip.setTextColor(Color.parseColor("#00BDCB"));
		tviParty.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviParty.setTextColor(Color.parseColor("#00BDCB"));
		tviOther.setBackgroundResource(R.drawable.rounded_appcolor_box);
		tviOther.setTextColor(Color.parseColor("#00BDCB"));
	}
	
	public void ProcessUserImage() 
	{
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;

		String img="";
		if(imageBase64String!=null){
			img=imageBase64String;
			Log.e("User Id", imageBase64String);
		}
		try {
			myAsyncTask.setupParamsAndUrl("ProcessCreateGroup",getActivity(),AppUrlList.ACTION_URL, new String[]{
					"module", "action", "iduser", "groupname", "groupimage", "grouptype" },
				new String[]{
					"group", "add",	PreferenceManager.getInstance().getUserId(),
					txtEnterGroupName.getText().toString(), img,groupType});
				myAsyncTask.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if (from.equalsIgnoreCase("ProcessCreateGroup")) {
			if (output != null) {
				try {
				JSONObject jObj=new JSONObject(output);
				
					if(jObj.getBoolean("result")){
						Toast.makeText(getActivity(), "Group Create Successfully", Toast.LENGTH_LONG).show();
						((SlidingActivity)getActivity()).groupId=jObj.getString("idgroup");
						((SlidingActivity)getActivity()).groupName=jObj.getString("groupname");
						((SlidingActivity)getActivity()).groupAdmin=jObj.getString("groupadmin");
						((SlidingActivity)getActivity()).groupImage=jObj.getString("imagepath");
						((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Next_Fragment(), true);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				Toast.makeText(getActivity(), "Fail to Create Group", Toast.LENGTH_LONG).show();
			}
		}

	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	iviProfileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				/*Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	iviProfileImage);
				
                try {
                    Bitmap bitmapImage =decodeBitmap(selectedImage );
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                                // Show the Selected Image on ImageView
                    ImageView imageView = (ImageView) findViewById(R.id.imgView);
                    imageView.setImageBitmap(bitmapImage);*/
				Bitmap myBitmap = null;
				Uri selectedImage = data.getData();
				selectedImage_path = getPath(getActivity(), selectedImage);
				if (selectedImage_path.contains("http")) {
					final AjaxCallback<Bitmap> cb = new AjaxCallback<Bitmap>() 
							{
						@SuppressWarnings("deprecation")
						@Override
						public void callback(String url, Bitmap bm,
								AjaxStatus status) 
						{
							
							ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
							bm.compress(CompressFormat.JPEG, 100, outputStream);
							byte[] byteArray = outputStream.toByteArray();
							/*
							 * Bitmap.createBitmap(306, 306,
							 * Bitmap.Config.ARGB_8888);
							 */
							imageBase64String = Base64.encodeToString(byteArray,
									Base64.DEFAULT);
							// Log.i("Deepr", "Image Returned" + imageBase64String);
						}
					};
					final AQuery aq = new AQuery(getActivity());
					aq.ajax(selectedImage_path, Bitmap.class, 0, cb);

				} else {
					Bitmap p_image = BitmapFactory.decodeFile(selectedImage_path);
					try {
						ExifInterface exif = new ExifInterface(selectedImage_path);
						int orientation = exif.getAttributeInt(
								ExifInterface.TAG_ORIENTATION, 1);
						Log.d("EXIF", "Exif: " + orientation);
						Matrix matrix = new Matrix();
						if (orientation == 6) {
							matrix.postRotate(90);
						} else if (orientation == 3) {
							matrix.postRotate(180);
						} else if (orientation == 8) {
							matrix.postRotate(270);
						}
						myBitmap = Bitmap.createBitmap(p_image, 0, 0,
								p_image.getWidth(), p_image.getHeight(), matrix,
								true); // rotating bitmap
						/*Bitmap bitmap = Bitmap.createScaledBitmap(myBitmap, 300,
								300, false);*/
						ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
						myBitmap.compress(CompressFormat.JPEG, 100, outputStream);
						byte[] byteArray = outputStream.toByteArray();
						imageBase64String = Base64.encodeToString(byteArray,
								Base64.DEFAULT);
						//iviProfileImage.setImageBitmap(myBitmap);
						//Uri selectedImage = data.getData();
						String[] filePathColumn = { MediaStore.Images.Media.DATA };
						Cursor cursor = getActivity().getContentResolver().query(selectedImage, null, null, null, null);
						cursor.moveToFirst();
						int columnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
						picturePath = cursor.getString(columnIndex);
						cursor.close();
						ImageLoader.getInstance().displayImage(selectedImage_path,	iviProfileImage);
					} catch (Exception e) {

					}

				}

			}
			break;
		default:
			break;
		}
		
 }
	/**
	 * Get a file path from a Uri. This will get the the path for Storage Access
	 * Framework Documents, as well as the _data field for the MediaStore and
	 * other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @author paulburke
	 */
	@SuppressLint("NewApi")
	public static String getPath(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
	        String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
		
}
