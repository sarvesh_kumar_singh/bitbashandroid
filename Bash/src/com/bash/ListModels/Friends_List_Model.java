package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class Friends_List_Model {
	@SerializedName("contactid")
	public String contactid;
	@SerializedName("fullname")
	public String fullname;
	@SerializedName("email")
	public String email;
	@SerializedName("phone_no")
	public String phone_no;
	@SerializedName("imagepath")
	public String imagepath;
	@SerializedName("usertype")
	public String usertype;
	@SerializedName("currency")
	public String currency;
	@SerializedName("amount")
	public String amount;
	@SerializedName("amounttype")
	public String amounttype;
	

	/*public Friends_List_Model(String contactid2, String fullname2,
			String email2, String phone_no2, String imagepath2, String usertype2) {
		// TODO Auto-generated constructor stub
	}*/

	public String getContactid() {
		return this.contactid;
	}

	public String getFullname() {
		return this.fullname;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPhone_no() {
		return this.phone_no;
	}

	public String getImagepath() {
		return this.imagepath;
	}

	public String getUsertype() {
		return this.usertype;
	}
	
	public String getCurrency() {
		return this.currency;
	}

	public String getAmount() {
		return this.amount;
	}

	public String getAmounttype() {
		return this.amounttype;
	}

	public Friends_List_Model(String contactid, String fullname, String email,
			String phone_no, String imagepath, String usertype, String currency, String amount, String amounttype) {
		this.contactid = contactid;
		this.fullname = fullname;
		this.email = email;
		this.phone_no = phone_no;
		this.imagepath = imagepath;
		this.usertype = usertype;
		this.currency = currency;
		this.amount = amount;
		this.amounttype = amounttype;
	}
}
