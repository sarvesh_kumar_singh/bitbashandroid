package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedLikes_Model {
	@SerializedName("likes_count")
	public String likes_count;
	@SerializedName("payers_information")
	public NewsfeedLikeInformation_Model[] likes_information;

	public NewsfeedLikes_Model(String likes_count, NewsfeedLikeInformation_Model[] payers_information) {
		this.likes_count = likes_count;
		this.likes_information = payers_information;

	}

	public String getLikes_count() {
		return likes_count;
	}
	public void setLikes_count(String like_count) {
		 this.likes_count = like_count;
	}

	public NewsfeedLikeInformation_Model[] getLikes_information() {
		return likes_information;
	}

}
