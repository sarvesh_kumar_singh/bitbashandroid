package com.bash.Activities;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import me.grantland.widget.AutofitTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.CustomViews.CircularImageView;
import com.bash.CustomViews.ContactsCompletionView;
import com.bash.Fragments.AddTabPayFragment;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.interfaces.ActivityInterface;
import com.bash.interfaces.PaidbyInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

public class AddTabActivity extends FragmentActivity implements AsyncResponse, TokenCompleteTextView.TokenListener, ActivityInterface, PaidbyInterface{

	MyAsynTaskManager myAsyncTask;
	ContactsCompletionView searchView;
	Person[] people;
	ArrayList<Person> personList;
	public ArrayList<Person> addedPersonList;
	FilteredArrayAdapter<Person> adapter;
	ImageView datepickerimageview,categoryimageview;
	Button btnSave, btnSavePay;
	TextView currencyText,paidbytext;
	public TextView paidby;
	AutofitTextView amountedittext;
	EditText titleedittext;
	private int categorySelect = 9;
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "INR";
	private AlertDialog catedialog = null;
	private AlertDialog continuedialog = null;
	private boolean dateSet;
	private String userid = "";
	private String userame = "";
	private String userimagepath = "";
	CircularImageView userimageview;
	
	private int fragTag = 1;
	private int transId;
	
	public static FragmentTabHost mTabhost;
	public static View payView, splitView, receiveView;
	public TextView tvContinue;
	public static AddTabActivity AddTabActivityObj=null;
	AlertDialog dialog = null;
	SimpleDateFormat dateViewFormatter;
	
	LinearLayout llPaidBy;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_tab);
		AddTabActivityObj = AddTabActivity.this;
		
		initializeActivityViews();
		initializeFragmentTabHost();
		
		
		if (!PreferenceManager.getInstance().isPhoneConfigFixed()) {
			PreferenceManager.getInstance().setPhoneConfiguration(
					getWindowManager().getDefaultDisplay().getWidth(),
					getWindowManager().getDefaultDisplay().getHeight());
		}
		
	}

	private void initializeActivityViews() {
		searchView = (ContactsCompletionView)findViewById(R.id.searchView);
		datepickerimageview = (ImageView) findViewById(R.id.datepickerimageview);
		categoryimageview = (ImageView) findViewById(R.id.categoryimageview);
		llPaidBy = (LinearLayout) findViewById(R.id.llPaidBy);
		paidby = (TextView) findViewById(R.id.paidby);
		paidbytext = (TextView) findViewById(R.id.paidbytext);
		currencyText = (TextView) findViewById(R.id.currencyText);
		amountedittext = (AutofitTextView) findViewById(R.id.amountedittext);
		titleedittext = (EditText) findViewById(R.id.titleedittext);
		searchView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
		userimageview = (CircularImageView) findViewById(R.id.userimageview);
		
		
		//userame = PreferenceManager.getInstance().getUser_FullName();
		userame = "You";
		userimagepath = PreferenceManager.getInstance().getUserImagePath();
		userid = PreferenceManager.getInstance().getUserId();
		
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText("Continue");

		mTabhost = (FragmentTabHost)findViewById(R.id.addtabhost);
		mTabhost.setup(AddTabActivity.this, getSupportFragmentManager(), R.id.addtabframes_holder);
		
		llPaidBy.setVisibility(View.GONE);
		dateViewFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        dateString = dateViewFormatter.format(cal.getTime()); //2014-12-08
		
		((ImageView) findViewById(R.id.topleftsidebackimage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AddTabActivity.this.onBackPressed();
				
			}
		});
		
		
		if (personList == null) {
			personList = new ArrayList<Person>();
		}
		if (addedPersonList == null) {
			addedPersonList = new ArrayList<Person>();
		}

		if ((personList.size() == 0)) {
			personList.add(new Person("bash", userid, userame, userimagepath));
		}
		
		setMultiAutoTextViewAdapter();
		
		if (!addedPersonList.contains(personList.get(0))) {
			addedPersonList.add(personList.get(0));
			
			if (userimagepath != null) {

				UrlImageViewHelper.setUrlDrawable( userimageview, userimagepath);
			}
			
			//searchView.addObject(personList.get(0));
		}

		searchView.setTokenListener(this);
		
		paidby.setText(personList.get(0).getName());
		
		datepickerimageview.setOnClickListener(clickListener);
		categoryimageview.setOnClickListener(clickListener);
		currencyText.setOnClickListener(clickListener);
		paidby.setOnClickListener(clickListener);
		tvContinue.setOnClickListener(clickListener);
		
		searchView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				String chrstr = s.toString();
				int commaindex = chrstr.lastIndexOf(",");
				chrstr = chrstr.substring(commaindex + 1);
				if (chrstr.trim().length() > 2) {
					getSearchUserDetails(chrstr.trim());
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		
		
	}
	private void initializeFragmentTabHost() {
		payView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout,null);
		splitView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout, null);
		receiveView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout, null);

		//mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable payViewtoSpan = new SpannableString("Pay");
		payViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) payView.findViewById(R.id.tabtext)).setText(payViewtoSpan);

		Spannable splittoSpan = new SpannableString("Split");
		splittoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) splitView.findViewById(R.id.tabtext)).setText(splittoSpan);

		Spannable reeivetoSpan = new SpannableString("Receive");
		reeivetoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 6,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) receiveView.findViewById(R.id.tabtext)).setText(reeivetoSpan);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_PAY_TAG).setIndicator(payView),
				ContainerProvider.AddTab_Pay_Container.class, null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_SPLIT_TAG).setIndicator(splitView),
				ContainerProvider.AddTab_Pay_Container.class, null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_RECEIVE_TAG).setIndicator(receiveView),
						ContainerProvider.AddTab_Pay_Container.class, null);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {

				if(mTabhost.getCurrentTabTag().toString().equals(AppConstants.TAB_PAY_TAG)){
					fragTag = 1;
					mTabhost.getTabWidget().getChildTabViewAt(0).setClickable(true);
					mTabhost.getTabWidget().getChildTabViewAt(1).setClickable(true);
					mTabhost.getTabWidget().getChildTabViewAt(2).setClickable(true);
					llPaidBy.setVisibility(View.GONE);
				}else if(mTabhost.getCurrentTabTag().toString().equals(AppConstants.TAB_SPLIT_TAG)){
					fragTag = 2;
					llPaidBy.setVisibility(View.VISIBLE);
				}else if(mTabhost.getCurrentTabTag().toString().equals(AppConstants.TAB_RECEIVE_TAG)){
					fragTag = 3;
					mTabhost.getTabWidget().getChildTabViewAt(0).setClickable(true);
					mTabhost.getTabWidget().getChildTabViewAt(1).setClickable(true);
					mTabhost.getTabWidget().getChildTabViewAt(2).setClickable(true);
					llPaidBy.setVisibility(View.GONE);
				}
				
			}
		});
	}	
	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			String text = "";

			switch (v.getId()) {

			case R.id.datepickerimageview:
				text = dateString;
				dateDialog();
				break;
			case R.id.categoryimageview:
				showCategoryAlertDialog();
				break;
			case R.id.currencyText:
				text = "currencyText";
				CurrencyActivity.delegate = AddTabActivity.this;
				Intent startIntent = new Intent(AddTabActivity.this, CurrencyActivity.class);
				startActivity(startIntent);
				break;
			case R.id.linear1:
				categorySelect = 1;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear2:
				categorySelect = 2;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear3:
				categorySelect = 3;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear4:
				categorySelect = 4;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear5:
				categorySelect = 5;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear6:
				categorySelect = 6;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear7:
				categorySelect = 7;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear8:
				categorySelect = 8;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear9:
				categorySelect = 9;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.toprightsidecontinuetext:
				if (fragTag == 1 || fragTag == 3) {
					if (validateData()) {
						showContinueAlertDialog();
					}
				} else if (fragTag == 2) {

					if (validateSplit()) {
						
						if (addedPersonList.size() > 0) {
							boolean paidbybool = false;
							
							for (Person paidPersonobj : addedPersonList) {

								if (paidPersonobj.getPaidamount() != 0.0) {
									paidbybool = true;	
								}
							}
							if (!paidbybool) {
								paidby.setText(addedPersonList.get(0).getName());
								addedPersonList.get(0).setPaidpersonbool(true);
								addedPersonList.get(0).setPaidamount(Float.valueOf(amountedittext.getText().toString().trim()));
							}
						}

						setPaidUserList();
						setReceiverList();

						Bundle bndle = new Bundle();
						bndle.putParcelableArrayList("PaidUserDetails", addedPersonList);
						bndle.putParcelableArrayList("ReceiveUserDetails", AddTabPayFragment.AddTabPayFragmentObj.photosList);
						bndle.putString("CategorySelect", "" + categorySelect);
						bndle.putString("DateString", "" + dateString);
						bndle.putString("TransationTitle", "" + transationTitle);
						bndle.putString("TransationAnount", "" + transationAnount);
						bndle.putString("CurrencyCodeString", "" + currencyCodeString);

						Intent splittabIntent = new Intent(AddTabActivity.this, AddTabSplitActivity.class);
						splittabIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						splittabIntent.putExtras(bndle);
						startActivity(splittabIntent);

					}

				}
				text = "" + "Continue Click";

				break;
			case R.id.continuesavebtn:

				sendPayTransationDetails(fragTag);

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();

				break;
			case R.id.continuesavepaybtn:
				text = "save and pay clicked";
				
				sendPayTransationDetails(fragTag);

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();
				
				
				
				break;
			case R.id.paidby:
				text = "paidby clicked";

				if (validatePaidBy()) {

					PaidbySplitActivity.delegate = AddTabActivity.this;
					Intent paidByIntent = new Intent(AddTabActivity.this, PaidbySplitActivity.class);
					paidByIntent.putExtra("TotalAmount", amountedittext.getText().toString());
					paidByIntent.putParcelableArrayListExtra("AddedPersonList", addedPersonList);
					startActivity(paidByIntent);

				}

				break;

			}
		}
	};
	@Override
	public void onTokenAdded(Object token) {
		// TODO Auto-generated method stub
		
		Person personobj = (Person) token;
		
		if(personList.contains(personobj)){
			personList.remove(personobj);
		}
		
		if (!addedPersonList.contains(personobj)) {
			addedPersonList.add(personobj);
		
			if(addedPersonList.size() > 2){
				fragTag = 2;
				mTabhost.setCurrentTab(1);
				mTabhost.getTabWidget().getChildTabViewAt(0).setClickable(false);
				mTabhost.getTabWidget().getChildTabViewAt(2).setClickable(false);
				llPaidBy.setVisibility(View.VISIBLE);
			}
			if (fragTag == 1 || fragTag == 3) {
				if (addedPersonList.size() == 1) {
					AddTabPayFragment.AddTabPayFragmentObj.showSinglePayLayout(addedPersonList.size());
				} else if (addedPersonList.size() == 2) {
					AddTabPayFragment.AddTabPayFragmentObj.showSinglePayLayout(addedPersonList.size());
				}
			}
			
			if (fragTag == 2) {
				boolean paidbybool = false;
				StringBuilder sb = new StringBuilder();
	
				if (addedPersonList.size() > 0) {
					
					AddTabPayFragment.AddTabPayFragmentObj.showSplitPayLayout();
					
					for (Person paidPersonobj : addedPersonList) {
	
						if (paidPersonobj.getPaidamount() != 0.0) {
							paidbybool = true;
							sb.append(paidPersonobj.getName());
							sb.append(" & ");
						}
					}
					if (!paidbybool) {
						paidby.setText(addedPersonList.get(0).getName());
						paidby.setText("You");
					} else {
						int sbindex = sb.lastIndexOf("&");
						sb.deleteCharAt(sbindex);
						String sbstr = (sb.toString().trim());
						paidby.setText(sbstr);
					}
				}
	
			}
		}
	}

	@Override
	public void onTokenRemoved(Object token) {
		// TODO Auto-generated method stub
		Person personobj = (Person) token;
		addedPersonList.remove(personobj);

		if (fragTag == 1 || fragTag == 3) {
			if (addedPersonList.size() == 1) {
				AddTabPayFragment.AddTabPayFragmentObj.showSinglePayLayout(addedPersonList.size());
			} else if (addedPersonList.size() == 2) {
				AddTabPayFragment.AddTabPayFragmentObj.showSinglePayLayout(addedPersonList.size());
			}
		}
		if (fragTag == 2) {
			if (addedPersonList.size() > 0) {
					AddTabPayFragment.AddTabPayFragmentObj.showSplitPayLayout();
			}
			if(addedPersonList.size()<=2){
				mTabhost.getTabWidget().getChildTabViewAt(0).setClickable(true);
				mTabhost.getTabWidget().getChildTabViewAt(2).setClickable(true);
				llPaidBy.setVisibility(View.GONE);
			}
		}
		Toast.makeText(AddTabActivity.this, "searchView :" + searchView.getText().toString(), Toast.LENGTH_LONG).show();
	}
	
	private void getSearchUserDetails(String searchText) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getSearchUserDetails", AddTabActivity.this,
						AppUrlList.ACTION_URL, new String[] { "module",
								"action", "iduser", "search_string" },
						new String[] { "transaction", "getList",
								PreferenceManager.getInstance().getUserId(),
								searchText });
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		ArrayList<PhotosListModel> photosList1 = new ArrayList<PhotosListModel>();
		Bundle bndle = new Bundle();
		bndle.putParcelableArrayList("PaidUserDetails", addedPersonList);
		
		bndle.putString("CategorySelect", "" + categorySelect);
		bndle.putString("DateString", "" + dateString);
		bndle.putString("TransationTitle", "" + transationTitle);
		bndle.putString("TransationAnount", "" + transationAnount);
		bndle.putString("CurrencyCodeString", "" + currencyCodeString);
		
		if (from.equalsIgnoreCase("getSearchUserDetails")) {
			if (output != null && output.length() > 0) {
				try {
					Gson gson = new Gson();

					JSONObject baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					if (result) {

						JSONArray resultsArray = baseJsonObject.getJSONArray("results");

						Type collectionType = new TypeToken<ArrayList<Person>>() {}.getType();
						personList.clear();
						ArrayList<Person> detailList = gson.fromJson(resultsArray.toString(), collectionType);

						personList.addAll(detailList);

						setMultiAutoTextViewAdapter();

						try {
							searchView.showDropDown();
						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						adapter.notifyDataSetInvalidated();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		if (from.equalsIgnoreCase("PayTransationDetails")) {
			if (output != null && output.length() > 0) {
				JSONObject baseJsonObject;
				try {
					baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					String msgStr = baseJsonObject.getString("msg");
					if (result) {
						
						transId = Integer.parseInt(baseJsonObject.getString("idtrans"));
						Toast.makeText(AddTabActivity.this, msgStr, Toast.LENGTH_SHORT).show();
						
						Person person = addedPersonList.get(1);
						photosList1.add(new PhotosListModel(person.getUserType(),
								person.getId(), person.getImage_location(), person.getName()));
						bndle.putParcelableArrayList("ReceiveUserDetails",photosList1);

						Intent socialIntent = new Intent(AddTabActivity.this, SocialPageActivity.class);
						socialIntent.putExtra("transationdetails", bndle);
						socialIntent.putExtra("transID", transId);
						socialIntent.putExtra("postType", "pay");
						
						socialIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(socialIntent);
					} else {
						Toast.makeText(AddTabActivity.this, msgStr, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		if (from.equalsIgnoreCase("ReceiveTransationDetails")) {
			if (output != null && output.length() > 0) {
				JSONObject baseJsonObject;
				try {
					baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					String msgStr = baseJsonObject.getString("msg");
					if (result) {
						
						transId = Integer.parseInt(baseJsonObject.getString("idtrans"));
						Toast.makeText(AddTabActivity.this, msgStr, Toast.LENGTH_SHORT).show();
						
						Person person = addedPersonList.get(0);
						photosList1.add(new PhotosListModel(person.getUserType(),
								person.getId(), person.getImage_location(), person.getName()));
						bndle.putParcelableArrayList("ReceiveUserDetails",photosList1);
									
						Intent socialIntent = new Intent(AddTabActivity.this, SocialPageActivity.class);
						socialIntent.putExtra("transationdetails", bndle);
						socialIntent.putExtra("transID", transId);
						socialIntent.putExtra("postType", "pay");
						
						socialIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(socialIntent);
						
					} else {
						Toast.makeText(AddTabActivity.this, msgStr,
								Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	protected void showCategoryAlertDialog() {
		// TODO Auto-generated method stub

		Context context = AddTabActivity.this;
		LinearLayout linear1, linear2, linear3, linear4, linear5, linear6, linear7, linear8, linear9;

		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_categories_picker, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		catedialog = alertDialogBuilder.create();
		catedialog.show();
		linear1 = (LinearLayout) view.findViewById(R.id.linear1);
		linear2 = (LinearLayout) view.findViewById(R.id.linear2);
		linear3 = (LinearLayout) view.findViewById(R.id.linear3);
		linear4 = (LinearLayout) view.findViewById(R.id.linear4);
		linear5 = (LinearLayout) view.findViewById(R.id.linear5);
		linear6 = (LinearLayout) view.findViewById(R.id.linear6);
		linear7 = (LinearLayout) view.findViewById(R.id.linear7);
		linear8 = (LinearLayout) view.findViewById(R.id.linear8);
		linear9 = (LinearLayout) view.findViewById(R.id.linear9);

		if (categorySelect == 1) {
			linear1.setPressed(true);
			linear9.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 2) {
			linear2.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 3) {
			linear3.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 4) {
			linear4.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 5) {
			linear5.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 6) {
			linear6.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 7) {
			linear7.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 8) {
			linear8.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
		} else if (categorySelect == 9) {
			linear9.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		}

		linear1.setOnClickListener(clickListener);
		linear2.setOnClickListener(clickListener);
		linear3.setOnClickListener(clickListener);
		linear4.setOnClickListener(clickListener);
		linear5.setOnClickListener(clickListener);
		linear6.setOnClickListener(clickListener);
		linear7.setOnClickListener(clickListener);
		linear8.setOnClickListener(clickListener);
		linear9.setOnClickListener(clickListener);

	}
	
	public void dateDialog1() {
		// TODO Auto-generated method stub
		int day, month, year;
		Calendar cal = Calendar.getInstance();
		day = cal.get(Calendar.DAY_OF_MONTH);
		month = cal.get(Calendar.MONTH);
		year = cal.get(Calendar.YEAR);
		final DateSetListener _datePickerDialogCallback = new DateSetListener();
		final DatePickerDialog dpd = new DatePickerDialog(AddTabActivity.this,
				_datePickerDialogCallback, year, month, day);

		dpd.setButton(DialogInterface.BUTTON_POSITIVE, "SET",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							dateSet = true;
							DatePicker datePicker = dpd.getDatePicker();
							_datePickerDialogCallback.onDateSet(datePicker,
									datePicker.getYear(),
									datePicker.getMonth(),
									datePicker.getDayOfMonth());
						}
					}
				});

		dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_NEGATIVE) {
							dateSet = false;
							dpd.hide();
						}
					}
				});
		dpd.show();
	}
	
	public void dateDialog() {
		
		 LayoutInflater inflater = (LayoutInflater) getLayoutInflater();
		 final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		 View customView = inflater.inflate(R.layout.custom_date_picker, null);
		 dialogBuilder.setView(customView);
		 final Calendar now = Calendar.getInstance();
		 final DatePicker datePicker = (DatePicker) customView.findViewById(R.id.dialog_datepicker);
		 final TextView okDatePicker = (TextView) customView.findViewById(R.id.okbtn);
		 final TextView dateTextView = (TextView) customView.findViewById(R.id.dialog_dateview);
	        /*final SimpleDateFormat dateViewFormatter = 
	            new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
	        final SimpleDateFormat formatter = 
	            new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);*/
	        // Minimum date
		 Calendar minDate = Calendar.getInstance();
		 try {
			 minDate.setTime(dateViewFormatter.parse("2010-12-12"));
		 } catch (ParseException e) {
	            e.printStackTrace();
	     }
	        datePicker.setMinDate(minDate.getTimeInMillis());
	        // View settings
	     //   dialogBuilder.setTitle("Choose a date");
	        Calendar choosenDate = Calendar.getInstance();
	        int year = choosenDate.get(Calendar.YEAR);
	        int month = choosenDate.get(Calendar.MONTH);
	        int day = choosenDate.get(Calendar.DAY_OF_MONTH);
	        try {
//	            Date choosenDateFromUI = formatter.parse(
//	                datePickerShowDialogButton.getText().toString()
//	            );
//	            choosenDate.setTime(choosenDateFromUI);
	            year = choosenDate.get(Calendar.YEAR);
	            month = choosenDate.get(Calendar.MONTH);
	            day = choosenDate.get(Calendar.DAY_OF_MONTH);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        Calendar dateToDisplay = Calendar.getInstance();
	        dateToDisplay.set(year, month, day);
	        dateTextView.setText(dateViewFormatter.format(dateToDisplay.getTime()));
	        // Buttons
	        
	        okDatePicker.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 Calendar choosen = Calendar.getInstance();
	                    choosen.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
//	                    datePickerShowDialogButton.setText(
//	                        dateViewFormatter.format(choosen.getTime())
//	                    );
	                    dateString = dateViewFormatter.format(choosen.getTime());
	                    Toast.makeText(AddTabActivity.this, dateString, Toast.LENGTH_SHORT).show();
	                    dialog.dismiss();
				}
			});

	
	        dialog = dialogBuilder.create();
	        // Initialize datepicker in dialog atepicker
	        datePicker.init(year, month, day, 
	            new DatePicker.OnDateChangedListener() {
	                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	                    Calendar choosenDate = Calendar.getInstance();
	                    choosenDate.set(year, monthOfYear, dayOfMonth);
	                    dateTextView.setText(dateViewFormatter.format(choosenDate.getTime()));
	                    Toast.makeText(AddTabActivity.this, dateTextView.getText().toString(), Toast.LENGTH_SHORT).show();
	                }
	            }
	        );
	        // Finish
	        dialog.show();
	}

	private class DateSetListener implements DatePickerDialog.OnDateSetListener {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (dateSet) {
				dateString = year + "-" + (1 + monthOfYear) + "-" + dayOfMonth;
			}
		}
	}

	@Override
	public void onActivityData(String output) {
		// TODO Auto-generated method stub
		currencyCodeString = output.trim();
		Toast.makeText(AddTabActivity.this, output, Toast.LENGTH_SHORT).show();
	}
	
	private boolean validateData() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (TextUtils.isEmpty(transationTitle)) {
			titleedittext.setError("Field is Empty");
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(AddTabActivity.this, "Please add transation client", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (TextUtils.isEmpty(dateString)) {
			Toast.makeText(AddTabActivity.this, "Please select a date", Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private void sendPayTransationDetails(int fragmentint) {

		if (fragmentint == 1) {
			myAsyncTask = new MyAsynTaskManager();
			myAsyncTask.delegate = this;
			myAsyncTask.setupParamsAndUrl("PayTransationDetails",
					AddTabActivity.this, AppUrlList.ACTION_URL, new String[] {
							"module", "action", "payer_iduser",
							"receiver_user_type", "receiver_user_id",
							"transaction_currency", "transaction_amount",
							"transaction_category", "transaction_date",
							"transaction_title" }, new String[] {
							"transaction", "insertPayTransaction", userid,
							""+BashApplication.userTypeHash.get(addedPersonList.get(1).getUserType()), addedPersonList.get(1).getId(),
							currencyCodeString, transationAnount,
							"" + categorySelect, dateString, transationTitle });
			myAsyncTask.execute();
		} else if (fragmentint == 3) {
			myAsyncTask = new MyAsynTaskManager();
			myAsyncTask.delegate = AddTabActivity.this;
			myAsyncTask.setupParamsAndUrl("ReceiveTransationDetails",
					AddTabActivity.this, AppUrlList.ACTION_URL, new String[] {
							"module", "action", "receiver_iduser",
							"payer_user_type", "payer_user_id",
							"transaction_currency", "transaction_amount",
							"transaction_category", "transaction_date",
							"transaction_title" }, new String[] {
							"transaction", "insertReceiveTransaction", userid,
							""+BashApplication.userTypeHash.get(addedPersonList.get(1).getUserType()), addedPersonList.get(1).getId(),
							currencyCodeString, transationAnount,
							"" + categorySelect, dateString, transationTitle });
			myAsyncTask.execute();
		}
	}

	protected void showContinueAlertDialog() {
		// TODO Auto-generated method stub

		Context context = AddTabActivity.this;

		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.continue_save_dialog, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		continuedialog = alertDialogBuilder.create();
		continuedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = continuedialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.TOP | Gravity.RIGHT;
		wmlp.x = 10; // x position
		wmlp.y = 10; // y position
		continuedialog.show();
		btnSave = (Button) view.findViewById(R.id.continuesavebtn);
		btnSavePay = (Button) view.findViewById(R.id.continuesavepaybtn);
		if(fragTag == 3){
			btnSavePay.setVisibility(View.GONE);
		}else{
			btnSavePay.setVisibility(View.VISIBLE);
		}

		btnSave.setOnClickListener(clickListener);
		btnSavePay.setOnClickListener(clickListener);

	}

	private boolean validatePaidBy() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(AddTabActivity.this, "Please add transation client", Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private boolean validateSplit() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		boolean imageSelectBool = false;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		for (PhotosListModel photolistobj : AddTabPayFragment.AddTabPayFragmentObj.horizontalPhotoAdapter.getAllPhotosList()) {
			if (photolistobj.isImageSelect()) {
				imageSelectBool = true;
			}
		}
		if (!imageSelectBool) {
			Toast.makeText(AddTabActivity.this, "Please select your friends", Toast.LENGTH_SHORT).show();
			return false;
		}

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (TextUtils.isEmpty(transationTitle)) {
			titleedittext.setError("Field is Empty");
			return false;
		}
		if (TextUtils.isEmpty(dateString)) {
			Toast.makeText(AddTabActivity.this, "Please select a date", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(AddTabActivity.this, "Please add transation client", Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private void setMultiAutoTextViewAdapter() {

		adapter = new FilteredArrayAdapter<Person>(AddTabActivity.this,
				R.layout.person_layout, personList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {

					LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
					convertView = l.inflate(R.layout.person_layout, parent, false);
				}

				Person p = getItem(position);
				((TextView) convertView.findViewById(R.id.personnametext)).setText(p.getName());

				if (p.getImage_location().length() > 0) {

					UrlImageViewHelper.setUrlDrawable((CircularImageView) convertView.findViewById(R.id.personimageview), 
							p.getImage_location());
				}

				return convertView;
			}

			@Override
			protected boolean keepObject(Person person, String mask) {
				mask = mask.toLowerCase();
				return person.getName().toLowerCase().startsWith(mask);
			}
		};
		searchView.setAdapter(adapter);
	}

	@Override
	public void onPaidByData(ArrayList<Person> output) {
		// TODO Auto-generated method stub
		addedPersonList = output;
		Toast.makeText(AddTabActivity.this, "PaidbySuccess", Toast.LENGTH_SHORT).show();
		setPaidUserList();

	}

	private void setPaidUserList() {
		boolean paidbybool = false;
		StringBuilder sb = new StringBuilder();

		if (addedPersonList.size() > 0) {
			for (Person paidPersonobj : addedPersonList) {

				if (paidPersonobj.isPaidpersonbool()) {
					paidbybool = true;

					sb.append(paidPersonobj.getName());
					sb.append(" & ");
				}
			}
			if (!paidbybool) {
				paidby.setText(addedPersonList.get(0).getName());
				addedPersonList.get(0).setPaidpersonbool(true);
			} else {
				int sbindex = sb.lastIndexOf("&");
				sb.deleteCharAt(sbindex);
				String sbstr = (sb.toString().trim());
				paidby.setText(sbstr);
			}
		}
	}

	private void setReceiverList() {
		PhotosListModel photosListObj = null;

		Iterator<PhotosListModel> photoIterator = AddTabPayFragment.AddTabPayFragmentObj.photosList.iterator();
		while (photoIterator.hasNext()) {

			photosListObj = photoIterator.next();
			if (!photosListObj.isImageSelect()) {
				photoIterator.remove();
			}
		}

	}

	public void setUpTopBarFields(int leftSideImage, String endText) {
		((ImageView) findViewById(R.id.topleftsidebackimage)).setImageResource(leftSideImage);
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText(endText);
	}
	
}
