package com.bash.Activities;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Fragments.CreateGroup_Fragment;
import com.bash.Fragments.InviteFriends_Fragment;
import com.bash.Fragments.MyGroupsFragment;
import com.bash.Fragments.MyTrans_Pending_Fragment;
import com.bash.Fragments.NewsFeed_Fragment.MyFeedAdapter;
import com.bash.Fragments.NewsFeed_Fragment.MyFeedAdapter.ViewHolder;
import com.bash.GsonClasses.Friends_Class;
import com.bash.ListModels.GroupTransactionDetail_Model;
import com.bash.ListModels.Group_Model;
import com.bash.ListModels.NewsFeed_Model;
import com.bash.ListModels.NewsfeedImageInformation_model;
import com.bash.ListModels.NewsfeedImage_Model;
import com.bash.ListModels.NewsfeedLikeInformation_Model;
import com.bash.ListModels.NewsfeedLikes_Model;
import com.bash.ListModels.NewsfeedPaidforInformation_Model;
import com.bash.ListModels.NewsfeedPaidfor_model;
import com.bash.ListModels.NewsfeedPayersInformation_Model;
import com.bash.ListModels.NewsfeedPayers_model;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Receivers;
import com.bash.ListModels.Transactions_information;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.Singleton;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GroupDetailActivity extends Activity implements AsyncResponse,
		OnClickListener {
	private Activity mActivity;
	private Context mContext;
	View view;
	int pos = 0;
	
	MyAsynTaskManager myAsyncTask;
	View mRootView, addgroupAlertView;
	public static SwipeListView myGroupListView;
	ListView myfeedListView;
	Dialog addmemberDialog;
	MyGroupsAdapter adapter;
	
	Friends_Class responseForFriends;
	public ArrayList<Transactions_information> feedList1, feedlistNew;
	public ArrayList<GroupTransactionDetail_Model> feedList = new ArrayList<GroupTransactionDetail_Model>();
	
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView iviProfileImage, fileImage, iviBack, iviOption;
	private TextView tviProfilename;
	String idGroup, picturePath = "",group_member_id, groupName, image_location;
	RelativeLayout rlProfileImageLayout, rlGroupList;
	
	private FloatingActionMenu fab;
	private FloatingActionButton fab1, fab2, fab3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.group_detail);
		Bundle extras = getIntent().getExtras();
		idGroup = extras.getString("idGroup");
		
		initializeView();
		
		fab = (FloatingActionMenu) findViewById(R.id.menu1);
	 	fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);
        fab3.setOnClickListener(clickListener);  
        
        fab.hideMenuButton(false);
        fab.setClosedOnTouchOutside(true);
        fab.showMenuButton(true);
        fab.setIconAnimated(true);
		
		iviBack.setOnClickListener(this);
		iviOption.setOnClickListener(this);
		groupName = extras.getString("profileName");
		tviProfilename.setText(groupName);
		
		
		iviProfileImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				initializePictureAlertView();
			}
		});
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		mContext = GroupDetailActivity.this;
		mActivity = GroupDetailActivity.this;
		iviBack = (ImageView) findViewById(R.id.iviBack);
		iviOption = (ImageView) findViewById(R.id.iviOption);
		iviProfileImage = (ImageView) findViewById(R.id.iviProfileImage);
		tviProfilename=(TextView)findViewById(R.id.tviProfilename);
		
		rlProfileImageLayout = (RelativeLayout) findViewById(R.id.rlProfileImageLayout);
		rlGroupList = (RelativeLayout) findViewById(R.id.rlGroupList);
		
		myGroupListView = (SwipeListView) findViewById(R.id.groupListView);
		feedList1 = new ArrayList<Transactions_information>();
		adapter = new MyGroupsAdapter(mActivity, feedList);
		myGroupListView.setAdapter(adapter);
		

		getGroupDetailList();

		myGroupListView
				.setSwipeListViewListener(new BaseSwipeListViewListener() {
					@Override
					public void onClickFrontView(int position) {
						
					}

					@Override
					public void onClickBackView(int position) {
						Log.d("swipe",
								String.format("onClickBackView %d", position));
						myGroupListView.closeAnimate(position);// when you touch
																// back view it
																// will close
					}

					@Override
					public void onDismiss(int[] reverseSortedPositions) {
					}
				});

		myGroupListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there
																		// are
																		// five
																		// swiping
																		// modes
		myGroupListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); // there
																				// are
																				// four
																				// swipe
																				// actions
		myGroupListView.setOffsetLeft(PreferenceManager.getInstance()
				.getPercentageFromWidth(38));
		myGroupListView.setAnimationTime(50); // animarion time
		myGroupListView.setSwipeOpenOnLongPress(true); // enable or disable
														// SwipeOpenOnLongPress

		
	}

	
	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(mContext,
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(mContext);
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);

		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						startActivityForResult(i, AppConstants.CODE_GALLERY);
					}
				});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getContentResolver().insert(
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
								values);
						Intent intent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								mCapturedImageURI);
						startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});

	}

	public void getGroupDetailList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupDetailList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group", "getAllGroupDetails",
						idGroup, PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
		
	}

	public void createGroupInServer(final String groupName) {

		MyAsynImageTaskManager
				.setupParamsAndUrl(AppUrlList.ACTION_URL,
						new String[] { "module", "action", "iduser",
								"groupname", "groupimage" }, new String[] {
								"group", "add",
								PreferenceManager.getInstance().getUserId(),
								groupName, picturePath }, new String[] { "0",
								"0", "0", "0", "1" });

		new MyAsynImageTaskManager(mActivity,
				new MyAsynImageTaskManager.LoadListener() {

					@Override
					public void onLoadComplete(final String jsonResponse) {

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								try {
									if (jsonResponse == null) {
										Log.e("Null", "retured");
									}
									
									JSONObject rootObj = new JSONObject(
											jsonResponse);
									if (rootObj.getBoolean("result")) {
										
										adapter.notifyWithDataSet(feedList);
										addmemberDialog.dismiss();
										DialogManager.showDialog(mActivity,
												"Group Created Successfully!");

										
									} else {
										if (rootObj.getString("msg").equals(
												"Group Name exist"))
											DialogManager
													.showDialog(mActivity,
															"Group Name is Already Exist!");
										else
											DialogManager
													.showDialog(mActivity,
															"Error In Creating New Group! Try Again!");
									}
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
									DialogManager.showDialog(mActivity,
											"Server Error Occured! Try Again!");
								}
							}
						});

					}

					@Override
					public void onError(final String errorMessage) {
						runOnUiThread(new Runnable() {
							public void run() {
								DialogManager.showDialog(mActivity,
										errorMessage);
							}
						});

					}
				}).execute();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == mActivity.RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						fileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == mActivity.RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						fileImage);
			}
			break;
		default:
			break;
		}

	}

	public class MyGroupsAdapter extends BaseAdapter
	{
		 	public ArrayList<GroupTransactionDetail_Model> feedList = new ArrayList<GroupTransactionDetail_Model>();
		 	public Activity context;
		 	public LinearLayout.LayoutParams backViewParams;
		    
		    public MyGroupsAdapter(Activity context, ArrayList<GroupTransactionDetail_Model> feedList) {
		        this.context = context;
		    	this.feedList = feedList;
		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
		    			LayoutParams.MATCH_PARENT);
		    }
		    
		    public void notifyWithDataSet(ArrayList<GroupTransactionDetail_Model> newlist){
		    	this.feedList.clear();
		    	this.feedList = newlist;
		    	Log.e("Size of Adapter", String.valueOf(feedList.size()));
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        GroupTransactionDetail_Model listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	if(convertView == null)
		        	 {
		        	  	convertView = inflater.inflate(R.layout.custom_myprofile_listview, null);
			            holder = new ViewHolder();
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.tviTime = (TextView) convertView.findViewById(R.id.tviTime);
			            holder.tviAmount = (TextView) convertView.findViewById(R.id.tviAmount);
			            holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
			            holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
			            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
			            holder.tviDetail = (TextView) convertView.findViewById(R.id.tviDetail);
			            holder.tviDelete = (TextView) convertView.findViewById(R.id.tviDelete);
		        	}
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		      
		        	
		        	holder.backView.setLayoutParams(backViewParams);
		        	if(listItem.getTransaction_title().length() <= 20){
		        		holder.friendName.setText(listItem.getTransaction_title());
		        	} else if(listItem.getTransaction_title().length() > 20){
		        		holder.friendName.setText(listItem.getTransaction_title().substring(0,16) + " ...");
		        	}
		        	//holder.friendName.setText(listItem.getTransaction_title());
		        	String timeText = Utils.getFormattedTimerText(listItem.getDatetime());
					holder.tviTime.setText(timeText);
		        	
		        	 if(listItem.getAmountType() != null && listItem.getAmountType().equals("pay")){
		        		 holder.tviAmount.setVisibility(View.VISIBLE);
				        	if(listItem.getAmount().equals("0")){
				        		holder.tviAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
				        		holder.tviAmount.setText(" Settled ");
				        	}else{
				        		holder.tviAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
				        		holder.tviAmount.setText(" + " + listItem.getAmount());
				        	}
				        	
				        }else if(listItem.getAmountType() != null && listItem.getAmountType().equals("receive")){
				        	holder.tviAmount.setVisibility(View.VISIBLE);
				        	if(listItem.getAmount().equals("0")){
				        		holder.tviAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
				        		holder.tviAmount.setText(" Settled ");
				        	}else{
				        		holder.tviAmount.setBackgroundResource(R.drawable.amount_displayred_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
				        		holder.tviAmount.setText(" - " + listItem.getAmount());
				        	}
				        	
					    } else {
					    	holder.tviAmount.setVisibility(View.INVISIBLE);
					    }
				        
				        if(listItem.getImage_location() != null && listItem.getImage_location().length()!= 0 && holder.friendImageSource != null){
				        	ImageLoader.getInstance().displayImage(listItem.getImage_location(), 
				        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
				        }else{
				        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
				        }
		        
		        	holder.deleteFriend.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							pos=position;
							// TODO Auto-generated method stub
							//removeFriendFromWebservice(pos);
							/*removeItemFromList(position);
							MyFriendsFragment.friendsListView.closeAnimate(position);*/
						}
					});
		        	holder.friendName.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						}
					});
		        	holder.tviDetail.setOnClickListener(new OnClickListener() {
		 				@Override
		 				public void onClick(View v) {
		 					// TODO Auto-generated method stub
		 					pos=position;
		 					//Toast.makeText(context, "Charge will Come soon...", Toast.LENGTH_LONG).show();
		 					Intent transectionDetail = new Intent(mContext, TransectionDetailActivity.class);
		 					transectionDetail.putExtra("group_member_id", group_member_id);
		 					Singleton.getInstance().listItem = getItem(pos);
		 					startActivity(transectionDetail);
		 				}
		 			});
		 	      holder.tviDelete.setOnClickListener(new OnClickListener() {
		 				@Override
		 				public void onClick(View v) {
		 					// TODO Auto-generated method stub
		 					pos=position;
		 					Toast.makeText(context, "Pay will Come soon...", Toast.LENGTH_LONG).show();
		 				}
		 			});
		        
		        
		        return convertView;
		    }

		   
		    @Override
		    public int getCount() {
		    	//	checkEmptyList();
		        return feedList.size();
		    }

		    @Override
		    public GroupTransactionDetail_Model getItem(int position) {
		        return feedList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }

		    private class ViewHolder {
		        TextView friendName, tviTime, tviAmount, tviDetail, tviDelete;
		        ImageView friendImageSource, deleteFriend;
		        RelativeLayout backView;
		    }
		  
	}
	

	public void removeGroupFromServer(final String groupId, final int poistion) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		Log.e("Group Name", groupId);
		myAsyncTask.setupParamsAndUrl("removeGroupFromServer", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group",
						"remove", groupId,
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		
		
		String idtrans, date, transaction_title, datetime, timestamp, amount = null, amountType = null;
		if (from.equalsIgnoreCase("getGroupDetailList")) {
			if (output != null) {
				try {
					
					JSONObject jObj=new JSONObject(output);
					if(jObj.getBoolean("result")){
						//ArrayList<Transactions_information> transaction=new ArrayList<Transactions_information>();
						group_member_id = jObj.getString("group_member_id");
						
						JSONObject jObjGroupInfo = jObj.getJSONObject("group_information");
						
						groupName = jObjGroupInfo.getString("groupname");
						image_location = jObjGroupInfo.getString("imagepath");
						
						if(image_location != null && image_location.length()!= 0 && iviProfileImage != null){
				        	ImageLoader.getInstance().displayImage(image_location, 
				        			iviProfileImage, BashApplication.options, BashApplication.animateFirstListener);
				        }
						
						JSONArray transArray=jObj.getJSONArray("transactions_information");
						//for(int i=0;i<transArray.length();i++){
						for(int i=0;i<transArray.length();i++){
							JSONObject res=transArray.getJSONObject(i);
							
							idtrans = res.getString("idtrans"); 
							date = res.getString("date"); 
							transaction_title = res.getString("transaction_title");
							datetime = res.getString("datetime");
							timestamp = res.getString("timestamp");
							
							ArrayList<Payers> payerslist = new ArrayList<Payers>();
							ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
							
							ArrayList<Group_Model> group_information = new ArrayList<Group_Model>();
							ArrayList<Group_Model> group_information_recever = new ArrayList<Group_Model>();
							
							JSONObject payerObj=res.getJSONObject("payers");
							JSONObject rcvrObj=res.getJSONObject("receivers");
							
							int totalTranAmount = Integer.parseInt(res.getString("total_amount"));
							
							
							JSONArray rcvrArray=rcvrObj.getJSONArray("receivers_information");
							if(payerObj.getBoolean("result")){
								JSONArray payerArray=payerObj.getJSONArray("payers_information");
								for(int p=0;p<payerArray.length();p++){
									JSONObject payerRes=payerArray.getJSONObject(p);
									if(payerRes.getString("iduser").equals(group_member_id)){
										if(payerRes.getString("payment_type").equals("0")){
											int amtpaid = Integer.parseInt(payerRes.getString("amount"));
											int hadtopay = Integer.parseInt(payerRes.getString("had_to_pay"));
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
										} else if(payerRes.getString("payment_type").equals("1")){
											int ratio=0,ratioAmount;
											for(int pay=0;pay<payerslist.size();pay++){
							    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
							    			}
							    			for(int rec=0;rec<receiverslist.size();rec++){
							    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
							    			}
							    			ratioAmount=totalTranAmount/ratio;
							    			
							    			int amtpaid = Integer.parseInt(payerRes.getString("amount"));
											int hadtopay = ratioAmount * Integer.parseInt(payerRes.getString("had_to_pay"));
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
							    			
										} else if(payerRes.getString("payment_type").equals("2")){
											
											int amtpaid = Integer.parseInt(payerRes.getString("amount"));
											int hadtopay = (totalTranAmount * Integer.parseInt(payerRes.getString("had_to_pay")))/100;
											int amt = 0;
											if(amtpaid>hadtopay){
							    				amt = amtpaid - hadtopay;
							    				amountType = "pay";
							    			}else{
							    				amt = hadtopay - amtpaid;
							    				amountType = "receive";
							    			}
											
											amount = String.valueOf(amt);
											
										}
										//amount = "";
										
									}
									
									JSONObject payerGroup = payerRes.getJSONObject("group");
									group_information.clear();
									if(payerGroup.getBoolean("result")){
										JSONObject payerGroupinformation = payerGroup.getJSONObject("group_information");
										group_information.add(new Group_Model(payerGroupinformation.getString("idgroup"), payerGroupinformation.getString("groupname"), payerGroupinformation.getString("image_location")));
									}
									payerslist.add(new Payers(payerRes.getString("full_name"), payerRes.getString("image_location"), payerRes.getString("idpayer"), payerRes.getString("idtrans"), payerRes.getString("user_type"), payerRes.getString("iduser"), payerRes.getString("amount"), payerRes.getString("payment_type"), payerRes.getString("had_to_pay"), group_information));
								}
							}
							if(rcvrObj.getBoolean("result")){
								for(int r=0; r<rcvrArray.length(); r++){
									JSONObject rcvrRes = rcvrArray.getJSONObject(r);
									/*JSONObject rcvrGroup = rcvrRes.getJSONObject("group");
									group_information_recever.clear();
									if(rcvrGroup.getBoolean("result")){
										JSONObject rcvrGroupinformation = rcvrGroup.getJSONObject("group_information");
										group_information_recever.add(new Group_Model(rcvrGroupinformation.getString("idgroup"), rcvrGroupinformation.getString("groupname"), rcvrGroupinformation.getString("image_location")));
									}*/
									if(rcvrRes.getString("iduser").equals(group_member_id)){
										if(rcvrRes.getString("payment_type").equals("0")){
											int amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
										} else if(rcvrRes.getString("payment_type").equals("1")){
											int ratio=0,ratioAmount;
											for(int pay=0;pay<payerslist.size();pay++){
							    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
							    			}
							    			for(int rec=0;rec<receiverslist.size();rec++){
							    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
							    			}
							    			ratioAmount=totalTranAmount/ratio;
							    			
							    			int amtpaid = ratioAmount * Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
							    			
										} else if(rcvrRes.getString("payment_type").equals("2")){
											
											int amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
											
											amount = String.valueOf(amtpaid);
											amountType = "receive";
										}
										//amount = "";
										
									}
									
									receiverslist.add(new Receivers(rcvrRes.getString("full_name"), rcvrRes.getString("image_location"), rcvrRes.getString("idreceiver"), rcvrRes.getString("idtrans"), rcvrRes.getString("user_type"), rcvrRes.getString("iduser"), rcvrRes.getString("amount"), rcvrRes.getString("payment_type"), group_information_recever));							
								}
							}
							Collections.sort(payerslist);
							Collections.sort(receiverslist);
							feedList1.add(new Transactions_information(res.getString("idtrans"),res.getString("trans_type"),res.getString("entered_by_iduser"),
									res.getString("currency"),res.getString("total_amount"),"", res.getString("category"),res.getString("date"),res.getString("transaction_title"),
									res.getString("datetime"),res.getString("timestamp"),res.getString("status"),res.getString("entered_by_full_name"), payerslist,receiverslist));
							feedList.add(new GroupTransactionDetail_Model(idtrans, "", groupName, res.getString("entered_by_full_name"), image_location, res.getString("currency"), date, transaction_title, datetime, timestamp, amount, res.getString("total_amount"), amountType, payerslist, receiverslist));
						}

							adapter.notifyDataSetChanged();
					
					
				}else{
					//((RelativeLayout) mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
					DialogManager.showDialog(mActivity, "No Transaction Found!");
					//updateAdapter(newList);
				}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("removeGroupFromServer")) {
			if (output != null) {

				try {
					if (output == null) {
						Log.e("Null", "retured");
					}
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						removeItemFromList(pos);
						DialogManager.showDialog(mActivity,
								"Group Deleted Successfully!");

					} else {
						DialogManager.showDialog(mActivity,
								"Error in Delete Group! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		}  
	}
	public void updateAdapter(ArrayList<GroupTransactionDetail_Model> newList){
		/*feedList.clear();
		feedList = DataBaseManager.getInstance().getFriendsList();*/
		//this.feedList = newList;
		//feedList = DataBaseManager.getInstance().getFriendsList();
		if(newList != null && newList.size() != 0){
			adapter.notifyWithDataSet(newList);
		} 
	}
	public void removeItemFromList(int position) {
		MyGroupsFragment.myGroupListView.closeAnimate(position);
		feedList.remove(position);
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviBack:
			mActivity.finish();
			break;
		case R.id.iviOption:
			openOptionsMenu();
			/*final CharSequence[] items = {"Red", "Green", "Blue"};

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			//builder.setTitle("Pick a color");
			builder.setItems(items, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int item) {
			        Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
			    }
			});
			AlertDialog alert = builder.create();
			alert.show();*/
			break;

		default:
			break;
		}
	}
	public void getNewsFeedListByType(String type) {
		// getlatest
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupFeed", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup" }, new String[] { "post", type, idGroup});

		myAsyncTask.execute();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		myfeedListView.setVisibility(View.GONE);
		rlProfileImageLayout.setVisibility(View.VISIBLE);
		rlGroupList.setVisibility(View.VISIBLE);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	MenuInflater inflater = getMenuInflater();
	inflater.inflate(R.menu.group_menu, menu);
	return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.itemSettings:
		    // do part 1
			Intent profileIntent = new Intent(mContext, GroupSettingsActivity.class);
			profileIntent.putExtra("idGroup", idGroup);
			profileIntent.putExtra("profileName", groupName);
			profileIntent.putExtra("image", image_location);
			startActivity(profileIntent);
		    return true;
		case R.id.itemViewFeed:
		    // do part 2
			Intent feedIntent = new Intent(mContext, ViewFeedActivity.class);
			feedIntent.putExtra("groupName", groupName);
			feedIntent.putExtra("idGroup", idGroup);
			feedIntent.putExtra("type", "group");
			startActivity(feedIntent);
		    return true;
		case R.id.itemSummary:
		    // do part 3
			Intent summaryIntent = new Intent(mContext, Summary_Activity.class);
			summaryIntent.putExtra("groupName", groupName);
			summaryIntent.putExtra("idGroup", idGroup);
			startActivity(summaryIntent);
		    return true;
		case R.id.itemExit:
		    // do part 3
			Toast.makeText(mContext, "Fourth Item Click", Toast.LENGTH_LONG).show();
		    return true;
		default:
		    return super.onOptionsItemSelected(item);
		}
	}
	
	private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.menu1:         	
                break;
                case R.id.fab1:
                	//((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Fragment(), true);
                    break;
                case R.id.fab2:
                	//((BaseFragment)getParentFragment()).replaceFragment(new InviteFriends_Fragment(), true);
    				//((SlidingActivity)getActivity()).currentFragment = MyTrans_Pending_Fragment.this;
                    break;
                case R.id.fab3:
                    startActivity(new Intent(mContext, AddTabActivity.class));
                    break;
            }  	
        }
    };
	
}
