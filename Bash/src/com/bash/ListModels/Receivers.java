package com.bash.ListModels;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Receivers implements Comparable{
	@SerializedName("full_name")
	public String full_name;
	@SerializedName("image_location")
	public String image_location;
	@SerializedName("idreceiver")
	public String idreceiver;
	@SerializedName("idtrans")
	public String idtrans;
	@SerializedName("user_type")
	public String user_type;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("payment_type")
	public String payment_type;
	@SerializedName("amount")
	public String amount;
	@SerializedName("group_information")
	public ArrayList<Group_Model> group_information;

	public String getFull_name() {
		return this.full_name;
	}
	
	public String getImage_location() {
		return this.image_location;
	}

	public String getIdreceiver() {
		return this.idreceiver;
	}

	public String getIdtrans() {
		return this.idtrans;
	}

	public String getUser_type() {
		return this.user_type;
	}

	public String getIduser() {
		return this.iduser;
	}

	public String getAmount() {
		return this.amount;
	}

	public String getPayment_type() {
		return this.payment_type;
	}
	
	public ArrayList<Group_Model> getGroupInformation() {
		return this.group_information;
	}

	public Receivers(String full_name, String image_location, String idreceiver,
			String idtrans, String user_type, String iduser,
			String amount, String payment_type, ArrayList<Group_Model> group_information) {
		this.full_name = full_name;
		this.image_location = image_location;
		this.idreceiver = idreceiver;
		this.idtrans = idtrans;
		this.user_type = user_type;
		this.iduser = iduser;
		this.amount = amount;
		this.payment_type = payment_type;
		this.group_information = group_information;
	}

	@Override
	public int compareTo(Object another) {
		// TODO Auto-generated method stub
		int compareage=Integer.parseInt(((Receivers)another).getAmount());
        /* For Ascending order*/
        //return Integer.parseInt(this.amount)-compareage;
        /* For Descending order do like this */
        return compareage-Integer.parseInt(this.amount);
	}
}
