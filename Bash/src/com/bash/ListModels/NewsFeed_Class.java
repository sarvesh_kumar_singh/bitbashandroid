package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsFeed_Class {

	@SerializedName("idtrans")
	public String idtrans;
	
	@SerializedName("imagepath")
	public String imagepath;
	
	@SerializedName("paid_for")
	public String paid_for;
	
	@SerializedName("recorded_on")
	public String recorded_on;
	
	@SerializedName("feed_comment")
	public String feed_comment;
	
	@SerializedName("location")
	public String location;
	
	@SerializedName("paid_by")
	public String paid_by;
	
	@SerializedName("comments")
	public String comments; 
	
	@SerializedName("likes_count")
	public String likes_count; 
	 
	@SerializedName("isliked")
	public String isliked;
	
	@SerializedName("type")
	public String type;
	
	@SerializedName("idpaid_by")
	public String idpaid_by;
	
	@SerializedName("idpaid_for")
	public String idpaid_for;
	
	@SerializedName("status")
	public String status;
	
	@SerializedName("feedpath")
	public String feedpath;	
	
	public NewsFeed_Class(String idtrans,  String feedpath, String imagepath, String paid_for, String comments, 
			String recorded_on, String feed_comment, String location, String likes_count, String paid_by, String isliked, String type,
			String idpaid_by, String idpaid_for, String status){
		this.feedpath = feedpath;
		this.status = status;
		this.idpaid_by = idpaid_by;
		this.idpaid_for = idpaid_for;
		this.isliked= isliked;
		this.idtrans = idtrans;
		this.imagepath = imagepath;
		this.paid_for = paid_for;
		this.comments = comments;
		this.recorded_on = recorded_on;
		this.feed_comment = feed_comment;
		this.location = location;
		this.likes_count = likes_count;
		this.paid_by = paid_by;
		this.type = type;
	}
	
	public String getFeedPath(){
		return feedpath;
	}
	
	public String getStatus(){
		return status;
	}
	public String getIdPaidBy(){
		return idpaid_by;
	}
	
	public String getIdPaidFor(){
		return idpaid_for;
	}
	
	public String getType(){
		return type;
	}
	
	public String getIsLiked(){
		return isliked;
	}
	
	public void setIsLiked(String isliked){
		this.isliked = isliked;
	}
	
	public String getIdTrans(){
		return idtrans;
	}
	public String getImagepath(){
		return imagepath;
	}
	
	public String getPaidFor(){
		return paid_for;
	}
	
	public String getCommentsCount(){
		return comments;
	}
	
	public void setFeedPath(String feedpath){
		this.feedpath = feedpath;
	}
	
	public void setCommentsCount(String comments){
		this.comments = comments;
	}
	
	public String getRecordedOn(){
		return recorded_on;
	}
	public String getFeedComment(){
		return feed_comment;
	}
	
	public String getLocation(){
		return location;
	}
	
	public String getLikeCount(){
		return likes_count;
	}
	
	public void setLikeCount(String likes_count){
		this.likes_count = likes_count;
	}
	
	public String getPaidBy(){
		return paid_by;
	}
	
}
