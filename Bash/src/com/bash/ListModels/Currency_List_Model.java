package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class Currency_List_Model {
	@SerializedName("currencyName")
	public String currencyName;
	@SerializedName("countryName")
	public String countryName;
	

	public String getCurrencyName() {
		return this.currencyName;
	}

	public String getCountryName() {
		return this.countryName;
	}

	
	public Currency_List_Model(String currencyName, String countryName) {
		this.currencyName = currencyName;
		this.countryName = countryName;
	}
}
