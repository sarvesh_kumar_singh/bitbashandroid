package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.Comment_Activity;
import com.bash.Activities.SlidingActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.NewsFeed_Model;
import com.bash.ListModels.NewsfeedImageInformation_model;
import com.bash.ListModels.NewsfeedImage_Model;
import com.bash.ListModels.NewsfeedLikeInformation_Model;
import com.bash.ListModels.NewsfeedLikes_Model;
import com.bash.ListModels.NewsfeedPaidforInformation_Model;
import com.bash.ListModels.NewsfeedPaidfor_model;
import com.bash.ListModels.NewsfeedPayersInformation_Model;
import com.bash.ListModels.NewsfeedPayers_model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ShowToast") public class NewsFeed_Fragment extends Fragment implements AsyncResponse {
	View view;
	int pos = 0, likeModel;
	String commentaction = "";
	String[] name;
	MyAsynTaskManager myAsyncTask;
	View mRootView;
	ListView myfeedListView;
	MyFeedAdapter adapter;
	ArrayList<NewsFeed_Model> feedList = new ArrayList<NewsFeed_Model>();

	private FloatingActionMenu fab;
	private FloatingActionButton fab1, fab2, fab3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_feed_page, null);
		// RootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

		fab = (FloatingActionMenu) mRootView.findViewById(R.id.menu1);
		fab1 = (FloatingActionButton) mRootView.findViewById(R.id.fab1);
		fab2 = (FloatingActionButton) mRootView.findViewById(R.id.fab2);
		fab3 = (FloatingActionButton) mRootView.findViewById(R.id.fab3);

		fab1.setOnClickListener(clickListener);
		fab2.setOnClickListener(clickListener);
		fab3.setOnClickListener(clickListener);

		fab.hideMenuButton(false);
		fab.setClosedOnTouchOutside(true);
		fab.showMenuButton(true);
		fab.setIconAnimated(true);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}

	private void initializeView() {

		myfeedListView = (ListView) mRootView
				.findViewById(R.id.generalListView);

		getNewsFeedListByType("getAllPosts");
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.menu_btn,
				"", "Newsfeed", "", 0);
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).popFragment();
				//((SlidingActivity) getActivity()).slidingmenu_layout.toggleMenu();
				((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
			}
	 });
		adapter = new MyFeedAdapter(getActivity(), feedList);
		myfeedListView.setAdapter(adapter);
	}

	public void getNewsFeedListByType(String type) {
		// getlatest
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getNewsFeedListByType", getActivity(),
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "post", type,
						PreferenceManager.getInstance().getUserId() });

		myAsyncTask.execute();

	}

	public class MyFeedAdapter extends BaseAdapter {

		public Activity context;
		public int msPerHour = 1000 * 60 * 60;
		boolean islike = false;
		ViewHolder holder = null;

		public MyFeedAdapter(Activity context,
				ArrayList<NewsFeed_Model> feedList) {
			this.context = context;
			feedList = feedList;
		}

		public void notifyWithDataSet(ArrayList<NewsFeed_Model> newList) {
			feedList = newList;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			String totalString = null;

			final NewsFeed_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						R.layout.custom_feed_myfeed_listviewnew, null);
				holder = new ViewHolder();
				holder.numberofLikes = (TextView) convertView.findViewById(R.id.numberofLikes);
				holder.numberofComments = (TextView) convertView.findViewById(R.id.numberofComments);
				holder.resonforPayment = (TextView) convertView.findViewById(R.id.resonforPayment);
				holder.amoutPaidBy = (TextView) convertView.findViewById(R.id.amoutPaidBy);
				holder.amoutPaidFor = (TextView) convertView.findViewById(R.id.amoutPaidFor);
				holder.placeofPayment = (TextView) convertView.findViewById(R.id.placesofPayment);
				holder.timeofComment = (TextView) convertView.findViewById(R.id.timeofComment);
				holder.paidText = (TextView) convertView.findViewById(R.id.paidText);
				holder.isLikeButton = (ImageView) convertView.findViewById(R.id.likesButton);
				holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
				holder.iviFeedOption = (ImageView) convertView.findViewById(R.id.iviFeedOption);
				holder.iviFeedImage1 = (ImageView) convertView.findViewById(R.id.iviFeedImage1);
				holder.iviFeedImage2 = (ImageView) convertView.findViewById(R.id.iviFeedImage2);
				holder.iviFeedImage3 = (ImageView) convertView.findViewById(R.id.iviFeedImage3);
				holder.iviFeedImage4 = (ImageView) convertView.findViewById(R.id.iviFeedImage4);
				holder.commentBox = (LinearLayout) convertView.findViewById(R.id.commentBox);
				holder.likesBox = (LinearLayout) convertView.findViewById(R.id.likesBox);
				holder.llFeedImage = (LinearLayout) convertView.findViewById(R.id.llFeedImage);
				holder.rlImageView = (RelativeLayout) convertView.findViewById(R.id.rlImageView);
				holder.llOption = (LinearLayout) convertView.findViewById(R.id.llOption);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			// holder.amoutPaidBy.setText(listItem.getPoster_full_name().split(" ",
			// 0)[0].toString());

			if (listItem.getPost_type().equals("pay")) {
				holder.paidText.setText(" paid ");
				totalString = listItem.getPoster_full_name().split(" ", 0)[0]
						.toString() + " paid";
			} else {
				holder.paidText.setText(" received ");
				totalString = listItem.getPoster_full_name().split(" ", 0)[0]
						.toString() + " received";
			}

			NewsfeedPayers_model payers = listItem.getPayers();
			NewsfeedPaidfor_model paid_for = listItem.getPaid_for();
			NewsfeedImage_Model images = listItem.getImages();

			String[] paidFor_namearray = null;
			String paidfor_name = null;
			int count = 0;
			if (payers != null && paid_for != null
					&& Integer.parseInt(payers.getTotal_payers()) > 0
					&& Integer.parseInt(paid_for.getTotal_paid_for()) > 0) {
				paidFor_namearray = new String[Integer.parseInt(payers
						.getTotal_payers())
						+ Integer.parseInt(paid_for.getTotal_paid_for())];
			} else if (payers != null
					&& Integer.parseInt(payers.getTotal_payers()) > 0) {
				paidFor_namearray = new String[Integer.parseInt(payers
						.getTotal_payers())];
			} else if (paid_for != null
					&& Integer.parseInt(paid_for.getTotal_paid_for()) > 0) {
				paidFor_namearray = new String[Integer.parseInt(paid_for
						.getTotal_paid_for())];
			}

			if (payers != null
					&& Integer.parseInt(payers.getTotal_payers()) > 0) {
				for (int i = 0; i < payers.getPayers_information().length; i++) {
					paidFor_namearray[count] = payers.getPayers_information()[i]
							.getPayer_full_name().split(" ", 0)[0].toString();
					count++;
				}
			}
			if (paid_for != null
					&& Integer.parseInt(paid_for.getTotal_paid_for()) > 0) {
				for (int i = 0; i < paid_for.getPaidfor_information().length; i++) {
					paidFor_namearray[count] = paid_for
							.getPaidfor_information()[i].getFullname().split(
							" ", 0)[0].toString();
					count++;
				}
			}
			for (int totalcout = 0; totalcout < paidFor_namearray.length; totalcout++) {
				if (totalcout == 0) {
					paidfor_name = paidFor_namearray[totalcout];
				} else if (totalcout > 0
						&& totalcout < paidFor_namearray.length - 1) {
					paidfor_name = paidfor_name + ", "
							+ paidFor_namearray[totalcout];
				} else if (totalcout > 0
						&& totalcout == paidFor_namearray.length - 1) {
					paidfor_name = paidfor_name + " and "
							+ paidFor_namearray[totalcout];
				}
			}
			holder.amoutPaidFor.setText(paidfor_name);
			holder.resonforPayment.setText(" For " + listItem.getPost_content());
			totalString = totalString + " " + paidfor_name + " for " + listItem.getPost_content();
			int length1 = 0, length2 = 0, length3 = 0;
			String[] spanStringArray = totalString.split(" ");
			length1 = spanStringArray[0].length();
			length2 = spanStringArray[0].length() + spanStringArray[1].length() + 1;
			length3 = spanStringArray[0].length() + spanStringArray[1].length() + paidfor_name.length() + 2;
			// Create a new spannable with the two strings
			Spannable spannable = new SpannableString(totalString);

			// Set the custom typeface to span over a section of the spannable
			// object
			spannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, length1,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, length1,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.GRAY), length1,
					length2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.BLACK), length2,
					length3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new StyleSpan(Typeface.BOLD), length2, length3,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.GRAY), length3,
					spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			// Set the text of a textView with the spannable object
			holder.amoutPaidBy.setText(spannable);

			// Sarvesh
			// Set comment from list item in listview

			holder.numberofComments.setText("(" + listItem.getCommentCount() + ")");

			/*
			 * if(listItem.getLocation() != null &&
			 * listItem.getLocation().length() != 0)
			 * holder.placeofPayment.setText("At "+listItem.getLocation()); else
			 * holder.placeofPayment.setVisibility(View.INVISIBLE);
			 */

			// Log.e("Timer Text ", listItem.getRecordedOn());
			String timeText = Utils.getFormattedTimerText(listItem
					.getRecorded_on());
			holder.timeofComment.setText(timeText);
			holder.llOption.setTag(position);
			

			holder.iviFeedOption.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					pos = position;
					Toast.makeText(getActivity(), " Option Will come soon...", Toast.LENGTH_LONG); 
					  if(feedList.get(pos).getPosted_by_iduser().equals(PreferenceManager.getInstance().getUserId())){
						  Toast.makeText(getActivity(), position + " Option Will come soon...", Toast.LENGTH_LONG); 
					  }
					 
				}
			});

			holder.commentBox.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (Integer.parseInt(listItem.getLikeCount()) > 0) {
						NewsfeedLikeInformation_Model[] newsfeedlike = listItem.getLikes().getLikes_information();
						name = new String[newsfeedlike.length];
						int count = 0;

						if (listItem.getIslike()) {
							name[count] = "You";
							count++;
						} else {
							for (int lkinfo = 0; lkinfo < newsfeedlike.length; lkinfo++) {
								name[count] = newsfeedlike[lkinfo].getFullname().split(" ")[0];
								count++;
							}
						}
					}
					String name1 = "";
					int totalLike = Integer.parseInt(listItem.getLikeCount());
					if (Integer.parseInt(listItem.getLikeCount()) == 1) {
						name1 = name[0] + " like this";
					} else if (Integer.parseInt(listItem.getLikeCount()) == 2) {
						name1 = name[0] + " & " + name[1] + " like this";
					} else if (Integer.parseInt(listItem.getLikeCount()) == 3) {
						name1 = name[0] + ", " + name[1] + " & " + name[2] + " like this";
					} else if (Integer.parseInt(listItem.getLikeCount()) > 3) {
						name1 = name[0] + ", " + name[1] + " & " + String.valueOf(totalLike - 2) + " other like this";
					}
					// HomeFragment.parentFragment.replaceFragment(new
					// TabFeed_MyFeed_Comment_Fragment(listItem), true);
					// ((BaseFragment) getParentFragment()).replaceFragment(new
					// Comment_Fragment(listItem), true);
					// ((BaseFragment) getParentFragment()).replaceFragment(new
					// EditProfileFragment(), true);
					Intent intentComment = new Intent(getActivity(), Comment_Activity.class);
					intentComment.putExtra("idPost", listItem.getIdpost());
					intentComment.putExtra("like", listItem.getIslike());
					intentComment.putExtra("name", name1);
					startActivity(intentComment);
				}
			});

			// Sarvesh

			if (listItem.getIslike())
				holder.isLikeButton.setImageResource(R.drawable.like_button_s);
			else
				holder.isLikeButton.setImageResource(R.drawable.like_button_unsel_s);

			final TextView numberOfLikeView = holder.numberofLikes;

			view = convertView;
			// Sarvesh
			islike = listItem.getIslike();

			holder.numberofLikes.setText("(" + listItem.getLikeCount() + ")");

			holder.likesBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (listItem.getIslike()) {
						commentaction = "unlikefeed";
						pos = position;
						makeAnUnLike(listItem.getIdpost(), view, pos, commentaction);
					} else if (!listItem.getIslike()) {
						commentaction = "feedlike";
						pos = position;
						makeAnLike(listItem.getIdpost(), view, pos, commentaction);
					}

					/*
					 * if(likes.get.equals("1")){ commentaction="unlikefeed";
					 * pos=position; makeAnLike(listItem.getIdTrans(), view,
					 * pos, commentaction); }else{ commentaction="feedlike";
					 * pos=position; makeAnLike(listItem.getIdTrans(), view,
					 * pos, commentaction); }
					 */
				}
			});

			final ImageView myuserImage = holder.userImage;

			if (listItem.getPoster_image_location() != null
					&& listItem.getPoster_image_location().length() != 0) {
				ImageLoader.getInstance().displayImage(
						listItem.getPoster_image_location(), holder.userImage,
						BashApplication.options,
						BashApplication.animateFirstListener);
			} else {
				holder.userImage
						.setImageResource(R.drawable.addphoto_img_block);
			}

			// Sarvesh

			if (images != null && Integer.parseInt(images.total_images) != 0) {
				NewsfeedImageInformation_model[] imageInfo = images
						.getImages_information();
				holder.rlImageView.setVisibility(View.VISIBLE);
				ImageLoader.getInstance().displayImage(
						imageInfo[0].getImage_location(), holder.iviFeedImage1);
				if (imageInfo.length > 1) {
					holder.llFeedImage.setVisibility(View.VISIBLE);
					if (imageInfo.length >= 5) {
						ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2);
						ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3);
						ImageLoader.getInstance().displayImage(imageInfo[3].getImage_location(), holder.iviFeedImage4);
					} else if (imageInfo.length == 4) {
						ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2);
						ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3);
						ImageLoader.getInstance().displayImage(imageInfo[3].getImage_location(), holder.iviFeedImage4);
					} else if (imageInfo.length == 3) {
						ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2);
						ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3);
						holder.iviFeedImage4.setVisibility(View.GONE);
					} else if (imageInfo.length == 2) {
						ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2);
						holder.iviFeedImage3.setVisibility(View.GONE);
						holder.iviFeedImage4.setVisibility(View.GONE);
					}
				} else {
					holder.llFeedImage.setVisibility(View.GONE);
				}

			} else {
				holder.rlImageView.setVisibility(View.GONE);
			}

			return convertView;
		}

		@Override
		public int getCount() {
			return feedList.size();
		}

		@Override
		public NewsFeed_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public class ViewHolder {
			TextView numberofLikes;
			TextView numberofComments;
			TextView resonforPayment;
			TextView amoutPaidBy;
			TextView amoutPaidFor;
			TextView placeofPayment;
			TextView timeofComment;
			TextView paidText;
			ImageView isLikeButton;
			ImageView userImage, iviFeedOption, iviFeedImage1, iviFeedImage2, iviFeedImage3, iviFeedImage4;
			LinearLayout commentBox, likesBox, llFeedImage, llOption;
			RelativeLayout rlImageView;
		}

	}

	public void makeAnLike(String commetnId, final View view,
			final int position, final String commentAction) {
		commentaction = commentAction;
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask
				.setupParamsAndUrl("makeAnLike", getActivity(),
						AppUrlList.ACTION_URL, new String[] { "module",
								"action", "iduser", "idpost" }, new String[] {
								"post", "likeAPost",
								PreferenceManager.getInstance().getUserId(),
								commetnId });
		myAsyncTask.execute();

	}

	public void makeAnUnLike(String commetnId, final View view,
			final int position, final String commentAction) {
		commentaction = commentAction;
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask
				.setupParamsAndUrl("makeAnLike", getActivity(),
						AppUrlList.ACTION_URL, new String[] { "module",
								"action", "iduser", "idpost" }, new String[] {
								"post", "undoLikeOnAPost",
								PreferenceManager.getInstance().getUserId(),
								commetnId });
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub

		ArrayList<NewsFeed_Model> newList = new ArrayList<NewsFeed_Model>();
		if (from.equalsIgnoreCase("getNewsFeedListByType")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						JSONArray jsonArray = rootObj
								.getJSONArray("posts_information");
						JSONObject jObj;

						NewsfeedImage_Model newsfeedImageList = null;
						NewsfeedLikes_Model newsfeedLikeList = null;
						NewsfeedPayers_model newsfeedPayersList = null;
						NewsfeedPaidfor_model newsfeedPaidforList = null;
						for (int i = 0; i < jsonArray.length(); i++) {
							boolean islike = false;
							jObj = jsonArray.getJSONObject(i);
							JSONObject imageObj = jObj.getJSONObject("images");
							JSONObject likeObj = jObj.getJSONObject("likes");
							JSONObject payersObj = jObj.getJSONObject("payers");
							JSONObject paidforObj = jObj.getJSONObject("paid_for");
							JSONArray imageArray = null, likeArray = null, payersArray = null, paidforArray = null;
							NewsfeedImageInformation_model[] newsfeedImageInfoList = null;
							NewsfeedLikeInformation_Model[] newsfeedLikeInfoList = null;
							NewsfeedPayersInformation_Model[] newsfeedPayersInfoList = null;
							NewsfeedPaidforInformation_Model[] newsfeedPaidforInfoList = null;

							if (!imageObj.getString("total_images").equals("0")) {
								imageArray = imageObj.getJSONArray("images_information");
							}
							if (!likeObj.getString("likes_count").equals("0")) {
								likeArray = likeObj.getJSONArray("likes_information");
							}
							if (!payersObj.getString("total_payers").equals("0")) {
								payersArray = payersObj.getJSONArray("payers_information");
							}
							if (!paidforObj.getString("total_paid_for").equals("0")) {
								paidforArray = paidforObj.getJSONArray("paid_for_information");
							}

							if (imageArray != null) {
								newsfeedImageInfoList = new NewsfeedImageInformation_model[imageArray.length()];
								for (int image = 0; image < imageArray.length(); image++) {
									JSONObject jObjImage = imageArray.getJSONObject(image);

									NewsfeedImageInformation_model newsfeedImageInfoListItem = new NewsfeedImageInformation_model(
											jObjImage.getString("idimage"),
											jObjImage.getString("idpost"),
											jObjImage.getString("image_location"));

									newsfeedImageInfoList[image] = newsfeedImageInfoListItem;
								}

								newsfeedImageList = new NewsfeedImage_Model(imageObj.getString("total_images"), newsfeedImageInfoList);
							} else {
								newsfeedImageList = new NewsfeedImage_Model(imageObj.getString("total_images"), newsfeedImageInfoList);
							}
							if (likeArray != null) {
								newsfeedLikeInfoList = new NewsfeedLikeInformation_Model[likeArray.length()];
								// newsfeedLikeInfoList.clear();
								for (int like = 0; like < likeArray.length(); like++) {
									JSONObject jObjlike = likeArray.getJSONObject(like);

									NewsfeedLikeInformation_Model newsfeedLikeInfoListItem = new NewsfeedLikeInformation_Model(
											jObjlike.getString("idlike"),
											jObjlike.getString("idpost"),
											jObjlike.getString("iduser"),
											jObjlike.getString("fullname"));

									newsfeedLikeInfoList[like] = newsfeedLikeInfoListItem;
									if (jObjlike.getString("iduser").equals(PreferenceManager.getInstance().getUserId()) && !islike) {
										islike = true;
									} 
								}

								newsfeedLikeList = new NewsfeedLikes_Model(likeObj.getString("likes_count"), newsfeedLikeInfoList);
							} else {
								newsfeedLikeList = new NewsfeedLikes_Model(likeObj.getString("likes_count"), newsfeedLikeInfoList);
							}
							if (payersArray != null) {
								newsfeedPayersInfoList = new NewsfeedPayersInformation_Model[payersArray.length()];
								for (int payer = 0; payer < payersArray.length(); payer++) {
									JSONObject jObjPayer = payersArray.getJSONObject(payer);

									NewsfeedPayersInformation_Model newsfeedPayersInfoListItem = new NewsfeedPayersInformation_Model(
											jObjPayer.getString("idpost_payer"),
											jObjPayer.getString("idpost"),
											jObjPayer.getString("payer_type"),
											jObjPayer.getString("user_or_group_id"),
											jObjPayer.getString("payer_full_name"));

									newsfeedPayersInfoList[payer] = newsfeedPayersInfoListItem;
								}
								newsfeedPayersList = new NewsfeedPayers_model(payersObj.getString("total_payers"),newsfeedPayersInfoList);
							} else {
								newsfeedPayersList = new NewsfeedPayers_model(payersObj.getString("total_payers"),newsfeedPayersInfoList);
							}
							if (paidforArray != null) {
								newsfeedPaidforInfoList = new NewsfeedPaidforInformation_Model[paidforArray.length()];
								for (int paidfor = 0; paidfor < paidforArray.length(); paidfor++) {
									JSONObject jObjPaidfor = paidforArray.getJSONObject(paidfor);

									NewsfeedPaidforInformation_Model newsfeedPaidforInfoListItem = new NewsfeedPaidforInformation_Model(
											jObjPaidfor.getString("idpost_paid_for"),
											jObjPaidfor.getString("idpost"),
											jObjPaidfor.getString("paid_for_type"),
											jObjPaidfor.getString("user_or_group_id"),
											jObjPaidfor.getString("fullname"));

									newsfeedPaidforInfoList[paidfor] = newsfeedPaidforInfoListItem;
								}
								newsfeedPaidforList = new NewsfeedPaidfor_model(
										paidforObj.getString("total_paid_for"),
										newsfeedPaidforInfoList);
							} else {
								newsfeedPaidforList = new NewsfeedPaidfor_model(
										paidforObj.getString("total_paid_for"),
										newsfeedPaidforInfoList);
							}
							JSONObject jobjComments = jObj.getJSONObject("comments");
							newList.add(new NewsFeed_Model(jObj
									.getString("idpost"), jObj
									.getString("post_type"), jObj
									.getString("post_content"), jObj
									.getString("recorded_on"), jObj
									.getString("posted_by_iduser"), jObj
									.getString("poster_full_name"), jObj
									.getString("poster_image_location"),
									newsfeedImageList, newsfeedLikeList,
									islike, jobjComments.getString("comments_count"),
									likeObj.getString("likes_count"),
									newsfeedPayersList, newsfeedPaidforList));
						}
						adapter.notifyWithDataSet(newList);
					} else {
						Toast.makeText(getActivity(), rootObj.getString("msg"),
								Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					// TODO: handle exception
					Log.i("Error", e.toString());
					DialogManager.showDialog(getActivity(),
							"Server Error Occured! Try Again!");
				}
			} else {

				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");

			}
		} else if (from.equalsIgnoreCase("makeAnLike")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {

						// likeTextView.setText(rootObj.getString("likes_count"));
						// ((TextView)
						// view.findViewById(R.id.numberofLikes)).setText(rootObj.getString("likes_count"));

						// feedList.get(pos).setLikeCount(((TextView)view.findViewById(R.id.numberofLikes)).getText().toString());
						// feedList.get(pos).setIslike(true);
						likeModel = Integer.parseInt(feedList.get(pos)
								.getLikeCount());

						NewsfeedLikeInformation_Model[] info = feedList
								.get(pos).getLikes().getLikes_information();
						if (commentaction.equals("unlikefeed")) {
							// feedList.get(pos).setIsLiked("0");
							feedList.get(pos).setIslike(false);
							likeModel--;
							feedList.get(pos).setLikeCount(
									String.valueOf(likeModel));
							((ImageView) view.findViewById(R.id.likesButton))
									.setImageResource(R.drawable.like_btn_deselect);
							// ((TextView)
							// view.findViewById(R.id.numberofLikes)).setText("("+String.valueOf(likeModel)+")");
						} else {
							// feedList.get(pos).setIsLiked("1");
							// likeModel=Integer.parseInt(feedList.get(pos).getLikes().getLikes_count());
							likeModel++;
							feedList.get(pos).setIslike(true);
							((ImageView) view.findViewById(R.id.likesButton))
									.setImageResource(R.drawable.like_btn_select);
							// ((TextView)
							// view.findViewById(R.id.numberofLikes)).setText("("+String.valueOf(likeModel)+")");
							feedList.get(pos).setLikeCount(
									String.valueOf(likeModel));

						}

						adapter.notifyDataSetChanged();

					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(),
							"Server Error Occured! Try Again!");
				}
			} else {

				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");

			}
		}
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.menu1:
				break;
			case R.id.fab1:
				((BaseFragment) getParentFragment()).replaceFragment(
						new CreateGroup_Fragment(), true);
				break;
			case R.id.fab2:
				((BaseFragment) getParentFragment()).replaceFragment(
						new InviteFriends_Fragment(), true);
				((SlidingActivity) getActivity()).currentFragment = NewsFeed_Fragment.this;
				break;
			case R.id.fab3:
				startActivity(new Intent(getActivity(), AddTabActivity.class));
				break;
			}
		}
	};

}
