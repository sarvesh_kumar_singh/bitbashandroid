package com.bash.TwitterConnection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint({ "NewApi", "ValidFragment" })
public class TwitterFragment  extends Fragment
{
	
	  	public final String consumer_key = "nTgEyeHgsz7o1cBlK40xKw";
	    public final String secret_key = "99NOQwBMSUoQ6asWfZMn8c1VVNGzUmdTRpGHZU9us";
	    String string_img_url = null, string_msg = null;
	    Button btn;
	    Activity mActivity;
	    Dialog twittDialog;
		View alertView;

		public TwitterFragment(Activity mActivity)
		{
			    this.mActivity = mActivity;
			  //  this.string_msg = string_msg;
		}
		
		public void updateToTwitt(String string_msg) 
		{
		if (isNetworkAvailable())
			{
			    Twitt_Sharing twitt = new Twitt_Sharing(mActivity,  consumer_key, secret_key);
			    
			    //string_img_url = "http://3.bp.blogspot.com/_Y8u09A7q7DU/S-o0pf4EqwI/AAAAAAAAFHI/PdRKv8iaq70/s1600/id-do-anything-logo.jpg";
			    //string_msg = "http://chintankhetiya.wordpress.com/";
			    // here we have web url image so we have to make it as file to
			    // upload
			   // String_to_File(string_img_url);
			    // Now share both message & image to sharing activity
//			    twitt.shareToTwitter(string_msg, casted_image);
			    twitt.shareToTwitter(string_msg);

			} else 
			{
			    showToast("No Network Connection Available !!!");
			}
		}
		
		public void logoutfromTwitter() {
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.removeSessionCookie();
			TwitterSession.resetAccessToken();
			Twitter_Handler.twitterObj.shutdown();
		}
		
		public boolean isNetworkAvailable() 
			{
				ConnectivityManager connectivity = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
				if (connectivity == null) {
				    return false;
				} else {
				    NetworkInfo[] info = connectivity.getAllNetworkInfo();
				    if (info != null) {
					for (int i = 0; i < info.length; i++) {
					    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					    }
					}
				    }
				}
				return false;
			}
				
	  private void showToast(String msg) 
	  	{
		  Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show();
		}
}