package com.bash.Activities;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.bash.R;
import com.bash.Adapters.ItemArrayAdapter;
import com.bash.ListModels.CurrencyModel;
import com.bash.Managers.CSVFile;
import com.bash.interfaces.ActivityInterface;

public class CurrencyActivity extends Activity {
	ImageView currencyBackImage;
	ListView lvCurrency;

	public static final int RESULT_OK = 101;
	public static final int RESULT_CANCELED = 102;
	public static ActivityInterface delegate;
	EditText searchCurrencyBox;

	private ItemArrayAdapter itemArrayAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_currency_picker);
		
		searchCurrencyBox = (EditText) findViewById(R.id.searchCurrencyBox);
		currencyBackImage = (ImageView) findViewById(R.id.currencybackimage);
		lvCurrency = (ListView) findViewById(R.id.lvCurrencyPicker);
		
		currencyBackImage.setOnClickListener(clickListener);
		
		searchCurrencyBox.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(searchCurrencyBox.getText().toString().length()>0){
					String text = searchCurrencyBox.getText().toString().toLowerCase(Locale.getDefault());
					itemArrayAdapter.filter(text);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	    InputStream inputStream = getResources().openRawResource(R.raw.currencies);
        CSVFile csvFile = new CSVFile(inputStream);
        ArrayList<CurrencyModel> scoreList = csvFile.read();
		
		 itemArrayAdapter = new ItemArrayAdapter(getApplicationContext(), R.layout.currency_item_layout,scoreList);

	        Parcelable state = lvCurrency.onSaveInstanceState();
	        lvCurrency.setAdapter(itemArrayAdapter);
	        lvCurrency.onRestoreInstanceState(state);

	        
	        lvCurrency.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
//					Intent returnIntent = new Intent();
//			        returnIntent.putExtra("result",itemArrayAdapter.getItem(position).getCurrencyCode());
//			        setResult(RESULT_OK,returnIntent);
					
					delegate.onActivityData(itemArrayAdapter.getItem(position).getCurrencyCode());
			        
			        finish();
				}
			});
	        
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.currencybackimage:
				CurrencyActivity.this.onBackPressed();
				break;


			}
		}
	};
}
