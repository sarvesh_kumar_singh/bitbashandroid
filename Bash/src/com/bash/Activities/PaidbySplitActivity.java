package com.bash.Activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.PaidByListAdapter;
import com.bash.ListModels.Person;
import com.bash.interfaces.ActivityInterface;
import com.bash.interfaces.PaidbyInterface;

public class PaidbySplitActivity extends Activity {

	TextView topleftpaidbyText,toprightsidepaidbytext,totalunequalamount;
	public static TextView pendingunequalamount, totalsplitunequalamount;
	ListView lvpaidbylist;
	public static LinearLayout unequalamountlinear;
	public static float totalAmount;
	ArrayList<Person> addedPersonArrayList;
	PaidByListAdapter paidByListAdapter;
	public static PaidbyInterface delegate;
	private int countperson = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paidby);
	
		topleftpaidbyText = (TextView) findViewById(R.id.topleftpaidbyText);
		toprightsidepaidbytext = (TextView) findViewById(R.id.toprightsidepaidbytext);
		lvpaidbylist = (ListView) findViewById(R.id.lvpaidbylist);
		unequalamountlinear = (LinearLayout) findViewById(R.id.unequalamountlinear);
		totalsplitunequalamount = (TextView) findViewById(R.id.totalsplitunequalamount);
		totalunequalamount = (TextView) findViewById(R.id.totalunequalamount);
		pendingunequalamount = (TextView) findViewById(R.id.pendingunequalamount);
	
		addedPersonArrayList = new ArrayList<Person>();
		
		Intent paidbyIntent = getIntent();
		
		if(paidbyIntent != null){
			addedPersonArrayList = paidbyIntent.getParcelableArrayListExtra("AddedPersonList");
			String amtstr = paidbyIntent.getStringExtra("TotalAmount");	
			totalAmount = Float.parseFloat(amtstr);
		}
	
	

		for (Person person : addedPersonArrayList) {
			if (person.isPaidpersonbool()) {
				countperson++;
			}
		}
		if(countperson == 0){
			addedPersonArrayList.get(0).setChechbxbool(true);
			addedPersonArrayList.get(0).setPaidpersonbool(true);
			addedPersonArrayList.get(0).setPaidamount(totalAmount);
		}
		
		totalunequalamount.setText(""+totalAmount);
		paidByListAdapter = new PaidByListAdapter(PaidbySplitActivity.this,
				R.layout.adapter_paidby, addedPersonArrayList);
		
		lvpaidbylist.setAdapter(paidByListAdapter);
		
		
		topleftpaidbyText.setOnClickListener(clickListener);
		toprightsidepaidbytext.setOnClickListener(clickListener);
		
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			//String text = "";

			switch (v.getId()) {

			case R.id.topleftpaidbyText:		
				//text = "back";
				PaidbySplitActivity.this.onBackPressed();
				break;
			case R.id.toprightsidepaidbytext:
				
				//Float ttAmount = paidByListAdapter.findTotalAmount();
				if(paidByListAdapter.findTotalAmount() == totalAmount){
							
					ArrayList<Person> addedPersonArrayList1 = paidByListAdapter.addedPersonArrayList;
					delegate.onPaidByData(addedPersonArrayList1);
					
					finish();
				}else{
					Toast.makeText(PaidbySplitActivity.this, "Error in Amount", Toast.LENGTH_SHORT).show();
				}
				
				//text = totalAmount+" , "+ttAmount ;
				break;
				
			}
			//Toast.makeText(PaidbySplitActivity.this, text, Toast.LENGTH_SHORT).show();
		}
	};
	
}
