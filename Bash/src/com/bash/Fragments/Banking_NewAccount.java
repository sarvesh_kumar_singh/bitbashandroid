package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.bash.R;

public class Banking_NewAccount extends Fragment{

	View mRootView;
	Spinner accountSpinner;
	ArrayAdapter<String> dataAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_wallbank_addaccountpage, null);
		initializeView();
		return mRootView;
	}
	
	public void initializeView(){
		accountSpinner = (Spinner)mRootView.findViewById(R.id.accountSpinner);
		List<String> list = new ArrayList<String>();
		list.add("Indian Bank");
		list.add("SBI");
		list.add("Indian Overseas Bank");
		list.add("Tamil Nadu Mercantile Bank");
		list.add("HDFC");
		list.add("Yes bank");
		list.add("CITI");
		list.add("ICICI");
		list.add("Syndicate Bank");
		dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		accountSpinner.setAdapter(dataAdapter);
	}
	
}
