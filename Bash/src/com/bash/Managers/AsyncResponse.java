package com.bash.Managers;

public interface AsyncResponse {
	void backgroundProcessFinish(String from,String output);
}
