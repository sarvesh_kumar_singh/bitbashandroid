package com.bash.ListModels;

public class MyFriendOnGroup_Class {

	public String idfriend;
	public String email_id;
	public String phone_no;
	public String friendName;
	public String friendImageSource;
	public String friendAmount;
	public String percenTage;
	public boolean isSelected = true; 
	
	public MyFriendOnGroup_Class(String idfriend, String email_id, String phone_no, String friendName, String friendImageSource, String friendAmount, 
			String percenTage){
		this.percenTage = percenTage;
		this.idfriend = idfriend;
		this.email_id = email_id;
		this.phone_no = phone_no;
		this.friendName = friendName;
		this.friendImageSource = friendImageSource;
		this.friendAmount = friendAmount;
	}
	
	public MyFriendOnGroup_Class(String idfriend, String email_id, String phone_no, String friendName, String friendImageSource, String friendAmount, 
			String percenTage, boolean isSelected){
		this.isSelected = isSelected;
		this.percenTage = percenTage;
		this.idfriend = idfriend;
		this.email_id = email_id;
		this.phone_no = phone_no;
		this.friendName = friendName;
		this.friendImageSource = friendImageSource;
		this.friendAmount = friendAmount;
	}
	
	public void setIsSelected(boolean isSelected){
		this.isSelected = isSelected;
	}
	public boolean getIsSelected(){
		return isSelected;
	}
	
	public String getPercentage(){
		return percenTage;
	}
	
	public void setPercentage(String percenTage){
		this.percenTage = percenTage;
	}
	
	public String getfriendAmount(){
		return friendAmount;
	}
	
	public void setfriendAmount(String amonut){
		this.friendAmount = amonut;
	}
	
	public String getemail_id(){
		return email_id;
	}
	
	public String getphone_no(){
		return phone_no;
	}
	public String getidfriend(){
		return idfriend;
	}
	
	public String getfriendName(){
		return friendName;
	}
	
	public String getfriendImageSource(){
		return friendImageSource;
	}
}

