package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedPayersInformation_Model {
	@SerializedName("idpost_payer")
	public String idpost_payer;
	@SerializedName("idpost")
	public String idpost;
	@SerializedName("payer_type")
	public String payer_type;
	@SerializedName("user_or_group_id")
	public String user_or_group_id;
	@SerializedName("payer_full_name")
	public String payer_full_name;
	

	public NewsfeedPayersInformation_Model(String idpost_payer, String idpost, String payer_type,
			 String user_or_group_id, String payer_full_name) {
		this.idpost_payer = idpost_payer;
		this.idpost = idpost;
		this.payer_type = payer_type;
		this.user_or_group_id = user_or_group_id;
		this.payer_full_name = payer_full_name;
	}

	public String getIdpost_payer() {
		return idpost_payer;
	}

	public String getIdpost() {
		return idpost;
	}

	public String getPayer_type() {
		return payer_type;
	}

	public String getUser_or_group_id() {
		return user_or_group_id;
	}

	public String getPayer_full_name() {
		return payer_full_name;
	}
}
