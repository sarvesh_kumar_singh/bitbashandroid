package com.bash.ListModels;

public class ValuseHolder{
	public Double percentage;
	public Double amount;
	public Double id;
	
	public ValuseHolder(Double id, Double percentage, Double amount) {
		this.percentage = percentage;
		this.id = id;
		this.amount = amount;
	}
	
	public void setPercentage(Double percentage){
		this.percentage = percentage;
	}
	
	public void setAmount(Double amount){
		this.amount = amount;
	}
	
	public Double getId(){
		return id;
	}
	public Double getPercentage(){
		return percentage;
	}
	public Double getAmount(){
		return amount;
	}
}
