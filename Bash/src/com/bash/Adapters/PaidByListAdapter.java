package com.bash.Adapters;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bash.R;
import com.bash.Activities.PaidbySplitActivity;
import com.bash.CustomViews.CircularImageView;
import com.bash.ListModels.Person;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class PaidByListAdapter extends ArrayAdapter<Person> {
	Activity activity;
	LayoutInflater inflater;
	int resource;
	PersonItemViewHolder holder = null;
	public ArrayList<Person> addedPersonArrayList;
	Toast mtoast;
	ImageView imageNext;
	private static EditText editText[];
	private static CheckBox checkBox[];
	private float equalAmount = 0.00f;
	private boolean editable = false; 
	private float oldValue = 0.00f;

	public PaidByListAdapter(Activity activity, int resource,
			ArrayList<Person> addedPersonArrayList) {
		super(activity, resource, addedPersonArrayList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.addedPersonArrayList = addedPersonArrayList;
		this.resource = resource;
		editText = new EditText[addedPersonArrayList.size()];
		checkBox = new CheckBox[addedPersonArrayList.size()];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return addedPersonArrayList.size();
	}

	@Override
	public Person getItem(int position) {
		// TODO Auto-generated method stub
		return addedPersonArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new PersonItemViewHolder();
			holder.paidbyimageview = (CircularImageView) convertView
					.findViewById(R.id.paidbyimageview);
			holder.paidbyamount = (EditText) convertView
					.findViewById(R.id.paidbyamount);
			holder.tviPaidByAmount = (TextView) convertView.findViewById(R.id.tviPaidByAmount);
			holder.paidbyname = (TextView) convertView
					.findViewById(R.id.paidbyname);
			holder.paidbyckbx = (CheckBox) convertView
					.findViewById(R.id.paidbyckbx);
			holder.paidbyamount.setTag("edit" + position);

			
			

			convertView.setTag(holder);
		} else {
			holder = (PersonItemViewHolder) convertView.getTag();
		}

		final PersonItemViewHolder holder = (PersonItemViewHolder) convertView
				.getTag();
		editText[position] = holder.paidbyamount;
		checkBox[position] = holder.paidbyckbx;
		final Person personObj = getItem(position);

		if ((personObj.getImage_location() != null)
				&& (personObj.getImage_location().length() > 0)) {
			UrlImageViewHelper.setUrlDrawable(holder.paidbyimageview,
					personObj.getImage_location());
			// Toast.makeText(activity, photosListObj.getName(),
			// Toast.LENGTH_SHORT).show();
		}

		holder.paidbyname.setText(personObj.getName());

		if (personObj.isChechbxbool()) {

			holder.paidbyckbx.setChecked(true);
		} else {
			holder.paidbyckbx.setChecked(false);
		}
		
		holder.tviPaidByAmount.setText("" + personObj.getPaidamount());


		holder.paidbyckbx.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()) {
					editable = false;
					PaidbySplitActivity.unequalamountlinear.setVisibility(View.GONE);

					personObj.setChechbxbool(true);
				} else {

					personObj.setChechbxbool(false);
				}
				equalAmount = findEqualAmount();
				setEqualAmount();
				notifyDataSetChanged();
			}
		});
		/*holder.paidbyamount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editable = true;
				PaidbySplitActivity.unequalamountlinear.setVisibility(View.VISIBLE);
				holder.paidbyamount.setFocusable(true);
				setUnCheckBox();
			}
		});*/
		holder.paidbyamount.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(editable){
					float changeValue = 0.0f;
					if(s.length() > 0){
					changeValue = Float.parseFloat(s.toString());
					} else {
						changeValue = 0.0f;
					}
					
					if(changeValue != oldValue){
						addedPersonArrayList.get(position).setPaidamount(changeValue);
						//findUnEqualAmount(changeValue);
						//editText[position].setText(s);
						//oldValue = changeValue;
					}
					
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
	
				
			}
		});
		
//		holder.paidbyamount.setOnTouchListener(new OnTouchListener() {
//			
//			@SuppressLint("ClickableViewAccessibility")
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				  editable = true;
//					PaidbySplitActivity.unequalamountlinear.setVisibility(View.VISIBLE);
//					setUnCheckBox();
//				return false;
//			}
//		});
		
		holder.tviPaidByAmount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editable = true;
				holder.paidbyamount.setVisibility(View.VISIBLE);
				holder.tviPaidByAmount.setVisibility(View.GONE);
				PaidbySplitActivity.unequalamountlinear.setVisibility(View.VISIBLE);
				//setUnCheckBox();
			}
		});
		holder.paidbyamount.setTag(addedPersonArrayList.get(position));
		return convertView;
	}
	
	private class PersonItemViewHolder {
		CircularImageView paidbyimageview;
		TextView paidbyname;
		EditText paidbyamount;
		TextView tviPaidByAmount;
		CheckBox paidbyckbx;
	}

	public float findTotalAmount() {
		float total = 0;

		for (int i = 0; i < getCount(); i++) {

				String enterAmount = editText[i].getText().toString();
				if (enterAmount.length() > 0) {
					//addedPersonArrayList.get(i).setPaidamount(
							//Float.parseFloat(enterAmount));
					if(Float.parseFloat(enterAmount) > 0){
						addedPersonArrayList.get(i).setPaidpersonbool(true);
					} else {
						addedPersonArrayList.get(i).setPaidpersonbool(false);
					}
					total += Float.parseFloat(enterAmount);
				}
			
		}
		return total;
	}

	private float findEqualAmount() {
		float equalAmt = 0.00f;
		int count = 0;

		for (Person person : addedPersonArrayList) {
			if (person.isChechbxbool()) {

				count++;
			}
		}

		equalAmt = PaidbySplitActivity.totalAmount / count;

		return equalAmt;
	}

	private void setEqualAmount() {

		for (int i = 0; i < addedPersonArrayList.size(); i++) {

			Person person = addedPersonArrayList.get(i);
			if (person.isChechbxbool()) {

				editText[i].setText("" + equalAmount);
				person.setPaidamount(equalAmount);
				person.setPaidpersonbool(true);

			} else {
				editText[i].setText("0.00");
				person.setPaidamount(0.00f);
				person.setPaidpersonbool(false);

			}

		}
	}
	private void setUnCheckBox() {
		for (int i = 0; i < addedPersonArrayList.size(); i++) {

			Person person = addedPersonArrayList.get(i);
			
			person.setChechbxbool(false);	
			checkBox[i].setChecked(false);
		}
//		notifyDataSetChanged();
	}
	private void findUnEqualAmount(float changeValue){
		
		float pendingAmt = 0.0f;
		float paidAmt = 0.0f;
		for(int i = 0 ; i < getCount() ; i++){	
		
			String enterAmount= editText[i].getText().toString();
			if(enterAmount.length() > 0)
			{
				addedPersonArrayList.get(i).setPaidamount(Float.parseFloat(enterAmount));
				paidAmt += Float.parseFloat(enterAmount);
			}	
			
			
	}
		pendingAmt = PaidbySplitActivity.totalAmount - paidAmt;
		
		PaidbySplitActivity.pendingunequalamount.setText((String.format("%.2f", pendingAmt)));
		PaidbySplitActivity.totalsplitunequalamount.setText(String.format("%.2f", paidAmt));
		
	}

}
