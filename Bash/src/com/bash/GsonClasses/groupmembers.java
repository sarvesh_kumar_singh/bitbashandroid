package com.bash.GsonClasses;

import com.google.gson.annotations.SerializedName;

public class groupmembers {
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("fullname")
	public String fullname;
	@SerializedName("email")
	public String email;
	@SerializedName("phone_no")
	public String phone_no;
	@SerializedName("imagepath")
	public String imagepath;
	@SerializedName("usertype")
	public String usertype;

	public String getiduser() {
		return this.iduser;
	}

	public String getfullname() {
		return this.fullname;
	}

	public String getemail() {
		return this.email;
	}

	public String getphone_no() {
		return this.phone_no;
	}

	public String getimagepath() {
		return this.imagepath;
	}

	public String getusertype() {
		return this.usertype;
	}

	public groupmembers(String iduser, String fullname, String email,
			String phone_no, String imagepath, String usertype) {
		this.iduser = iduser;
		this.fullname = fullname;
		this.email = email;
		this.phone_no = phone_no;
		this.imagepath = imagepath;
		this.usertype = usertype;
	}
}
