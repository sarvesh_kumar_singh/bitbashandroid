package com.bash.ListModels;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Payers implements Comparable{
	@SerializedName("full_name")
	public String full_name;
	@SerializedName("image_location")
	public String image_location;
	@SerializedName("idpayer")
	public String idpayer;
	@SerializedName("idtrans")
	public String idtrans;
	@SerializedName("user_type")
	public String user_type;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("amount")
	public String amount;
	@SerializedName("payment_type")
	public String payment_type;
	@SerializedName("had_to_pay")
	public String had_to_pay;
	@SerializedName("group_information")
	public ArrayList<Group_Model> group_information;

	public String getFull_name() {
		return this.full_name;
	}

	public String getImage_location(){
		return this.image_location;
	}
	
	public String getIdpayer() {
		return this.idpayer;
	}

	public String getIdtrans() {
		return this.idtrans;
	}

	public String getUser_type() {
		return this.user_type;
	}

	public String getIduser() {
		return this.iduser;
	}

	public String getAmount() {
		return this.amount;
	}

	public String getPayment_type() {
		return this.payment_type;
	}

	public String getHad_to_pay() {
		return this.had_to_pay;
	}

	public ArrayList<Group_Model> getGroupInformation() {
		return this.group_information;
	}
	
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public Payers(String full_name, String image_location, String idpayer,
			String idtrans, String user_type, String iduser,
			String amount, String payment_type, String had_to_pay, ArrayList<Group_Model> group_information) {
		this.full_name = full_name;
		this.image_location = image_location;
		this.idpayer = idpayer;
		this.idtrans = idtrans;
		this.user_type = user_type;
		this.iduser = iduser;
		this.amount = amount;
		this.payment_type = payment_type;
		this.had_to_pay = had_to_pay;
		this.group_information = group_information;
	}

	/*public int compareTo(Payers payers) {
		// TODO Auto-generated method stub
		 int compareage=Integer.parseInt(((Payers)payers).getHad_to_pay());
	         For Ascending order
	        return Integer.parseInt(this.had_to_pay)-compareage;

	         For Descending order do like this 
	        //return compareage-this.studentage;
	}*/

	@Override
	public int compareTo(Object another) {
		// TODO Auto-generated method stub
		int compareage=Integer.parseInt(((Payers)another).getHad_to_pay());
        /* For Ascending order*/
        //return Integer.parseInt(this.had_to_pay)-compareage;
        /* For Descending order do like this */
       return compareage-Integer.parseInt(this.had_to_pay);
	}
}
