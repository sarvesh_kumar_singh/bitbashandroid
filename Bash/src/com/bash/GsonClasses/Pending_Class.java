package com.bash.GsonClasses;

import java.util.ArrayList;
import com.bash.ListModels.MyTrans_Friends_Class;
import com.bash.ListModels.MyTrans_Groups_Class;
import com.google.gson.annotations.SerializedName;

public class Pending_Class {

	@SerializedName("result")
	public boolean result;
	
	@SerializedName("total_bal")
	public total_bal total_bal;
	
	@SerializedName("you_owe")
	public you_are_owe you_owe;
	
	@SerializedName("you_are_owe")
	public you_are_owe you_are_owe;
	
	
	public class total_bal {
		
		@SerializedName("total")
		public String total;
		
		@SerializedName("friend")
		public ArrayList<MyTrans_Friends_Class> friend = new ArrayList<MyTrans_Friends_Class>();
		
		@SerializedName("group")
		public ArrayList<MyTrans_Groups_Class> group = new ArrayList<MyTrans_Groups_Class>();
		
	}
	
	public class you_are_owe {
		
		@SerializedName("total")
		public String total;
		
		@SerializedName("friend")
		public ArrayList<MyTrans_Friends_Class> friend = new ArrayList<MyTrans_Friends_Class>();
		
		@SerializedName("group")
		public ArrayList<MyTrans_Groups_Class> group = new ArrayList<MyTrans_Groups_Class>();
		
	}
	
	public class you_owe {
		
		@SerializedName("total")
		public String total;
		
		@SerializedName("friend")
		public ArrayList<MyTrans_Friends_Class> friend = new ArrayList<MyTrans_Friends_Class>();
		
		@SerializedName("group")
		public ArrayList<MyTrans_Groups_Class> group = new ArrayList<MyTrans_Groups_Class>();
		
	}

}

