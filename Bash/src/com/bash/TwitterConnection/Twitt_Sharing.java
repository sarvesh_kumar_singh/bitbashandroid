package com.bash.TwitterConnection;

import java.io.File;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bash.R;
import com.bash.Managers.DialogManager;
import com.bash.TwitterConnection.Twitter_Handler.TwDialogListener;


public class Twitt_Sharing {

    private final Twitter_Handler mTwitter;
    private final Activity activity;
    private String twitt_msg;
    private File image_path;
    Dialog twittDialog;
    View alertView;
    public Twitt_Sharing(Activity act, String consumer_key, String consumer_secret) 
    {
	this.activity = act;
	mTwitter = new Twitter_Handler(activity, consumer_key, consumer_secret);
	twittDialog = new Dialog(activity);
	twittDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    alertView = View.inflate(activity, R.layout.twitter_box, null);
    twittDialog.setContentView(alertView);
    
    ((Button) alertView.findViewById(R.id.twitterBtn)).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) 
		{
			// TODO Auto-generated method stub
			//String twitText = ((EditText)alertView.findViewById(R.id.twittText)).getText().toString();
			if(twitt_msg.trim() == null || twitt_msg.trim() == "")
			{
				Toast.makeText(activity, "Please Enter Twitt Text!", Toast.LENGTH_LONG).show();
			}
			else
			{
				UpdateStatus();
				twittDialog.dismiss();	
			}
			
		}
	});
    ((Button) alertView.findViewById(R.id.cancelTwittbtn)).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) 
		{
			twittDialog.dismiss();	
		}
	});
    
}
    
    
    public void shareToTwitter(String msg) {
		this.twitt_msg = msg;
		((EditText)alertView.findViewById(R.id.twittText)).setText(twitt_msg);
		mTwitter.setListener(mTwLoginDialogListener);
		
		if (mTwitter.hasAccessToken()) {
		    // this will post data in asyn background thread
		    showTwittDialog();
		} else {
		    mTwitter.authorize();
		}
    }

    private void showTwittDialog() 
    {
	    twittDialog.show();
	    //new PostTwittTask().execute(twitt_msg);
    }
    
   
    
    public void UpdateStatus()
	{
	  	// Check for blank text
    	//((EditText)alertView.findViewById(R.id.twittText)).setText(twitterText);
    	
		if (twitt_msg.trim().length() > 0) {
			// update status
			 new PostTwittTask().execute(twitt_msg);
		} else 
		{
			// EditText is empty
			Toast.makeText(activity, "Please enter status message", Toast.LENGTH_SHORT).show();
		}
		
	}

    private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {

	@Override
	public void onError(String value) {
	    showToast("Login Failed");
	    mTwitter.resetAccessToken();
	}

	@Override
	public void onComplete(String value) {
	    showTwittDialog();
	}
    };

    void showToast(final String msg) {
	activity.runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
	    	Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	    }
	});

    }

    class PostTwittTask extends AsyncTask<String, Void, String> {
	ProgressDialog pDialog;

	@Override
	protected void onPreExecute() {
	    pDialog = new ProgressDialog(activity);
	    pDialog.setMessage("Posting Twitt...");
	    pDialog.setCancelable(false);
	    pDialog.show();
	    super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... twitt) {
	    try {
		// mTwitter.updateStatus(twitt[0]);
		// File imgFile = new File("/sdcard/bluetooth/Baby.jpg");

		Share_Pic_Text_Titter(twitt_msg, mTwitter.twitterObj);
		return "success";

	    } catch (Exception e) {
		if (e.getMessage().toString().contains("duplicate")) {
			return "Posting Failed because of Duplicate message...";
		}
		e.printStackTrace();
		return "Posting Failed!!!";
	    }

	}

	@Override
	protected void onPostExecute(String result) {
	    pDialog.dismiss();

	    if (null != result && result.equals("success")) {
	    	
	    	DialogManager.showDialog(activity, "Message Twitted Sucessfully!");
		//showToast("Posted Successfully");

	    } else {
	    	DialogManager.showDialog(activity, "Duplicate Messge Couldn't be Twitt! Try Again!");
		showToast(result);
	    }

	    super.onPostExecute(result);
	}
    }

    public void Share_Pic_Text_Titter(String message,
	    Twitter twitter) throws Exception {
	try {
	    StatusUpdate st = new StatusUpdate(message);

	   // st.setMedia(image_path);
	     twitter.updateStatus(st);

	    /*
	     * Toast.makeText(activity, "Successfully update on Twitter...!",
	     * Toast.LENGTH_SHORT).show();
	     */
	} catch (TwitterException e) {
	    Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
	    /*Toast.makeText(activity,
		    "Ooopss..!!! Failed to update on Twitter.",
		    Toast.LENGTH_SHORT).show();*/
	    throw e;
	}
    }

    public void Authorize_UserDetail() {

    }
}
