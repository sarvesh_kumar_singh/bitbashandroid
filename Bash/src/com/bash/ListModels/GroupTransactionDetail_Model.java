package com.bash.ListModels;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class GroupTransactionDetail_Model{
	@SerializedName("idtrans")
	public String idtrans;
	@SerializedName("group_member_id")
	public String group_member_id;
	@SerializedName("groupname")
	public String groupname;
	@SerializedName("entered_by_full_name")
	public String entered_by_full_name;
	@SerializedName("image_location")
	public String image_location;
	@SerializedName("currency")
	public String currency;
	@SerializedName("date")
	public String date;
	@SerializedName("transaction_title")
	public String transaction_title;
	@SerializedName("datetime")
	public String datetime;
	@SerializedName("timestamp")
	public String timestamp;
	@SerializedName("amount")
	public String amount;
	@SerializedName("totalAmount")
	public String totalAmount;
	@SerializedName("amountType")
	public String amountType;
	@SerializedName("payers")
	public ArrayList<Payers> payerslist = new ArrayList<Payers>();
	@SerializedName("receivers")
	public ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
	
	
	public String getIdtrans() {
		return this.idtrans;
	}
	
	public String getGroup_member_id() {
		return this.group_member_id;
	}
	
	public String getGroupname() {
		return this.groupname;
	}

	public String getEntered_by_full_name() {
		return this.entered_by_full_name;
	}
	
	public String getImage_location() {
		return this.image_location;
	}
	
	public String getCurrency() {
		return this.currency;
	}
		
	public String getDate() {
		return this.date;
	}
	
	public String getTransaction_title() {
		return this.transaction_title;
	}

	public String getDatetime() {
		return this.datetime;
	}
	public String getTimestamp() {
		return this.timestamp;
	}
	
	public String getAmount() {
		return this.amount;
	}
	
	public String getTotalAmount() {
		return this.totalAmount;
	}

	public String getAmountType() {
		return this.amountType;
	}

	public ArrayList<Payers> getPayers() {
		return this.payerslist;
	}

	public ArrayList<Receivers> getReceivers() {
		return this.receiverslist;
	}
	
	public GroupTransactionDetail_Model(String idtrans, String group_member_id, String groupname, String entered_by_full_name,
			String image_location, String currency, String date, String transaction_title,
			String datetime, String timestamp, String amount, String totalAmount,
			String amountType, ArrayList<Payers> payerslist, ArrayList<Receivers> receiverslist) {
		this.idtrans = idtrans;
		this.group_member_id = group_member_id;
		this.groupname = groupname;
		this.entered_by_full_name = entered_by_full_name;
		this.image_location = image_location;
		this.currency = currency;
		this.date = date;
		this.transaction_title = transaction_title;
		this.datetime = datetime;
		this.timestamp = idtrans;
		this.amount = amount;
		this.totalAmount = totalAmount;
		this.amountType = amountType;
		this.payerslist=payerslist;
		this.receiverslist=receiverslist;
	}

	
}
