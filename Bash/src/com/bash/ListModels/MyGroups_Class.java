package com.bash.ListModels;

public class MyGroups_Class {
	
private String groupId;
private String groupName;
private String imagePath;
	
	public void setFieldValues(String groupName, String groupId, String imagePath){
		this.groupName = groupName;
		this.groupId = groupId;
		this.imagePath = imagePath;
	}
	
	public MyGroups_Class(String groupName, String groupId, String imagePath){
		this.groupName = groupName;
		this.groupId = groupId;
		this.imagePath = imagePath;
	}
	
	public String getgroupId(){
		return groupId;
	}
	
	public String getgroupName(){
		return groupName;
	}
	
	public String getimagePath(){
		return imagePath;
	}
}


