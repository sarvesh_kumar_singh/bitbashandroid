package com.bash.ListModels;

public class FeedCommentInformation_Model {

	public String idcomment, idpost, iduser, commenter_name, commenter_image,
			comment;

	public FeedCommentInformation_Model(String idcomment, String idpost,
			String iduser, String commenter_name, String commenter_image,
			String comment) {
		this.idcomment = idcomment;
		this.idpost = idpost;
		this.iduser = iduser;
		this.commenter_name = commenter_name;
		this.commenter_image = commenter_image;
		this.comment = comment;
	}

	public String getIdcomment() {
		return idcomment;
	}

	public void setIdcomment(String idcomment) {
		this.idcomment = idcomment;
	}

	public String getIdpost() {
		return idpost;
	}

	public String getIduser() {
		return iduser;
	}

	public String getCommenter_name() {
		return commenter_name;
	}

	public String getCommenter_image() {
		return commenter_image;
	}

	public String getComment() {
		return comment;
	}

}
