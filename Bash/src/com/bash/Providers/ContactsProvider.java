package com.bash.Providers;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Contacts.Data;
import android.util.Log;

import com.bash.Application.BashApplication;
import com.bash.GsonClasses.Login_Register_Class;
import com.bash.ListModels.Contacts_Class;
import com.bash.Managers.DataBaseManager;

public class ContactsProvider {

	public static Activity mActivity;
	public static ArrayList<Contacts_Class> contactsList = new ArrayList<Contacts_Class>();
	public static ProgressDialog pd;

	public static void loadContacts(Activity activity) {
		mActivity = activity;
		pd = new ProgressDialog(mActivity);
		pd.setMessage("Loading Contacts List");
		pd.setCancelable(false);
		pd.show();
		try{
		loadDeviceContacts();
		if(contactsList.size() != 0)
				new LoadContactsTask().execute();
		else
			pd.dismiss();
			
		} catch (Exception e) {
			e.printStackTrace();
			pd.dismiss();
		} finally {
			pd.dismiss();
		}
	}
	
	
	public static void ReloadContactsTable(Activity activity, Login_Register_Class responseForLogin) {
		mActivity = activity;
		loadDeviceContacts();
		DataBaseManager.getInstance().storeContactDetails(contactsList);
		DataBaseManager.getInstance().checkOutContactTable(responseForLogin.bashusers);
	}
	
	
	public static void loadDeviceContacts() {
		
		Cursor contacts = BashApplication.mContext.getContentResolver().query(Contacts.CONTENT_URI, 
		        null, Contacts.HAS_PHONE_NUMBER + " != 0", null, Contacts._ID + " ASC");
		
		Cursor data = BashApplication.mContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, 
		        Data.MIMETYPE + "=? OR " + Data.MIMETYPE + "=?", 
		        new String[]{Email.CONTENT_ITEM_TYPE, Phone.CONTENT_ITEM_TYPE}, 
		        ContactsContract.Data.CONTACT_ID + " ASC");

		int idIndex = contacts.getColumnIndexOrThrow(Contacts._ID);
		int nameIndex = contacts.getColumnIndexOrThrow(Contacts.DISPLAY_NAME);
		int cidIndex = data.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
		int data1Index = data.getColumnIndexOrThrow(Data.DATA1);
		
		boolean hasData = data.moveToNext();

		contactsList.clear();
		
	 	Contacts_Class contactDetails = null;
		
		while (contacts.moveToNext()) {
			contactDetails = new Contacts_Class();
		    long id = contacts.getLong(idIndex);
		    
		    //Log.e("Fun City Name", contacts.getString(nameIndex)+"");
		    contactDetails.setcontactName(contacts.getString(nameIndex));
		    
		    if (hasData) {
		        long cid = data.getLong(cidIndex);
		        while (cid <= id && hasData) {
		            if (cid == id) {
		            	String dataString = getFormattedString(data.getString(data1Index));
		            	if(dataString.contains("@")){
		            		contactDetails.setcontactEmailId(dataString);
		            	}else{
		            		contactDetails.setcontactNumber(dataString);
		            	}
		            }
		            hasData = data.moveToNext();
		            if (hasData) {
		                cid = data.getLong(cidIndex);
		            }
		        }
		    }
		    
		    if(contactDetails.getcontactNumber() != null)
		    	contactsList.add(contactDetails);
		}
		
	}
	
	private static String getFormattedString(String data){
		data = data.replaceAll(" ", "");
		data = data.replaceAll("\\(", "");
		data = data.replaceAll("\\)", "");
		data = data.replaceAll("\\+", "");
		data = data.replaceAll("\\-", "");
		return data;
	}
	
	 
	public static class LoadContactsTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
		/*	for(Contacts_Class item : contactsList){
			//	Log.e("Contact Name", item.getcontactName());
			//	Log.e("Contact Number", item.getcontactNumber());
				if(item.getcontactEmailId()!=null)
					Log.e("Contact EmailId", item.getcontactEmailId());
			}
			*/
			
			DataBaseManager.getInstance().storeContactDetails(contactsList);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
		}
	}
 
}
