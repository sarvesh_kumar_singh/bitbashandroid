package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class BashUsers_Class {

	@SerializedName("idfriend")
	public String idfriend;

	@SerializedName("phone_no")
	public String phone_no;

	@SerializedName("name")
	public String name;

	@SerializedName("imagepath")
	public String imagepath;

	@SerializedName("is_send_request")
	public String is_send_request;

	@SerializedName("is_request_received")
	public String is_request_received;

	@SerializedName("is_friend")
	public String is_friend;
	
	@SerializedName("emailid")
	public String emailid;
	
	@SerializedName("isselected")
	public boolean isselected = false;
	
	// 0        1     2          3               4         5            6               7                8
	//phoneno, name, bashid, contactbashname, imagepath, isbashuser, isrequestsent, isrequestreceived, isfriend
	public BashUsers_Class(String idfriend, String phone_no, String name, String imagepath, String is_send_request, 
						String is_request_received, String is_friend, boolean isselected){
		this.isselected = isselected;
		this.idfriend = idfriend;
		this.phone_no = phone_no;
		this.name = name;
		this.imagepath = imagepath;
		this.is_send_request = is_send_request;
		this.is_request_received = is_request_received;
		this.is_friend = is_friend;
	}
	
	
	// 0        1     2          3               4         5            6               7                8
	//phoneno, name, bashid, contactbashname, imagepath, isbashuser, isrequestsent, isrequestreceived, isfriend
	public BashUsers_Class(String idfriend, String phone_no, String name, String imagepath, String is_send_request, 
						String is_request_received, String is_friend){
		this.idfriend = idfriend;
		this.phone_no = phone_no;
		this.name = name;
		this.imagepath = imagepath;
		this.is_send_request = is_send_request;
		this.is_request_received = is_request_received;
		this.is_friend = is_friend;
		isselected = false;
	}
	
	public BashUsers_Class(String idfriend, String phone_no, String name, String imagepath, String is_send_request, 
			String is_request_received, String is_friend, String emaild){
		this.emailid = emaild;
		this.idfriend = idfriend;
		this.phone_no = phone_no;
		this.name = name;
		this.imagepath = imagepath;
		this.is_send_request = is_send_request;
		this.is_request_received = is_request_received;
		this.is_friend = is_friend;
		isselected = false;
	}
	
	public void setIsSelected(boolean isselected){
		this.isselected = isselected;
	}
	
	public boolean getIsSelected(){
		return isselected;
	}
	
	public String getEmailId(){
		return this.emailid;
	}
	
	public String getis_friend(){
		return this.is_friend;
	}
	
	public String getisFriend(){
		return this.idfriend;
	}
	public String getphone_no(){
		return this.phone_no;
	}
	public String getname(){
		return this.name;
	}
	public String getimagepath(){
		return this.imagepath;
	}
	public String getis_send_request(){
		return this.is_send_request;
	}
	public String getis_request_received(){
		return this.is_request_received;
	}
	
}
