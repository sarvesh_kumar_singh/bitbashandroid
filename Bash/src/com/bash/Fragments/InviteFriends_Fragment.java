package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.SlidingActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.ContactList_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

public class InviteFriends_Fragment extends BaseFragment implements
		OnClickListener, AsyncResponse {
	View mRootView;

	MyAsynTaskManager  myAsyncTask;
	private EditText txtName, txtEmailPhone;
	//public TextView outputText;
	MyFeedAdapter adapter;
	ListView lviContactList;
	String name, email, phone_no, facebookid, type, idfriend, image;
	String regexStr = "^[0-9]*$";
	boolean number=false;
	Dialog progressDialog;

	ArrayList<ContactList_Model> contactList = new ArrayList<ContactList_Model>();
	ArrayList<ContactList_Model> feedList = new ArrayList<ContactList_Model>();
	ContactList_Model listItem;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.fragment_invitefriends, null);
		init();
		//outputText = (TextView) mRootView.findViewById(R.id.textView1);
		
		if(((SlidingActivity)getActivity()).getContactList()==null){
			fetchContactsAsync();
			((SlidingActivity)getActivity()).initializeContact(contactList);	
		} else {
			contactList = ((SlidingActivity)getActivity()).getContactList();
			adapter.notifyWithDataSet(contactList);
		}
		

		return mRootView;
	}

	private void fetchContactsAsync() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() 
        {
            @Override
            protected void onPreExecute() 
            {
            	progressDialog=new Dialog((SlidingActivity)getActivity());
        		progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        		progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        		progressDialog.setContentView(R.layout.progress_dialog_view);
        		progressDialog.setCancelable(false);
        		/*progressDialog.setOnCancelListener(new OnCancelListener() {
        			@Override
        			public void onCancel(DialogInterface arg0) {
        				// TODO Auto-generated method stub
        				Log.e("Progress Dialog Closed!", "Manually!");
        				cancelAsynTask();
        			}
        		});*/
        		progressDialog.show();
            }// End of onPreExecute method

            @Override
            protected Void doInBackground(Void... params) 
            {
            	fetchContacts();

                return null;
            }// End of doInBackground method

            @Override
            protected void onPostExecute(Void result)
            {
            	progressDialog.dismiss();
                adapter.notifyWithDataSet(contactList);
            }//End of onPostExecute method
        }.execute((Void[]) null);
	}

	private void init() {
		// TODO Auto-generated method stub
		txtName = (EditText) mRootView.findViewById(R.id.txtName);
		txtEmailPhone = (EditText) mRootView.findViewById(R.id.txtEmailPhone);
		lviContactList = (ListView) mRootView.findViewById(R.id.lviContactList);

		adapter = new MyFeedAdapter(getActivity(), feedList);
		lviContactList.setAdapter(adapter);
		
		((SlidingActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn,
				"Back", "", "Invite", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
				});
		((TextView) getActivity().findViewById(R.id.toprightText))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// ((BaseFragment)getParentFragment()).popFragment();
						if(txtEmailPhone.getText().toString().length()>0 && txtName.getText().toString().length()>0){
							String subString = null;
							
							if(number)
							{
								if(txtEmailPhone.getText().toString().length()==13 )
								{
									subString=txtEmailPhone.getText().toString().substring(3, txtEmailPhone.getText().toString().length());
								}else if(txtEmailPhone.getText().toString().length()==10)
								{
									subString=txtEmailPhone.getText().toString();
								}
									phone_no=subString;
								if(listItem.getcontactEmailId()!=null)
									email=listItem.getcontactEmailId();
								else
									email="";
								
							} else {
								//txtEmailPhone.setText(listItem.getcontactEmailId());
								email=txtEmailPhone.getText().toString();
								if(listItem.getcontactNumber()!=null)
									phone_no=listItem.getcontactNumber();
								else
									phone_no="";
							}
							name=txtName.getText().toString();
							facebookid="";
							type="other";
							idfriend="";
							image="";
							//name, email, phone_no, facebookid, type, idfriend, image
							String emailid = email.trim();

							String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

							// onClick of button perform this simplest code.
							if (!email.equals("") && !emailid.matches(emailPattern))
							{
								Toast.makeText(getActivity(), "Please Enter a valid email id", Toast.LENGTH_LONG).show();
							}else if(phone_no!=null && phone_no.length()!=10){
								Toast.makeText(getActivity(), "Please Enter a valid phone no.", Toast.LENGTH_LONG).show();
							}else {
								inviteFriends(name, email, phone_no, facebookid, type, idfriend, image);
							}
						}else{
							Toast.makeText(getActivity(), "Please Enter friend's email/phone no and friend's name", Toast.LENGTH_LONG).show();
						}
						
					}
				});
		
		txtEmailPhone.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
				String text = txtEmailPhone.getText().toString().toLowerCase(Locale.getDefault());
				if(text.length()>0){
					lviContactList.setVisibility(View.VISIBLE);
				}else{
					lviContactList.setVisibility(View.GONE);
				}
				adapter.getFilter().filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
		});
		lviContactList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				listItem = getItem(position);

				if(txtEmailPhone.getText().toString().trim().matches(regexStr))
				{
					txtEmailPhone.setText(listItem.getcontactNumber());
					//email=txtEmailPhone.getText().toString();
					number=true;
				} else {
					txtEmailPhone.setText(listItem.getcontactEmailId());
					//phone_no=txtEmailPhone.getText().toString();
					number=false;
				}
				
				txtName.setText(listItem.getcontactName());
				
				lviContactList.setVisibility(View.GONE);
			}
		});
		

	}
	public void inviteFriends(String name, String email, String phoneNo, String facebookid, String type, String idfriend, String image) {
		 myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
			myAsyncTask.setupParamsAndUrl("inviteFriends",getActivity(),AppUrlList.ACTION_URL, 
					new String[] { "module", "action", "iduser","name", "email", "phone_no","facebookid", "type", "idfriend","image"}, 
					new String[] { "friend", "invitefriend", PreferenceManager.getInstance().getUserId(),name,email,phoneNo,facebookid,type,idfriend,image});
			myAsyncTask.execute();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviDebit:
			break;
		default:
			break;
		}
	}

	private ContactList_Model getItem(int position) {
		// TODO Auto-generated method stub
		return feedList.get(position);
	}
	
	public void fetchContacts() {

		String contact_id = null;
		String phoneNumber = null;
		String email = null;
		String name = null;

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();

		ContentResolver contentResolver = ((SlidingActivity) getActivity()).contentResolver;

		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				contact_id = cursor.getString(cursor.getColumnIndex(_ID));
				name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));

				if (hasPhoneNumber > 0) {

					output.append("\n First Name:" + name);

					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
						output.append("\n Phone number:" + phoneNumber);

					}

					phoneCursor.close();

					// Query and loop for every email of the contact
					Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (emailCursor.moveToNext()) {

						email = emailCursor.getString(emailCursor.getColumnIndex(DATA));

						output.append("\nEmail:" + email);
					}
					emailCursor.close();
				}
				contactList.add(new ContactList_Model(contact_id, name, phoneNumber, email));
				output.append("\n");
			}
			
		}
	}

	public class MyFeedAdapter extends BaseAdapter implements Filterable{

		//public ArrayList<ContactList_Model> feedList = new ArrayList<ContactList_Model>();
		public ArrayList<ContactList_Model> originalList = new ArrayList<ContactList_Model>();
		public Activity context;
		public FriendFilter filter;

		public MyFeedAdapter(Activity context, ArrayList<ContactList_Model> feedList1) {
			this.context = context;
			feedList = feedList1;
			this.originalList = feedList;
		}

		public void notifyWithDataSet(ArrayList<ContactList_Model> newList) {
			feedList = newList;
 	    	this.originalList = newList;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;

			final ContactList_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.custom_contactlistview, null);
				holder = new ViewHolder();
				holder.tviName = (TextView) convertView.findViewById(R.id.tviName);
				holder.tviEmail = (TextView) convertView.findViewById(R.id.tviEmail);
				holder.tviContact = (TextView) convertView.findViewById(R.id.tviContact);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
				holder.tviName.setText(listItem.getcontactName());
				if(listItem.getcontactEmailId()!=null){
					holder.tviEmail.setText(listItem.getcontactEmailId()+", ");
				}else{
					holder.tviEmail.setVisibility(View.GONE);
				}
				holder.tviContact.setText(listItem.getcontactNumber());
			return convertView;
		}

		@Override
		public int getCount() {
			return feedList.size();
		}

		@Override
		public ContactList_Model getItem(int position) {
			return feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public class ViewHolder {
			TextView tviName,tviEmail,tviContact;
			
		}

		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			if (filter == null)
				filter = new FriendFilter();
			return filter;
		}
		// filter Class...
				private class FriendFilter extends Filter {
					@Override
					protected FilterResults performFiltering(CharSequence constraint) {
						FilterResults results = new FilterResults();
						// We implement here the filter logic
						if (constraint == null || constraint.length() == 0) {
							// No filter implemented we return all the list
							results.values = originalList;
							results.count = originalList.size();
						} else {
							// We perform filtering operation
							List<ContactList_Model> tempList = new ArrayList<ContactList_Model>();
							for (ContactList_Model p : feedList) {
								if (p.getcontactEmailId()!=null && p.getcontactEmailId().toUpperCase().contains(constraint.toString().toUpperCase()))
									tempList.add(p);
								else if (p.getcontactNumber()!=null && p.getcontactNumber().contains(constraint.toString()))
									tempList.add(p);
							}
							results.values = tempList;
							results.count = tempList.size();
						}
						return results;
					}

					@Override
					protected void publishResults(CharSequence constraint, FilterResults results) {
						// Now we have to inform the adapter about the new list filtered
						if (results.count == 0)
							notifyDataSetInvalidated();
						else {
							feedList = (ArrayList<ContactList_Model>) results.values;
							notifyDataSetChanged();
						}
					}
				}

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("inviteFriends"))
		{
			
			if(output!=null)
			{
					try {
						JSONObject rootObj = new JSONObject(output);
						if(rootObj.getBoolean("result")){
							InviteFriends_Success_Fragment success=new InviteFriends_Success_Fragment();
							Bundle args = new Bundle();
							args.putString("name", rootObj.getString("fullname"));
							success.setArguments(args);
							((BaseFragment)getParentFragment()).replaceFragment(success, true);
							
						}
					else{
						DialogManager.showDialog(getActivity(), "Error in Delete Group! Try Again!");
					}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
			
			}else
			{

				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			
			}
		}
	}

}
