package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class NewsfeedPaidfor_model {
	@SerializedName("total_paid_for")
	public String total_paid_for;
	@SerializedName("payers_information")
	public NewsfeedPaidforInformation_Model[] paidfor_information;

	public NewsfeedPaidfor_model(String total_paid_for, NewsfeedPaidforInformation_Model[] paidfor_information) {
		this.total_paid_for = total_paid_for;
		this.paidfor_information = paidfor_information;

	}

	public String getTotal_paid_for() {
		return total_paid_for;
	}

	public NewsfeedPaidforInformation_Model[] getPaidfor_information() {
		return paidfor_information;
	}

}
