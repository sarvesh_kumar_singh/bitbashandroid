package com.bash.Activities;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import android.R.color;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.Fragments.MyGroupsFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.GroupTransactionDetail_Model;
import com.bash.ListModels.Group_Model;
import com.bash.ListModels.NewsFeed_Model;
import com.bash.ListModels.NewsfeedImageInformation_model;
import com.bash.ListModels.NewsfeedImage_Model;
import com.bash.ListModels.NewsfeedLikeInformation_Model;
import com.bash.ListModels.NewsfeedLikes_Model;
import com.bash.ListModels.NewsfeedPaidforInformation_Model;
import com.bash.ListModels.NewsfeedPaidfor_model;
import com.bash.ListModels.NewsfeedPayersInformation_Model;
import com.bash.ListModels.NewsfeedPayers_model;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Receivers;
import com.bash.ListModels.Transactions_information;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.Singleton;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyProfileActivity extends Activity implements AsyncResponse,
		OnClickListener {
	private Activity mActivity;
	private Context mContext;
	View view;
	int pos = 0,likeModel;
	String commentaction = "";
	String[] name;
	MyAsynTaskManager myAsyncTask;
	View mRootView, addgroupAlertView;
	public static SwipeListView myGroupListView;
	ListView myfeedListView;
	Dialog addmemberDialog;
	MyFriendsAdapter adapter;
	MyFeedAdapter feedAdapter;
	Friends_Class responseForFriends;
	public ArrayList<Transactions_information> feedList1, feedlistNew;
	public ArrayList<GroupTransactionDetail_Model> feedList = new ArrayList<GroupTransactionDetail_Model>();
	ArrayList<Group_Model> group_information = new ArrayList<Group_Model>();
	ArrayList<NewsFeed_Model> feedListFeed = new ArrayList<NewsFeed_Model>();
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView iviProfileImage, fileImage, iviBack, iviOption;
	private TextView tviProfilename,tviProfileAmount;
	String idGroup,group_member_id, userName, picturePath = "", amount, transtype, usertype, groupName, image_location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.profile_page_friend);
		Bundle extras = getIntent().getExtras();
       
		
		if(extras.getString("idUser")!=null)
			idGroup = extras.getString("idUser");
		if(extras.getString("Profilename")!=null)
			userName = extras.getString("Profilename");
		if(extras.getString("image")!=null)
			picturePath = extras.getString("image");
		if(extras.getString("amount")!=null)
			amount = extras.getString("amount");
		if(extras.getString("type")!=null)
			transtype = extras.getString("type");
		if(extras.getString("usertype")!=null)
		 usertype = extras.getString("usertype");
		
		initializeView();
		initializePictureAlertView();
		iviBack.setOnClickListener(this);
		iviOption.setOnClickListener(this);

		tviProfilename.setText(userName);
		if(transtype != null && transtype.equals("Pay")){
			tviProfileAmount.setText(amount);
			if(amount.equals("0")){
				tviProfileAmount.setBackgroundColor(color.transparent);//(R.drawable.amount_displaygreen_s);
				tviProfileAmount.setTextColor(Color.parseColor("#ffffff"));
				tviProfileAmount.setText(" Settled ");
        	}else{
        		tviProfileAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
        		tviProfileAmount.setTextColor(Color.parseColor("#ffffff"));
        		tviProfileAmount.setText(" + " + amount + " ₹");
        	}
		}else if(transtype != null && transtype.equals("Receive")){
			tviProfileAmount.setText(amount);
			if(amount.equals("0")){
				tviProfileAmount.setBackgroundColor(color.transparent);//(R.drawable.amount_displaygreen_s);
				tviProfileAmount.setTextColor(Color.parseColor("#ffffff"));
				tviProfileAmount.setText(" Settled ");
        	}else{
        		tviProfileAmount.setBackgroundResource(R.drawable.amount_displayred_s);
        		tviProfileAmount.setTextColor(Color.parseColor("#ffffff"));
        		tviProfileAmount.setText(" - " + amount + " ₹");
        	}
		} else {
			tviProfileAmount.setVisibility(View.GONE);
		}
		
		if(picturePath != null && picturePath.length()!= 0 && iviProfileImage != null){
        	ImageLoader.getInstance().displayImage(picturePath, 
        			iviProfileImage, BashApplication.options, BashApplication.animateFirstListener);
        }else{
        	iviProfileImage.setImageResource(R.drawable.picture_icon_s);
        }
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		mContext = MyProfileActivity.this;
		mActivity = MyProfileActivity.this;
		iviBack = (ImageView) findViewById(R.id.iviBack);
		iviOption = (ImageView) findViewById(R.id.iviOption);
		tviProfilename=(TextView)findViewById(R.id.tviProfilename);
		tviProfileAmount = (TextView)findViewById(R.id.tviProfileAmount);
		
		iviProfileImage=(ImageView)findViewById(R.id.iviProfileImage);

		//feedList.clear();
		myGroupListView = (SwipeListView) findViewById(R.id.groupListView);
		feedList1 = new ArrayList<Transactions_information>();
		adapter = new MyFriendsAdapter(mActivity, feedList);
		myGroupListView.setAdapter(adapter);
		
		/*myfeedListView = (ListView) findViewById(R.id.generalListView);
		feedAdapter = new MyFeedAdapter(mActivity, feedListFeed);
		myfeedListView.setAdapter(feedAdapter);*/

		if(idGroup != null)
			getGroupList();

		myGroupListView
				.setSwipeListViewListener(new BaseSwipeListViewListener() {
					@Override
					public void onClickFrontView(int position) {
					}

					@Override
					public void onClickBackView(int position) {
						Log.d("swipe",
								String.format("onClickBackView %d", position));
						myGroupListView.closeAnimate(position);// when you touch
																// back view it
																// will close
					}

					@Override
					public void onDismiss(int[] reverseSortedPositions) {
					}
				});

		myGroupListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there
																		// are
																		// five
																		// swiping
																		// modes
		myGroupListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); // there
																				// are
																				// four
																				// swipe
																				// actions
		myGroupListView.setOffsetLeft(PreferenceManager.getInstance()
				.getPercentageFromWidth(38));
		myGroupListView.setAnimationTime(50); // animarion time
		myGroupListView.setSwipeOpenOnLongPress(true); // enable or disable
														// SwipeOpenOnLongPress

		// Capture Text in EditText
		/*
		 * ((EditText)
		 * mRootView.findViewById(R.id.searchBox)).addTextChangedListener(new
		 * TextWatcher() {
		 * 
		 * @Override public void afterTextChanged(Editable arg0) { String text =
		 * ((EditText) mRootView.findViewById(R.id.searchBox))
		 * .getText().toString(); adapter.getFilter().filter(text); }
		 * 
		 * @Override public void beforeTextChanged(CharSequence arg0, int arg1,
		 * int arg2, int arg3) { }
		 * 
		 * @Override public void onTextChanged(CharSequence arg0, int arg1, int
		 * arg2, int arg3) { } });
		 */

	}

	private void initializeAlertView() {

		addmemberDialog = new Dialog(mContext);
		addmemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		addgroupAlertView = View.inflate(mContext,
				R.layout.alert_addgroup_dialog, null);
		addmemberDialog.setContentView(addgroupAlertView);

		fileImage = ((ImageView) addgroupAlertView
				.findViewById(R.id.groupImage));

		((ImageView) addgroupAlertView.findViewById(R.id.cancelDialog))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						addmemberDialog.dismiss();
					}
				});
		// block
		// ((ImageView)
		// addgroupAlertView.findViewById(R.id.addtoGroupButton)).setOnClickListener(new
		// OnClickListener() {
		// @Override
		// public void onClick(View v)
		// {
		// createGroupInServer(((EditText)addgroupAlertView.findViewById(R.id.groupText)).getText().toString());
		// }
		// });

		((ImageView) addgroupAlertView.findViewById(R.id.groupImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.show();
					}
				});

	}

	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(mContext,
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(mContext);
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);

		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						startActivityForResult(i, AppConstants.CODE_GALLERY);
					}
				});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});

	}

	public void getGroupList() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getGroupList", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action", "iduser", "idfriend", "idfriendtype"},
				new String[] { "friend", "getfriendtransaction", PreferenceManager.getInstance().getUserId(), idGroup , usertype});
		myAsyncTask.execute();

		// new MyAsynTaskManager(getActivity(), new LoadListener() {
		// @Override
		// public void onLoadComplete(final String jsonResponse) {
		// getActivity().runOnUiThread(new Runnable() {
		// @Override
		// public void run() {}
		// });
		// }
		// @Override
		// public void onError(final String errorMessage) {
		// getActivity().runOnUiThread(new Runnable() {
		// public void run() {
		// DialogManager.showDialog(getActivity(), errorMessage);
		// }
		// });
		//
		// }
		// }).execute();
	}

	public void createGroupInServer(final String groupName) {

		MyAsynImageTaskManager
				.setupParamsAndUrl(AppUrlList.ACTION_URL,
						new String[] { "module", "action", "iduser",
								"groupname", "groupimage" }, new String[] {
								"group", "add",
								PreferenceManager.getInstance().getUserId(),
								groupName, picturePath }, new String[] { "0",
								"0", "0", "0", "1" });

		new MyAsynImageTaskManager(mActivity,
				new MyAsynImageTaskManager.LoadListener() {

					@Override
					public void onLoadComplete(final String jsonResponse) {

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								try {
									if (jsonResponse == null) {
										Log.e("Null", "retured");
									}
									// 01-07 15:52:46.816: E/Reponse :(15432):
									// {"result":true,"msg":"success","idgroup":1}
									// 01-07 15:53:32.783: E/Reponse :(15432):
									// {"result":true,"msg":"success","grouplist":[{"idgroup":"1","groupname":"test group","imagepath":""}]}

									JSONObject rootObj = new JSONObject(
											jsonResponse);
									if (rootObj.getBoolean("result")) {
										addmemberDialog.dismiss();
										DialogManager.showDialog(mActivity,
												"Group Created Successfully!");

									} else {
										if (rootObj.getString("msg").equals(
												"Group Name exist"))
											DialogManager
													.showDialog(mActivity,
															"Group Name is Already Exist!");
										else
											DialogManager
													.showDialog(mActivity,
															"Error In Creating New Group! Try Again!");
									}
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
									DialogManager.showDialog(mActivity,
											"Server Error Occured! Try Again!");
								}
							}
						});

					}

					@Override
					public void onError(final String errorMessage) {
						runOnUiThread(new Runnable() {
							public void run() {
								DialogManager.showDialog(mActivity,
										errorMessage);
							}
						});

					}
				}).execute();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == mActivity.RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						iviProfileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == mActivity.RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = mActivity.getContentResolver().query(
						selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,
						iviProfileImage);
			}
			break;
		default:
			break;
		}

	}

	public class MyFriendsAdapter extends BaseAdapter
	{
		 	public ArrayList<GroupTransactionDetail_Model> feedList = new ArrayList<GroupTransactionDetail_Model>();
		 	//public ArrayList<Transactions_information> originalList = new ArrayList<Transactions_information>();
		 	public Activity context;
		 	public LinearLayout.LayoutParams backViewParams;
		    
		    public MyFriendsAdapter(Activity context, ArrayList<GroupTransactionDetail_Model> feedList) {
		        this.context = context;
		    	this.feedList = feedList;
		    	//this.originalList = feedList;
		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
		    			LayoutParams.MATCH_PARENT);
		    }
		    
		    public void notifyWithDataSet(ArrayList<GroupTransactionDetail_Model> newlist){
		    	this.feedList.clear();
		    	this.feedList = newlist;
		    	Log.e("Size of Adapter", String.valueOf(feedList.size()));
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        GroupTransactionDetail_Model listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	if(convertView == null)
		        	 {
		        	  	convertView = inflater.inflate(R.layout.custom_myprofile_listview, null);
			            holder = new ViewHolder();
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.tviTime = (TextView) convertView.findViewById(R.id.tviTime);
			            holder.tviAmount = (TextView) convertView.findViewById(R.id.tviAmount);
			            holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
			            holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
			            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
			            holder.tviDetail = (TextView) convertView.findViewById(R.id.tviDetail);
			            holder.tviDelete = (TextView) convertView.findViewById(R.id.tviDelete);
		        	}
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		       // Log.e("String from Adpater", listItem.getphone_no());
		        
		        //if(!listItem.getphone_no().equals("0")){
		        	
		        	holder.backView.setLayoutParams(backViewParams);
		        	//holder.friendImageSource.setImageResource(listItem.getfriendImageSource());
		        	if(listItem.getTransaction_title().length() <= 20){
		        		holder.friendName.setText(listItem.getTransaction_title());
		        	} else if(listItem.getTransaction_title().length() > 20){
		        		holder.friendName.setText(listItem.getTransaction_title().substring(0,16) + " ...");
		        	}
		        	String timeText = Utils.getFormattedTimerText(listItem.getDatetime());
					holder.tviTime.setText(timeText);
		        	
		        	 if(listItem.getAmountType() != null && listItem.getAmountType().equals("pay")){
		        		 holder.tviAmount.setVisibility(View.VISIBLE);
				        	if(listItem.getAmount().equals("0")){
				        		holder.tviAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
				        		holder.tviAmount.setText(" Settled ");
				        	}else{
				        		holder.tviAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
				        		holder.tviAmount.setText(" + " + listItem.getAmount());
				        	}
				        	
				        }else if(listItem.getAmountType() != null && listItem.getAmountType().equals("receive")){
				        	holder.tviAmount.setVisibility(View.VISIBLE);
				        	if(listItem.getAmount().equals("0")){
				        		holder.tviAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
				        		holder.tviAmount.setText(" Settled ");
				        	}else{
				        		holder.tviAmount.setBackgroundResource(R.drawable.amount_displayred_s);
				        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
				        		holder.tviAmount.setText(" - " + listItem.getAmount());
				        	}
				        	
					    } else {
					    	holder.tviAmount.setVisibility(View.INVISIBLE);
					    }
				        
				        if(listItem.getImage_location() != null && listItem.getImage_location().length()!= 0 && holder.friendImageSource != null){
				        	ImageLoader.getInstance().displayImage(listItem.getImage_location(), 
				        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
				        }else{
				        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
				        }
		        
		        	holder.deleteFriend.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							pos=position;
							// TODO Auto-generated method stub
							//removeFriendFromWebservice(pos);
							/*removeItemFromList(position);
							MyFriendsFragment.friendsListView.closeAnimate(position);*/
						}
					});
		        	holder.friendName.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						}
					});
		        	holder.tviDetail.setOnClickListener(new OnClickListener() {
		 				@Override
		 				public void onClick(View v) {
		 					// TODO Auto-generated method stub
		 					pos=position;
		 					//Toast.makeText(context, "Charge will Come soon...", Toast.LENGTH_LONG).show();
		 					Intent transectionDetail = new Intent(mContext, TransectionDetailActivity.class);
		 					transectionDetail.putExtra("group_member_id", feedList.get(pos).getGroup_member_id());
		 					Singleton.getInstance().listItem = getItem(pos);
		 					startActivity(transectionDetail);
		 				}
		 			});
		 	      holder.tviDelete.setOnClickListener(new OnClickListener() {
		 				@Override
		 				public void onClick(View v) {
		 					// TODO Auto-generated method stub
		 					pos=position;
		 					Toast.makeText(context, "Will Come soon...", Toast.LENGTH_LONG).show();
		 				}
		 			});
		        //}
		        
		        /*if(listItem.getimagepath() != null && listItem.getimagepath().length()!= 0 && holder.friendImageSource != null){
		        	ImageLoader.getInstance().displayImage(listItem.getimagepath(), 
		        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
		        }else{
		        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
		        }*/
		        
		        return convertView;
		    }

		    
		   /* public void checkEmptyList(){
		    	if(feedList.size() == 0){
					this.feedList.add(emptyList);
				}
		    }
		    */
		    @Override
		    public int getCount() {
		    	//	checkEmptyList();
		        return feedList.size();
		    }

		    @Override
		    public GroupTransactionDetail_Model getItem(int position) {
		        return feedList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }

		    private class ViewHolder {
		        TextView friendName, tviTime, tviAmount, tviDetail, tviDelete;
		        ImageView friendImageSource, deleteFriend;
		        RelativeLayout backView;
		    }

	}

	public void removeGroupFromServer(final String groupId, final int poistion) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		Log.e("Group Name", groupId);
		myAsyncTask.setupParamsAndUrl("removeGroupFromServer", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"idgroup", "iduser" }, new String[] { "group",
						"remove", groupId,
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();

	}
	
public class MyFeedAdapter extends BaseAdapter {
		
		public Activity context;
		public int msPerHour = 1000 * 60 * 60;
		boolean islike=false;

		public MyFeedAdapter(Activity context,
				ArrayList<NewsFeed_Model> feedList) {
			this.context = context;
			feedListFeed = feedList;
		}

		public void notifyWithDataSet(ArrayList<NewsFeed_Model> newList) {
			feedListFeed = newList;
			this.notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;
			String totalString = null;
			
			final NewsFeed_Model listItem = getItem(position);
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.custom_feed_myfeed_listviewnew, null);
				holder = new ViewHolder();
				holder.numberofLikes = (TextView) convertView.findViewById(R.id.numberofLikes);
				holder.numberofComments = (TextView) convertView.findViewById(R.id.numberofComments);
				holder.resonforPayment = (TextView) convertView.findViewById(R.id.resonforPayment);
				holder.amoutPaidBy = (TextView) convertView.findViewById(R.id.amoutPaidBy);
				holder.amoutPaidFor = (TextView) convertView.findViewById(R.id.amoutPaidFor);
				holder.placeofPayment = (TextView) convertView.findViewById(R.id.placesofPayment);
				holder.timeofComment = (TextView) convertView.findViewById(R.id.timeofComment);
				holder.paidText = (TextView) convertView.findViewById(R.id.paidText);
				holder.isLikeButton = (ImageView) convertView.findViewById(R.id.likesButton);
				holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
				holder.iviFeedOption =(ImageView) convertView.findViewById(R.id.iviFeedOption);
				holder.iviFeedImage1 = (ImageView) convertView.findViewById(R.id.iviFeedImage1);
				holder.iviFeedImage2 = (ImageView) convertView.findViewById(R.id.iviFeedImage2);
				holder.iviFeedImage3 = (ImageView) convertView.findViewById(R.id.iviFeedImage3);
				holder.iviFeedImage4 = (ImageView) convertView.findViewById(R.id.iviFeedImage4);
				holder.commentBox = (LinearLayout) convertView.findViewById(R.id.commentBox);
				holder.likesBox = (LinearLayout) convertView.findViewById(R.id.likesBox);
				holder.llFeedImage = (LinearLayout) convertView.findViewById(R.id.llFeedImage);
				holder.rlImageView=(RelativeLayout)convertView.findViewById(R.id.rlImageView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			//holder.amoutPaidBy.setText(listItem.getPoster_full_name().split(" ", 0)[0].toString());
			
			if (listItem.getPost_type().equals("pay")) {
				holder.paidText.setText(" paid ");
				totalString=listItem.getPoster_full_name().split(" ", 0)[0].toString() +" paid";
			} else {
				holder.paidText.setText(" received ");
				totalString=listItem.getPoster_full_name().split(" ", 0)[0].toString() +" received";
			}
			
			NewsfeedPayers_model payers=listItem.getPayers();
			NewsfeedPaidfor_model paid_for=listItem.getPaid_for();
			NewsfeedImage_Model images=listItem.getImages();
			
			String[] paidFor_namearray = null;
			String paidfor_name = null;
			int count=0;
			if(payers != null && paid_for != null && Integer.parseInt(payers.getTotal_payers()) > 0 && Integer.parseInt(paid_for.getTotal_paid_for())>0){
				paidFor_namearray=new String[Integer.parseInt(payers.getTotal_payers())+Integer.parseInt(paid_for.getTotal_paid_for())];
			}else if(payers != null && Integer.parseInt(payers.getTotal_payers())>0){
				paidFor_namearray=new String[Integer.parseInt(payers.getTotal_payers())];
			}else if(paid_for != null && Integer.parseInt(paid_for.getTotal_paid_for())>0){
				paidFor_namearray=new String[Integer.parseInt(paid_for.getTotal_paid_for())];
			}
			
			if(payers != null && Integer.parseInt(payers.getTotal_payers())>0){
				for(int i=0;i<payers.getPayers_information().length;i++){
					paidFor_namearray[count]=payers.getPayers_information()[i].getPayer_full_name().split(" ", 0)[0].toString();
					count++;
				}
			}
			if(paid_for != null && Integer.parseInt(paid_for.getTotal_paid_for())>0){
				for(int i=0;i<paid_for.getPaidfor_information().length;i++){
					paidFor_namearray[count]=paid_for.getPaidfor_information()[i].getFullname().split(" ", 0)[0].toString();
					count++;
				}
			}
			for(int totalcout = 0; totalcout < paidFor_namearray.length; totalcout++){
				if(totalcout == 0){
					paidfor_name = paidFor_namearray[totalcout];
				}else if(totalcout > 0 && totalcout < paidFor_namearray.length-1){
					paidfor_name = paidfor_name + ", " +paidFor_namearray[totalcout];
				}else if(totalcout>0 && totalcout==paidFor_namearray.length-1){
					paidfor_name=paidfor_name+" and "+paidFor_namearray[totalcout];
				}
			}
			holder.amoutPaidFor.setText(paidfor_name);
			holder.resonforPayment.setText(" For "+listItem.getPost_content());
			totalString=totalString +" "+ paidfor_name + " for "+listItem.getPost_content();
			int length1=0,length2=0,length3=0;
			String[] spanStringArray=totalString.split(" ");
			length1=spanStringArray[0].length();
			length2=spanStringArray[0].length()+spanStringArray[1].length()+1;
			length3=spanStringArray[0].length()+spanStringArray[1].length()+paidfor_name.length()+2;
			// Create a new spannable with the two strings
			Spannable spannable = new SpannableString(totalString);

			// Set the custom typeface to span over a section of the spannable object
			spannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, length1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, length1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.GRAY), length1,	length2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.BLACK), length2, length3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new StyleSpan(Typeface.BOLD), length2, length3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannable.setSpan(new ForegroundColorSpan(Color.GRAY), length3, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			// Set the text of a textView with the spannable object
			holder.amoutPaidBy.setText( spannable );

			// Sarvesh
			// Set comment from list item in listview
			  
			holder.numberofComments.setText("("+listItem.getCommentCount()+")");
			  
			  /*if(listItem.getLocation() != null &&
			  listItem.getLocation().length() != 0)
			  holder.placeofPayment.setText("At "+listItem.getLocation()); else
			  holder.placeofPayment.setVisibility(View.INVISIBLE);*/
			 

			// Log.e("Timer Text ", listItem.getRecordedOn());
			String timeText = Utils.getFormattedTimerText(listItem.getRecorded_on());
			holder.timeofComment.setText(timeText);

			holder.iviFeedOption.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					pos=position;
					if(feedListFeed.get(pos).getPosted_by_iduser().equals(PreferenceManager.getInstance().getUserId())){
						Toast.makeText(mContext, "Option Will come soon...", Toast.LENGTH_LONG);
					}
				}
			});
			
			holder.commentBox.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(Integer.parseInt(listItem.getLikeCount())>0){
						NewsfeedLikeInformation_Model[] newsfeedlike = listItem.getLikes().getLikes_information();
						name = new String[newsfeedlike.length];
						int count=0;
						
						if(listItem.getIslike()){
							name[count]="You";
							count++;
						}else{
							for(int lkinfo=0;lkinfo<newsfeedlike.length;lkinfo++){
								name[count]=newsfeedlike[lkinfo].getFullname().split(" ")[0];
								count++;
							}
						}
					}
					String name1="";
					int totalLike=Integer.parseInt(listItem.getLikeCount());
					if(Integer.parseInt(listItem.getLikeCount())==1){
						name1=name[0] + " like this";
					}else if(Integer.parseInt(listItem.getLikeCount())==2){
						name1=name[0] + " & "+name[1] + " like this";
					}else if(Integer.parseInt(listItem.getLikeCount())==3){
						name1=name[0] + ", "+name[1] + " & "+name[2] + " like this";
					}else if(Integer.parseInt(listItem.getLikeCount())>3){
						name1=name[0] + ", "+name[1] + " & " + String.valueOf(totalLike-2) + " other like this";
					}
					Intent intentComment= new Intent(mContext, Comment_Activity.class);
					intentComment.putExtra("idPost", listItem.getIdpost());
					intentComment.putExtra("like", listItem.getIslike());
					intentComment.putExtra("name", name1);
					startActivity(intentComment);
				}
			});


			// Sarvesh
			
			if(listItem.getIslike())
			  holder.isLikeButton.setImageResource(R.drawable.like_icon_selected); 
			else
			  holder.isLikeButton.setImageResource(R.drawable.like_icon_unselected);
			 

			final TextView numberOfLikeView = holder.numberofLikes;

			view = convertView;
			// Sarvesh
			islike=listItem.getIslike();
			
			holder.numberofLikes.setText("("+listItem.getLikeCount()+")");
			
			holder.likesBox.setOnClickListener(new OnClickListener() {
			  @Override 
			  public void onClick(View v) {
				  if(listItem.getIslike()){
					  commentaction="unlikefeed"; 
					  pos=position;
					  makeAnUnLike(listItem.getIdpost(), view, pos, commentaction);
				  }else if(!listItem.getIslike()){
					  commentaction="feedlike"; 
					  pos=position;
					  makeAnLike(listItem.getIdpost(), view, pos, commentaction);
				  }
			  
				  /*if(likes.get.equals("1")){
					  commentaction="unlikefeed"; pos=position;
					  makeAnLike(listItem.getIdTrans(), view, pos, commentaction);
				  }else{ 
					  commentaction="feedlike"; pos=position;
					  makeAnLike(listItem.getIdTrans(), view, pos, commentaction); 
				  } */
				  }
			  });
			 

			final ImageView myuserImage = holder.userImage;

			if (listItem.getPoster_image_location() != null
					&& listItem.getPoster_image_location().length() != 0) {
				ImageLoader.getInstance().displayImage(
						listItem.getPoster_image_location(), holder.userImage,
						BashApplication.options,
						BashApplication.animateFirstListener);
			} else {
				holder.userImage
						.setImageResource(R.drawable.addphoto_img_block);
			}

			// Sarvesh
			
			  if(images != null && Integer.parseInt(images.total_images) != 0){
				  NewsfeedImageInformation_model[] imageInfo = images.getImages_information();
				  holder.rlImageView.setVisibility(View.VISIBLE);
				  ImageLoader.getInstance().displayImage(imageInfo[0].getImage_location(), holder.iviFeedImage1); 
				  if(imageInfo.length>1){
					  holder.llFeedImage.setVisibility(View.VISIBLE);
					  if(imageInfo.length >= 5){
						  ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2); 
						  ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3); 
						  ImageLoader.getInstance().displayImage(imageInfo[3].getImage_location(), holder.iviFeedImage4); 
					  }else if(imageInfo.length == 4){
						  ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2); 
						  ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3); 
						  ImageLoader.getInstance().displayImage(imageInfo[3].getImage_location(), holder.iviFeedImage4); 
					  } else if(imageInfo.length == 3){
						  ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2); 
						  ImageLoader.getInstance().displayImage(imageInfo[2].getImage_location(), holder.iviFeedImage3); 
						  holder.iviFeedImage4.setVisibility(View.GONE);
					  } else if(imageInfo.length == 2){
						  ImageLoader.getInstance().displayImage(imageInfo[1].getImage_location(), holder.iviFeedImage2);
						  holder.iviFeedImage3.setVisibility(View.GONE);
						  holder.iviFeedImage4.setVisibility(View.GONE);
					  }
				  }else{
					  holder.llFeedImage.setVisibility(View.GONE);
				  }
				  
			  }else{
				  holder.rlImageView.setVisibility(View.GONE); 
			  }
			 

			return convertView;
		}

		@Override
		public int getCount() {
			return feedListFeed.size();
		}

		@Override
		public NewsFeed_Model getItem(int position) {
			return feedListFeed.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public class ViewHolder {
			TextView numberofLikes;
			TextView numberofComments;
			TextView resonforPayment;
			TextView amoutPaidBy;
			TextView amoutPaidFor;
			TextView placeofPayment;
			TextView timeofComment;
			TextView paidText;
			ImageView isLikeButton;
			ImageView userImage, iviFeedOption, iviFeedImage1,iviFeedImage2,iviFeedImage3,iviFeedImage4;
			LinearLayout commentBox, likesBox,llFeedImage;
			RelativeLayout rlImageView;
		}

	}

	public void makeAnLike(String commetnId, final View view,
			final int position, final String commentAction) {
		commentaction = commentAction;
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("makeAnLike", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser", "idpost" }, new String[] { "post",
						"likeAPost",PreferenceManager.getInstance().getUserId(), commetnId});
		myAsyncTask.execute();
		
	}
	public void makeAnUnLike(String commetnId, final View view,
			final int position, final String commentAction) {
		commentaction = commentAction;
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("makeAnLike", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser", "idpost" }, new String[] { "post",
						"undoLikeOnAPost",PreferenceManager.getInstance().getUserId(), commetnId});
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		ArrayList<NewsFeed_Model> newList = new ArrayList<NewsFeed_Model>();
		String idtrans, date, transaction_title, datetime, timestamp, amount = null, amountType = null;
		if (from.equalsIgnoreCase("getGroupList")) {
			if (output != null) {
				try {
					JSONObject jObj=new JSONObject(output);
					if(jObj.getBoolean("result")){
						JSONArray transArray=jObj.getJSONArray("Transaction_information");
							for(int i=0;i<transArray.length();i++){
							JSONObject TransRes=transArray.getJSONObject(i);
							JSONObject res=TransRes.getJSONObject("transaction");
							/*JSONArray grpDetail=res.getJSONObject("groupdetails").getJSONArray("groupmembers");
							for(int gd = 0; gd < grpDetail.length(); gd++){
								if(grpDetail.getJSONObject(gd).getString("contactid").equals(idGroup)){
									group_member_id = grpDetail.getJSONObject(gd).getString("group_member_id");
								}
							}*/
							ArrayList<Payers> payerslist = new ArrayList<Payers>();
							ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
							
							idtrans = res.getString("idtrans"); 
							date = res.getString("date"); 
							transaction_title = res.getString("transaction_title");
							datetime = res.getString("datetime");
							timestamp = res.getString("timestamp");
							
							JSONArray payerArray = res.getJSONArray("payers");
							JSONArray rcvrArray = res.getJSONArray("receivers");
							
							int totalTranAmount = Integer.parseInt(res.getString("total_amount"));
							
							for(int p=0;p<payerArray.length();p++){
								JSONObject payerRes=payerArray.getJSONObject(p);
								if(payerRes.getString("iduser").equals(PreferenceManager.getInstance().getUserId())){
									if(payerRes.getString("payment_type").equals("0")){
										int amtpaid = Integer.parseInt(payerRes.getString("amount"));
										int hadtopay = Integer.parseInt(payerRes.getString("had_to_pay"));
										int amt = 0;
										if(amtpaid>hadtopay){
						    				amt = amtpaid - hadtopay;
						    				amountType = "pay";
						    			}else{
						    				amt = hadtopay - amtpaid;
						    				amountType = "receive";
						    			}
										
										amount = String.valueOf(amt);
									} else if(payerRes.getString("payment_type").equals("1")){
										int ratio=0,ratioAmount;
										for(int pay=0;pay<payerslist.size();pay++){
						    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
						    			}
						    			for(int rec=0;rec<receiverslist.size();rec++){
						    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
						    			}
						    			ratioAmount=totalTranAmount/ratio;
						    			
						    			int amtpaid = Integer.parseInt(payerRes.getString("amount"));
										int hadtopay = ratioAmount * Integer.parseInt(payerRes.getString("had_to_pay"));
										int amt = 0;
										if(amtpaid>hadtopay){
						    				amt = amtpaid - hadtopay;
						    				amountType = "pay";
						    			}else{
						    				amt = hadtopay - amtpaid;
						    				amountType = "receive";
						    			}
										
										amount = String.valueOf(amt);
						    			
									} else if(payerRes.getString("payment_type").equals("2")){
										
										int amtpaid = Integer.parseInt(payerRes.getString("amount"));
										int hadtopay = (totalTranAmount * Integer.parseInt(payerRes.getString("had_to_pay")))/100;
										int amt = 0;
										if(amtpaid>hadtopay){
						    				amt = amtpaid - hadtopay;
						    				amountType = "pay";
						    			}else{
						    				amt = hadtopay - amtpaid;
						    				amountType = "receive";
						    			}
										
										amount = String.valueOf(amt);
										
									}
									//amount = "";
									
								}
								
								payerslist.add(new Payers(payerRes.getString("fullname"), payerRes.getString("imagepath"), payerRes.getString("idpayer"), payerRes.getString("idtrans"), payerRes.getString("user_type"), payerRes.getString("iduser"), payerRes.getString("amount"), payerRes.getString("payment_type"), payerRes.getString("had_to_pay"), group_information));
							}
							
							for(int r=0;r<rcvrArray.length();r++){
								JSONObject rcvrRes=rcvrArray.getJSONObject(r);
								
								if(rcvrRes.getString("iduser").equals(PreferenceManager.getInstance().getUserId())){
									if(rcvrRes.getString("payment_type").equals("0")){
										int amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
										
										amount = String.valueOf(amtpaid);
										amountType = "receive";
									} else if(rcvrRes.getString("payment_type").equals("1")){
										int ratio=0,ratioAmount;
										for(int pay=0;pay<payerslist.size();pay++){
						    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
						    			}
						    			for(int rec=0;rec<receiverslist.size();rec++){
						    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
						    			}
						    			ratioAmount=totalTranAmount/ratio;
						    			
						    			int amtpaid = ratioAmount * Integer.parseInt(rcvrRes.getString("amount"));
										
										amount = String.valueOf(amtpaid);
										amountType = "receive";
						    			
									} else if(rcvrRes.getString("payment_type").equals("2")){
										
										int amtpaid = Integer.parseInt(rcvrRes.getString("amount"));
										
										amount = String.valueOf(amtpaid);
										amountType = "receive";
									}
									//amount = "";
									
								}
								
								receiverslist.add(new Receivers(rcvrRes.getString("fullname"), rcvrRes.getString("imagepath"), rcvrRes.getString("idreceiver"), rcvrRes.getString("idtrans"), rcvrRes.getString("user_type"), rcvrRes.getString("iduser"), rcvrRes.getString("amount"), rcvrRes.getString("payment_type"), group_information));							
							}
							
							Collections.sort(payerslist);
							Collections.sort(receiverslist);
							/*feedList.add(new Transactions_information(res.getString("idtrans"),res.getString("trans_type"),"",res.getString("entered_by_iduser"),
									res.getString("currency"),res.getString("total_amount"),"", res.getString("category"),res.getString("date"),res.getString("transaction_title"),
									res.getString("datetime"),res.getString("timestamp"),res.getString("status"),payerslist,receiverslist));*/
							feedList1.add(new Transactions_information(res.getString("idtrans"),res.getString("trans_type"), "A",res.getString("entered_by_iduser"),
									res.getString("currency"),res.getString("total_amount"),"A", res.getString("category"),res.getString("date"),res.getString("transaction_title"),
									res.getString("datetime"),res.getString("timestamp"),res.getString("status"), payerslist,receiverslist));
							feedList.add(new GroupTransactionDetail_Model(idtrans, group_member_id, groupName, "A", image_location, res.getString("currency"), date, transaction_title, datetime, timestamp, amount, res.getString("total_amount"), amountType, payerslist, receiverslist));
						}
							//calculateMyTabs();
							adapter.notifyDataSetChanged();
					} else {
						DialogManager.showDialog(mActivity, "No Friends Found!");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.i("Error message", e.toString());
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");
			}
		} else if (from.equalsIgnoreCase("removeGroupFromServer")) {
			if (output != null) {

				try {
					if (output == null) {
						Log.e("Null", "retured");
					}
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {
						removeItemFromList(pos);
						DialogManager.showDialog(mActivity,
								"Group Deleted Successfully!");

					} else {
						DialogManager.showDialog(mActivity,
								"Error in Delete Group! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}

			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		} else if (from.equalsIgnoreCase("makeAnLike")) {
			if (output != null) {
				try {
					JSONObject rootObj = new JSONObject(output);
					if (rootObj.getBoolean("result")) {

						// likeTextView.setText(rootObj.getString("likes_count"));
						//((TextView) view.findViewById(R.id.numberofLikes)).setText(rootObj.getString("likes_count"));
						
						 //feedList.get(pos).setLikeCount(((TextView)view.findViewById(R.id.numberofLikes)).getText().toString());
						//feedList.get(pos).setIslike(true);
						likeModel=Integer.parseInt(feedListFeed.get(pos).getLikeCount());
						
						
						NewsfeedLikeInformation_Model[] info=feedListFeed.get(pos).getLikes().getLikes_information();
						if (commentaction.equals("unlikefeed")) {
							//feedList.get(pos).setIsLiked("0");
							feedListFeed.get(pos).setIslike(false);
							likeModel--;
							feedListFeed.get(pos).setLikeCount(String.valueOf(likeModel));
							((ImageView) view.findViewById(R.id.likesButton)).setImageResource(R.drawable.like_btn_deselect);
							//((TextView) view.findViewById(R.id.numberofLikes)).setText("("+String.valueOf(likeModel)+")");
						} else {
							//feedList.get(pos).setIsLiked("1");
							//likeModel=Integer.parseInt(feedList.get(pos).getLikes().getLikes_count());
							likeModel++;
							feedListFeed.get(pos).setIslike(true);
							((ImageView) view.findViewById(R.id.likesButton)).setImageResource(R.drawable.like_btn_select);
							//((TextView) view.findViewById(R.id.numberofLikes)).setText("("+String.valueOf(likeModel)+")");
							feedListFeed.get(pos).setLikeCount(String.valueOf(likeModel));
							
						}

						feedAdapter.notifyDataSetChanged();

					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(mActivity,
							"Server Error Occured! Try Again!");
				}
			} else {

				DialogManager.showDialog(mActivity,
						"Server Error Occured! Try Again!");

			}
		} 
	}
	public void updateAdapter(ArrayList<BashUsers_Class> newList){
		/*feedList.clear();
		feedList = DataBaseManager.getInstance().getFriendsList();*/
		//this.feedList = newList;
		//feedList = DataBaseManager.getInstance().getFriendsList();
		if(newList != null && newList.size() != 0){
			//adapter.notifyWithDataSet(newList);
		} 
	}
	public void removeItemFromList(int position) {
		MyGroupsFragment.myGroupListView.closeAnimate(position);
		feedList.remove(position);
		//adapter.notifyDataSetInvalidated();
		//adapter.notifyDataSetChanged();
	}
	
	public void getNewsFeedListByType(String type) {
		// getlatest
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getNewsFeedListByType", mActivity,
				AppUrlList.ACTION_URL, new String[] { "module", "action",
						"iduser" }, new String[] { "post", type, idGroup});
		myAsyncTask.execute();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iviBack:
			mActivity.finish();
			break;
		case R.id.iviOption:
			openOptionsMenu();
			break;
		default:
			break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	MenuInflater inflater = getMenuInflater();
	inflater.inflate(R.menu.profile_menu, menu);
	return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.itemRemind:
		    // do part 1
			Toast.makeText(mContext, "First Item Click", Toast.LENGTH_LONG).show();
		    return true;
		case R.id.itemSettle:
		    // do part 2
			Toast.makeText(mContext, "Second Item Click", Toast.LENGTH_LONG).show();
		    return true;
		case R.id.itemViewFeed:
		    // do part 3
			Intent feedIntent = new Intent(mContext, ViewFeedActivity.class);
			feedIntent.putExtra("groupName", userName);
			feedIntent.putExtra("idGroup", idGroup);
			feedIntent.putExtra("type", "friend");
			startActivity(feedIntent);
			//Toast.makeText(mContext, "Third Item Click", Toast.LENGTH_LONG).show();
			/*myGroupListView.setVisibility(View.GONE);
			myfeedListView.setVisibility(View.VISIBLE);
			getNewsFeedListByType("getAllPosts");*/
		    return true;
		case R.id.itemBlock:
		    // do part 3
			Toast.makeText(mContext, "Fourth Item Click", Toast.LENGTH_LONG).show();
		    return true;
		case R.id.itemDelete:
		    // do part 3
			Toast.makeText(mContext, "Fifth Item Click", Toast.LENGTH_LONG).show();
		    return true;
		default:
		    return super.onOptionsItemSelected(item);
		}
	}
}
