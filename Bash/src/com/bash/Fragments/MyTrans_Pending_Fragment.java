package com.bash.Fragments;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings.Global;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.MyProfileActivity;
import com.bash.Activities.SlidingActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.MyTabs_Model;
import com.bash.ListModels.Group_Model;
import com.bash.ListModels.MyTabs_ValueModel;
import com.bash.ListModels.Payers;
import com.bash.ListModels.Receivers;
import com.bash.ListModels.Transactions_information;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyTrans_Pending_Fragment extends BaseFragment implements
		AsyncResponse {
	MyAsynTaskManager myAsyncTask;
	public MyTabs_Model pendingNewClass;
	public ArrayList<Transactions_information> feedList,feedlistNew;
	public ArrayList<MyTabs_ValueModel> myListoriginal, rawMyList, myList;;
	ArrayList<Group_Model> group_information = new ArrayList<Group_Model>();
	View mRootView;
	public static FragmentTabHost mTabhost;
	public static SwipeListView lviMyTabs;
	public Transactions_information tabModel;
	public MyTabsAdapter myTabsAdapter;
	int pos = 0;
	boolean show=false;

	private FloatingActionMenu fab;
	private FloatingActionButton fab1, fab2, fab3;

	public static View totalbalanceView, youOweView, youAreOweView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.fragment_transaction_home, null);
		mTabhost = (FragmentTabHost) mRootView
				.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(),
				R.id.fragmentframe_holder);
		lviMyTabs = (SwipeListView) mRootView.findViewById(R.id.lviMyTabs);
		lviMyTabs.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		lviMyTabs.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); //there are four swipe actions
		lviMyTabs.setOffsetLeft(PreferenceManager.getInstance().getPercentageFromWidth(38));
		lviMyTabs.setAnimationTime(50); // animarion time
		lviMyTabs.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
		
		rawMyList = new ArrayList<MyTabs_ValueModel>();
		myList = new ArrayList<MyTabs_ValueModel>();
		myListoriginal = new ArrayList<MyTabs_ValueModel>();
		feedList = new ArrayList<Transactions_information>();
		feedlistNew = new ArrayList<Transactions_information>();
		myTabsAdapter = new MyTabsAdapter(getActivity(), myList);
		
		lviMyTabs.setAdapter(myTabsAdapter);

		// initializeListValues();
		((SlidingActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn,"", "My Tabs","", 0);
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).popFragment();
				//((SlidingActivity) getActivity()).slidingmenu_layout.toggleMenu();
				((SlidingActivity) getActivity()).dLayout.openDrawer(Gravity.LEFT);
			}
	 });
		initializeTabHost();
		//listclickPerform();
		// initializeRangeSeekBar();
		
		fab = (FloatingActionMenu) mRootView.findViewById(R.id.menu1);
	 	fab1 = (FloatingActionButton) mRootView.findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) mRootView.findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) mRootView.findViewById(R.id.fab3);
        
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);
        fab3.setOnClickListener(clickListener);  
        
        fab.hideMenuButton(false);
        fab.setClosedOnTouchOutside(true);
        fab.showMenuButton(true);
        fab.setIconAnimated(true);
		
		return mRootView;
	}

	

	private void initializeTabHost() {

		totalbalanceView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mytransaction_selector_bg, null);
		youOweView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mytransaction_selector_bg, null);
		youAreOweView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mytransaction_selector_bg, null);

		mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable totalbaltoSpan = new SpannableString("Total Balance\n"
				+ AppConstants.RASYMBOL + " 0");
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#75C814")), 13,
				totalbaltoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) totalbalanceView.findViewById(R.id.tabText))
				.setText(totalbaltoSpan);

		Spannable toreceivetoSpan = new SpannableString("To Receive\n"
				+ AppConstants.RASYMBOL + " 0");
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#75C814")), 10,
				toreceivetoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youOweView.findViewById(R.id.tabText))
				.setText(toreceivetoSpan);

		Spannable topaytoSpan = new SpannableString("To Pay\n"
				+ AppConstants.RASYMBOL + " 0");
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 5,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.RED), 6,
				topaytoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youAreOweView.findViewById(R.id.tabText))
				.setText(topaytoSpan);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_TAG)
						.setIndicator(totalbalanceView),
				ContainerProvider.Tab_MyTransactions_TotalBalacne_Container.class,
				null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_OWE_TAG)
				.setIndicator(youOweView),
				ContainerProvider.Tab_MyTransactions_YouOwe_Container.class,
				null);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_ARE_OWED_TAG)
						.setIndicator(youAreOweView),
				ContainerProvider.Tab_MyTransactions_YouAreOwed_Container.class,
				null);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				Log.i("Click", tabId);
				//Toast.makeText(mActivity, tabId, Toast.LENGTH_LONG).show();
				if(tabId.equals("tab_home_totalbalance_tag")){
					myList.clear();
					myList.addAll(myListoriginal);
				}else if(tabId.equals("tab_home_you_owe_tag")){
					myList.clear();
					for(int i=0;i<myListoriginal.size();i++){
						if(myListoriginal.get(i).getType().equals("receive") && !myListoriginal.get(i).getAmount().equals("0")){
							myList.add(new MyTabs_ValueModel(myListoriginal.get(i).getFull_name(), myListoriginal.get(i).getDate(), myListoriginal.get(i).getDatetime(), myListoriginal.get(i).getTimestamp(), myListoriginal.get(i).getUser_type(), myListoriginal.get(i).getIduser(), myListoriginal.get(i).getAmount(), myListoriginal.get(i).getImage(),"receive"));
						}
					}
				}else if(tabId.equals("tab_home_you_are_owed_tag")){
					myList.clear();
					for(int i=0;i<myListoriginal.size();i++){
						if(myListoriginal.get(i).getType().equals("pay") && !myListoriginal.get(i).getAmount().equals("0")){
							myList.add(new MyTabs_ValueModel(myListoriginal.get(i).getFull_name(), myListoriginal.get(i).getDate(), myListoriginal.get(i).getDatetime(), myListoriginal.get(i).getTimestamp(), myListoriginal.get(i).getUser_type(), myListoriginal.get(i).getIduser(), myListoriginal.get(i).getAmount(), myListoriginal.get(i).getImage(),"pay"));
						}
					}
				}
				myTabsAdapter.notifyDataSetChanged();
			}
		});
		show=false;
		getPendingTransactionDetails();
	}

	public void getPendingTransactionDetails() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getAllTransactionDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "iduser" }, new String[] { "transaction",
						"getAllTransactions",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
		

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
		if (from.equalsIgnoreCase("getAllTransactionDetails")) {
			if (output != null) {

				try {
					JSONObject jObj=new JSONObject(output);
					if(jObj.getBoolean("result")){
						JSONArray transArray=jObj.getJSONArray("transactions_information");
							for(int i=0;i<transArray.length();i++){
							JSONObject res=transArray.getJSONObject(i);
							
							ArrayList<Payers> payerslist = new ArrayList<Payers>();
							ArrayList<Receivers> receiverslist = new ArrayList<Receivers>();
							
							JSONObject payerObj=res.getJSONObject("payers");
							JSONObject rcvrObj=res.getJSONObject("receivers");
							
							int totalTranAmount = Integer.parseInt(res.getString("total_amount"));
							
							
							if(payerObj.getBoolean("result")){
								JSONArray payerArray=payerObj.getJSONArray("payers_information");
								for(int p=0;p<payerArray.length();p++){
									JSONObject payerRes=payerArray.getJSONObject(p);
									payerslist.add(new Payers(payerRes.getString("full_name"), payerRes.getString("image_location"), payerRes.getString("idpayer"), 
											payerRes.getString("idtrans"), payerRes.getString("user_type"), payerRes.getString("iduser"), payerRes.getString("amount"), 
											payerRes.getString("payment_type"), payerRes.getString("had_to_pay"), group_information));
								}
							}
							if(rcvrObj.getBoolean("result")){
								JSONArray rcvrArray=rcvrObj.getJSONArray("receivers_information");
								if(rcvrObj.getBoolean("result")){
									for(int r=0;r<rcvrArray.length();r++){
										JSONObject rcvrRes=rcvrArray.getJSONObject(r);
										receiverslist.add(new Receivers(rcvrRes.getString("full_name"), rcvrRes.getString("image_location"), rcvrRes.getString("idreceiver"), 
												rcvrRes.getString("idtrans"), rcvrRes.getString("user_type"), rcvrRes.getString("iduser"), rcvrRes.getString("amount"),
												rcvrRes.getString("payment_type"), group_information));							
									}
								}
							}
							Collections.sort(payerslist);
							Collections.sort(receiverslist);
							feedList.add(new Transactions_information(res.getString("idtrans"),res.getString("trans_type"),"",res.getString("entered_by_iduser"),
									res.getString("currency"),res.getString("total_amount"),"", res.getString("category"),res.getString("date"),res.getString("transaction_title"),
									res.getString("datetime"),res.getString("timestamp"),res.getString("status"),payerslist,receiverslist));
						}
							calculateMyTabs();
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
					

			} else {
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");
			}

		}

	}
	public void calculateMyTabs(){
		int paymentType, hadtoPay;
		//ArrayList<Receivers> receiverslistNew = new ArrayList<Receivers>();
		for(int fList=0;fList<feedList.size();fList++){
			int pAmt, pTAmt, ratioAmount=0;
			int ratio=0;
			ArrayList<Payers> payerslistNew = new ArrayList<Payers>();
			ArrayList<Payers> payerslist = feedList.get(fList).getPayers();
			ArrayList<Receivers> receiverslistNew = new ArrayList<Receivers>();
	    	int totalAmount=Integer.parseInt(feedList.get(fList).getTotal_amount());
	    	ArrayList<Receivers> receiverslist = feedList.get(fList).getReceivers();
	    	boolean isRatio=true;
	    	if(!feedList.get(fList).getTrans_type().equals("pay") && !feedList.get(fList).getTrans_type().equals("receive")){
		    	for(int p = 0; p < payerslist.size(); p++){
					paymentType = Integer.parseInt(payerslist.get(p).getPayment_type());
					hadtoPay = Integer.parseInt(payerslist.get(p).getHad_to_pay());
					//if(paymentType == 0){
						pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
						pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
						if(pTAmt >= pAmt){
							payerslistNew.add(new Payers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), payerslist.get(p).getAmount(), payerslist.get(p).getPayment_type(), payerslist.get(p).getHad_to_pay(), group_information));
						}else{
							int amt = pAmt-pTAmt;
							receiverslist.add(new Receivers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), String.valueOf(amt), payerslist.get(p).getPayment_type(), group_information));
						}
					receiverslistNew.clear();
					receiverslistNew.addAll(receiverslist);
		    	}
	    	} else {
	    		receiverslistNew.clear();
				receiverslistNew.add(new Receivers(payerslist.get(0).getFull_name(), payerslist.get(0).getImage_location(), payerslist.get(0).getIdpayer(), payerslist.get(0).getIdtrans(), payerslist.get(0).getUser_type(), payerslist.get(0).getIduser(), payerslist.get(0).getHad_to_pay(), payerslist.get(0).getPayment_type(), payerslist.get(0).getGroupInformation()));
				payerslistNew.clear();
				payerslistNew.add(new Payers(receiverslist.get(0).getFull_name(), receiverslist.get(0).getImage_location(), receiverslist.get(0).getIdreceiver(), receiverslist.get(0).getIdtrans(), receiverslist.get(0).getUser_type(), receiverslist.get(0).getIduser(), "0", receiverslist.get(0).getPayment_type(), payerslist.get(0).getHad_to_pay(), receiverslist.get(0).getGroupInformation()));
	    	}
	    	Collections.sort(receiverslistNew);
	    	feedlistNew.add(new Transactions_information(feedList.get(fList).getIdtrans(), feedList.get(fList).getTrans_type(), feedList.get(fList).getEntered_by_full_name(),feedList.get(fList).getEntered_by_iduser(), feedList.get(fList).getCurrency(), feedList.get(fList).getTotal_amount(), String.valueOf(ratio), feedList.get(fList).getCategory(), feedList.get(fList).getDate(), feedList.get(fList).getTransaction_title(), feedList.get(fList).getDatetime(), feedList.get(fList).getTimestamp(), feedList.get(fList).getStatus(), payerslistNew, receiverslistNew));
		}
		
		calculatePayerReceiver(feedlistNew);
		//calculatePayerReceiverOld(feedlistNew);
	}
	
	
	
	private void calculatePayerReceiver(ArrayList<Transactions_information> feedlistNew2) {
		// TODO Auto-generated method stub
		ArrayList<Transactions_information> feedlistNew = feedlistNew2;
		for (int i=0; i < feedlistNew.size(); i++){
			Transactions_information listItem = feedlistNew.get(i);
			
			String name = null, date=null, datetime=null, timestamp=null, image = null, userType = null,userId = null;
			
			date = listItem.getDate();
			datetime = listItem.getDatetime();
			timestamp = listItem.getTimestamp();
	    	ArrayList<Payers> payerslist = listItem.getPayers();
	    	ArrayList<Receivers> receiverslist = listItem.getReceivers();
	    	
		    int myAmountPayers=0,payer=0,receiver=0,pAmt,pTAmt;
		 // Find Receiver data
			for(int r=0; r<receiverslist.size(); r++){
				receiver=Integer.parseInt(receiverslist.get(r).getAmount());
		    	for(int p=0; p<payerslist.size(); p++){
	    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
	    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
	    			if(pAmt>pTAmt){
	    				payer=pAmt-pTAmt;
	    			}else{
	    				payer=pTAmt-pAmt;
	    			}
	    			name=payerslist.get(p).getFull_name();
				    userType=payerslist.get(p).getUser_type();
				    userId=payerslist.get(p).getIduser();
				    image=payerslist.get(p).getImage_location();
				    
				    if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){	
					    MyTabs_ValueModel tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(payer), image,"pay");
					    rawMyList.add(tvm);
				    }
		    	}
		    }
			// Find Payer data
			for(int p = 0; p< payerslist.size(); p++){
    			pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
    			if(pAmt > pTAmt){
    				payer = pAmt - pTAmt;
    			}else{
    				payer = pTAmt - pAmt;
    			}
    			myAmountPayers=payer;
    			for(int r=0; r < receiverslist.size(); r++){
		    		receiver = Integer.parseInt(receiverslist.get(r).getAmount());
		    		name = receiverslist.get(r).getFull_name();
		    		userType = receiverslist.get(r).getUser_type();
		    		userId = receiverslist.get(r).getIduser();
		    		image = receiverslist.get(r).getImage_location();

		    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
			    		MyTabs_ValueModel tvm1 = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(receiver), image, "receive");
			    		rawMyList.add(tvm1);
		    		}
		    		
    			}
			}
		}
		mergeRawMyList(rawMyList);
		/*myList.addAll(rawMyList);
		myListoriginal.addAll(myList);
		myTabsAdapter.notifyDataSetChanged();*/
	}

	private void mergeRawMyList(ArrayList<MyTabs_ValueModel> rawMyList2) {
		// TODO Auto-generated method stub
		ArrayList<MyTabs_ValueModel> rawMyList = rawMyList2;
		boolean add = false;
		for(int i = 0; i < rawMyList.size(); i++){
			 MyTabs_ValueModel tvm = rawMyList.get(i);
			 if(myList.size() > 0){
				for(int x=0; x < myList.size(); x++){
					MyTabs_ValueModel tvm1 = myList.get(x);
					add = false;
					int receiver = 0;
					String type = null;
			    	if(tvm.getIduser().equals(tvm1.getIduser())){
			    		if(tvm.getType().equals("receive") && tvm1.getType().equals("receive")){
			    			receiver = Integer.parseInt(tvm1.getAmount()) + Integer.parseInt(tvm.getAmount());
				    		myList.set(x, new MyTabs_ValueModel(tvm1.getFull_name(), tvm1.getDate(), tvm1.getDatetime(), tvm1.getTimestamp(), 
						    			tvm1.getUser_type(), tvm1.getIduser(), String.valueOf(receiver), tvm1.getImage(), "receive"));
				    		add=true;
				    		break;			    			
			    		} else if(tvm.getType().equals("pay") && tvm1.getType().equals("pay")){
			    			receiver = Integer.parseInt(tvm1.getAmount()) + Integer.parseInt(tvm.getAmount());
				    		myList.set(x, new MyTabs_ValueModel(tvm1.getFull_name(), tvm1.getDate(), tvm1.getDatetime(), tvm1.getTimestamp(), 
						    			tvm1.getUser_type(), tvm1.getIduser(), String.valueOf(receiver), tvm1.getImage(), "pay"));
				    		add=true;
				    		break;			    			
			    		} else if(tvm.getType().equals("receive") && tvm1.getType().equals("pay")){
			    			if(Integer.parseInt(tvm1.getAmount()) >= Integer.parseInt(tvm.getAmount())){
			    				receiver = Integer.parseInt(tvm1.getAmount()) - Integer.parseInt(tvm.getAmount());
			    				type = "pay";
			    			} else {
			    				receiver = Integer.parseInt(tvm.getAmount()) - Integer.parseInt(tvm1.getAmount());
			    				type = "receive";
			    			}
				    		myList.set(x, new MyTabs_ValueModel(tvm1.getFull_name(), tvm1.getDate(), tvm1.getDatetime(), tvm1.getTimestamp(), 
						    			tvm1.getUser_type(), tvm1.getIduser(), String.valueOf(receiver), tvm1.getImage(), type));
				    		add=true;
				    		break;
			    			
			    		} else if(tvm.getType().equals("pay") && tvm1.getType().equals("receive")){
			    			if(Integer.parseInt(tvm1.getAmount()) >= Integer.parseInt(tvm.getAmount())){
			    				receiver = Integer.parseInt(tvm1.getAmount()) - Integer.parseInt(tvm.getAmount());
			    				type = "receive";
			    			} else {
			    				receiver = Integer.parseInt(tvm.getAmount()) - Integer.parseInt(tvm1.getAmount());
			    				type = "pay";
			    			}
				    		myList.set(x, new MyTabs_ValueModel(tvm1.getFull_name(), tvm1.getDate(), tvm1.getDatetime(), tvm1.getTimestamp(), 
						    			tvm1.getUser_type(), tvm1.getIduser(), String.valueOf(receiver), tvm1.getImage(), type));
				    		add=true;
				    		break;
			    			
			    		}
			    		
			    	} 
			   	}
			   if(!add){
				   myList.add(tvm);
			    }
			 } else {
				 myList.add(tvm);
			 }
		}
		myListoriginal.addAll(myList);
		myTabsAdapter.notifyDataSetChanged();
	}

	public class MyTabsAdapter extends BaseAdapter
	{
		 	public ArrayList<MyTabs_ValueModel> myList = new ArrayList<MyTabs_ValueModel>();
		 	public ArrayList<MyTabs_ValueModel> originalList = new ArrayList<MyTabs_ValueModel>();
		 	public Activity context;
		 	ViewHolder holder = null;
		 	public LinearLayout.LayoutParams backViewParams;
		   
		    
		    public MyTabsAdapter(Activity context, ArrayList<MyTabs_ValueModel> feedList) {
		        this.context = context;
		    	this.myList = feedList;
		    	this.originalList = feedList;
		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
		    			LayoutParams.MATCH_PARENT);
		    }
		    
			public void notifyWithDataSet(ArrayList<MyTabs_ValueModel> newlist) {
				this.myList.clear();
				this.myList = newlist;
				this.originalList = newlist;
				Log.e("Size of Adapter", String.valueOf(feedList.size()));
				this.notifyDataSetChanged();
			}

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        
		    	MyTabs_ValueModel listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	if(convertView == null)
		        	 {
		        	  	convertView = inflater.inflate(R.layout.custom_myfriendongroup_listview, null);
			            holder = new ViewHolder();
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.tviPayRec = (TextView) convertView.findViewById(R.id.tviPayRec);
			            holder.tviAmount=(TextView)convertView.findViewById(R.id.tviAmount);
			            holder.friendImageSource = (ImageView) convertView.findViewById(R.id.iviCommentantImage);
			            holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
			            holder.llAmount = (LinearLayout) convertView.findViewById(R.id.llAmount);
			            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
			            holder.tviCharge = (TextView) convertView.findViewById(R.id.tviCharge);
			            holder.tviPay = (TextView) convertView.findViewById(R.id.tviPay);
		        	}
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        
		        if(!show){
		        	
			        long totalPay = getTotalPay();
			        long totalReceive = getTotalReceive();
			        long totalBalance = totalReceive-totalPay;
			        String symbol = null;
			        if(totalReceive > totalPay){
			        	symbol="+";
			        	totalBalance = totalReceive - totalPay;
			        } else if(totalReceive < totalPay){
			        	symbol="-";
			        	totalBalance=totalPay - totalReceive;
			        } else if(totalReceive == totalPay){
			        	symbol="";
			        }
			        ((SlidingActivity)getActivity()).setUserAmount(symbol, String.valueOf(totalBalance));
			        show=true;
			        Spannable totalbaltoSpan = new SpannableString("Total Balance\n"
							+ AppConstants.RASYMBOL + " " + symbol + totalBalance);
					totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					
					if(symbol.equals("-")){
					totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.RED), 13,
							totalbaltoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					} else {
						totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#75C814")), 13,
								totalbaltoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					}
					
					((TextView) totalbalanceView.findViewById(R.id.tabText))
							.setText(totalbaltoSpan);
	
					Spannable toreceivetoSpan = new SpannableString("To Receive\n"
							+ AppConstants.RASYMBOL + " "+totalReceive);
					toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#75C814")), 10,
							toreceivetoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					((TextView) youOweView.findViewById(R.id.tabText))
							.setText(toreceivetoSpan);
	
					Spannable topaytoSpan = new SpannableString("To Pay\n"
							+ AppConstants.RASYMBOL + " "+totalPay);
					topaytoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 5,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					topaytoSpan.setSpan(new ForegroundColorSpan(Color.RED), 6,
							topaytoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					((TextView) youAreOweView.findViewById(R.id.tabText))
							.setText(topaytoSpan);
		        
		        }
		        
		        holder.tviCharge.setText("Remind");
	            holder.tviPay.setText("Settle");
		    			    		
			        holder.friendName.setText(listItem.getFull_name());
			        if(listItem.getType().equals("receive")){
			        	holder.llAmount.setVisibility(View.VISIBLE);
			        	if(listItem.getAmount().equals("0")){
			        		holder.llAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
			        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
			        		holder.tviPayRec.setVisibility(View.INVISIBLE);
			        		holder.tviAmount.setText(" Settled ");
			        		//holder.tviAmount.setTypeface(Typeface.DEFAULT);
			        	}else{
			        		holder.llAmount.setBackgroundResource(R.drawable.amount_displaygreen_s);
			        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
			        		holder.tviPayRec.setText("+");
			        		holder.tviAmount.setText(listItem.getAmount());
			        		//holder.tviAmount.setTypeface(Typeface.DEFAULT_BOLD);
			        	}
			        	
			        }else if(listItem.getType().equals("pay")){
			        	holder.llAmount.setVisibility(View.VISIBLE);
			        	if(listItem.getAmount().equals("0")){
			        		holder.llAmount.setBackgroundColor(Color.parseColor("#ffffff"));//(R.drawable.amount_displaygreen_s);
			        		holder.tviAmount.setTextColor(Color.parseColor("#000000"));
			        		holder.tviPayRec.setVisibility(View.INVISIBLE);
			        		holder.tviAmount.setText(" Settled ");
			        		//holder.tviAmount.setTypeface(Typeface.DEFAULT);
			        	}else{
			        		holder.llAmount.setBackgroundResource(R.drawable.amount_displayred_s);
			        		holder.tviAmount.setTextColor(Color.parseColor("#ffffff"));
			        		holder.tviPayRec.setText("-");
			        		holder.tviAmount.setText(listItem.getAmount());
			        		//holder.tviAmount.setTypeface(Typeface.DEFAULT_BOLD);
			        	}
			        	
				    } 
			        
			        if(listItem.getImage() != null && listItem.getImage().length()!= 0 && holder.friendImageSource != null){
			        	ImageLoader.getInstance().displayImage(listItem.getImage(), 
			        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
			        }else{
			        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
			        }
			        
			        holder.friendImageSource.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent profileIntent = new Intent(((SlidingActivity)getActivity()), MyProfileActivity.class);
							profileIntent.putExtra("idUser", myList.get(position).getIduser());
							profileIntent.putExtra("Profilename", myList.get(position).getFull_name());
							profileIntent.putExtra("image", myList.get(position).getImage());
							profileIntent.putExtra("amount", myList.get(position).getAmount());
							profileIntent.putExtra("type", myList.get(position).getType());
							profileIntent.putExtra("usertype", myList.get(position).getUser_type());
							startActivity(profileIntent);
						}
					});
			        holder.friendName.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent profileIntent = new Intent(((SlidingActivity)getActivity()), MyProfileActivity.class);
							profileIntent.putExtra("idUser", myList.get(position).getIduser());
							profileIntent.putExtra("Profilename", myList.get(position).getFull_name());
							profileIntent.putExtra("image", myList.get(position).getImage());
							profileIntent.putExtra("amount", myList.get(position).getAmount());
							profileIntent.putExtra("type", myList.get(position).getType());
							profileIntent.putExtra("usertype", myList.get(position).getUser_type());
							startActivity(profileIntent);
						}
					});
					holder.llAmount.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent profileIntent = new Intent(((SlidingActivity)getActivity()), MyProfileActivity.class);
							profileIntent.putExtra("idUser", myList.get(position).getIduser());
							profileIntent.putExtra("Profilename", myList.get(position).getFull_name());
							profileIntent.putExtra("image", myList.get(position).getImage());
							profileIntent.putExtra("amount", myList.get(position).getAmount());
							profileIntent.putExtra("type", myList.get(position).getType());
							profileIntent.putExtra("usertype", myList.get(position).getUser_type());
							startActivity(profileIntent);
						}
					});
		    	
		      
		        return convertView;
		    }

		    
		    @Override
		    public int getCount() {
		    	//	checkEmptyList();
		        return myList.size();
		    }

		    @Override
		    public MyTabs_ValueModel getItem(int position) {
		        return myList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }
		    
		    public long getTotalPay() {
		    	long totalReceiv=0;
		    	for(int count=0;count<myList.size();count++){
		    		if(myList.get(count).getType().equals("pay")){
			        	totalReceiv = totalReceiv + Integer.parseInt(myList.get(count).getAmount());
				    } 
		    	}
		        return totalReceiv;
		    }

		    public long getTotalReceive() {
		    	long totalPay=0;
		    	for(int count=0;count<myList.size();count++){
		    		if(myList.get(count).getType().equals("receive")){
		    			totalPay = totalPay + Integer.parseInt(myList.get(count).getAmount());
				    } 
		    	}
		        return totalPay;
		    }

		    private class ViewHolder {
		        TextView friendName, tviPayRec, tviAmount, tviCharge, tviPay;
		        ImageView friendImageSource, deleteFriend;
		        RelativeLayout backView;
		        LinearLayout llAmount;
		    }
				
	}
	
	private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.menu1:         	
                break;
                case R.id.fab1:
                	((BaseFragment)getParentFragment()).replaceFragment(new CreateGroup_Fragment(), true);
                    break;
                case R.id.fab2:
                	((BaseFragment)getParentFragment()).replaceFragment(new InviteFriends_Fragment(), true);
    				((SlidingActivity)getActivity()).currentFragment=MyTrans_Pending_Fragment.this;
                    break;
                case R.id.fab3:
                    startActivity(new Intent(getActivity(), AddTabActivity.class));
                    break;
            }  	
        }
    };
    
    private void calculatePayerReceiverOld(ArrayList<Transactions_information> feedlistNew2) {
		// Payer Calculation
		for(int fList=0;fList<feedlistNew.size();fList++){
			String name = null, date=null, datetime=null, timestamp=null, image=null, userType = null,userId = null;
			boolean add=false;
			Transactions_information listItem = feedlistNew.get(fList);
			int totalAmount=Integer.parseInt(listItem.getTotal_amount());
			date = listItem.getDate();
			datetime = listItem.getDatetime();
			timestamp = listItem.getTimestamp();
	    	ArrayList<Payers> payerslist = listItem.getPayers();
	    	
	    	ArrayList<Receivers> receiverslist = listItem.getReceivers();
	    	if(!feedlistNew.get(fList).getTrans_type().equals("pay") && !feedlistNew.get(fList).getTrans_type().equals("receive")){
	    		
	    		// Calculation for split kind of transection
	    		
	    		int receverCount=0, ratio=Integer.parseInt(listItem.getRatio()), myAmountPayers=0,payer=0,recever=0,pAmt,pTAmt,ratioAmount=0;
		    	if(!listItem.getRatio().equals("")){
		    		ratio=Integer.parseInt(listItem.getRatio());
		    	}
				for(int p = 0; p< payerslist.size(); p++){
		    			pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers=payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    			recever = Integer.parseInt(receiverslist.get(r).getAmount());
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		MyTabs_ValueModel tvm = null ;
				    		if(myAmountPayers - recever == 0){
				    			receverCount = r+1;
				    			//break;
				    		} else if(myAmountPayers - recever > 0){
				    			myAmountPayers = myAmountPayers - recever;
				    			receverCount = r;
				    			//break;
				    		} 
				    		payer = payer - recever;
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null
						    				&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
							    			int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    			} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
						    				&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"pay");
					    			myList.add(tvm);
					    		}
				    		}
				    		
		    			}
			    }
			} else {
				
				// Calculation for pay and receive kind of transection
				
				int myAmountPayers=0,receverCount=0,payer=0,recever=0,pAmt,pTAmt,ratio=Integer.parseInt(listItem.getRatio()),ratioAmount=0;
		    	if(!listItem.getRatio().equals("")){
		    		ratio=Integer.parseInt(listItem.getRatio());
		    	}
				for(int p = 0; p< payerslist.size(); p++){
					//if(payerslist.get(p).getPayment_type().equals("0")){
		    			pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers=payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    		//if(receiverslist.get(r).getPayment_type().equals("0")){
				    			recever = Integer.parseInt(receiverslist.get(r).getAmount());
				    		//}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		} else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer=payer-recever;
				    		
		    			}
					if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
			    		add = false;
			    		MyTabs_ValueModel tvm = null ;
			    		for(int x=0; x < myList.size(); x++){
				    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
				    				&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
			    				/*int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
				    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");*/
				    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    			int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
				    			} else {
			    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
				    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
			    				}
				    			myList.set(x, tvm);
				    			add=true;
				    			break;
				    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
				    				&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
			    				if(Integer.parseInt(myList.get(x).getAmount()) >= myAmountPayers){
			    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
				    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
			    				} else {
			    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
				    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
			    				}
			    				
			    				myList.set(x, tvm);
			    				add=true;
			    				break;
			    			}  
			    		}
			    		if(!add){
			    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"pay");
			    			myList.add(tvm);
			    		}
			    	}
			    }
			}
		}
		
		// Receiver Calculation
		for(int fList = 0; fList < feedlistNew.size(); fList++){
			String name = null, date=null, datetime=null, timestamp=null, image = null, userType = null,userId = null;
			boolean add=false;
			
			Transactions_information listItem = feedlistNew.get(fList);
			int totalAmount=Integer.parseInt(listItem.getTotal_amount());
			date = listItem.getDate();
			datetime = listItem.getDatetime();
			timestamp = listItem.getTimestamp();
	    	ArrayList<Payers> payerslist = listItem.getPayers();
	    	ArrayList<Receivers> receiverslist = listItem.getReceivers();
	    	
	    	if(!feedlistNew.get(fList).getTrans_type().equals("pay") && !feedlistNew.get(fList).getTrans_type().equals("receive")){
		        int myAmountPayers=0,payersCount=0,payer=0,receiver=0,pAmt,pTAmt,ratio=0,ratioAmount=0;
				for(int r=0;r<receiverslist.size();r++){
					//if(receiverslist.get(r).getPayment_type().equals("0")){
						receiver=Integer.parseInt(receiverslist.get(r).getAmount());
		    			for(int p=payersCount;p<payerslist.size();p++){
				    		//if(payerslist.get(p).getPayment_type().equals("0")){
				    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
				    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
				    			if(pAmt>pTAmt){
				    				payer=pAmt-pTAmt;
				    			}else{
				    				payer=pTAmt-pAmt;
				    			}
				    			myAmountPayers=payer;
				    		//}
				    		name=payerslist.get(p).getFull_name();
				    		userType=payerslist.get(p).getUser_type();
				    		userId=payerslist.get(p).getIduser();
				    		image=payerslist.get(p).getImage_location();
				    		if(payer-receiver==0){
				    			payer=payer-receiver;
				    			payersCount=p+1;
				    			//break;
				    		}else if(payer-receiver<=0){
				    			payer=payer-receiver;
				    			payersCount=p;
				    			//break;
				    		}
				    		payer=payer-receiver;
				    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
					    					&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= receiver){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - receiver;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				} else {
					    					int amnt = receiver - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
					    				/*int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");*/
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
					    					&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= receiver){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - receiver;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = receiver - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				/*int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");*/
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(receiver), image,"receive");
					    			myList.add(tvm);
					    		}
					    	}
		    			}
		    		} 
		    } else {
		    	 int myAmountPayers=0,payersCount=0,payer=0,receiver=0,pAmt,pTAmt,ratio=0,ratioAmount=0;
			    	
					for(int r=0;r<receiverslist.size();r++){
						//if(receiverslist.get(r).getPayment_type().equals("0")){
							receiver=Integer.parseInt(receiverslist.get(r).getAmount());
			    			for(int p=payersCount;p<payerslist.size();p++){
					    		//if(payerslist.get(p).getPayment_type().equals("0")){
					    			
					    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
					    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
					    			if(pAmt>pTAmt){
					    				payer=pAmt-pTAmt;
					    			}else{
					    				payer=pTAmt-pAmt;
					    			}
					    			myAmountPayers=payer;
					    		//}
					    		name=payerslist.get(p).getFull_name();
					    		userType=payerslist.get(p).getUser_type();
					    		userId=payerslist.get(p).getIduser();
					    		image=payerslist.get(p).getImage_location();
					    		if(payer-receiver==0){
					    			payer=payer-receiver;
					    			payersCount=p+1;
					    			//break;
					    		}else if(payer-receiver<=0){
					    			payer=payer-receiver;
					    			payersCount=p;
					    			//break;
					    		}
					    		payer=payer-receiver;
					    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
						    					&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + receiver;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null 
						    					&& myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= receiver){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - receiver;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = receiver - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(receiver), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}
			    			}
			    		} 
		    }
		}
		
		myListoriginal.addAll(myList);
		myTabsAdapter.notifyDataSetChanged();
	}
    
    public void calculateMyTabsOld(){
		int paymentType, hadtoPay;
		//ArrayList<Receivers> receiverslistNew = new ArrayList<Receivers>();
		for(int fList=0;fList<feedList.size();fList++){
			int pAmt, pTAmt, ratioAmount=0;
			int ratio=0;
			ArrayList<Payers> payerslistNew = new ArrayList<Payers>();
			ArrayList<Payers> payerslist = feedList.get(fList).getPayers();
			ArrayList<Receivers> receiverslistNew = new ArrayList<Receivers>();
	    	int totalAmount=Integer.parseInt(feedList.get(fList).getTotal_amount());
	    	ArrayList<Receivers> receiverslist = feedList.get(fList).getReceivers();
	    	boolean isRatio=true;
	    	if(!feedList.get(fList).getTrans_type().equals("pay") && !feedList.get(fList).getTrans_type().equals("receive")){
		    	for(int p = 0; p < payerslist.size(); p++){
					paymentType = Integer.parseInt(payerslist.get(p).getPayment_type());
					hadtoPay = Integer.parseInt(payerslist.get(p).getHad_to_pay());
					//if(paymentType == 0){
						pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
						pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
						if(pTAmt >= pAmt){
							payerslistNew.add(new Payers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), payerslist.get(p).getAmount(), payerslist.get(p).getPayment_type(), payerslist.get(p).getHad_to_pay(), group_information));
						}else{
							int amt = pAmt-pTAmt;
							receiverslist.add(new Receivers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), String.valueOf(amt), payerslist.get(p).getPayment_type(), group_information));
						}
						
					/*} else if(paymentType == 1){
						if(isRatio){
							for(int pay = 0; pay < payerslist.size(); pay++){
			    				ratio = ratio + Integer.parseInt(payerslist.get(pay).getHad_to_pay());
			    			}
			    			for(int r = 0; r < receiverslist.size(); r++){
			    				ratio = ratio + Integer.parseInt(receiverslist.get(r).getAmount());
			    			}
			    			ratioAmount=totalAmount/ratio;
			    			isRatio=false;
						}
		    			pAmt = ratioAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
						
		    			if(pTAmt>=pAmt){
							payerslistNew.add(new Payers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), payerslist.get(p).getAmount(), payerslist.get(p).getPayment_type(), payerslist.get(p).getHad_to_pay(), group_information));
						}else{
							int amt=pAmt-pTAmt;
							receiverslist.add(new Receivers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), String.valueOf(amt/ratioAmount), payerslist.get(p).getPayment_type(), group_information));
						}
						
					} else if(paymentType == 2){
						pAmt=totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay())/100;
		    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pTAmt>=pAmt){
							payerslistNew.add(new Payers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), payerslist.get(p).getAmount(), payerslist.get(p).getPayment_type(), payerslist.get(p).getHad_to_pay(), group_information));
						}else{
							int amt=pAmt-pTAmt;
							receiverslist.add(new Receivers(payerslist.get(p).getFull_name(), payerslist.get(p).getImage_location(), payerslist.get(p).getIdpayer(), payerslist.get(p).getIdtrans(), payerslist.get(p).getUser_type(), payerslist.get(p).getIduser(), String.valueOf(amt), payerslist.get(p).getPayment_type(), group_information));
						}
					}*/
					receiverslistNew.clear();
					receiverslistNew.addAll(receiverslist);
		    	}
	    	} else /*if(feedList.get(fList).getTrans_type().equals("pay"))*/{
	    		receiverslistNew.clear();
				//receiverslistNew.addAll(receiverslist);
				receiverslistNew.add(new Receivers(payerslist.get(0).getFull_name(), payerslist.get(0).getImage_location(), payerslist.get(0).getIdpayer(), payerslist.get(0).getIdtrans(), payerslist.get(0).getUser_type(), payerslist.get(0).getIduser(), payerslist.get(0).getHad_to_pay(), payerslist.get(0).getPayment_type(), payerslist.get(0).getGroupInformation()));
				payerslistNew.clear();
//				payerslistNew.addAll(payerslist);
				//payerslistNew.add(new Payers(full_name, image_location, idpayer, idtrans, user_type, iduser, amount, payment_type, had_to_pay, group_information));
				payerslistNew.add(new Payers(receiverslist.get(0).getFull_name(), receiverslist.get(0).getImage_location(), receiverslist.get(0).getIdreceiver(), receiverslist.get(0).getIdtrans(), receiverslist.get(0).getUser_type(), receiverslist.get(0).getIduser(), "0", receiverslist.get(0).getPayment_type(), payerslist.get(0).getHad_to_pay(), receiverslist.get(0).getGroupInformation()));
	    	/*} else if(feedList.get(fList).getTrans_type().equals("receive")){
	    		receiverslistNew.clear();
				receiverslistNew.addAll(receiverslist);
				payerslistNew.clear();
				payerslistNew.addAll(payerslist);*/
	    	}
	    	Collections.sort(receiverslistNew);
	    	feedlistNew.add(new Transactions_information(feedList.get(fList).getIdtrans(), feedList.get(fList).getTrans_type(), feedList.get(fList).getEntered_by_full_name(),feedList.get(fList).getEntered_by_iduser(), feedList.get(fList).getCurrency(), feedList.get(fList).getTotal_amount(), String.valueOf(ratio), feedList.get(fList).getCategory(), feedList.get(fList).getDate(), feedList.get(fList).getTransaction_title(), feedList.get(fList).getDatetime(), feedList.get(fList).getTimestamp(), feedList.get(fList).getStatus(), payerslistNew, receiverslistNew));
		}
		
		// Payer Calculation
		for(int fList=0;fList<feedlistNew.size();fList++){
			String name = null, date=null, datetime=null, timestamp=null, image=null, userType = null,userId = null;
			boolean add=false;
			Transactions_information listItem = feedlistNew.get(fList);
			int totalAmount=Integer.parseInt(listItem.getTotal_amount());
			date = listItem.getDate();
			datetime = listItem.getDatetime();
			timestamp = listItem.getTimestamp();
	    	ArrayList<Payers> payerslist = listItem.getPayers();
	    	
	    	ArrayList<Receivers> receiverslist = listItem.getReceivers();
	    	if(!feedlistNew.get(fList).getTrans_type().equals("pay") && !feedlistNew.get(fList).getTrans_type().equals("receive")){
	    		
	    		// Calculation for split kind of transection
	    		
	    		int receverCount=0, ratio=Integer.parseInt(listItem.getRatio()), myAmountPayers=0,payer=0,recever=0,pAmt,pTAmt,ratioAmount=0;
		    	if(!listItem.getRatio().equals("")){
		    		ratio=Integer.parseInt(listItem.getRatio());
		    	}
				for(int p = 0; p< payerslist.size(); p++){
					//if(payerslist.get(p).getPayment_type().equals("0")){
		    			pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers=payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    		//if(receiverslist.get(r).getPayment_type().equals("0")){
				    			recever = Integer.parseInt(receiverslist.get(r).getAmount());
				    		//}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		MyTabs_ValueModel tvm = null ;
				    		if(myAmountPayers - recever == 0){
				    			//myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"receive");
				    			receverCount = r+1;
				    			//break;
				    		} else if(myAmountPayers - recever > 0){
				    			myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
				    			receverCount = r;
				    			//break;
				    		} 
				    		payer = payer - recever;
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
							    			int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    			} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
				    		}
				    		
		    			}
		    		/*} else if(payerslist.get(p).getPayment_type().equals("1")){
		    			ratioAmount = totalAmount/ratio;
		    			pAmt = ratioAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt-pTAmt;
		    			}else{
		    				payer = pTAmt-pAmt;
		    			}
		    			myAmountPayers = payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    		if(receiverslist.get(r).getPayment_type().equals("1")){
				    			recever = Integer.parseInt(receiverslist.get(r).getAmount());
				    		}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		MyTabs_ValueModel tvm = null ;
				    		if(myAmountPayers - recever == 0){
				    			//myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"receive");
				    			receverCount = r+1;
				    			//break;
				    		} else if(myAmountPayers - recever > 0){
				    			myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
				    			receverCount = r;
				    			//break;
				    		} 
				    		payer = payer - recever;
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
							    			int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
							    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    			} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
				    		}
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= myAmountPayers){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
				    		}
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		} else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer=payer-recever;
		    			}
		    		} else if(payerslist.get(p).getPayment_type().equals("2")){
		    			pAmt = totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay())/100;
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers = payer;
		    			for(int r = receverCount; r < receiverslist.size(); r++){
				    		if(receiverslist.get(r).getPayment_type().equals("2")){
				    			recever = (totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()))/100;
				    		}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		MyTabs_ValueModel tvm = null ;
				    		if(myAmountPayers - recever == 0){
				    			//myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"receive");
				    			receverCount = r+1;
				    			//break;
				    		} else if(myAmountPayers - recever > 0){
				    			myAmountPayers = myAmountPayers - recever;
				    			//tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
				    			receverCount = r;
				    			//break;
				    		} 
				    		payer = payer - recever;
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
							    			int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
							    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    			} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
				    		}
				    		if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
				    			add = false;
				    			for(int x=0; x < myList.size(); x++){
						    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    			myList.set(x, tvm);
						    			add=true;
						    			break;
						    		} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} 
					    		}
				    			if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
				    		}
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		}else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer = payer - recever;
		    			}
		    		}*/
					/*if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
			    		add = false;
			    		MyTabs_ValueModel tvm = null ;
			    		for(int x=0; x < myList.size(); x++){
				    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
			    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + myAmountPayers;
				    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
				    			myList.set(x, tvm);
				    			add=true;
				    			break;
				    		}
			    		}
			    		if(!add){
			    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"pay");
			    			myList.add(tvm);
			    		}
			    	}*/
			    }
			} else {
				
				// Calculation for pay and receive kind of transection
				
				int myAmountPayers=0,receverCount=0,payer=0,recever=0,pAmt,pTAmt,ratio=Integer.parseInt(listItem.getRatio()),ratioAmount=0;
		    	if(!listItem.getRatio().equals("")){
		    		ratio=Integer.parseInt(listItem.getRatio());
		    	}
				for(int p = 0; p< payerslist.size(); p++){
					//if(payerslist.get(p).getPayment_type().equals("0")){
		    			pAmt = Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers=payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    		//if(receiverslist.get(r).getPayment_type().equals("0")){
				    			recever = Integer.parseInt(receiverslist.get(r).getAmount());
				    		//}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		} else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer=payer-recever;
				    		
		    			}
		    		/*} else if(payerslist.get(p).getPayment_type().equals("1")){
		    			ratioAmount = totalAmount/ratio;
		    			pAmt = ratioAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay());
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt-pTAmt;
		    			}else{
		    				payer = pTAmt-pAmt;
		    			}
		    			myAmountPayers = payer;
		    			for(int r=receverCount; r < receiverslist.size(); r++){
				    		if(receiverslist.get(r).getPayment_type().equals("1")){
				    			recever = (totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()));
				    		}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    	
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		} else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer=payer-recever;
		    			}
		    		} else if(payerslist.get(p).getPayment_type().equals("2")){
		    			pAmt = totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay())/100;
		    			pTAmt = Integer.parseInt(payerslist.get(p).getAmount());
		    			if(pAmt > pTAmt){
		    				payer = pAmt - pTAmt;
		    			}else{
		    				payer = pTAmt - pAmt;
		    			}
		    			myAmountPayers = payer;
		    			for(int r = receverCount; r < receiverslist.size(); r++){
				    		if(receiverslist.get(r).getPayment_type().equals("2")){
				    			recever = (totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()))/100;
				    		}
				    		name = receiverslist.get(r).getFull_name();
				    		userType = receiverslist.get(r).getUser_type();
				    		userId = receiverslist.get(r).getIduser();
				    		image = receiverslist.get(r).getImage_location();
				    		
				    		if(payer - recever == 0){
				    			payer = payer - recever;
				    			receverCount = r+1;
				    			break;
				    		}else if(payer - recever <= 0){
				    			payer = payer - recever;
				    			receverCount = r;
				    			break;
				    		}
				    		payer = payer - recever;
		    			}
		    		}*/
					if(payerslist.get(p).getIduser().equals(PreferenceManager.getInstance().getUserId())){
			    		add = false;
			    		MyTabs_ValueModel tvm = null ;
			    		for(int x=0; x < myList.size(); x++){
				    		if(myList.get(x).getIduser() != null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
			    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
				    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
				    			myList.set(x, tvm);
				    			add=true;
				    			break;
				    		}else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
			    				if(Integer.parseInt(myList.get(x).getAmount()) >= myAmountPayers){
			    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
				    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
			    				} else {
			    					int amnt = myAmountPayers - Integer.parseInt(myList.get(x).getAmount());
				    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
			    				}
			    				
			    				myList.set(x, tvm);
			    				add=true;
			    				break;
			    			}  
			    		}
			    		if(!add){
			    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"pay");
			    			myList.add(tvm);
			    		}
			    	}
			    }
			}
		}
		
		// Receiver Calculation
		for(int fList = 0; fList < feedlistNew.size(); fList++){
			String name = null, date=null, datetime=null, timestamp=null, image = null, userType = null,userId = null;
			boolean add=false;
			
			Transactions_information listItem = feedlistNew.get(fList);
			int totalAmount=Integer.parseInt(listItem.getTotal_amount());
			date = listItem.getDate();
			datetime = listItem.getDatetime();
			timestamp = listItem.getTimestamp();
	    	ArrayList<Payers> payerslist = listItem.getPayers();
	    	ArrayList<Receivers> receiverslist = listItem.getReceivers();
	    	
	    	if(!feedlistNew.get(fList).getTrans_type().equals("pay") && !feedlistNew.get(fList).getTrans_type().equals("receive")){
		        int myAmountPayers=0,payersCount=0,payer=0,recever=0,pAmt,pTAmt,ratio=0,ratioAmount=0;
				for(int r=0;r<receiverslist.size();r++){
					//if(receiverslist.get(r).getPayment_type().equals("0")){
						recever=Integer.parseInt(receiverslist.get(r).getAmount());
		    			for(int p=payersCount;p<payerslist.size();p++){
				    		//if(payerslist.get(p).getPayment_type().equals("0")){
				    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
				    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
				    			if(pAmt>pTAmt){
				    				payer=pAmt-pTAmt;
				    			}else{
				    				payer=pTAmt-pAmt;
				    			}
				    			myAmountPayers=payer;
				    		//}
				    		name=payerslist.get(p).getFull_name();
				    		userType=payerslist.get(p).getUser_type();
				    		userId=payerslist.get(p).getIduser();
				    		image=payerslist.get(p).getImage_location();
				    		if(payer-recever==0){
				    			payer=payer-recever;
				    			payersCount=p+1;
				    			//break;
				    		}else if(payer-recever<=0){
				    			payer=payer-recever;
				    			payersCount=p;
				    			//break;
				    		}
				    		payer=payer-recever;
				    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
					    				/*int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");*/
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				/*int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");*/
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"pay");
					    			myList.add(tvm);
					    		}
					    	}
		    			}
		    		} /*else if(receiverslist.get(r).getPayment_type().equals("1")){
		    			for(int pay=0;pay<payerslist.size();pay++){
		    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
		    			}
		    			for(int rec=0;rec<receiverslist.size();rec++){
		    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
		    			}
		    			ratioAmount=totalAmount/ratio;
		    			recever=(totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()));
		    			for(int p=payersCount;p<payerslist.size();p++){
				    		if(payerslist.get(p).getPayment_type().equals("1")){
				    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
				    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
				    			if(pAmt>pTAmt){
				    				payer=pAmt-pTAmt;
				    			}else{
				    				payer=pTAmt-pAmt;
				    			}
				    			myAmountPayers=payer;
				    		}
				    		name=payerslist.get(p).getFull_name();
				    		userType=payerslist.get(p).getUser_type();
				    		userId=payerslist.get(p).getIduser();
				    		image=payerslist.get(p).getImage_location();
				    		if(payer-recever==0){
				    			payer=payer-recever;
				    			payersCount=r+1;
				    			//break;
				    		}else if(payer-recever<=0){
				    			payer=payer-recever;
				    			payersCount=r;
				    			//break;
				    		}
				    		payer=payer-recever;
				    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= myAmountPayers){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - myAmountPayers;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				} else {
					    					int amnt = myAmountPayers - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + myAmountPayers;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"receive");
					    			myList.add(tvm);
					    		}
					    	}
				    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
					    	}
		    			}
		    		}else if(receiverslist.get(r).getPayment_type().equals("2")){
		    			recever=(totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()))/100;
		    			for(int p=payersCount;p<payerslist.size();p++){
				    		if(payerslist.get(p).getPayment_type().equals("2")){
				    			pAmt=totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay())/100;
				    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
				    			if(pAmt>pTAmt){
				    				payer=pAmt-pTAmt;
				    			}else{
				    				payer=pTAmt-pAmt;
				    			}
				    			myAmountPayers=payer;
				    		}
				    		name=payerslist.get(p).getFull_name();
				    		userType=payerslist.get(p).getUser_type();
				    		userId=payerslist.get(p).getIduser();
				    		image=payerslist.get(p).getImage_location();
				    		if(payer-recever==0){
				    			payer=payer-recever;
				    			payersCount=p+1;
				    			//break;
				    		}else if(payer-recever<=0){
				    			payer=payer-recever;
				    			payersCount=p;
				    			//break;
				    		}
				    		payer=payer-recever;
				    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    			if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
					    	}*/
				    		/*if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
					    		add=false;
					    		MyTabs_ValueModel tvm = null ;
					    		for(int x=0; x < myList.size(); x++){
					    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
					    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
					    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				} else {
					    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
					    				}
					    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
					    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
					    				
					    				myList.set(x, tvm);
					    				add=true;
					    				break;
					    			}  
					    		}
					    		if(!add){
					    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
					    			myList.add(tvm);
					    		}
					    	}
		    			}
		    		}
				}*/
		    	/*if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
		    		add=false;
		    		MyTabs_ValueModel tvm = null ;
		    		for(int x=0; x < myList.size(); x++){
		    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
		    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + myAmountPayers;
		    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
		    				myList.set(x, tvm);
		    				add=true;
		    				break;
		    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
		    				if(Integer.parseInt(myList.get(x).getAmount()) >= myAmountPayers){
		    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - myAmountPayers;
			    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
		    				} else {
		    					int amnt = myAmountPayers - Integer.parseInt(myList.get(x).getAmount());
			    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
		    				}
		    				myList.set(x, tvm);
		    				add=true;
		    				break;
		    			}  
		    		}
		    		if(!add){
		    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(myAmountPayers), image,"receive");
		    			myList.add(tvm);
		    		}
		    	}*/
		    } else {
		    	 int myAmountPayers=0,payersCount=0,payer=0,recever=0,pAmt,pTAmt,ratio=0,ratioAmount=0;
			    	
					for(int r=0;r<receiverslist.size();r++){
						//if(receiverslist.get(r).getPayment_type().equals("0")){
							recever=Integer.parseInt(receiverslist.get(r).getAmount());
			    			for(int p=payersCount;p<payerslist.size();p++){
					    		//if(payerslist.get(p).getPayment_type().equals("0")){
					    			
					    			pAmt=Integer.parseInt(payerslist.get(p).getHad_to_pay());
					    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
					    			if(pAmt>pTAmt){
					    				payer=pAmt-pTAmt;
					    			}else{
					    				payer=pTAmt-pAmt;
					    			}
					    			myAmountPayers=payer;
					    		//}
					    		name=payerslist.get(p).getFull_name();
					    		userType=payerslist.get(p).getUser_type();
					    		userId=payerslist.get(p).getIduser();
					    		image=payerslist.get(p).getImage_location();
					    		if(payer-recever==0){
					    			payer=payer-recever;
					    			payersCount=p+1;
					    			//break;
					    		}else if(payer-recever<=0){
					    			payer=payer-recever;
					    			payersCount=p;
					    			//break;
					    		}
					    		payer=payer-recever;
					    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}
			    			}
			    		} /*else if(receiverslist.get(r).getPayment_type().equals("1")){
			    			for(int pay=0;pay<payerslist.size();pay++){
			    				ratio=ratio+Integer.parseInt(payerslist.get(pay).getHad_to_pay());
			    			}
			    			for(int rec=0;rec<receiverslist.size();rec++){
			    				ratio=ratio+Integer.parseInt(receiverslist.get(rec).getAmount());
			    			}
			    			ratioAmount=totalAmount/ratio;
			    			recever=(totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()));
			    			for(int p=payersCount;p<payerslist.size();p++){
					    		if(payerslist.get(p).getPayment_type().equals("1")){
					    			pAmt=totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay());
					    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
					    			if(pAmt>pTAmt){
					    				payer=pAmt-pTAmt;
					    			}else{
					    				payer=pTAmt-pAmt;
					    			}
					    			myAmountPayers=payer;
					    		}
					    		name=payerslist.get(p).getFull_name();
					    		userType=payerslist.get(p).getUser_type();
					    		userId=payerslist.get(p).getIduser();
					    		image=payerslist.get(p).getImage_location();
					    		if(payer-recever==0){
					    			payer=payer-recever;
					    			payersCount=r+1;
					    			//break;
					    		}else if(payer-recever<=0){
					    			payer=payer-recever;
					    			payersCount=r;
					    			//break;
					    		}
					    		payer=payer-recever;
					    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}
					    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}
			    			}
			    		}else if(receiverslist.get(r).getPayment_type().equals("2")){
			    			recever=(totalAmount*Integer.parseInt(receiverslist.get(r).getAmount()))/100;
			    			for(int p=payersCount;p<payerslist.size();p++){
					    		if(payerslist.get(p).getPayment_type().equals("2")){
					    			pAmt=totalAmount*Integer.parseInt(payerslist.get(p).getHad_to_pay())/100;
					    			pTAmt=Integer.parseInt(payerslist.get(p).getAmount());
					    			if(pAmt>pTAmt){
					    				payer=pAmt-pTAmt;
					    			}else{
					    				payer=pTAmt-pAmt;
					    			}
					    			myAmountPayers=payer;
					    		}
					    		name=payerslist.get(p).getFull_name();
					    		userType=payerslist.get(p).getUser_type();
					    		userId=payerslist.get(p).getIduser();
					    		image=payerslist.get(p).getImage_location();
					    		if(payer-recever==0){
					    			payer=payer-recever;
					    			payersCount=p+1;
					    			//break;
					    		}else if(payer-recever<=0){
					    			payer=payer-recever;
					    			payersCount=p;
					    			//break;
					    		}
					    		payer=payer-recever;
					    		if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}*/
					    		/*if(receiverslist.get(r).getIduser().equals(PreferenceManager.getInstance().getUserId())){
						    		add=false;
						    		MyTabs_ValueModel tvm = null ;
						    		for(int x=0; x < myList.size(); x++){
						    			if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("receive")){
						    				int amnt = Integer.parseInt(myList.get(x).getAmount()) + recever;
						    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			} else if(myList.get(x).getIduser()!=null && myList.get(x).getIduser().equals(userId) && myList.get(x).getUser_type() != null && myList.get(x).getUser_type().equals(userType) && myList.get(x).getType().equals("pay")){
						    				if(Integer.parseInt(myList.get(x).getAmount()) >= recever){
						    					int amnt = Integer.parseInt(myList.get(x).getAmount()) - recever;
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"pay");
						    				} else {
						    					int amnt = recever - Integer.parseInt(myList.get(x).getAmount());
							    				tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(amnt), image,"receive");
						    				}
						    				
						    				myList.set(x, tvm);
						    				add=true;
						    				break;
						    			}  
						    		}
						    		if(!add){
						    			tvm = new MyTabs_ValueModel(name, date, datetime, timestamp, userType, userId, String.valueOf(recever), image,"receive");
						    			myList.add(tvm);
						    		}
						    	}*/
					    		
			    			//}
			    		//}
					//}
		    }
		}
		
		myListoriginal.addAll(myList);
		myTabsAdapter.notifyDataSetChanged();
		
		
	}
}
