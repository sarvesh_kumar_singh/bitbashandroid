package com.bash.Activities;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.ItemWiseListAdapter;
import com.bash.Adapters.ItemWiseTwoWayViewAdapter;
import com.bash.Application.BashApplication;
import com.bash.ListModels.ItemWiseListModel;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.ListModels.TwoWayPersonModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class ItemWiseSplitActivity extends Activity implements AsyncResponse{

	TextView topleftitemwiseText,toprightitemwisetext, additem_text;
	TwoWayView horizontalvcrollviewphoto;
	public static ListView lvitemwiselist;
	
	
	private String categorySelect = "";
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "";
	private ArrayList<Person> addedPersonArrayList;
	private ArrayList<PhotosListModel> photoListArrayList;
	
	ArrayList<TwoWayPersonModel> twowayPersonModelList;
	//ArrayList<String> twowayPersonModelList;
	
	ArrayList<ItemWiseListModel> itemwiseModelList;
	
	ItemWiseTwoWayViewAdapter itemWiseTwoWayViewAdapter;
	ItemWiseListAdapter itemWiseListAdapter;
	
	private AlertDialog taxdialog = null;
	
	private int editTextCount = 0;
	
	boolean taxSkip = false;
	
	JSONArray jsonArrayPayerInfo = null;
	JSONArray jsonArrayReceiverInfo = null;
	JSONArray jsonArrayTaxInfo = null;
	JSONArray jsonArrayItemInfo = null;
	
	
	
	

	MyAsynTaskManager myAsyncTask = null;
	String userId = "";
	
	int transId;
	Bundle fragbundle = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_wise_split);
		
		Intent itemwiseIntent = getIntent();			
		
		fragbundle = itemwiseIntent.getBundleExtra("itemwisebndle");
	
		currencyCodeString = fragbundle.getString("CurrencyCodeString");
		categorySelect = fragbundle.getString("CategorySelect");
		dateString = fragbundle.getString("DateString");
		transationTitle = fragbundle.getString("TransationTitle");
		transationAnount = fragbundle.getString("TransationAnount");
		addedPersonArrayList = fragbundle
				.getParcelableArrayList("PaidUserDetails");
		photoListArrayList = fragbundle
				.getParcelableArrayList("ReceiveUserDetails");
		
		
		topleftitemwiseText = (TextView) findViewById(R.id.topleftitemwiseText);
		toprightitemwisetext = (TextView) findViewById(R.id.toprightitemwisetext);
		additem_text = (TextView) findViewById(R.id.additem_text);
		horizontalvcrollviewphoto = (TwoWayView) findViewById(R.id.itemwisetwowayviewphoto);
		
		lvitemwiselist = (ListView) findViewById(R.id.lvitemwiselist);
		
		userId = PreferenceManager.getInstance().getUserId();
		intializeViews();
	}
private void intializeViews(){
	
	
	
	//twowayPersonModelList = new ArrayList<String>();
	
	twowayPersonModelList = new ArrayList<TwoWayPersonModel>();

	twowayPersonModelList.clear();
	twowayPersonModelList.add(new TwoWayPersonModel("","",""));
	//twowayPersonModelList.add("");
	itemwiseModelList = new ArrayList<ItemWiseListModel>();
	itemwiseModelList.clear();
	itemwiseModelList.add(new ItemWiseListModel("","",twowayPersonModelList));
	
	
	itemWiseListAdapter = new ItemWiseListAdapter(ItemWiseSplitActivity.this,
			R.layout.item_itemwise_layout, itemwiseModelList);
	lvitemwiselist.setAdapter(itemWiseListAdapter);
	
	itemWiseTwoWayViewAdapter = new ItemWiseTwoWayViewAdapter(ItemWiseSplitActivity.this,
			R.layout.item_twowayview_itemwise_layout, photoListArrayList);
	horizontalvcrollviewphoto.setAdapter(itemWiseTwoWayViewAdapter);
	

	//lvitemwiselist.setOnItemClickListener(itemclickListener);
	topleftitemwiseText.setOnClickListener(clickListener);
	toprightitemwisetext.setOnClickListener(clickListener);
	additem_text.setOnClickListener(clickListener);
	
	
	
}
private View.OnClickListener clickListener = new View.OnClickListener() {
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.topleftitemwiseText:
			
			ItemWiseSplitActivity.this.onBackPressed();
			
			break;
		case R.id.toprightitemwisetext:
			
			if(taxSkip){
				
				getIntemsInfo();
				setPayerReceiverDetails();
				
				sendItemWiseSplitDetails();
				
			}else{
			
			showTaxAlertDialog();
			
			}
			
			break;
		case R.id.additem_text:
			
			if(itemWiseListAdapter.isLastFilled()){
				twowayPersonModelList.clear();
				twowayPersonModelList.add(new TwoWayPersonModel("","",""));
				//twowayPersonModelList.add("");
				itemwiseModelList.add(new ItemWiseListModel("","",twowayPersonModelList));
				itemWiseListAdapter.notifyDataSetChanged();
			}else{
				Toast.makeText(ItemWiseSplitActivity.this, "pls fill last item details", Toast.LENGTH_SHORT).show();
			}
			
			break;
		}
		
	}
};

public ItemWiseListAdapter getItemWiseAdater(){
	
	return itemWiseListAdapter;
}
public ItemWiseTwoWayViewAdapter getItemWiseTwoWayViewAdapter(){
	
	return itemWiseTwoWayViewAdapter;
}

protected void showTaxAlertDialog() {
	// TODO Auto-generated method stub

	Context context = this;
	final LinearLayout taxmainlinear;
	LinearLayout addtaxlinear;
	final EditText tipedittext;
	final EditText discountedittext;
	Button add_button;
	TextView donetext, skiptext;

	final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			context);
	LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View view = inflater.inflate(R.layout.activity_item_wise_taxes, null);
	alertDialogBuilder.setView(view);
	alertDialogBuilder.setCancelable(true);
	taxdialog = alertDialogBuilder.create();
	taxdialog.show();
	
	taxmainlinear = (LinearLayout) view.findViewById(R.id.taxmainlinear);
	addtaxlinear = (LinearLayout) view.findViewById(R.id.addtaxlinear);
	tipedittext = (EditText) view.findViewById(R.id.tipedittext);
	discountedittext = (EditText) view.findViewById(R.id.discountedittext);
	add_button = (Button) view.findViewById(R.id.add_button);
	skiptext = (TextView) view.findViewById(R.id.skiptext);
	donetext = (TextView) view.findViewById(R.id.donetext);
	
	add_button.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			editTextCount = editTextCount+1;
			
			createTaxLayout(taxmainlinear);
		}
	});
	
	donetext.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			getTaxLayout(taxmainlinear);
			
			JSONObject jsonObject1 = null;
			JSONObject jsonObject2 = null;
			
			if((tipedittext.getText().toString().trim()).length() > 0){
				jsonObject1 = new JSONObject();
				
				try {
					jsonObject1.put("type", "tip");
					jsonObject1.put("amount", (tipedittext.getText().toString().trim()));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			jsonArrayTaxInfo.put(jsonObject1);
			}
			
			if((discountedittext.getText().toString().trim()).length() > 0){
				jsonObject2 = new JSONObject();
				
				try {
					jsonObject2.put("type", "discount");
					jsonObject2.put("amount", (discountedittext.getText().toString().trim()));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				jsonArrayTaxInfo.put(jsonObject2);
			
			getIntemsInfo();
			
			setPayerReceiverDetails();
			
			sendItemWiseSplitDetails();
			
			taxdialog.dismiss();
			}
		}
	});
	
	
	skiptext.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			taxSkip = true;
			taxdialog.dismiss();
		}
	});
}
private void createTaxLayout(LinearLayout taxmainlinear){
	LinearLayout childTaxLinear;
	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View dynamicView = inflater.inflate(R.layout.item_taxwise, null);
	final EditText extratax;
	TextView taxName;
	childTaxLinear = (LinearLayout) dynamicView;
	extratax = (EditText) dynamicView.findViewWithTag("taxedit1");
	taxName = (TextView) dynamicView.findViewWithTag("taxtext1");
	
	taxName.setTag("taxtext" +editTextCount);
	taxName.setText("Tax "+(editTextCount+1));
	
	extratax.setTag("taxedit" +editTextCount);
	
	LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
		     LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		layoutParams.setMargins(0, 0, 0, 15);
		childTaxLinear.setLayoutParams(layoutParams);
	taxmainlinear.addView(childTaxLinear);
}
private void getTaxLayout(LinearLayout taxmainlinear){
	
	jsonArrayTaxInfo = new JSONArray();
		
	int childCount = taxmainlinear.getChildCount();
	JSONObject jsonObject1 = null;
	
	int i = 0;
	while (i < childCount) {
		
		LinearLayout linearparent = (LinearLayout) taxmainlinear.getChildAt(i);
		
		EditText taxamount = (EditText) linearparent.findViewWithTag("taxedit" + i);
		TextView taxname = (TextView) linearparent.findViewWithTag("taxtext" + i);
		
		jsonObject1 = new JSONObject();
		
		try {
			jsonObject1.put("type", "tax");
			jsonObject1.put("name", (taxname.getText().toString().trim()));
			jsonObject1.put("amount",(taxamount.getText().toString().trim()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		jsonArrayTaxInfo.put(jsonObject1);
		
		i++;
	}
	
}
private void getIntemsInfo(){
	
	itemwiseModelList.remove((itemwiseModelList.size()-1));
	JSONArray jsonArraySharedInfo = null;
	jsonArrayItemInfo = new JSONArray();
	
	JSONObject jsonObject1 = null;
	JSONObject jsonObject2 = null;
	
	for(ItemWiseListModel itemWiseListObj : itemwiseModelList){
		jsonArraySharedInfo= new JSONArray();

		for(TwoWayPersonModel twoWayPersonObj : itemWiseListObj.getTwowayPersonList()){
			
			jsonObject1 = new JSONObject();
			
			try {
				jsonObject1.put("user_type", BashApplication.userTypeHash.get(twoWayPersonObj.getUserType()));
				jsonObject1.put("user_id", Integer.parseInt(twoWayPersonObj.getId()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonArraySharedInfo.put(jsonObject1);
		}
		
		jsonObject2 = new JSONObject();
		
		try {
		jsonObject2.put("item_name", itemWiseListObj.getPrsnName());
		jsonObject2.put("item_cost", Float.parseFloat(itemWiseListObj.getItemAmount()));
		jsonObject2.put("shared_among", jsonArraySharedInfo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		jsonArrayItemInfo.put(jsonObject2);
	}	
}

private void setPayerReceiverDetails() {
	jsonArrayPayerInfo= new JSONArray();
	jsonArrayReceiverInfo = new JSONArray();
	
	JSONObject jsonObject1 = null;
	JSONObject jsonObject2 = null;
	
	for (Person person : addedPersonArrayList) {
		float paidAmt = 0.0f;
		jsonObject1 = new JSONObject();

		if (person.isPaidpersonbool()) {

			paidAmt = person.getPaidamount();

			try {
				jsonObject1.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
				jsonObject1.put("idpayer", Integer.parseInt(person.getId()));
				jsonObject1.put("paid_amount", paidAmt);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			jsonArrayPayerInfo.put(jsonObject1);
		}
	}

	for (PhotosListModel photoListObj : photoListArrayList) {
		jsonObject2 = new JSONObject();
		
		try {
			jsonObject2.put("user_type",
					BashApplication.userTypeHash.get(photoListObj.getUserType()));
			jsonObject2.put("idreceiver", Integer.parseInt(photoListObj.getID()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		jsonArrayReceiverInfo.put(jsonObject2);

	}

}

private void sendItemWiseSplitDetails() {

	myAsyncTask = new MyAsynTaskManager();
	myAsyncTask.delegate = this;
	
	if(taxSkip){
		myAsyncTask.setupParamsAndUrl("ItemSplitTransaction",
				this, AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information","items_information"},
				new String[] { "transaction", "insertItemSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayerInfo.toString(), jsonArrayReceiverInfo.toString(),jsonArrayItemInfo.toString()});
		
	}
	else{
		myAsyncTask.setupParamsAndUrl("ItemSplitTransaction",
				this, AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information","items_information","extra_payment_information"},
				new String[] { "transaction", "insertItemSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						jsonArrayPayerInfo.toString(), jsonArrayReceiverInfo.toString(),jsonArrayItemInfo.toString(),jsonArrayTaxInfo.toString()});
		
	}
	myAsyncTask.execute();

}

@Override
public void backgroundProcessFinish(String from, String output) {
	// TODO Auto-generated method stub
	if (from.equalsIgnoreCase("ItemSplitTransaction")) {

		if (output != null && output.length() > 0) {
			JSONObject baseJsonObject;
			try {
				baseJsonObject = new JSONObject(output);
				boolean result = baseJsonObject.getBoolean("result");
				String msgStr = baseJsonObject.getString("msg");
				
				if (result) {
					
					transId = Integer.parseInt(baseJsonObject.getString("idtrans"));
					Toast.makeText(ItemWiseSplitActivity.this, msgStr,
							Toast.LENGTH_SHORT).show();
					
					Intent socialIntent = new Intent(ItemWiseSplitActivity.this, SocialPageActivity.class);
					socialIntent.putExtra("transationdetails", fragbundle);
					socialIntent.putExtra("transID", transId);
					socialIntent.putExtra("postType", "pay");
					
					socialIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(socialIntent);
					
				} else {
					Toast.makeText(ItemWiseSplitActivity.this, msgStr,
							Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}
}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	//super.onBackPressed();
	Intent backHome = new Intent(ItemWiseSplitActivity.this,SlidingActivity.class);
	backHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	startActivity(backHome);
	finish();
}
}
